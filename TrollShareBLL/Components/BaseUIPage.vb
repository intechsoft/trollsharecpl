﻿Imports System.Threading
Imports System.Globalization
Imports System.Web

Public Class BaseUIPage
    Inherits System.Web.UI.Page

    Protected Overrides Sub InitializeCulture()

        If HttpContext.Current.Session("UICulture") Is Nothing Then
            HttpContext.Current.Session.Add("UICulture", "en")
        End If
        '  System.Threading.Thread.Sleep(5000)
        If Not HttpContext.Current.Session("SelectedLang") Is Nothing Then
            Dim LanguageOption As String = ""
            If IsNullValue(HttpContext.Current.Session("SelectedLang"), "") <> "" Then
                LanguageOption = HttpContext.Current.Session("SelectedLang")
                If LanguageOption.ToLower = "ar" Then
                    UICulture = "ar-KW"
                    Culture = "ar-KW"
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ar-KW")
                    Thread.CurrentThread.CurrentUICulture = New CultureInfo("ar-KW")
                Else
                    UICulture = "en-US"
                    Culture = "en-US"
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US")
                    Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-US")
                End If
            End If
        Else
            If Not HttpContext.Current.Session("LanguageOption") Is Nothing Then
                Dim LanguageOption As String = ""
                If IsNullValue(HttpContext.Current.Session("LanguageOption"), "") <> "" Then
                    LanguageOption = HttpContext.Current.Session("LanguageOption")
                    If LanguageOption.ToLower = "ar" Then
                        UICulture = "ar-KW"
                        Culture = "ar-KW"
                        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ar-KW")
                        Thread.CurrentThread.CurrentUICulture = New CultureInfo("ar-KW")
                    Else
                        UICulture = "en-US"
                        Culture = "en-US"
                        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US")
                        Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-US")
                    End If
                End If
            Else

                UICulture = "en-US"
                Culture = "en-US"
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US")
                Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-US")
            End If
        End If
        MyBase.InitializeCulture()
    End Sub

    Protected Overloads Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Application("SitePath") Is Nothing Then Application("SitePath") = CurrentPagePath()
        If CheckPrivRequired() And Not IsSessionValid() Then
            Response.Redirect(LoginSitePath())
        End If


    End Sub

    Public Sub setMySession(SessionName As String, SessionVal As String)
        Session(SessionName) = SessionVal
        Session("UICulture") = SessionVal
    End Sub

    Public ReadOnly Property AddPrivilage() As Boolean
        Get
            Return IIf(Session(QueryStrings.AddPrivilage).ToString.ToLower = "true", True, False)
        End Get
    End Property

    Public ReadOnly Property EditPrivilage() As Boolean
        Get
            Return IIf(Session(QueryStrings.EditPrivilage).ToString.ToLower = "true", True, False)
        End Get
    End Property

    Public ReadOnly Property DeletePrivilage() As Boolean
        Get
            Return IIf(Session(QueryStrings.DeletePrivilage).ToString.ToLower = "true", True, False)
        End Get
    End Property

    Public ReadOnly Property DetailsPrivilage() As Boolean
        Get
            Return IIf(Session(QueryStrings.DetailsPrivilage).ToString.ToLower = "true", True, False)
        End Get
    End Property


#Region "Functions"

    Public Function getActiveImage(ByVal Status As Integer) As String
        Select Case Status
            Case 0
                Return "~/images/red.gif"
            Case 1
                Return "~/images/green.gif"
            Case Else
                Return "~/images/red.gif"
        End Select
        Return ""
    End Function

    Public Function getActiveImageForFeedback(ByVal Status As Integer) As String
        Select Case Status
            Case 0
                Return "~/images/red.gif"
            Case Else
                Return "~/images/green.gif"
        End Select
        Return ""
    End Function

    Public Function CreateCPLActivitiesLog(Mode As Integer, BeforeText As String, AfterText As String) As Boolean
        Dim CplLog As New cplactivitieslog
        Dim logID As Integer = 0
        Try
            With CplLog
                .ActionMode = Mode
                .ActionTime = Now
                .AfterUpdate = AfterText
                .AppTypeID = Val(GetAppSetings("AppID"))
                .BeforeUpdate = BeforeText
                .IPAddress = Web.HttpContext.Current.Request.UserHostAddress.ToString
                .UserID = Val(Session("UserID").ToString)
                .WebPageName = Privilege.getPageName().ToLower()
                logID = .insertMember()
            End With
        Catch ex As Exception

        End Try
        If (logID > 0) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function getMessageType(ByVal Type As Integer) As String
        Select Case Type
            Case 1
                Return "Service Message"
            Case Else
                Return "General Message"
        End Select
        Return ""
    End Function

    Protected Function getServerFilePath(UPLOADFOLDER As String, ByVal SaveFileName As String, ext As String()) As String
        Dim path__1 As String = Me.Server.MapPath(UPLOADFOLDER)
        SaveFileName = path__1 & "\" & SaveFileName & "." & ext(ext.Length - 1)
        Return SaveFileName
    End Function

    Public Function GetImageFilePath(FilePath As String) As String
        FilePath = FilePath.Substring(FilePath.IndexOf("UserInterface"))
        FilePath = FilePath.Replace("\", "/")
        FilePath = GetAppSetings("ImageURLBase") & FilePath
        Return FilePath
    End Function

#End Region

End Class
