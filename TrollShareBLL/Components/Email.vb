﻿Imports System.Net
Imports System.Net.Mail
Imports System.IO
Imports System.ComponentModel
Imports System.Configuration

Public Class Email

    Private _htmlFilePath As String
    Public Property HtmlFilePath() As String
        Get
            Return _htmlFilePath
        End Get
        Set(ByVal value As String)
            _htmlFilePath = value
        End Set
    End Property

    Private _subject As String
    Public Property Subject() As String
        Get
            Return _subject
        End Get
        Set(ByVal value As String)
            _subject = value
        End Set
    End Property

    Private _smtpServer As String

    Public Property SMTPServer() As String
        Get
            Return _smtpServer
        End Get
        Set(ByVal value As String)
            _smtpServer = value
        End Set
    End Property

    Private _fromEmail As String
    Public Property FromEmail() As String
        Get
            Return _fromEmail
        End Get
        Set(ByVal value As String)
            _fromEmail = value
        End Set
    End Property

    Private _CCEmail As String
    Public Property CCEmail() As String
        Get
            Return _CCEmail
        End Get
        Set(ByVal value As String)
            _CCEmail = value
        End Set
    End Property

    Private _BCCEmail As String
    Public Property BCCEmail() As String
        Get
            Return _BCCEmail
        End Get
        Set(ByVal value As String)
            _BCCEmail = value
        End Set
    End Property

    Private strBody As String
    Public Property Body() As String
        Get
            Return strBody
        End Get
        Set(ByVal value As String)
            strBody = value
        End Set
    End Property

    Private evLog As EventLog

    Public Sub New()
        strBody = ""
        _smtpServer = IsNullValue(ConfigurationManager.AppSettings("SMTPServer"), "localhost")
        evLog = New EventLog("Application", ".", "Alaaly.AsyncEmail")
    End Sub

    Private Function ReadBodyFile() As Boolean
        If IsNullValue(Me.HtmlFilePath, "") = "" Then Return False
        Try
            Dim reader As StreamReader = New StreamReader(Me.HtmlFilePath)
            strBody = reader.ReadToEnd
            Return True
        Catch ex As Exception
            evLog.WriteEntry("error reading HTML File" + ex.Message)
            Return False
        End Try
    End Function

    Public Function SendAsyncEmail(ByVal Name As String, ByVal EmailAddress As String, ByVal MailId As String) As Boolean

        If (strBody = "") Then
            If Not ReadBodyFile() Then
                Return False
            End If
        End If

        Try
            Dim Message As MailMessage = New MailMessage()
            Message.IsBodyHtml = True

            FillCollection(Name, EmailAddress, Message.To)
            FillCollection(_CCEmail, _CCEmail, Message.CC)
            FillCollection(_BCCEmail, _BCCEmail, Message.Bcc)

            Message.From = New MailAddress(_fromEmail)
            Message.Subject = _subject
            Message.Body = strBody
            Dim client As SmtpClient = New SmtpClient(_smtpServer)
            AddHandler client.SendCompleted, New SendCompletedEventHandler(AddressOf SendCompletedCallback)
            client.Credentials = CredentialCache.DefaultNetworkCredentials
            Dim userState As String = MailId
            client.SendAsync(Message, userState)
        Catch ex As Exception
            evLog.WriteEntry("Send Error: " + ex.Message)
            Return False
        End Try
        Return True
    End Function

    Private Sub FillCollection(ByVal Name As String, ByVal EmailAddress As String, ByRef ACollection As MailAddressCollection)
        If IsNullValue(EmailAddress, "") = "" Then Exit Sub
        Dim J As Integer
        Dim strName As String = ""
        Dim arrTo As String() = Split(EmailAddress, ",")
        Dim arrName As String() = Split(Name, ",")
        For J = 0 To UBound(arrTo)
            If J > arrName.Length - 1 Then
                strName = arrTo(J)
            Else
                strName = arrName(J)
            End If
            ACollection.Add(New MailAddress(arrTo(J), strName))
        Next
    End Sub

    Public Function SendEmail(ByVal Name As String, ByVal EmailAddress As String) As Boolean
        If (strBody = "") Then
            If Not ReadBodyFile() Then
                Return False
            End If
        End If
        Try
            Dim Message As MailMessage = New MailMessage()
            Message.IsBodyHtml = True

            FillCollection(Name, EmailAddress, Message.To)
            FillCollection(_CCEmail, _CCEmail, Message.CC)
            FillCollection(_BCCEmail, _BCCEmail, Message.Bcc)

            Message.From = New MailAddress(_fromEmail)
            Message.Subject = _subject
            Message.Body = strBody

            Dim client As SmtpClient = New SmtpClient(_smtpServer)
            client.Credentials = CredentialCache.DefaultNetworkCredentials
            client.Send(Message)
        Catch ex As Exception
            evLog.WriteEntry("Send Error: " + ex.Message)
            Return False
        End Try
        Return True
    End Function

    Private Shared Sub SendCompletedCallback(ByVal sender As Object, ByVal e As AsyncCompletedEventArgs)
        Dim token As String = CStr(e.UserState)
        Dim evLog As New EventLog("Application", ".", "Alaaly.AsyncEmail")
        If e.Cancelled Then
            evLog.WriteEntry("Sending email Canceled" + token)
        End If
        If Not e.Error Is Nothing Then
            evLog.WriteEntry("Error sending email" + e.Error.Message)
        End If
    End Sub
End Class
