﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.Mail
Imports MySql.Data.MySqlClient
Imports System.Diagnostics

Imports System.Web.HttpContext
Imports System.Web
Imports System.Web.Caching
Imports System.Web.UI.HtmlControls
Imports System.Web.UI
Imports System.Text
Imports System.Configuration

Public Module UtilityModule
    Public KLID As Integer
    Public MachineID As String
    Public KioskOwner As Integer
    Public PinOwner As Integer

    Public Structure QueryStrings
        Public Const PageViewPrivilage As String = "PageViewPrivilage"
        Public Const AddPrivilage As String = "AddPrivilage"
        Public Const EditPrivilage As String = "EditPrivilage"
        Public Const DeletePrivilage As String = "DeletePrivilage"
        Public Const DetailsPrivilage As String = "DetailsPrivilage"
    End Structure

    Public Structure PrivilageMsgs

        Public Const PageViewPrivilage_Violated As String = "You don't have enough permission to view this page "
        Public Const AddPrivilage_Violated As String = "You don't have enough permission to add "
        Public Const EditPrivilage_Violated As String = "You don't have enough permission to edit "
        Public Const UpdatePrivilage_Violated As String = "You don't have update permission in this page"
        Public Const DeletePrivilage_Violated As String = "You don't have delete permission in this page"
        Public Const DetailsPrivilage_Violated As String = "You don't have enough permission to view the details of this page"

    End Structure

    Public Structure KnetAmountValidator
        Public Result As Boolean
        Public Reson As String
    End Structure

    Public Enum PaymentGatewayType
        Cash = 1
        Credit = 2
        KNET = 3
    End Enum

    Public Enum TokenAction
        NoTocken = 0
        TokenGenerated = 1
        Resubmit = 2
        Cancel = 3
        Adjust = 4
        MakeSuccess = 5
        Succeeded = 6
        Failure = 7
    End Enum

    Public Enum ServiceType
        UtilityVouchers = 1
        Alacarte = 2
        TelecomServices = 3
        OnlineServices = 4
        Entertainment = 5
    End Enum

    Public Enum Category
        MobileServices = 1
        UtilityCards = 2
        Finance = 3
        OnlineServices = 4
        ChannelService = 5
    End Enum

    Public Enum DeviceType
        Android = 1
        iPhone = 2
        Website = 3
        WindowsApp = 4
        WindowsPhone = 5
    End Enum

    Public Enum Channel
        Website = 1
        MobileApps = 2
        WindowsStore = 3
        NULL
    End Enum

    Public Enum TempInsState
        Sucesss = 1
        ValKnetFailed = 2
        ReadCardFailed = 3
        NavigateFailed = 4
        TemInsFailed = 5
        Failed = 0
    End Enum

    Public Structure TempInsStatus
        Public Status As Integer
        Public Description As String
    End Structure

    Public Enum TransState
        Sucesss = 1
        NotCaptured = 2
        ReadCardFailed = 3
        NavigateFailed = 4
        TemInsFailed = 5
        Failed = 0
    End Enum

    Public Structure InstallDetails
        Public MobileInstallID As Integer
        Public CustomerID As Integer
        Public InstalDate As DateTime
        'Public UpdateReq As Boolean
        Public ServiceProfileID As Integer
        Public CommissioneProfileID As Integer
        Public CustomerChannelID As Integer
    End Structure

    Public Enum Service

        WataniyaBillPayment = 1
        WataniyaXpress = 2
        WataniyaCharger = 3
        ZainEeZee = 4
        ZainPostPaid = 5
        RapidShare = 6
        Q8Online = 7
        BeeHealthy = 8
        CashU = 9
        Wallie = 10
        FoneCard = 11
        Almanar = 12
        CFC = 13
        Fasttelco = 14
        TeleCard = 15
        UltraPower = 16
        MegaUpload = 17
        Warcraft = 18
        VivaPrepaid = 19
        QualityNetPrePaid = 20
        ART = 27
        Pehla = 28
        FirstNet = 29
        Orbit = 30
        ShowTime = 31
        TFC = 32
        ZakatHouse = 36
        VivaETop = 39
        Awqaf = 40
        QualityNetDSL = 47
        DirectAid = 48
        VivaPostPaid = 51
        ZainTopup = 52
        Eureka = 58
        BentAlHalal = 59
        ISYS = 60
        DirectSale = 66
        WiFi = 71
        KFH = 72
        Tazur = 89
        CashuOnline = 95
        OneCard = 96

        ' live
        'WataniyaBillPayment = 1
        'WataniyaXpress = 2
        'WataniyaCharger = 3
        'ZainEeZee = 4
        'ZainPostPaid = 5
        'RapidShare = 6
        'Q8Online = 7
        'BeeHealthy = 8
        'CashU = 9
        'Wallie = 10
        'FoneCard = 11
        'Almanar = 12
        'CFC = 13
        'Fasttelco = 14
        'TeleCard = 15
        'UltraPower = 16
        'MegaUpload = 17
        'Warcraft = 18
        'VivaPrepaid = 19
        'QualityNetPrePaid = 20
        'ART = 27
        'Pehla = 28
        'FirstNet = 29
        'Orbit = 30
        'ShowTime = 31
        'TFC = 32
        'ZakatHouse = 36
        'VivaETop = 39
        'Awqaf = 40
        'DirectAid = 44
        'QualityNetDSL = 45
        'Eureka = 46
        'ZainTopup = 47
        'VivaPostPaid = 48
        'BentAlHalal = 49
        'WiFi = 50
        'ISYS = 51
        'DirectSale = 52
        'Amazing = 53
        'Skills = 54
        'EzeTop = 55
        'Tazur = 57
        'CashuOnline = 59
        'OneCard = 60
        '-----------------------
    End Enum

    Public Enum PaymentMethod
        Cash = 0
        Card = 1
    End Enum

    Public Enum UserRole
        Administrator = 1
        Operations = 2
        Board_of_Directors = 3
        BusinessManagement = 4
        HelpDesk = 5
        Client_Partner = 6
        Accounts_Manager = 7
        Accounts_Assistant = 8
        Accounts_Data_Entry = 9
        Accounts_Store_Use = 10
        Vendor_User = 11
    End Enum

    Public Enum UserType
        AdminUser = 1
        EmployeeUser = 2
        ClientUser = 3
        VendorUser = 4
    End Enum

    Public Enum Visiblity
        Visible = True
        InVisible = False
    End Enum

    Public Enum AccountType
        OldIndividual = 0
        GroupDealer = 1
        GroupBranch = 2
        NewIndividual = 3
    End Enum


    Public Function Request() As HttpRequest
        Return HttpContext.Current.Request
    End Function

    Public Function Response() As HttpResponse
        Return HttpContext.Current.Response
    End Function

    Public Function Cache() As Cache
        Return HttpContext.Current.Cache
    End Function

    Public Function UniqueLocationId() As String
        UniqueLocationId = Replace(Trim(LocationIP), ".", "")
    End Function

    Private Function KioskKey(ByVal param As String) As String
        KioskKey = UniqueLocationId() & param
    End Function

    Public Property TransValue(ByVal Key As String) As Object
        Get
            Return Cache(KioskKey(Key))
        End Get
        Set(ByVal value As Object)
            If value Is Nothing Then
                Cache.Remove(KioskKey(Key))
            Else
                Cache(KioskKey(Key)) = value
            End If
        End Set
    End Property

    Public ReadOnly Property LocationIP() As String
        Get
            Return MachineID
        End Get
    End Property

    'Public ReadOnly Property DefaultSitePath() As String
    '    Get
    '        Dim strPath As String = ConfigurationManager.AppSettings("RootPath").ToString
    '        If strPath Is Nothing Then
    '            strPath = "/"
    '        End If
    '        Return Replace(strPath.ToLower, "default.aspx", "")
    '    End Get
    'End Property

    Public Sub RedirectToDefaultErrorPage(ByVal Msg As String)
        HttpContext.Current.Response.Redirect(DefaultSitePath() & "Ex-Error.aspx?Err=" & Msg)
    End Sub

    Public Sub RedirectToDefaultSQLErrorPage(ByVal Msg As String)
        HttpContext.Current.Response.Redirect(DefaultSitePath() & "Ex-Error.aspx?Err=" & Msg)
    End Sub

    Public Sub RedirectToTransFailpage(ByVal Msg As String)
        Response.Redirect(DefaultSitePath() & "Ex-Error.aspx?Err=" & Msg)
    End Sub

    Public Function DefaultErrorPage(ByVal Msg As String, ByVal ErrorPage As String) As String
        Return ErrorPage & Msg.Replace(vbCrLf, ". ")
    End Function

    Function IsActiveTime(ByVal FromTime As String, ByVal ToTime As String) As Boolean
        Return (Format(Now(), "HH:mm") > FromTime = True And Format(Now(), "HH:mm") < ToTime = True)
    End Function

    Public Function CurrentPath() As String
        Return Request.AppRelativeCurrentExecutionFilePath.Substring(0, Request.AppRelativeCurrentExecutionFilePath.LastIndexOf("/")) & "/"
    End Function

    Public Function PaymentMethodRedirectByType(ByVal RedirectUrl1 As String, ByVal RedirectUrl2 As String, ByVal PaymentType As String) As String
        If PaymentType.ToUpper = "VAR" Then
            Return RedirectUrl1
        Else
            Return RedirectUrl2
        End If
    End Function

    Public Sub CacheClear()
        Dim objItem As DictionaryEntry
        For Each objItem In Cache()
            If InStr(Trim(objItem.Key.ToString()), UniqueLocationId) <> 0 Then
                Cache.Remove(objItem.Key.ToString())
            End If
        Next
    End Sub

    Public Function getCommonURLPath(ByVal RedirectUrl As String, ByVal Querystring As String, Optional ByVal TransType As String = "") As String
        Dim StrUrl As String
        Select Case TransType.ToUpper
            Case "CASH"
                RedirectUrl = TransValue("CashPage").ToString
                StrUrl = DefaultSitePath() & "Common/" & RedirectUrl & Querystring
            Case Else
                StrUrl = RedirectUrl & Querystring
        End Select
        getCommonURLPath = StrUrl
    End Function

    Public Sub ReportSQLError(ByVal DB As String, ByVal ErrorMsg As String)
        SendInternalEMail("Error", "Sql Exception :", ErrorMsg, DB, "No Transaction")
        Response.Redirect(DefaultSitePath() & "Ex-Error.aspx?Err=Application DB Connection Error")
    End Sub

    Public Function ImageRoot() As String
        Return System.Configuration.ConfigurationManager.AppSettings("ImageRoot").Trim
    End Function

    Public Sub LogToEvent(ByVal Message As String)
        Dim e As New EventLog("Application")
        e.Source = "AlaalyKioskApplication"
        Try
            e.WriteEntry(Message)
        Catch ex As Exception
            'do nothing
        End Try
    End Sub

    Public Function SendProfileReceipt(ByVal PTransNo As String, ByVal PTranTime As String, _
                                       ByVal PTranAmt As String, ByVal PRefID As String, _
                                       ByVal PAuthCode As String, ByVal PPaymentID As String, _
                                       ByVal PTranID As String, ByVal PTranResult As String, _
                                       ByVal PTotTranAmt As String, ByVal PPrevBal As String, _
                                       ByVal PKnetAmt As String, ByVal PSusTranAmt As String, _
                                       ByVal PFailedTranAmt As String, ByVal PBalance As String, _
                                       ByVal PEmailID As String, ByVal dt As DataTable) As Boolean
        Try
            Dim objMail As New MailMessage()
            objMail.From = System.Configuration.ConfigurationManager.AppSettings("AlaalyFromMailID").Trim
            objMail.Subject = "Alaaly Profile Transaction Receipt" & PTransNo
            objMail.To = PEmailID
            objMail.BodyFormat = MailFormat.Html
            objMail.BodyEncoding = Encoding.UTF8
            objMail.Priority = Net.Mail.MailPriority.Normal
            Dim strBody As New StringBuilder
            strBody.Append("<table border ='1' width='490px'>")
            strBody.Append("<tr><td colspan=2 align='center'>Transaction Receipt</td></tr>")

            If PTransNo <> "" Then
                strBody.Append("<tr><td colspan=2>Knet Transaction Details</td></tr>")
                strBody.Append("<tr><td>Tran. No.</td><td>" & PTransNo & "</td></tr>")
                strBody.Append("<tr><td>Tran. Time</td><td>" & PTranTime & "</td></tr>")
                strBody.Append("<tr><td>Tran. Amount</td><td>" & PTranAmt & "</td></tr>")
                strBody.Append("<tr><td>Reference ID</td><td>" & PRefID & "</td></tr>")
                strBody.Append("<tr><td>AuthCode</td><td>" & PAuthCode & "</td></tr>")
                strBody.Append("<tr><td>Payment ID</td><td>" & PPaymentID & "</td></tr>")
                strBody.Append("<tr><td>Tran.ID</td><td>" & PTranID & "</td></tr>")
                strBody.Append("<tr><td>Tran. No.</td><td>" & PTranResult & "</td></tr>")
            End If

            ''Cart data.............
            strBody.Append("<tr><td colspan=2>Payment Details</td></tr>")
            strBody.Append("<tr><td colspan=2>")
            strBody.Append("<table border ='1' width='100%'>")
            strBody.Append("<tr><td>Company</td><td>Service</td><td>Tran.No.</td><td>Account</td><td>Amount</td><td>Status</td></tr>")
            For i = 0 To dt.Rows.Count - 1
                strBody.Append("<tr>")
                strBody.Append("<td>" & dt.Rows(i)("Company").ToString & "</td>")
                strBody.Append("<td>" & dt.Rows(i)("Service").ToString & "</td>")
                strBody.Append("<td>" & dt.Rows(i)("TranNo").ToString & "</td>")
                strBody.Append("<td>" & dt.Rows(i)("Account").ToString & "</td>")
                strBody.Append("<td>" & dt.Rows(i)("Amount").ToString & "</td>")
                strBody.Append("<td>" & IIf(dt.Rows(i)("Active").ToString = "1", "Sucess", "Failed") & "</td>")
                strBody.Append("<tr>")
            Next
            strBody.Append("</table>")
            strBody.Append("</td></tr>")
            ''End of Cart data.............

            strBody.Append("<tr><td>Total Transaction Amount</td><td>" & PTotTranAmt & "</td></tr>")
            strBody.Append("<tr><td>Profile Balance Before Transaction.</td><td>" & PPrevBal & "</td></tr>")
            strBody.Append("<tr><td>Amount Captured from Knet</td><td>" & PKnetAmt & "</td></tr>")
            strBody.Append("<tr><td>Total Sucessful Transaction Amount</td><td>" & PSusTranAmt & "</td></tr>")
            strBody.Append("<tr><td>Total Pending Transaction Amount</td><td>" & PFailedTranAmt & "</td></tr>")
            strBody.Append("<tr><td>Current Profile Balance</td><td>" & PBalance & "</td></tr>")
            strBody.Append("</table>")

            objMail.Body = strBody.ToString
            SmtpMail.Send(objMail)
        Catch ex As Exception
            LogToEvent("Error Sending Email to " & PEmailID & " Subject:Alaaly Profile Transaction Receipt" & PTransNo)
        End Try

    End Function


    Public Function SendReceiptDiv(ByVal MailID As String, ByVal TrnNo As String, ByVal MailBody As String) As Boolean
        Try
            Dim objMail As New MailMessage()
            objMail.From = System.Configuration.ConfigurationManager.AppSettings("AlaalyFromMailID").Trim
            objMail.Subject = "Alaaly Transaction Receipt: " & TrnNo
            objMail.To = MailID.Trim
            objMail.BodyFormat = MailFormat.Html
            objMail.BodyEncoding = Encoding.UTF8
            objMail.Priority = Net.Mail.MailPriority.Normal
            objMail.Body = MailBody
            SmtpMail.Send(objMail)
        Catch ex As Exception
            LogToEvent("Error Sending Email to " & MailID & " Subject:" & TrnNo)
        End Try
    End Function

    Public Function DivToHtml(ByVal hgc As HtmlGenericControl) As String
        Dim sb As New StringBuilder()
        Dim sw As New System.IO.StringWriter(sb)
        Dim hw As New HtmlTextWriter(sw)
        'hgc.InnerHtml
        Return sb.ToString()
    End Function

    Public Function SendNonCapturedReceipt(ByVal TransNo As String, ByVal TransTime As String, _
                           ByVal amount As String, ByVal PaymentID As String, ByVal Ref As String, ByVal Result As String, _
                           ByVal MailID As String, ByVal ServiceName As String) As Boolean
        Try
            Dim objMail As New MailMessage()
            objMail.From = System.Configuration.ConfigurationManager.AppSettings("AlaalyFromMailID").Trim
            objMail.Subject = "Alaaly Transaction Receipt: " & TransNo
            objMail.To = MailID.Trim
            objMail.BodyFormat = MailFormat.Html
            objMail.BodyEncoding = Encoding.UTF8
            objMail.Priority = Net.Mail.MailPriority.Normal
            Dim strBody As New StringBuilder
            strBody.Append("<table border ='1' width='400px'>")
            strBody.Append("<tr><td colspan=2 align='center'>Transaction Failed</td></tr>")
            strBody.Append("<tr><td colspan=2 align='center'>" & ServiceName & "</td></tr>")
            strBody.Append("<tr><td>Transaction No:</td><td>" & TransNo & "</td></tr>")
            strBody.Append("<tr><td>Transaction Time:</td><td>" & TransTime & "</td></tr>")
            strBody.Append("<tr><td>Amount:</td><td>" & amount & "</td></tr>")
            strBody.Append("<tr><td colspan=2 align='center'>Payment Result </td></tr>")
            strBody.Append("<tr><td>Result:</td><td>" & Result & " </td></tr>")
            strBody.Append("<tr><td>Payment ID :</td><td>" & PaymentID & " </td></tr>")
            strBody.Append("<tr><td>Ref :</td><td>" & Ref & " </td></tr>")
            strBody.Append("</table>")
            objMail.Body = strBody.ToString
            SmtpMail.Send(objMail)
        Catch ex As Exception
            LogToEvent("Error Sending Email to " & MailID & " Subject:" & TransNo)
        End Try
    End Function

    Public Sub SendMail(ByVal Msg As String, ByVal Subject As String, ByVal Trans_No As String, ByVal MailType As String, Optional ByVal FromMail As String = "", Optional ByVal ToMail As String = "")
        Dim objMail As New MailMessage()
        Try
            If MailType = "ERR" Then
                objMail.From = System.Configuration.ConfigurationManager.AppSettings("errormailID").Trim
                objMail.Subject = Subject & "  Error On " & Now().ToString & "Transaction "
                objMail.To = System.Configuration.ConfigurationManager.AppSettings("errormailID").Trim
            ElseIf MailType = "ORDERSUMMARY" Then
                objMail.From = FromMail
                objMail.Subject = Subject
                objMail.To = ToMail
            ElseIf MailType = "RESULT" Then
                'UNDO: will be defined later
            End If
            objMail.BodyFormat = MailFormat.Html
            objMail.Priority = Net.Mail.MailPriority.Normal
            objMail.Body = "Hi! <br><br><br> Transaction No:= " & Trans_No & "<br><br>" & Msg & "<br><br>Kiosk Location = " & TransValue("Location") & "<br><br>Date/Time = " & Now() & "<br><br><br> Thanks Alaaly Team"
            SmtpMail.Send(objMail)
        Catch ex As Exception
            LogToEvent("Error Sending Email to " & ToMail & " Subject:" & Subject)
        End Try
    End Sub

    Public Sub SendMail2Client(ByVal MailFrom As String, ByVal ToMail As String, ByVal CCMail As String, ByVal SubjectMail As String, ByVal BodyMail As String)
        Dim objMail As New MailMessage()
        With objMail
            .From = MailFrom
            .Subject = SubjectMail
            .To = ToMail
            .Cc = CCMail
            .Body = BodyMail
            .BodyEncoding = Encoding.UTF8
            .BodyFormat = MailFormat.Html
            .Priority = Net.Mail.MailPriority.Normal
            Try
                SmtpMail.Send(objMail)
            Catch ex As Exception
                LogToEvent("Error Sending Email to " & ToMail & " Subject:" & SubjectMail)
            End Try
        End With
    End Sub



    Public Sub SendInternalEMail(ByVal Type As String, ByVal OccuredIn As String, ByVal Msg As String, ByVal Client As String, ByVal TransNo As String)
        Dim objMail As New MailMessage()
        Dim IPADD As String = LocationIP
        Dim Subject, Body As String
        Dim MailTo, MailFrom As String
        Dim MailBcc As String = ""

        Body = "<br>Date/Time : " & Now() & "<br>From : " & OccuredIn & "<br> Location : Profile Services<br>LocationId : " & LocationIP & "<br>" & vbCrLf & vbCrLf & Msg
        Select Case Type
            Case "Error"
                Subject = Client & " Transaction Fail " & TransNo & " On " & Now().ToString
                MailFrom = ConfigurationManager.AppSettings("AlaalySupportMailID")
                MailTo = ConfigurationManager.AppSettings("errormailID")
            Case "StatusUpdate"
                Subject = Client & " Status update fail " & TransNo & " On " & Now().ToString
                MailFrom = ConfigurationManager.AppSettings("AlaalySupportMailID")
                MailTo = ConfigurationManager.AppSettings("errormailID")
            Case "Result"
                Subject = Client & "  Error On " & Now().ToString
                MailFrom = ConfigurationManager.AppSettings("AlaalySupportMailID")
                MailTo = ConfigurationManager.AppSettings("errormailID")
                Body = Body & "Transaction No:= " & TransNo
            Case "Finance"
                Subject = Client & "'s  cassette was changed at " & Now().ToString
                MailFrom = ConfigurationManager.AppSettings("AlaalyAdminMailID")
                MailTo = ConfigurationManager.AppSettings("AlaalyAccountsMailID")
                MailBcc = ConfigurationManager.AppSettings("AlaalyFromMailID")
            Case "Wataniya"
                Subject = "Failed Wataniya Transaction " & OccuredIn
                MailTo = ConfigurationManager.AppSettings("errormailID")
                MailFrom = ConfigurationManager.AppSettings("AlaalySupportMailID")
                MailBcc = ConfigurationManager.AppSettings("WataniyaMailID").Trim
            Case "Zain"
                Subject = "ZAIN Transaction Fail " & TransNo & " On " & Now().ToString
                MailFrom = ConfigurationManager.AppSettings("AlaalySupportMailID")
                MailTo = ConfigurationManager.AppSettings("errormailID").Trim
                MailBcc = ConfigurationManager.AppSettings("AlaalyDevMailID")
        End Select
        Try
            objMail.From = MailFrom
            objMail.To = MailTo
            If MailBcc <> "" Then objMail.Bcc = MailBcc
            objMail.BodyFormat = MailFormat.Html
            objMail.Priority = Net.Mail.MailPriority.Normal
            objMail.Subject = Subject
            objMail.Body = Body
            SmtpMail.Send(objMail)
        Catch ex As Exception
            LogToEvent("Error Sending Email to " & ConfigurationManager.AppSettings("errormailID") & " Subject:" & Subject)
        End Try
    End Sub

    Public Function MaskCard(ByVal GatewayName As String, ByVal Cardnumber As String) As String
        If UCase(GatewayName) <> "EMP" AndAlso UCase(GatewayName) <> "CASH" AndAlso Cardnumber.Length > 8 Then
            Cardnumber = Left(Cardnumber, 4) & StrDup(Cardnumber.Length - 8, "X") & Right(Cardnumber, 4)
        End If
        Return Cardnumber
    End Function

    Public Function MaskZero(ByVal SerialNumber As String, ByVal Digits As Integer) As String
        Return Right("00000000000000000000000000000000000000000000000" + SerialNumber, Digits)
    End Function

    Public Function FormatSerialNumber(ByVal SerialNumber As String, ByVal ShortCode As String) As String
        SerialNumber = MaskZero(SerialNumber, 10)
        SerialNumber = ShortCode & "-" & SerialNumber
        Return SerialNumber
    End Function

    Public Sub AddHTMLTagsToContext(Optional ByVal FontColor As String = "Black", Optional ByVal SubCompanyId As Integer = 0)

        Dim HTMLStrPayMthd, HTMLStrCardPage As New StringBuilder
        Dim AmtCaption As String
        Dim strCurr As String
        TransValue("FontColor") = FontColor
        If TransValue("Language") = "ar" Then
            AmtCaption = "المبلغ المستحق للدفع"
            strCurr = " د.ك "
        Else
            AmtCaption = "Total Amount To Pay "
            strCurr = " K.D "
        End If

        With HTMLStrPayMthd
            .Append("<div style='color:" & FontColor & "' >")
            .Append("<B> " & AmtCaption & ":</B>")
            .Append("<B> <U> " & TransValue("CardAmount") & "</U>" & strCurr & "</B>")
            .Append("</div>")
            HttpContext.Current.Items.Add("HTMLStrPayMthd", .ToString)
        End With

        With HTMLStrCardPage
            .Append("<div style='color:" & FontColor & ";font-family: Verdana; font-weight: bold; font-size: medium; z-index: 101;'>")
            .Append("<B> " & AmtCaption & ":</B>")
            .Append("<B> <U> " & TransValue("CardAmount") & "</U>" & strCurr & "</B>")
            .Append("</div>")
            HttpContext.Current.Items.Add("HTMLStrCardPage", .ToString)
        End With

    End Sub

    Public Function Gettdate() As DateTime
        Return IsNullValue(TransValue("TransDate"), Now)
    End Function

    Public Sub ShowArabicImages(ByVal CS As Control)
        Dim s As String = ""
        Dim c As Control
        For Each c In CS.Controls
            If TypeOf c Is HtmlImage Then
                CType(c, HtmlImage).Src = Replace(CType(c, HtmlImage).Src, "/en/", "/ar/")
            End If
            If TypeOf c Is HtmlTableCell Then
                s = CType(c, HtmlTableCell).Attributes("background")
                CType(c, HtmlTableCell).Attributes("background") = Replace(s, "/en/", "/ar/")
            End If
            If c.HasControls Then ShowArabicImages(c)
        Next
    End Sub

    Public Function SplitCookie(ByVal CookieString As String, ByVal Key As String) As String

        Dim str As String()
        Dim ReturnStr As String = ""
        str = CookieString.Split(";")
        For i = 0 To str.Length - 1
            If str(i).Split("=")(0) = Key Then
                ReturnStr = str(i).Split("=")(1)
                Exit For
            End If
        Next

        SplitCookie = ReturnStr
    End Function

    Public Function GetCookieKey(ByVal PID As String, _
                                 ByVal EmailID As String, _
                                 ByVal ProfileBalance As String, _
                                  ByVal GID As String, _
                                 Optional ByVal PDID As String = "0") As String
        Dim key As String = ""
        key = "PID=" & PID
        key = key & ";EmailID=" & EmailID
        key = key & ";ProfileBalance=" & ProfileBalance
        key = key & ";GID=" & GID
        If PDID <> "0" Then key = key & ";PDID=" & PDID
        GetCookieKey = key
    End Function


    Public Function GetAppSetings(ByVal Key As String) As String
        GetAppSetings = IsNullValue(ConfigurationManager.AppSettings(Key), "")
    End Function

    Public Function GetTerminalName(Optional ByVal TableName As String = "") As String

        Dim TermianlName As String = ""

        TermianlName = " Select (cast(" & TableName & "TerminalID as varchar(10)) + '-' + Case when " & TableName & "TerminalID=0 then 'Server' else  branch.name end)   from  ServerStore.dbo.terminal "
        TermianlName += " inner join  ServerStore.dbo.branch on branch.ID= ServerStore.dbo.Terminal.BranchID"
        TermianlName += " inner join  ServerStore.dbo.Machine on  ServerStore.dbo.Machine.ID= ServerStore.dbo.Terminal.MachineID"
        TermianlName += "  where terminal.ID=   " & TableName & "TerminalID"

        Return TermianlName

    End Function

    Public Function GetTerminalNameWithMachineName(Optional ByVal TableName As String = "") As String

        Dim TermianlName As String = ""

        TermianlName = " Select (cast(" & TableName & "TerminalID as varchar(10)) + '-' + Machine.Name  + '-'  + Case when " & TableName & "TerminalID=0 then 'Server' else  branch.name end)   from  ServerStore.dbo.terminal "
        TermianlName += " inner join  ServerStore.dbo.branch on branch.ID= ServerStore.dbo.Terminal.BranchID"
        TermianlName += " inner join  ServerStore.dbo.Machine on  ServerStore.dbo.Machine.ID= ServerStore.dbo.Terminal.MachineID"
        TermianlName += "  where terminal.ID=   " & TableName & "TerminalID"

        Return TermianlName

    End Function


    Public Function GetEmployeeName(Optional ByVal TableName As String = "") As String

        Dim SalesStaffName As String = ""

        SalesStaffName = " "
        SalesStaffName += " case when " & TableName & "EmployeeID > 0 then "
        SalesStaffName += " (select name from ServerStore.dbo.employee where Employee.ID=" & TableName & "EmployeeID) else "
        SalesStaffName += " (select UserActualName from ServerStore.dbo.UserLogins where UserLogins.ID=" & TableName & "AuthID) end "

        Return SalesStaffName

    End Function

    Public Function GetBranchName(Optional ByVal TableName As String = "") As String

        Dim SalesStaffName As String = ""

        SalesStaffName = " "
        SalesStaffName += " case when " & TableName & "EmployeeID > 0 then "
        SalesStaffName += " (select name from ServerStore.dbo.employee where Employee.ID=" & TableName & "EmployeeID) else "
        SalesStaffName += " (select Branch.Name  from  ServerStore.dbo.terminal  inner join  ServerStore.dbo.branch on "
        SalesStaffName += "  Branch.ID= ServerStore.dbo.Terminal.BranchID  where terminal.ID=" & TableName & "TerminalID) end "

        Return SalesStaffName

    End Function

    Public Function GetUserName(Optional ByVal TableNameAndFiedName As String = "") As String

        Dim SalesStaffName As String = ""
        SalesStaffName += "  select UserActualName from ServerStore.dbo.UserLogins where UserLogins.ID=" & TableNameAndFiedName
        Return SalesStaffName

    End Function

End Module
