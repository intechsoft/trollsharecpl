﻿Imports System.Web.Mail
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Drawing.Imaging
Imports System.Security.Cryptography
Imports System.Data
Imports MySql.Data.MySqlClient
Imports System.Web.UI.WebControls
Imports System.Web.UI
Imports System.Web
Imports System.Configuration

Public Module Library
    Public Enum MailComponent
        Windows = 0
        Rapidsite = 1
    End Enum

    'Public Enum ApplicatinType
    '    Kiosk = 1
    '    DealerPC = 2
    'End Enum

    Public ArabicLetters() As String = {"", "ا", "ب", "ت", "ث", "ج", "ح", "خ", "د", "ذ", "ر", "ز", "س", "ش", "ص", "ض", "ط", "ظ", "ع", "غ", "ف", "ق", "ك", "ل", "م", "ن", "ه", "و", "ي"}
    Private Months() As String = {"", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"}
    ReadOnly Property SMTPServer() As String
        Get

            Return ConfigurationManager.AppSettings("SMTPServer")
        End Get
    End Property
    ReadOnly Property SMTPUsername() As String
        Get
            Return ConfigurationManager.AppSettings("SMTPUsername")
        End Get
    End Property
    ReadOnly Property SMTPPassword() As String
        Get
            Return ConfigurationManager.AppSettings("SMTPPassword")
        End Get
    End Property

    Private err1 As Exception
    Public CurrentSQLCommand As String = ""
    Property Ex() As Exception
        Get
            Return err1
        End Get
        Set(ByVal Value As Exception)
            err1 = Value
        End Set
    End Property

    Public Function IsValue(ByVal s As Object, ByVal v As Object, ByVal s2 As Object) As Object
        If s = v Then
            Return s2
        Else
            Return s
        End If
    End Function

    Function StrToUnit(ByVal Value As String) As Unit
        If IsNullValue(Value, "") <> "" Then
            Return New Unit(Value)
        Else
            Return Nothing
        End If
    End Function

    Public Function toHTML(ByVal S As String) As String
        Dim str As String
        str = Replace(S, vbCrLf, "<br>")
        str = Replace(Replace(str, Chr(13), "<br>"), "_", "&nbsp;")
        str = Replace(Replace(str, Chr(11), "<br>"), "_", "&nbsp;")
        Return str
    End Function

    Public Function toJScript(ByVal S As String) As String
        Dim str As String = S
        str = Replace(Replace(str, "'", " "), """", " ")
        Return str
    End Function

    Function FixForHTML(ByVal tmpText1)
        ' define a working variable
        Dim tmpText2
        ' populate our working variable
        tmpText2 = IsNullValue(tmpText1, "")
        ' replace each CR with a line break tag and CR-LF
        tmpText2 = Replace(tmpText2, Chr(13), "<br>" & vbCrLf)
        ' replace each TAB character with four non-breaking space tags
        tmpText2 = Replace(tmpText2, Chr(9), "&#xa0;&#xa0;&#xa0;&#xa0;")
        ' return the fixed string
        FixForHTML = tmpText2
    End Function

    Public Function toScript(ByVal S As String)
        Dim str As String
        str = Replace(S, vbCrLf, "<br>")
        str = Replace(str, "'", "\'")
        Return str
    End Function

    Public Function CurrentDomainPath(Optional ByVal IsSSL As Boolean = False) As String
        Dim url As Uri = System.Web.HttpContext.Current.Request.Url
        If IsSSL Then
            CurrentDomainPath = "https://" & url.Host
        Else
            CurrentDomainPath = "http://" & url.Host
        End If
    End Function

    Public Function DefaultSitePath() As String

        Dim strPath As String = HttpContext.Current.Application("SitePath")
        If strPath Is Nothing Then
            strPath = "/"
        End If
        Return Replace(strPath.ToLower, "default.aspx", "")
    End Function

    Public Function LoginSitePath() As String

        Dim strPath As String = ""
        If System.Configuration.ConfigurationManager.AppSettings("ApplySSL").ToString = "True" Then
            strPath = Replace(DefaultSitePath, "http", "https") & "login.aspx"

        Else
            strPath = "~/Login.aspx"
        End If
        Return strPath
    End Function

    Public Sub GetselectedItem(ByRef dd As ListControl, ByVal value As Integer)
        Dim I As Integer
        For I = 0 To dd.Items.Count - 1
            If CInt(dd.Items(I).Value) = value Then
                dd.Items(I).Selected = True
                Exit For
            End If
        Next
    End Sub

    Public Sub GetselectedItemByString(ByRef dd As ListControl, ByVal value As String)
        Dim I As Integer
        For I = 0 To dd.Items.Count - 1
            If CStr(dd.Items(I).Value) = value Then
                dd.Items(I).Selected = True
                Exit For
            End If
        Next
    End Sub
    Public Sub GetselectedItemFromArray(ByRef dd As ListControl, ByVal value As String)
        Dim I As Integer
        ' Dim arrValues() As String = Split(value, ";")
        For I = 0 To dd.Items.Count - 1
            If InStr(value, CStr(dd.Items(I).Value)) > 0 Then
                dd.Items(I).Selected = True
            End If
        Next
    End Sub
    Public Sub GetselectedItemForCheckboxList(ByRef dd As CheckBoxList, ByVal value As String)
        Dim I As Integer
        For I = 0 To dd.Items.Count - 1
            If InStr(value, dd.Items(I).Text) > 0 Then
                dd.Items(I).Selected = True
            End If
        Next
    End Sub
    Public Sub GetselectedItemForRadioButtonList(ByRef dd As RadioButtonList, ByVal value As String)
        Dim I As Integer
        For I = 0 To dd.Items.Count - 1
            If InStr(value, dd.Items(I).Text) > 0 Then
                dd.Items(I).Selected = True
            End If
        Next
    End Sub

    Public Sub GetselectedItemByText(ByRef dd As ListControl, ByVal value As String)
        Dim I As Integer
        For I = 0 To dd.Items.Count - 1

            If CStr(dd.Items(I).Text) = value Then

                dd.Items(I).Selected = True
                Exit For
            End If
        Next
    End Sub

    Public Function GetCheckBoxValues(ByRef check1 As CheckBoxList) As String
        Dim I As Integer
        Dim strcomma As String = ","
        GetCheckBoxValues = ""
        For I = 0 To check1.Items.Count - 1
            If check1.Items(I).Selected Then
                GetCheckBoxValues = GetCheckBoxValues & strcomma & check1.Items(I).Value
                strcomma = ";"
            End If
        Next
        GetCheckBoxValues = Replace(GetCheckBoxValues, ",", "")
    End Function
    Function ConvertToSqlFormat(ByVal Value As String, ByVal dataType As Integer)

        Select Case dataType
            Case 1, 2, 5 'text,integer,email
                Return StrToSql(Value)
            Case 3, 6  'decimal,money
                Return IsNullValue(Value, "null")
            Case 4    ' DateTime   
                Dim arryear() As String = Split(Value, "/")
                If UBound(arryear) > 0 Then
                    ' Response.Write(DateSerial(arryear(2), arryear(1), arryear(0)))
                    Return "'" & sqlDate(DateSerial(arryear(2), arryear(1), arryear(0))) & "'"
                Else
                    Return "null"
                End If

        End Select

    End Function
    Function StrToSql(ByVal Value As String) As String
        If Value = "" Or IsDBNull(Value) Then
            StrToSql = "NULL"
        Else
            StrToSql = "'" & Replace(Value, "'", "''") & "'"
        End If
    End Function

    Function NStrToSql(ByVal Value As String) As String
        If Value = "" Or IsDBNull(Value) Then
            NStrToSql = "NULL"
        Else
            NStrToSql = "N'" & Replace(Value, "'", "''") & "'"
        End If
    End Function

    Function BoolToSql(ByVal Value As Boolean) As Integer
        If Value Then
            BoolToSql = 1
        Else
            BoolToSql = 0
        End If
    End Function

    Public Function sqlDate(ByVal d As Date) As String
        If d.Ticks <> 0 Then
            Return d.Day & "-" & Months(d.Month) & "-" & d.Year
        Else
            Return ""
        End If
    End Function
    Public Function sqlQueryDate(ByVal d As Date) As String
        If d.Ticks <> 0 Then
            Return "'" & d.Year & "-" & d.Month & "-" & d.Day & "'"
        Else
            Return ""
        End If
    End Function
  
    Public Function todbNullDate(ByVal d As Date) As Object
        HttpContext.Current.Response.Write("Ticks= " & d.Ticks)
        Dim returnNo As String

        If d.Ticks = 0 Then
            returnNo = "null"
        Else
            returnNo = "'" & d & "'"
        End If
        Return returnNo
    End Function

    Public Function loadinFramesScript(ByRef CurrentPage As Page, ByVal URL As String)
        Dim sbScript As New System.Text.StringBuilder
        sbScript.Append("<script language='javascript'>") 'Opening script tag
        sbScript.Append(Environment.NewLine) 'Newline
        sbScript.Append("if (parent.length!=3) document.location.href='default.aspx?page=" & URL & "'")
        sbScript.Append(Environment.NewLine) 'Newline
        sbScript.Append("</script>") 'Closing script tag
        'Add the script to the page
        CurrentPage.Controls.AddAt(1, New LiteralControl(sbScript.ToString()))
    End Function

    Public Function SendTextMail(ByVal senderEMail As String, ByVal RecipientEMail As String, ByVal Subject As String, ByVal BodyText As String, Optional ByVal attachFile As String = "") As Boolean
        SendTextMail = False
        Dim I As Integer
        Dim arrEmails() As String = Split(RecipientEMail, ";")
        Dim mmsg As MailMessage = New MailMessage
        Try
            mmsg.BodyEncoding = Encoding.GetEncoding(1256)
            mmsg.To = arrEmails(0)
            For I = 1 To arrEmails.Length - 1
                If arrEmails(I).Trim <> "" Then
                    mmsg.Cc = mmsg.Cc & arrEmails(I)
                    If I < (arrEmails.Length - 1) Then
                        mmsg.Cc = mmsg.Cc & ";"
                    End If
                End If
            Next
            mmsg.From = senderEMail
            mmsg.Subject = Subject
            mmsg.BodyFormat = MailFormat.Text
            mmsg.Body = BodyText
            If attachFile <> "" Then
                mmsg.Attachments.Add(New MailAttachment(attachFile))
            End If

            If IsNullValue(SMTPUsername, "") <> "" And IsNullValue(SMTPPassword, "") <> "" Then
                mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1")
                mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", SMTPUsername) 'set your username here
                mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", SMTPPassword) 'set the pwd here
            End If
            SmtpMail.SmtpServer = SMTPServer
            SmtpMail.Send(mmsg)
            SendTextMail = True

        Catch e As Exception
            If Not e.InnerException Is Nothing Then
                If Not e.InnerException.InnerException Is Nothing Then
                    Ex = e.InnerException.InnerException
                Else
                    Ex = e.InnerException
                End If
            Else
                Ex = e
            End If
        End Try
    End Function

    Function SendHTMLMail(ByVal senderEMail As String, ByVal RecipientEMail As String, ByVal Subject As String, ByVal BodyText As String, Optional ByVal attachFile As String = "") As Boolean
        SendHTMLMail = False
        Dim mmsg As MailMessage = New MailMessage
        mmsg.BodyEncoding = Encoding.UTF8
        mmsg.To = RecipientEMail
        mmsg.From = senderEMail
        mmsg.Subject = Subject
        If IsNullValue(SMTPUsername, "") <> "" And IsNullValue(SMTPPassword, "") <> "" Then
            mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1")
            mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", SMTPUsername) 'set your username here
            mmsg.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", SMTPPassword) 'set the pwd here
        End If

        mmsg.BodyFormat = MailFormat.Html
        mmsg.Body = BodyText
        If attachFile <> "" Then
            mmsg.Attachments.Add(New MailAttachment(attachFile))
        End If
        Try
            SmtpMail.SmtpServer = SMTPServer
            SmtpMail.Send(mmsg)
            SendHTMLMail = True
        Catch e As Exception
            If Not e.InnerException Is Nothing Then
                If Not e.InnerException.InnerException Is Nothing Then
                    Ex = e.InnerException.InnerException
                Else
                    Ex = e.InnerException
                End If
            Else
                Ex = e
            End If
        End Try
    End Function




    Public Function EMailToFromWebConfig(ByRef Request As System.Web.HttpRequest) As String
        'Read the to email address from web.config file of the requested page
        EMailToFromWebConfig = ConfigurationSettings.AppSettings("Email_" & RequestedPageName(Request))
    End Function

    Public Function RequestedPageName(ByRef Request As System.Web.HttpRequest) As String
        Dim P, M As Integer
        P = InStr(Request.Url().ToString, "/", CompareMethod.Text)
        While P > 0
            M = P
            P = InStr(M + 1, Request.Url().ToString, "/", CompareMethod.Text)
        End While
        P = InStr(1, Request.Url().ToString, ".aspx", CompareMethod.Text)
        Dim L = P - (M + 1)
        RequestedPageName = Mid(Request.Url().ToString, M + 1, L)
    End Function


    Public Function CreateEmailTextMessage(ByRef Request As System.Web.HttpRequest, ByVal Subject As String) As String
        Dim strBody As New System.Text.StringBuilder
        Dim strField As String
        Dim I As Integer
        strBody.Append(Subject & Environment.NewLine)
        strBody.Append("--------------------------------" & Environment.NewLine)
        For I = 0 To Request.Form.Count - 1
            If Left(Request.Form.Keys(I), 2) = "f_" Then
                strField = Request.Form.Keys(I).Replace("f_", "")
                ' strField = strField.Replace("txt", " ")
                strBody.Append(strField & ": " & Request.Form.Item(I) & vbCrLf)
                'Else
                '  strBody.Append(strField & ": " & Request.Form.Item(I) & vbCrLf)
            End If
        Next
        Return strBody.ToString
    End Function

    Public Function SendFormAsTextEmail(ByRef Request As System.Web.HttpRequest, ByVal Subject As String) As Boolean
        Dim strFrom, strTo, strBody As String
        Dim enBody As Encoding = Encoding.GetEncoding(1256)
        ' Dim unicodeBytes As Byte() = enBody.GetBytes(unicodeString)
        strTo = EMailToFromWebConfig(Request)
        strFrom = IsNullValue(Request.Form("f_Email"), "webform@fapco.com")
        strBody = CreateEmailTextMessage(Request, Subject)
        SendFormAsTextEmail = SendTextMail(strFrom, strTo, Subject, strBody)
    End Function
    Public Function SendFormAsTextEmail(ByRef Request As System.Web.HttpRequest, ByVal Subject As String, ByVal EmailTo As String) As Boolean
        Dim strFrom, strTo, strBody As String
        Dim enBody As Encoding = Encoding.GetEncoding(1256)
        'Dim unicodeBytes As Byte() = enBody.GetBytes(unicodeString)
        strTo = EmailTo
        strFrom = IsNullValue(Request.Form("f_Email"), "webform@fapco.com")
        strBody = CreateEmailTextMessage(Request, Subject)
        SendFormAsTextEmail = SendTextMail(strFrom, strTo, Subject, strBody)
    End Function

    Public Function FoldersList(ByVal Path As String) As ArrayList
        Dim di As New DirectoryInfo(Path)
        Dim execlude As String()
        execlude = New String() {"admin", "components", "bin", "modules"}
        Dim index As Integer = Array.IndexOf(execlude, "")
        Dim d As DirectoryInfo
        Dim s As New ArrayList
        Dim I As Integer = 0
        For Each d In di.GetDirectories()
            If Left(d.Name, 1) <> "_" And Array.IndexOf(execlude, d.Name.ToLower) = -1 Then
                s.Add(d.Name)
            End If
        Next
        Return s
    End Function

    Public Function OnClickConfirm(ByRef Button As WebControl, ByVal Msg As String)
        Button.Attributes.Add("onClick", "javascript:return confirm('" & Msg & "');")
    End Function

    Public Function PostURL(ByVal URL As String, ByVal Params As String)
        Dim result As WebResponse
        Dim str As String
        Try
            Dim req As WebRequest
            Dim RequestStream As Stream
            Dim ReceiveStream As Stream
            Dim encode As Encoding
            Dim sr As StreamReader

            req = WebRequest.Create(URL)
            req.Method = "POST"
            req.ContentType = "application/x-www-form-urlencoded"
            Dim SomeBytes() As Byte
            Dim UrlEncoded As New StringBuilder
            Dim reserved() As Char = {ChrW(63), ChrW(61), ChrW(38)}

            If Params <> Nothing Then
                Dim i As Integer = 0
                Dim j As Integer
                While i < Params.Length
                    j = Params.IndexOfAny(reserved, i)
                    If j = -1 Then
                        UrlEncoded.Append(HttpUtility.UrlEncode(Params.Substring(i, Params.Length - i)))
                        Exit While
                    End If
                    UrlEncoded.Append(HttpUtility.UrlEncode(Params.Substring(i, j - i)))
                    UrlEncoded.Append(Params.Substring(j, 1))
                    i = j + 1
                End While
                SomeBytes = System.Text.Encoding.UTF8.GetBytes(UrlEncoded.ToString())
                req.ContentLength = SomeBytes.Length
                RequestStream = req.GetRequestStream()
                RequestStream.Write(SomeBytes, 0, SomeBytes.Length)
                RequestStream.Close()
            Else
                req.ContentLength = 0
            End If
            result = req.GetResponse()
            ReceiveStream = result.GetResponseStream()
            encode = System.Text.Encoding.GetEncoding("utf-8")
            sr = New StreamReader(ReceiveStream, encode)

            'Console.WriteLine()
            'Console.WriteLine("Response stream received")
            Dim read(256) As Char
            Dim count As Integer = sr.Read(read, 0, 256)

            'Console.WriteLine("HTML...")
            'Console.WriteLine()

            Do While count > 0
                str += New String(read, 0, count)
                count = sr.Read(read, 0, 256)
            Loop
        Catch Exc As Exception
            'Console.WriteLine()
            str = "The request URI could not be found or was malformed"
        Finally
            If Not result Is Nothing Then
                result.Close()
            End If
        End Try
        Return str
    End Function

    Public Function GetURL(ByVal URL As String) As String
        Dim str As New System.Text.StringBuilder
        Try
            Dim myRequest As System.Net.WebRequest = System.Net.WebRequest.Create(URL)
            myRequest.Timeout = 10000
            Dim myResponse As System.Net.WebResponse = myRequest.GetResponse()
            Dim streamResponse As System.IO.Stream = myResponse.GetResponseStream()
            Dim streamRead As New System.IO.StreamReader(streamResponse)
            Dim readBuff(4069) As [Char]
            Dim count As Integer = streamRead.Read(readBuff, 0, 4069)
            While count > 0
                str.Append(New [String](readBuff, 0, count))
                count = streamRead.Read(readBuff, 0, 4069)
            End While
        Catch ex As Exception
            str.Append("")
        End Try
        Return str.ToString
    End Function
    Public Function GetWebURL(ByVal URL As String) As String
        Dim strContent As String = ""
        Try
            Dim objWebClient As New WebClient
            Dim objUTF8 As New UTF8Encoding
            strContent = objUTF8.GetString(objWebClient.DownloadData(URL))
        Catch ex As Exception
            strContent = ""
        End Try
        Return strContent
    End Function

    Public Function InArray(ByVal val As Object, ByRef A() As Object) As Boolean
        Dim I As Integer
        For I = 0 To UBound(A)
            If val = IsNullValue(A(I), Nothing) Then Return True
        Next
        Return False
    End Function
    Public Function GetBoolValFromArray(ByVal val As Object, ByRef A As String) As Boolean
        Dim I As Integer
        Try
            Dim arry As Array = Split(A, ",")
            If arry(val) = "1" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function GetTextValFromArray(ByVal val As Object, ByRef A As String) As String
        Dim I As Integer
        Try
            Dim arry As Array = Split(A, ",")
            If arry(val) = "0" Then
                Return ""
            Else
                Return arry(val)
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function GetIntValFromArray(ByVal val As Object, ByRef A As String) As Integer
        Dim I As Integer
        Try
            Dim arry As Array = Split(A, ",")
            Return CInt(arry(val))
        Catch ex As Exception
            Return -1
        End Try
    End Function

    Public Function InArrayList(ByVal val As Object, ByRef A As ArrayList) As Boolean
        Dim I As Integer
        For I = 0 To A.Count - 1
            If val = A(I) Then Return True
        Next
        Return False
    End Function

    Public Function ArrayToStr(ByVal arr() As String) As String
        Dim S As String
        Dim I As Integer
        For I = LBound(arr) To UBound(arr)
            If I > LBound(arr) Then
                S = S & Chr(9)
            End If
            S = S & arr(I)
        Next
        Return S
    End Function
    Public Function ArrayToArrList(ByVal arr() As String) As ArrayList
        Dim AL As New ArrayList

        Dim I As Integer
        For I = LBound(arr) To UBound(arr)
            AL.Add(arr(I))
        Next
        Return AL
    End Function

    Public Function strToArray(ByVal str As String, Optional ByVal Delimiter As String = Chr(9)) As String()
        Return Split(str, Delimiter)
    End Function

    Function StripHTMLTag(ByVal sText As String) As String
        Dim fFound As Boolean = False
        StripHTMLTag = ""
        Do While InStr(sText, "<")
            fFound = True
            StripHTMLTag = StripHTMLTag & Left(sText, InStr(sText, "<") - 1)
            sText = Mid(sText, InStr(sText, ">") + 1)
        Loop
        StripHTMLTag = Replace(StripHTMLTag & sText, "&nbsp;", " ", 1, -1, CompareMethod.Text)
        If Not fFound Then StripHTMLTag = sText
    End Function

    Public Function FormatExternalURL(ByVal URL As String) As String
        If InStr(URL, "http://", CompareMethod.Text) Or InStr(URL, "javascript:", CompareMethod.Text) Then
            Return URL
        Else
            Return "http://" & URL
        End If
    End Function

    Public Function DataFieldToString(ByRef DataTable As DataTable, ByVal FieldName As String, ByVal Delimiter As Char, Optional ByVal Quotes As Char = "")
        Dim str As String = ""
        Dim R As DataRow
        For Each R In DataTable.Rows
            If str <> "" Then
                str = str & Delimiter
            End If
            str = str & Quotes & R(FieldName) & Quotes
        Next
        Return str
    End Function

    Public Function ItemsListToString(ByRef ItemCollection As ListItemCollection, ByVal Delimiter As Char)
        Dim arr As Array
        ItemCollection.CopyTo(arr, 0)
        Return ArrayToStr(arr)
    End Function

    Public Function SelectedListItemsToString(ByRef ItemCollection As ListItemCollection, ByVal Delimiter As Char)
        Dim I As Integer
        Dim str As String = ""
        For I = 0 To ItemCollection.Count - 1
            If ItemCollection(I).Selected Then
                If str <> "" Then
                    str = str & Delimiter
                End If
                str = str & ItemCollection(I).Value
            End If
        Next
        Return str
    End Function

    Public Sub SelectedListItemsFromString(ByRef ItemCollection As ListItemCollection, ByVal Values As String, ByVal Delimiter As Char)
        Dim I As Integer
        Dim a As String() = Split(Values, Delimiter)
        For I = 0 To ItemCollection.Count - 1
            If InArray(ItemCollection(I).Value, a) Then
                ItemCollection(I).Selected = True
            End If
        Next
    End Sub

    Public Function ListItemsToBinStr(ByRef ItemCollection As ListItemCollection)
        Dim I As Integer
        Dim str As String = ""
        For I = 0 To ItemCollection.Count - 1
            str = str & BoolToSql(ItemCollection(I).Selected)
        Next
        Return str
    End Function

    Public Sub SelectedListItemsFromBinStr(ByRef ItemCollection As ListItemCollection, ByVal BinString As String)
        Dim I, J As Integer
        Dim L As Integer = Len(BinString)
        J = ItemCollection.Count
        For I = 0 To J - 1
            If I < L Then ItemCollection(I).Selected = Mid(BinString, I + 1, 1)
        Next
    End Sub

    Function BinStrToInt(ByVal BinaryString As String) As Integer
        Dim I, L As Integer
        Dim R As Integer = 0
        L = Len(BinaryString)
        For I = 0 To L - 1
            R = R + CInt(Mid(BinaryString, I + 1, 1)) * (2 ^ I)
        Next
        Return R
    End Function

    Function IntToBinStr(ByVal Number As Integer) As String
        Dim I As Integer
        Dim R As Integer = Number
        Dim str As String = ""
        While R > 1
            str = str & (R Mod 2)
            R = Int(R / 2)
        End While
        str = str & R
        Return str
    End Function

    ' convert datareader to dataset
    Public Function DataReaderToDataSet(ByVal reader As MySqlDataReader) As DataSet
        Dim dataSet As DataSet = New DataSet
        Dim schemaTable As DataTable = reader.GetSchemaTable()
        Dim dataTable As DataTable = New DataTable
        Dim intCounter As Integer
        For intCounter = 0 To schemaTable.Rows.Count - 1
            Dim dataRow As DataRow = schemaTable.Rows(intCounter)
            Dim columnName As String = CType(dataRow("ColumnName"), String)
            Dim column As DataColumn = New DataColumn(columnName, CType(dataRow("DataType"), Type))
            dataTable.Columns.Add(column)
        Next
        dataSet.Tables.Add(dataTable)
        While reader.Read()
            Dim dataRow As DataRow = dataTable.NewRow()
            For intCounter = 0 To reader.FieldCount - 1
                dataRow(intCounter) = reader.GetValue(intCounter)
            Next
            dataTable.Rows.Add(dataRow)
        End While
        Return dataSet
    End Function

    Public Function DataReaderToDataTable(ByVal reader As MySqlDataReader) As DataTable
        Dim schemaTable As DataTable = reader.GetSchemaTable()
        Dim dataTable As DataTable = New DataTable
        Dim intCounter As Integer
        For intCounter = 0 To schemaTable.Rows.Count - 1
            Dim dataRow As DataRow = schemaTable.Rows(intCounter)
            Dim columnName As String = CType(dataRow("ColumnName"), String)
            Dim column As DataColumn = New DataColumn(columnName, CType(dataRow("DataType"), Type))
            dataTable.Columns.Add(column)
        Next
        While reader.Read()
            Dim dataRow As DataRow = dataTable.NewRow()
            For intCounter = 0 To reader.FieldCount - 1
                dataRow(intCounter) = reader.GetValue(intCounter)
            Next
            dataTable.Rows.Add(dataRow)
        End While
        Return dataTable
    End Function

    Public Sub PrintControls(ByRef C As Control)
        Dim cc As Control
        Dim I As Integer = 0
        If C.HasControls Then
            Dim M As Integer = C.Controls.Count
            Do While I < M
                cc = C.Controls(I)
                PrintEffects(cc)
                If cc.HasControls Then
                    PrintControls(cc)
                Else
                    Select Case cc.GetType.ToString
                        Case "System.Web.UI.WebControls.TextBox"
                            Dim l As New Label
                            l.Text = toHTML(CType(cc, TextBox).Text)
                            l.CssClass = "text"
                            C.Controls.AddAt(I, l)
                            C.Controls.RemoveAt(I + 1)
                        Case "System.Web.UI.WebControls.CompareValidator", "System.Web.UI.WebControls.RequiredFieldValidator", "System.Web.UI.WebControls.RangeValidator"
                            C.Controls.RemoveAt(I)
                            I = I - 1
                            M = M - 1
                    End Select
                End If
                I = I + 1
            Loop
        End If
    End Sub

    Private Sub PrintEffects(ByRef Cn As Control)
        Select Case Cn.GetType.ToString
            Case "System.Web.UI.HtmlControls.HtmlTable"
                CType(Cn, System.Web.UI.HtmlControls.HtmlTable).Border = 1
        End Select
    End Sub

    Public Function GetTemplates(ByRef strTitle As String, ByRef strTemplate As String) As String
        GetTemplates = Replace(strTemplate, "[#title#]", strTitle, 1, , CompareMethod.Text)
        Return GetTemplates
    End Function

    Public Function getVAlign(ByVal Index As Integer) As String
        Dim strAlign As String = ""
        If Index > 0 Then
            Select Case Index
                Case 1
                    strAlign = "top"
                Case 2
                    strAlign = "middle"
                Case 3
                    strAlign = "bottom"
            End Select
        End If
        Return strAlign
    End Function

    Public Function getAlign(ByVal Index As Integer) As String
        Dim strAlign As String = ""
        If Index > 0 Then
            Select Case Index
                Case 1
                    strAlign = "left"
                Case 2
                    strAlign = "center"
                Case 3
                    strAlign = "right"
            End Select
        End If
        Return strAlign
    End Function

    Public Function DashTrim(ByVal s As String) As String
        Dim s1 As String = s
        If Left(s1, 1) = "/" Or Left(s1, 1) = "\" Then
            s1 = Mid(s1, 2)
        End If
        If Right(s1, 1) = "/" Or Right(s1, 1) = "\" Then
            s1 = Mid(s1, 1, Len(s1) - 1)
        End If
        Return s1
    End Function

    'Public Function getPageIdFromUrlReferrer() As Integer
    '    getPageIdFromUrlReferrer = 0
    '    If HttpContext.Current.Request.UrlReferrer Is Nothing Then
    '        Exit Function
    '    End If
    '    Dim I As Integer = InStr(HttpContext.Current.Request.UrlReferrer.ToString, "?")
    '    If I > 0 Then
    '        Dim qs As String = Strings.Mid(HttpContext.Current.Request.UrlReferrer.ToString, I + 1)
    '        Dim A() As String = Split(qs, "&")
    '        If IsArray(A) Then
    '            For I = 0 To UBound(A)
    '                If InStr(A(I), "pageId", CompareMethod.Text) > 0 Then
    '                    Try
    '                        getPageIdFromUrlReferrer = CInt(Strings.Mid(A(I), 8))
    '                    Catch ex As Exception
    '                    End Try
    '                End If
    '            Next
    '        End If
    '    End If
    '    If getPageIdFromUrlReferrer = 0 Then
    '        Dim pp As New PortalPage
    '        getPageIdFromUrlReferrer = pp.GetDefaultSitePage
    '    End If
    'End Function

    Function ObjectSerialize(ByRef MyObject As Object) As Byte()
        Dim m As New MemoryStream
        Dim BF As New BinaryFormatter
        BF.Serialize(m, MyObject)
        m.Position = 0
        Dim b As Byte()
        b = m.ToArray
        'Dim uc As Encoding = Encoding.Unicode
        'Return uc.GetString(b)
        Return b
    End Function

    Function ObjectDeSerialize(ByVal Data As Byte()) As Object
        Dim m As New MemoryStream(Data)
        Dim BF As New BinaryFormatter
        m.Position = 0
        Return BF.Deserialize(m)
    End Function

    'Function ObjectDeSerialize(ByVal UnicodeString As String) As Object
    '    Dim uc As Encoding = Encoding.Unicode
    '    Dim b As Byte()
    '    b = uc.GetBytes(UnicodeString)
    '    Dim m As New MemoryStream(b, 0, b.Length)
    '    Dim BF As New BinaryFormatter
    '    m.Position = 0
    '    Return BF.Deserialize(m)
    'End Function

    'Public Function Clone() As Object
    '    Dim m As New MemoryStream
    '    Dim b As New BinaryFormatter
    '    b.Serialize(m, Me)
    '    m.Position = 0
    '    Return b.Deserialize(m)
    'End Function

    Public Function ShowErrorInfo(ByVal currentError As Exception)
        Dim context As HttpContext = HttpContext.Current
        Return _
          "<link rel=""stylesheet"" href=""/Admin/Portal.css"">" & _
          "<h2>Error</h2><hr/>" & _
          "An unexpected error has occurred on this page." & _
          "The system administrators have been notified.<br/>" & _
          "<br/><b>The error occurred in:</b>" & _
            "<pre>" & context.Request.Url.ToString & "</pre>" & _
          "<br/><b>Error Message:</b>" & _
            "<pre>" & currentError.Message.ToString & "</pre>" & _
          "<br/><b>Error Stack:</b>" & _
           "<pre>" & currentError.ToString & "</pre>"
    End Function

    Public Function DomainFromEmail(ByVal Emails As String) As String
        DomainFromEmail = ""
        Dim Email As String() = Split(Emails, ";")
        If Email.Length > 1 Then
            Dim p As Integer = InStr(Email(0), "@", CompareMethod.Text)
            If p > 0 And Len(Email(0)) > p Then
                Return Mid(Email(0), p + 1)
            End If
        End If
    End Function

    Function RenderControlToHTML(ByRef C As Control) As String
        Dim writer As New IO.StringWriter
        Dim w As New HtmlTextWriter(writer)
        C.RenderControl(w)
        Return writer.ToString
    End Function
    Function getTableName(ByRef fid As Integer) As String
        Return "FTTABLE" & Right("00000" & fid, 6)
    End Function
    Public Function BuildSearchSql(ByVal searchtext As String, ByVal strsql As String, ByVal strcolumn As String) As String
        ' Create Instance of Connection and Command Object
        Dim arrWords(20)
        arrWords(0) = ""
        Dim I As Integer
        Dim P As Integer
        Dim strCondn As String = " or "
        '  If SearchType = 1 Then
        ' strCondn = " and "
        ' End If
        If searchtext <> "" Then
            I = 0
            P = InStr(1, searchtext, " ")
            Do While P > 0
                arrWords(I) = Mid(searchtext, 1, P - 1)
                searchtext = Mid(searchtext, P + 1)
                I = I + 1
                P = InStr(1, searchtext, " ")
            Loop
            If searchtext <> "" Then
                arrWords(I) = searchtext
            Else
                I = I - 1
            End If
        End If
        Dim intWords As Integer = I
        Dim J As Integer
        Dim sqlText As String = ""
        strsql = strsql & "("
        For J = 0 To intWords
            sqlText = strcolumn & " like " & StrToSql("%" & arrWords(J) & "%")
            If J < intWords Then
                sqlText = sqlText & strCondn
            End If
            strsql = strsql & sqlText
        Next
        strsql = strsql & ")"
        Return strsql
    End Function

    Function getThumpImage(ByVal imageUrl As String, ByRef imageWidth As Integer, ByVal imageHeight As Integer)
        Dim fullSizeImg As System.Drawing.Image
        fullSizeImg = System.Drawing.Image.FromFile(imageUrl)
        imageWidth = fullSizeImg.Width()
        If imageWidth > 0 And imageHeight = 0 Then
            imageHeight = Int(fullSizeImg.Height * imageWidth / fullSizeImg.Width)
        Else
            If imageHeight > 0 And imageWidth = 0 Then
                imageWidth = Int(fullSizeImg.Width * imageHeight / fullSizeImg.Height)
            End If
        End If

    End Function

    Public Sub ShowError(ByVal currentError As Exception)
        Dim context As HttpContext = HttpContext.Current
        context.Response.Write( _
          "<link rel=""stylesheet"" href=""/Admin/Portal.css"">" & _
          "<h2>Error</h2><hr/>" & _
          "An unexpected error has occurred on this page." & _
          "The system administrators have been notified.<br/>" & _
          "<br/><b>The error occurred in:</b>" & _
            "<pre>" & context.Request.Url.ToString & "</pre>" & _
          "<br/><b>Error Message:</b>" & _
            "<pre>" & currentError.Message.ToString & "</pre>" & _
          "<br/><b>Error Stack:</b>" & _
           "<pre>" & currentError.ToString & "</pre>")
    End Sub

    Public Function IsEmptyGUID(ByVal G As Guid) As Boolean
        IsEmptyGUID = False
        If G.Equals(Guid.Empty) Then Return True
    End Function

    Public Function IsEmptyGUID(ByVal G As Guid, ByVal G2 As Object) As Object
        If G.Equals(Guid.Empty) Then
            Return G2
        End If
        IsEmptyGUID = False
    End Function

    Public Function GuidToStr(ByVal G As Guid) As String
        If IsNothing(G) Then
            Return Nothing
        Else
            Return G.ToString
        End If
    End Function

    Public Function StrToGuid(ByVal S As String) As Guid
        If Len(S) < 36 Then
            Return Nothing
        Else
            Try
                StrToGuid = New Guid(S)
            Catch ex As Exception
                Return Nothing
            End Try
        End If
    End Function

    ' added by Jamal 21/11/2006 to fix adding parameters to portal URL (Forms Module)
    Public Function AddPortalURLParameter(ByVal baseUri As Uri, ByVal Parameter As String) As String
        If baseUri.Query > "" Then
            Return baseUri.ToString & "&" & Parameter
        Else
            If InStr(baseUri.ToString.ToLower, "default.aspx") > 0 Then
                Return baseUri.ToString & "?" & Parameter
            Else
                Dim newURL As New Uri(baseUri, "default.aspx?" & Parameter)
                Return newURL.ToString
            End If
        End If
    End Function

    Public Function getMonthName(ByVal Num As Integer) As String
        Dim Name As String = ""
        Select Case Num
            Case 1
                Name = "Januvary"
            Case 2
                Name = "February"
            Case 3
                Name = "March"
            Case 4
                Name = "April"
            Case 5
                Name = "May"
            Case 6
                Name = "June"
            Case 7
                Name = "July"
            Case 8
                Name = "August"
            Case 9
                Name = "September"
            Case 10
                Name = "October"
            Case 11
                Name = "November"
            Case 12
                Name = "December"
        End Select
        getMonthName = Name
    End Function
    Public Function DataTableToCSV(ByVal data As DataTable, filename As String) As Boolean
        Try
            Dim i, j As Integer
            Dim str As New StringBuilder

            If data.Rows.Count > 0 Then
                For i = 0 To data.Columns.Count - 1
                    If str.Length = 0 Then
                        str.Append(data.Columns(i).ColumnName.ToString)
                    Else
                        str.Append(",")
                        str.Append(data.Columns(i).ColumnName.ToString)
                    End If
                Next
                str.AppendLine()
                For i = 0 To data.Rows.Count - 1
                    For j = 0 To data.Columns.Count - 1
                        str.Append(data.Rows(i)(j).ToString())
                        str.Append(",")
                    Next
                    str.AppendLine()
                Next
            End If
            'Dim folderPath As String = "Reports\Report_" & Now.ToString("dd_MMM_yyy_hh_mm_ss")
            File.WriteAllText(filename, str.ToString)
            Return True
        Catch ex As Exception
            Response.Write(ex.Message)
            Response.End()
            Return False
        End Try

    End Function
End Module


