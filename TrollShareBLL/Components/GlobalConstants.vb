﻿Public Module GlobalConstants


    Public Enum DeviceTypeOrChannel
        Android = 1
        iPhone = 2
        Website = 3
        WindowsApp = 4
        WindowsPhone = 5
    End Enum

    Public Enum ServiceProfile
        DefaultProfile = 1
        CustomerProfile = 2
        VendorProfile = 3
    End Enum

    Public Enum CommissioneProfile
        DefaultProfile = 1
        CustomerProfile = 2
        VendorProfile = 3
    End Enum

    Public Enum CustomerStatus
        JustInstalled = 1
        Installed = 2
        KYCFilled = 3
        TrustedCustomer = 4

    End Enum

    Public Enum ValidateCustomerMode
        BeforeInstallation = 0
        AfterInstallation = 1
    End Enum

    Public Enum ServiceTypeList
        Troll_Share = 1
        Paattu_Pusthakam = 2
        Music_Browser = 3
        Casting_Call = 4
    End Enum

    Public Enum ActivitiesLog
        Insert = 1
        Update = 2
        Delete = 3
    End Enum

    Public Enum NotificationMode
        SelectedList = 1
        Android = 2
        iPhone = 3
        All = 4
    End Enum

    Public Enum UserRole
        Administrator = 1
        FileUploader = 2
        ReportUser = 3
        Dealer = 2
    End Enum
    Public Enum UploadFileTypes
        Category = 1
        SubCategory1 = 2
        SubCategory3 = 3
        Product = 4
        Lyrics = 5
        Thumbnil = 6
        Karaoke = 7
        Mp3 = 8
    End Enum

    Public Enum DropDownStatus
        NoSelection = -1
        Active = 1
        InActive = 0
        Suspended = 2
        Stopped = 3
    End Enum

    Public Enum Channels
        Website = 1
        MobileApp = 2
        WindowsStore = 3
    End Enum

    Public Structure SMSLang
        Public Const Arabic As String = "A"
        Public Const English As String = "L"
        Public Const Binary As String = "B"
        Public Const Unicodes As String = "U"
    End Structure


    Public Structure WSDLMsgs

        Public Const AddPrivilage_Violated As String = "You don't have enough permission to add "
        Public Const EditPrivilage_Violated As String = "You don't have enough permission to edit "
        Public Const UpdatePrivilage_Violated As String = "You don't have update permission in this page"
        Public Const DeletePrivilage_Violated As String = "You don't have delete permission in this page"
        Public Const DetailsPrivilage_Violated As String = "You don't have enough permission to view the details of this page"

        Public Const Service_not_available_en As String = "Service_not_available"
        Public Const Service_not_available_ar As String = "لا توجد أي خدمات"

        Public Const ContactUs_not_available_en As String = "ContactUs not available"
        Public Const ContactUs_not_available_ar As String = "لا توجد أي خدمات"

        Public Const WSDL_Success As String = "Success"
        Public Const WSDL_Failed As String = "Failed"

        Public Const UnAutherisedUser As String = "UnAutherised User"
        Public Const AutherisedUser As String = "Autherised User"
        Public Const UnAuthorisedRequest As String = "UnAuthorised Request"

        Public Const InvalidChannel As String = "Invalid Channel"
        Public Const InvalidDevice As String = "Invalid Device"
        Public Const InvalidCustomer As String = "Invalid Customer"

        Public Const VoucherOutofStock_en As String = "Requested Card Out of Stock"
        Public Const InvalidVoucher_en As String = "Invalid Voucher"
        Public Const VoucherDisplayOff_en As String = "Voucher is temporarily disabled"
        Public Const VoucherStopped_en As String = "Voucher is temporarily unavailable"

        Public Const VoucherOutofStock_ar As String = "  الكرت المطلوب غير متوفر  "
        Public Const InvalidVoucher_ar As String = " البطاقة غير متوفرة"
        Public Const VoucherDisplayOff_ar As String = " تم إيقاف خدمة البطاقات مؤقتا"
        Public Const VoucherStopped_ar As String = "تم إيقاف خدمة البطاقات مؤقتا"

        Public Const ServiceDisplayOff_en As String = "Service is temporarily disabled"
        Public Const ServiceStopped_en As String = "Service is currently unavailable "

        Public Const ServiceDisplayOff_ar As String = "تم إيقاف الخدمة مؤقتا"
        Public Const ServiceStopped_ar As String = "الخدمة غير متوفرة حاليا"

        Public Const STBError_ar As String = "رقم STB غير صحيح"
        Public Const STBError_en As String = "STB Number is not correct"
        Public Const ChannelStopped_en As String = "Channel is temporarily unavailable"
        Public Const ChannelStopped_ar As String = "القنوات غير متوفرة حاليا"
        Public Const ChannelDisplayOff_en As String = "Channel is temporarily disabled"
        Public Const ChannelDisplayOff_ar As String = "تم إيقاف خدمة القناة مؤقتا"
        'Public Function GetErrmsg(Code As Integer) As String

        '    Return msg
        'End Function

    End Structure

    Public Enum CategoryList
        Telecom = 1
        UtilityCards = 2
        Finance = 3
        OnlineServices = 4
        ChannelService = 5
    End Enum

    Public Enum SettlementTypes
        PostPaid = 1
        Prepaid = 2
        Voucher = 3

    End Enum

    Public Enum TokenAction
        NoTocken = 0
        TokenGenerated = 1
        Resubmit = 2
        Cancel = 3
        Adjust = 4
        MakeSuccess = 5
        Succeeded = 6
        Failure = 7
    End Enum

    Public Enum DisplayStatus
        isON = 1
        isOFF = 0
    End Enum

    Public Enum Status
        Active = 1
        InActive = 0
        Suspend = 2
        Disabled = 3
    End Enum


    Public Enum TargetType
        Invalid = 0
        Local = 1
        Online = 2
    End Enum

    Public Enum RemoteClient
        eNet = 1
        GlobalTopUp = 2
    End Enum


    Public Enum ApplicationTypeID
        Troll_Share_CPL = 1
        Troll_Share_Report = 2
        Android_TrollShare = 3
        Android_PaatuPusthakam = 4
        Music_Browser = 5
        Casting_Call = 6
    End Enum

End Module
