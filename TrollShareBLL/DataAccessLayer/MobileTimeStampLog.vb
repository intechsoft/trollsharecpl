﻿

'------------------------------------------------------------
'AbhiClassMaker autoGenerated Data Component 
'created for table [MobileTimeStampLog]  on 07-May-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary 

Public Class MobileTimeStampLog
    Inherits UneeshHelper
    Public CustomerID As Integer
    Public ServiceID As Integer
    Public VoucherTypeID As Integer
    Public AccountNumber As String
    Public TranAmount As Decimal
    Public AppTypeID As Integer
    Public Version As String
    Public LogTime As DateTime
    Public WSDLKey As String
    Public emailId As String
    Public MobileNumber As String
    Public DeviceID As String
    Public IpAddress As String

    Public Sub New(Optional ByVal MobileTimeStampLogID As Integer = 0)
        MyBase.New(MobileTimeStampLogID, "AbhiLOG")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "InsertMobileTimeStampLog"
        IUDProcedures.UpdateProcedure = "UpdateMobileTimeStampLog"
        IUDProcedures.DeleteProcedure = "DeleteMobileTimeStampLog"
    End Sub
    Protected Overrides Sub getData()
        If ObjId = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from MobileTimeStampLog where ID=" & ObjId, SqlReturnTypes.DataReader)
        If dr.Read() Then
            CustomerID = IsNullValue(dr("CustomerID"), 0)
            ServiceID = IsNullValue(dr("ServiceID"), 0)
            VoucherTypeID = IsNullValue(dr("VoucherTypeID"), 0)
            AccountNumber = IsNullValue(dr("AccountNumber"), "")
            TranAmount = IsNullValue(dr("TranAmount"), 0)
            AppTypeID = IsNullValue(dr("AppTypeID"), 0)
            Version = IsNullValue(dr("Version"), "")
            LogTime = IsNullValue(dr("LogTime"), New Date(0))
            WSDLKey = IsNullValue(dr("WSDLKey"), "")
            emailId = IsNullValue(dr("emailId"), "")
            MobileNumber = IsNullValue(dr("MobileNumber"), "")
            DeviceID = IsNullValue(dr("DeviceID"), "")
            IpAddress = IsNullValue(dr("IpAddress"), "")
        Else
            ObjId = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@CustomerID", MySqlDbType.Int16), _
        New MySqlParameter("@ServiceID", MySqlDbType.Int16), _
        New MySqlParameter("@VoucherTypeID", MySqlDbType.Int16), _
        New MySqlParameter("@AccountNumber", MySqlDbType.String, 50), _
        New MySqlParameter("@TranAmount", MySqlDbType.Decimal), _
        New MySqlParameter("@AppTypeID", MySqlDbType.Int16), _
        New MySqlParameter("@Version", MySqlDbType.String, 50), _
        New MySqlParameter("@LogTime", MySqlDbType.DateTime), _
        New MySqlParameter("@WSDLKey", MySqlDbType.String, 50), _
        New MySqlParameter("@emailId", MySqlDbType.String, 50), _
        New MySqlParameter("@MobileNumber", MySqlDbType.String, 50), _
        New MySqlParameter("@DeviceID", MySqlDbType.String, 50), _
        New MySqlParameter("@IpAddress", MySqlDbType.String, 50)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(CustomerID, 0)
        sp_params(2).Value = IsNullValue(ServiceID, 0)
        sp_params(3).Value = IsNullValue(VoucherTypeID, 0)
        sp_params(4).Value = IsNullValue(AccountNumber, DBNull.Value)
        sp_params(5).Value = IsNullValue(TranAmount, 0)
        sp_params(6).Value = IsNullValue(AppTypeID, 0)
        sp_params(7).Value = IsNullValue(Version, DBNull.Value)
        sp_params(8).Value = dbDate(LogTime)
        sp_params(9).Value = IsNullValue(WSDLKey, DBNull.Value)
        sp_params(10).Value = IsNullValue(emailId, DBNull.Value)
        sp_params(11).Value = IsNullValue(MobileNumber, DBNull.Value)
        sp_params(12).Value = IsNullValue(DeviceID, DBNull.Value)
        sp_params(13).Value = IsNullValue(IpAddress, DBNull.Value)

        Return sp_params
    End Function
End Class
