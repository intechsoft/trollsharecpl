﻿
'------------------------------------------------------------
'created for table [feedbackmessage]  on 15-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class feedbackmessage
    Inherits UneeshHelper
    Public ID As Integer
    Public CustomerID As Integer
    Public Name As String
    Public MobileNumber As String
    Public EmailID As String
    Public Message As String
    Public MessageTime As DateTime
    Public Remarks As String
    Public ActionTime As DateTime
    Public ActionTakenBy As Integer
    Public Status As Byte

    Public Sub New(Optional ByVal feedbackmessageID As Integer = 0)
        MyBase.New(feedbackmessageID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertfeedbackmessage"
        IUDProcedures.UpdateProcedure = "Updatefeedbackmessage"
        IUDProcedures.DeleteProcedure = "Deletefeedbackmessage"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from feedbackmessage where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            CustomerID = IsNullValue(dr("CustomerID"), 0)
            Name = IsNullValue(dr("Name"), "")
            MobileNumber = IsNullValue(dr("MobileNumber"), "")
            EmailID = IsNullValue(dr("EmailID"), "")
            Message = IsNullValue(dr("Message"), "")
            MessageTime = IsNullValue(dr("MessageTime"), New Date(0))
            Remarks = IsNullValue(dr("Remarks"), "")
            ActionTime = IsNullValue(dr("ActionTime"), New Date(0))
            ActionTakenBy = IsNullValue(dr("ActionTakenBy"), 0)
            Status = IsNullValue(dr("Status"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@CustomerID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 50), _
        New MySqlParameter("@MobileNumber", MySqlDbType.String, 50), _
        New MySqlParameter("@EmailID", MySqlDbType.String, 50), _
        New MySqlParameter("@Message", MySqlDbType.String, 500), _
        New MySqlParameter("@MessageTime", MySqlDbType.DateTime), _
        New MySqlParameter("@Remarks", MySqlDbType.String, 500), _
        New MySqlParameter("@ActionTime", MySqlDbType.DateTime), _
        New MySqlParameter("@ActionTakenBy", MySqlDbType.Int16), _
        New MySqlParameter("@Status", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(CustomerID, 0)
        sp_params(2).Value = IsNullValue(Name, DBNull.Value)
        sp_params(3).Value = IsNullValue(MobileNumber, DBNull.Value)
        sp_params(4).Value = IsNullValue(EmailID, DBNull.Value)
        sp_params(5).Value = IsNullValue(Message, DBNull.Value)
        sp_params(6).Value = dbDate(MessageTime)
        sp_params(7).Value = IsNullValue(Remarks, DBNull.Value)
        sp_params(8).Value = dbDate(ActionTime)
        sp_params(9).Value = IsNullValue(ActionTakenBy, 0)
        sp_params(10).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input
        sp_params(10).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function GetFeedbackforEdit(msgId As Integer) As MySqlDataReader
        Dim str As String = ""
        str += "select * from FeedbackMessage where ID=" & msgId
        Return RunSQLCommand(str, SqlReturnTypes.DataReader)
    End Function

    Public Function GetFeedbackMessages(ByVal where As String) As DataTable
        Dim str As String = ""
        str += " select row_number() over(order by FM.ID desc) as SrNo,FM.*,iif(FM.Status=0,'No Action',TA.Name) AS Action  "
        str += " from FeedbackMessage FM "
        str += " inner join PayMateStore.dbo.TokenActions TA on TA.ID=FM.Status "
        str += " where 1=1 " & where
        str += " order by FM.ID desc"

        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
    End Function

    Public Function GetFeedback() As DataTable
        Dim str As String = ""
        str += "select * from FeedbackMessage order by ID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
    End Function

    Public Function GetFeedback(CustomerID As Integer) As DataTable
        Dim str As String = ""
        str += " Select * from FeedbackMessage where CustomerID = " & CustomerID & " order by ID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

End Class
