﻿
'------------------------------------------------------------
'created for table [customermst]  on 29-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class customermst
    Inherits UneeshHelper
    Public ID As Integer
    Public MobileNumber As String
    Public EmailID As String
    Public CivilID As String
    Public Name As String
    Public NameAlt As String
    Public Age As Byte
    Public Gender As Byte
    Public MasterCustomerID As Integer
    Public MasterEmailID As String
    Public StartDate As DateTime
    Public LastUpdateDate As DateTime
    Public PresentDeviceType As Byte
    Public PresentDeviceData As String
    Public Status As Byte
    Public MasterStatus As Byte
    Public LastTranDate As DateTime
    Public LastInsallID As Integer
    Public LastVerificationKey As String
    Public SecPin As String
    Public SecPinChangedOn As DateTime
    Public CommissionProfileID As Integer
    Public ROL As Decimal
    Public ROQ As Decimal
    Public GCMKey As String
    Public CountryID As Integer
    Public IsRegistered As Byte
    Public AppVersion As String

    Public Sub New(Optional ByVal customermstID As Integer = 0)
        MyBase.New(customermstID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertcustomermst"
        IUDProcedures.UpdateProcedure = "Updatecustomermst"
        IUDProcedures.DeleteProcedure = "Deletecustomermst"
    End Sub
    Protected Overrides Sub getData()
        If ObjId = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from customermst where ID=" & ObjId, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            MobileNumber = IsNullValue(dr("MobileNumber"), "")
            EmailID = IsNullValue(dr("EmailID"), "")
            CivilID = IsNullValue(dr("CivilID"), "")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Age = IsNullValue(dr("Age"), 0)
            Gender = IsNullValue(dr("Gender"), 0)
            MasterCustomerID = IsNullValue(dr("MasterCustomerID"), 0)
            MasterEmailID = IsNullValue(dr("MasterEmailID"), "")
            StartDate = IsNullValue(dr("StartDate"), New Date(0))
            LastUpdateDate = IsNullValue(dr("LastUpdateDate"), New Date(0))
            PresentDeviceType = IsNullValue(dr("PresentDeviceType"), 0)
            PresentDeviceData = IsNullValue(dr("PresentDeviceData"), "")
            Status = IsNullValue(dr("Status"), 0)
            MasterStatus = IsNullValue(dr("MasterStatus"), 0)
            LastTranDate = IsNullValue(dr("LastTranDate"), New Date(0))
            LastInsallID = IsNullValue(dr("LastInsallID"), 0)
            LastVerificationKey = IsNullValue(dr("LastVerificationKey"), "")
            SecPin = IsNullValue(dr("SecPin"), "")
            SecPinChangedOn = IsNullValue(dr("SecPinChangedOn"), New Date(0))
            CommissionProfileID = IsNullValue(dr("CommissionProfileID"), 0)
            ROL = IsNullValue(dr("ROL"), 0)
            ROQ = IsNullValue(dr("ROQ"), 0)
            GCMKey = IsNullValue(dr("GCMKey"), "")
            CountryID = IsNullValue(dr("CountryID"), 0)
            IsRegistered = IsNullValue(dr("IsRegistered"), 0)
            AppVersion = IsNullValue(dr("AppVersion"), "")
        Else
            ObjId = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = {
        New MySqlParameter("@ID", MySqlDbType.Int16),
        New MySqlParameter("@MobileNumber", MySqlDbType.String, 500),
        New MySqlParameter("@EmailID", MySqlDbType.String, 150),
        New MySqlParameter("@CivilID", MySqlDbType.String, 50),
        New MySqlParameter("@Name", MySqlDbType.String, 250),
        New MySqlParameter("@NameAlt", MySqlDbType.String, 250),
        New MySqlParameter("@Age", MySqlDbType.Byte),
        New MySqlParameter("@Gender", MySqlDbType.Byte),
        New MySqlParameter("@MasterCustomerID", MySqlDbType.Int16),
        New MySqlParameter("@MasterEmailID", MySqlDbType.String, 150),
        New MySqlParameter("@StartDate", MySqlDbType.DateTime),
        New MySqlParameter("@LastUpdateDate", MySqlDbType.DateTime),
        New MySqlParameter("@PresentDeviceType", MySqlDbType.Byte),
        New MySqlParameter("@PresentDeviceData", MySqlDbType.String, 500),
        New MySqlParameter("@Status", MySqlDbType.Byte),
        New MySqlParameter("@MasterStatus", MySqlDbType.Byte),
        New MySqlParameter("@LastTranDate", MySqlDbType.DateTime),
        New MySqlParameter("@LastInsallID", MySqlDbType.Int16),
        New MySqlParameter("@LastVerificationKey", MySqlDbType.String, 500),
        New MySqlParameter("@SecPin", MySqlDbType.String, 50),
        New MySqlParameter("@SecPinChangedOn", MySqlDbType.DateTime),
        New MySqlParameter("@CommissionProfileID", MySqlDbType.Int16),
        New MySqlParameter("@ROL", MySqlDbType.Decimal),
        New MySqlParameter("@ROQ", MySqlDbType.Decimal),
        New MySqlParameter("@GCMKey", MySqlDbType.String, 500),
        New MySqlParameter("@CountryID", MySqlDbType.Int16),
        New MySqlParameter("@IsRegistered", MySqlDbType.Byte),
        New MySqlParameter("@AppVersion", MySqlDbType.String, 50)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(MobileNumber, DBNull.Value)
        sp_params(2).Value = IsNullValue(EmailID, DBNull.Value)
        sp_params(3).Value = IsNullValue(CivilID, DBNull.Value)
        sp_params(4).Value = IsNullValue(Name, DBNull.Value)
        sp_params(5).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(6).Value = IsNullValue(Age, 0)
        sp_params(7).Value = IsNullValue(Gender, 0)
        sp_params(8).Value = IsNullValue(MasterCustomerID, 0)
        sp_params(9).Value = IsNullValue(MasterEmailID, DBNull.Value)
        sp_params(10).Value = dbDate(StartDate)
        sp_params(11).Value = dbDate(LastUpdateDate)
        sp_params(12).Value = IsNullValue(PresentDeviceType, 0)
        sp_params(13).Value = IsNullValue(PresentDeviceData, DBNull.Value)
        sp_params(14).Value = IsNullValue(Status, 0)
        sp_params(15).Value = IsNullValue(MasterStatus, 0)
        sp_params(16).Value = dbDate(LastTranDate)
        sp_params(17).Value = IsNullValue(LastInsallID, 0)
        sp_params(18).Value = IsNullValue(LastVerificationKey, DBNull.Value)
        sp_params(19).Value = IsNullValue(SecPin, DBNull.Value)
        sp_params(20).Value = dbDate(SecPinChangedOn)
        sp_params(21).Value = IsNullValue(CommissionProfileID, 0)
        sp_params(22).Value = IsNullValue(ROL, DBNull.Value)
        sp_params(23).Value = IsNullValue(ROQ, DBNull.Value)
        sp_params(24).Value = IsNullValue(GCMKey, DBNull.Value)
        sp_params(25).Value = IsNullValue(CountryID, 0)
        sp_params(26).Value = IsNullValue(IsRegistered, 0)
        sp_params(27).Value = IsNullValue(AppVersion, DBNull.Value)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input
        sp_params(10).Direction = ParameterDirection.Input
        sp_params(11).Direction = ParameterDirection.Input
        sp_params(12).Direction = ParameterDirection.Input
        sp_params(13).Direction = ParameterDirection.Input
        sp_params(14).Direction = ParameterDirection.Input
        sp_params(15).Direction = ParameterDirection.Input
        sp_params(16).Direction = ParameterDirection.Input
        sp_params(17).Direction = ParameterDirection.Input
        sp_params(18).Direction = ParameterDirection.Input
        sp_params(19).Direction = ParameterDirection.Input
        sp_params(20).Direction = ParameterDirection.Input
        sp_params(21).Direction = ParameterDirection.Input
        sp_params(22).Direction = ParameterDirection.Input
        sp_params(23).Direction = ParameterDirection.Input
        sp_params(24).Direction = ParameterDirection.Input
        sp_params(25).Direction = ParameterDirection.Input
        sp_params(26).Direction = ParameterDirection.Input
        sp_params(27).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function GetCustomerCountbyDeviceType() As MySqlDataReader
        Dim str As String = ""
        str += " select  DT.Name as DeviceName,Count(*) as CustomerCount from CustomerMST CU"
        str += " inner join DeviceType DT on CU.PresentDeviceType=DT.ID  group by  PresentDeviceType,DT.Name"
        Return RunSQLCommand(str, SqlReturnTypes.DataReader)
    End Function

    Public Function GetWsLoginTicket(Authentication As MobileAuthHeader, ByVal IpAddr As String) As DataTable

        Dim dtTicket As New DataTable


        Dim sp_params As MySqlParameter() = {
                    New MySqlParameter("@CustomerID", MySqlDbType.Int16),
                    New MySqlParameter("@MobileNumber", MySqlDbType.String, 500),
                    New MySqlParameter("@MacAddress", MySqlDbType.String, 500),
                    New MySqlParameter("@IPAdd", MySqlDbType.VarChar, 100),
                    New MySqlParameter("@DeviceID", MySqlDbType.Int16)}
        sp_params(0).Value = Authentication.CustomerID
        sp_params(1).Value = Authentication.MobileNumber
        sp_params(2).Value = Authentication.MacAddress
        sp_params(3).Value = IpAddr
        sp_params(4).Value = Authentication.DeviceID

        dtTicket = RunProcedure("PROC_GetWsLoginTicket_Mobile", sp_params, SqlReturnTypes.DatatTable)

        If dtTicket Is Nothing Then
            GetWsLoginTicket = Nothing
        Else
            GetWsLoginTicket = dtTicket
        End If

        'If Rtnval = 1 Then
        '    getWsLoginTicket = WsLoginTicketID
        'Else
        '    getWsLoginTicket = 0
        'End If

        If ErrorMsg <> "" Then
            'ReportSQLError("getWsLoginTicket", ErrorMessage)
        End If
    End Function

    Public Function getCustomersInfo(Optional Status As Integer = 0) As DataTable

        Dim str As String = ""
        str += " select distinct ID = 0 , GCMKey from CustomerMST   "
        If Status > 0 Then
            str += " Where [Status] = " & Status
        End If

        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

    Public Function getCustomersInfo(DeviceID As Integer, Status As Integer) As DataTable

        Dim str As String = ""
        str += " Select distinct  ID = 0 ,C.GCMKey from CustomerMST C inner join CustomerChannels CCN on C.ID=CCN.ID   "
        str += " where CCN.DeviceID = " & DeviceID & " and C.[Status] = " & Status

        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

    Public Function getCustomersInfo(SelectedList As String) As DataTable

        Dim str As String = ""
        str += " Select distinct ID,GCMKey from CustomerMST   where ID in ( " & SelectedList & " ) "
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

    Public Function GetCustomerName() As MySqlDataReader

        Return RunSQLCommand("select ID,Name from customermst order by ID", SqlReturnTypes.DataReader)

    End Function
  
    Public Function GetCustomerName(CustomerID As Integer) As DataTable
        Dim str As String = ""
        str += " Select * from customermst where CustomerID = " & CustomerID & " order by CustomerID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

End Class