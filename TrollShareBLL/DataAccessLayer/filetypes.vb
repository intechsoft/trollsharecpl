﻿'####################################### VB.NET COMPONENT ###################################################



'------------------------------------------------------------
'created for table [filetypes]  on 18-Nov-2017
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class filetypes
    Inherits UneeshHelper
    Public ID As Integer
    Public Name As String
    Public NameAlt As String
    Public Status As Byte

    Public Sub New(Optional ByVal filetypesID As Integer = 0)
        MyBase.New(filetypesID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertfiletypes"
        IUDProcedures.UpdateProcedure = "Updatefiletypes"
        IUDProcedures.DeleteProcedure = "Deletefiletypes"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from filetypes where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Status = IsNullValue(dr("Status"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 50), _
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50), _
        New MySqlParameter("@Status", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Name, DBNull.Value)
        sp_params(2).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(3).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function CheckExist(ByVal Name As String, ByVal id As Integer) As Integer
        Dim fileid As Integer = 0
        Dim dr As MySqlDataReader
        If id = 0 Then
            dr = RunSQLCommand("Select ID FROM filetypes Where Name='" & Name & "' ", SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand("Select ID FROM filetypes Where Name='" & Name & "' " & _
                             " Except " & _
                             " Select ID  FROM filetypes  Where ID =" & id, SqlReturnTypes.DataReader)
        End If
        If Not dr Is Nothing Then
            If dr.Read() Then
                fileid = dr("ID")
            End If
            dr.Close()
        End If

        Return fileid
    End Function

    Public Function GetfiletypeforEdit(FileTypeID As Integer) As MySqlDataReader
        Return RunSQLCommand("select ID,Name from filetypes where ID=" & FileTypeID, SqlReturnTypes.DataReader)
    End Function

    Public Function GetFiletype() As DataTable
        Dim sql As String = ""
        sql = "Select * from filetypes order by Name"
        Return RunSQLCommand(sql, SqlReturnTypes.DatatTable)
    End Function

End Class
