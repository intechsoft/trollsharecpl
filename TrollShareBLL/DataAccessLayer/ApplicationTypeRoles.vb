﻿
'------------------------------------------------------------
'created for table [applicationtyperoles]  on 15-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class applicationtyperoles
    Inherits UneeshHelper
    Public ID As Integer
    Public RoleID As Integer
    Public applicationtype As Byte
    Public Privilege As String
    Public Status As Byte

    Public Sub New(Optional ByVal applicationtyperolesID As Integer = 0)
        MyBase.New(applicationtyperolesID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertapplicationtyperoles"
        IUDProcedures.UpdateProcedure = "Updateapplicationtyperoles"
        IUDProcedures.DeleteProcedure = "Deleteapplicationtyperoles"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from applicationtyperoles where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            RoleID = IsNullValue(dr("RoleID"), 0)
            applicationtype = IsNullValue(dr("ApplicationType"), 0)
            Privilege = IsNullValue(dr("Privilege"), "")
            Status = IsNullValue(dr("Status"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@RoleID", MySqlDbType.Int16), _
        New MySqlParameter("@ApplicationType", MySqlDbType.byte), _
        New MySqlParameter("@Privilege", MySqlDbType.String, 1000), _
        New MySqlParameter("@Status", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(RoleID, 0)
        sp_params(2).Value = IsNullValue(ApplicationType, 0)
        sp_params(3).Value = IsNullValue(Privilege, DBNull.Value)
        sp_params(4).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function CheckExist(ByVal RoleID As String, ByVal AppID As Integer, ByVal AppRoleID As Integer) As Integer
        Dim id As Integer = 0
        Dim dr As MySqlDataReader
        If AppID = 0 Then
            dr = RunSQLCommand("Select ID from applicationtyperoles Where ApplicationType= " & AppID & " and RoleID = " & RoleID, SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand("Select ID from applicationtyperoles Where ApplicationType= " & AppID & " and RoleID = " & RoleID & _
                                 " Except " & _
                                 " Select ID from applicationtyperoles Where ApplicationType= " & AppID & " and RoleID = " & RoleID & " and ID = " & AppRoleID, SqlReturnTypes.DataReader)
        End If
        If Not dr Is Nothing Then
            If dr.Read Then
                id = dr("ID")
            End If
            dr.Close()
        End If

        Return id
    End Function

    Public Function GetApplicationTypeRoles() As DataTable
        Dim str As String = ""
        str += "Select ROW_NUMBER() over (order by ATR.ID) as SrNo,ATR.ID,UR.Name as [Role],AT.Name as [Application],ATR.[Status] from applicationtyperoles ATR"
        str += " inner join UserRoles UR on UR.ID=ATR.RoleID"
        str += " inner joinapplicationtypeAT on AT.ID=ATR.ApplicationType"
        str += " order by ATR.ID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
    End Function

End Class
