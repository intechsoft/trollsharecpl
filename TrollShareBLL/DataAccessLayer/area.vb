﻿
'------------------------------------------------------------
'created for table [area]  on 29-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class area
    Inherits UneeshHelper
    Public ID As Integer
    Public Code As String
    Public Name As String
    Public NameAlt As String
    Public Latitude As String
    Public Longitude As String
    Public Status As Byte

    Public Sub New(Optional ByVal areaID As Integer = 0)
        MyBase.New(areaID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertarea"
        IUDProcedures.UpdateProcedure = "Updatearea"
        IUDProcedures.DeleteProcedure = "Deletearea"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from area where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Code = IsNullValue(dr("Code"), "")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Latitude = IsNullValue(dr("Latitude"), "")
            Longitude = IsNullValue(dr("Longitude"), "")
            Status = IsNullValue(dr("Status"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@Code", MySqlDbType.String, 10), _
        New MySqlParameter("@Name", MySqlDbType.String, 50), _
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50), _
        New MySqlParameter("@Latitude", MySqlDbType.String, 50), _
        New MySqlParameter("@Longitude", MySqlDbType.String, 50), _
        New MySqlParameter("@Status", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Code, DBNull.Value)
        sp_params(2).Value = IsNullValue(Name, DBNull.Value)
        sp_params(3).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(4).Value = IsNullValue(Latitude, DBNull.Value)
        sp_params(5).Value = IsNullValue(Longitude, DBNull.Value)
        sp_params(6).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function CheckExist(ByVal Name As String, ByVal Code As String, ByVal id As Integer) As Integer
        Dim areaid As Integer = 0
        Dim dr As MySqlDataReader
        If id = 0 Then
            dr = RunSQLCommand("Select ID FROM area Where Name='" & Name & "' or Code='" & Code & "'", SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand("Select ID FROM area Where Name='" & Name & "' or Code='" & Code & "'" & _
                             " Except " & _
                             " Select ID  FROM area  Where ID =" & id, SqlReturnTypes.DataReader)
        End If
        If Not dr Is Nothing Then
            If dr.Read() Then
                areaid = dr("ID")
            End If
            dr.Close()
        End If

        Return areaid
    End Function


    Public Function GetAreaforEdit(AreaId As Integer) As MySqlDataReader
        Return RunSQLCommand("select ID,Name from servicetype where ID=" & AreaId, SqlReturnTypes.DataReader)
    End Function

    Public Function GetArea() As DataTable
        Dim sql As String = ""
        sql = "Select * from area order by Name"
        Return RunSQLCommand(sql, SqlReturnTypes.DatatTable)
    End Function

End Class
