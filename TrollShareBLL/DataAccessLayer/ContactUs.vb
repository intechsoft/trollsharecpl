﻿'------------------------------------------------------------
'created for table [contactus]  on 16-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class contactus
    Inherits UneeshHelper
    Public ID As Integer
    Public AppID As Integer
    Public CompanyName As String
    Public CompanyNameAlt As String
    Public PhoneNumber As String
    Public Fax As String
    Public EmailID As String
    Public Website As String
    Public Facebook As String
    Public Twitter As String
    Public YouTube As String
    Public GooglePlus As String
    Public LinkedIn As String
    Public Instagram As String
    Public LastUpdatedDataSync As DateTime
    Public Address As String

    Public Sub New(Optional ByVal contactusID As Integer = 0)
        MyBase.New(contactusID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertcontactus"
        IUDProcedures.UpdateProcedure = "Updatecontactus"
        IUDProcedures.DeleteProcedure = "Deletecontactus"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from contactus where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            AppID = IsNullValue(dr("AppID"), 0)
            CompanyName = IsNullValue(dr("CompanyName"), "")
            CompanyNameAlt = IsNullValue(dr("CompanyNameAlt"), "")
            PhoneNumber = IsNullValue(dr("PhoneNumber"), "")
            Fax = IsNullValue(dr("Fax"), "")
            EmailID = IsNullValue(dr("EmailID"), "")
            Website = IsNullValue(dr("Website"), "")
            Facebook = IsNullValue(dr("Facebook"), "")
            Twitter = IsNullValue(dr("Twitter"), "")
            YouTube = IsNullValue(dr("YouTube"), "")
            GooglePlus = IsNullValue(dr("GooglePlus"), "")
            LinkedIn = IsNullValue(dr("LinkedIn"), "")
            Instagram = IsNullValue(dr("Instagram"), "")
            LastUpdatedDataSync = IsNullValue(dr("LastUpdatedDataSync"), New Date(0))
            Address = IsNullValue(dr("Address"), "")
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@AppID", MySqlDbType.Int16), _
        New MySqlParameter("@CompanyName", MySqlDbType.String, 50), _
        New MySqlParameter("@CompanyNameAlt", MySqlDbType.String, 50), _
        New MySqlParameter("@PhoneNumber", MySqlDbType.String, 200), _
        New MySqlParameter("@Fax", MySqlDbType.String, 20), _
        New MySqlParameter("@EmailID", MySqlDbType.String, 50), _
        New MySqlParameter("@Website", MySqlDbType.String, 1000), _
        New MySqlParameter("@Facebook", MySqlDbType.String, 1000), _
        New MySqlParameter("@Twitter", MySqlDbType.String, 1000), _
        New MySqlParameter("@YouTube", MySqlDbType.String, 1000), _
        New MySqlParameter("@GooglePlus", MySqlDbType.String, 1000), _
        New MySqlParameter("@LinkedIn", MySqlDbType.String, 1000), _
        New MySqlParameter("@Instagram", MySqlDbType.String, 1000), _
        New MySqlParameter("@LastUpdatedDataSync", MySqlDbType.DateTime), _
        New MySqlParameter("@Address", MySqlDbType.String, 500)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(AppID, 0)
        sp_params(2).Value = IsNullValue(CompanyName, DBNull.Value)
        sp_params(3).Value = IsNullValue(CompanyNameAlt, DBNull.Value)
        sp_params(4).Value = IsNullValue(PhoneNumber, DBNull.Value)
        sp_params(5).Value = IsNullValue(Fax, DBNull.Value)
        sp_params(6).Value = IsNullValue(EmailID, DBNull.Value)
        sp_params(7).Value = IsNullValue(Website, DBNull.Value)
        sp_params(8).Value = IsNullValue(Facebook, DBNull.Value)
        sp_params(9).Value = IsNullValue(Twitter, DBNull.Value)
        sp_params(10).Value = IsNullValue(YouTube, DBNull.Value)
        sp_params(11).Value = IsNullValue(GooglePlus, DBNull.Value)
        sp_params(12).Value = IsNullValue(LinkedIn, DBNull.Value)
        sp_params(13).Value = IsNullValue(Instagram, DBNull.Value)
        sp_params(14).Value = dbDate(LastUpdatedDataSync)
        sp_params(15).Value = IsNullValue(Address, DBNull.Value)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input
        sp_params(10).Direction = ParameterDirection.Input
        sp_params(11).Direction = ParameterDirection.Input
        sp_params(12).Direction = ParameterDirection.Input
        sp_params(13).Direction = ParameterDirection.Input
        sp_params(14).Direction = ParameterDirection.Input
        sp_params(15).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function GetContact() As DataTable
        Dim str As String = ""
        str += " select CU.ID,AT.Name as AppTypeName,CU.CompanyName,CU.PhoneNumber,CU.EmailID,CU.Fax from contactus CU"
        str += " inner join applicationtype AT on CU.AppID=AT.ID"
        str += " order by CU.ID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
    End Function

    Public Function GetContactforEdit(ContactId As Integer) As MySqlDataReader
        Dim str As String = ""
        str += "select CU.* from contactus CU "
        str += " where ID=" & ContactId
        Return RunSQLCommand(str, SqlReturnTypes.DataReader)
    End Function

    Public Function GetContactUs(AppID As Integer) As DataTable
        Dim str As String = ""
        str += " Select * from contactus where AppID = " & AppID & " order by AppID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

End Class
