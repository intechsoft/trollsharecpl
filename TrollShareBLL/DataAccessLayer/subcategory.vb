


'####################################### VB.NET COMPONENT ###################################################



'------------------------------------------------------------
'created for table [subcategory]  on 18-Jan-2016
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class subcategory
    Inherits UneeshHelper
    Public ID As Integer
    Public CategoryID As Integer
    Public Code As String
    Public Name As String
    Public NameAlt As String
    Public Description As String
    Public DescriptionAlt As String
    Public ImageURL As String
    Public ImageURLAlt As String
    Public ViewOrder As Byte
    Public Status As Byte
    Public DisplayStatus As Byte

    Public Sub New(Optional ByVal subcategoryID As Integer = 0)
        MyBase.New(subcategoryID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertsubcategory"
        IUDProcedures.UpdateProcedure = "Updatesubcategory"
        IUDProcedures.DeleteProcedure = "Deletesubcategory"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from subcategory where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            CategoryID = IsNullValue(dr("CategoryID"), 0)
            Code = IsNullValue(dr("Code"), "")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Description = IsNullValue(dr("Description"), "")
            DescriptionAlt = IsNullValue(dr("DescriptionAlt"), "")
            ImageURL = IsNullValue(dr("ImageURL"), "")
            ImageURLAlt = IsNullValue(dr("ImageURLAlt"), "")
            ViewOrder = IsNullValue(dr("ViewOrder"), 0)
            Status = IsNullValue(dr("Status"), 0)
            DisplayStatus = IsNullValue(dr("DisplayStatus"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = {
        New MySqlParameter("@ID", MySqlDbType.Int16),
        New MySqlParameter("@CategoryID", MySqlDbType.Int16),
        New MySqlParameter("@Code", MySqlDbType.String, 50),
        New MySqlParameter("@Name", MySqlDbType.String, 250),
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50),
        New MySqlParameter("@Description", MySqlDbType.Text),
        New MySqlParameter("@DescriptionAlt", MySqlDbType.Text),
        New MySqlParameter("@ImageURL", MySqlDbType.String, 250),
        New MySqlParameter("@ImageURLAlt", MySqlDbType.String, 250),
        New MySqlParameter("@ViewOrder", MySqlDbType.Byte),
        New MySqlParameter("@Status", MySqlDbType.Byte),
        New MySqlParameter("@DisplayStatus", MySqlDbType.Byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(CategoryID, 0)
        sp_params(2).Value = IsNullValue(Net.WebUtility.HtmlEncode(Code), DBNull.Value)
        sp_params(3).Value = IsNullValue(Net.WebUtility.HtmlEncode(Name), DBNull.Value)
        sp_params(4).Value = IsNullValue(Net.WebUtility.HtmlEncode(NameAlt), DBNull.Value)
        sp_params(5).Value = IsNullValue(Net.WebUtility.HtmlEncode(Description), DBNull.Value)
        sp_params(6).Value = IsNullValue(Net.WebUtility.HtmlEncode(DescriptionAlt), DBNull.Value)
        sp_params(7).Value = IsNullValue(Net.WebUtility.HtmlEncode(ImageURL), DBNull.Value)
        sp_params(8).Value = IsNullValue(Net.WebUtility.HtmlEncode(ImageURLAlt), DBNull.Value)
        sp_params(9).Value = IsNullValue(ViewOrder, 0)
        sp_params(10).Value = IsNullValue(Status, 0)
        sp_params(11).Value = IsNullValue(DisplayStatus, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input
        sp_params(10).Direction = ParameterDirection.Input
        sp_params(11).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function GetCategoryforEdit(catId As Integer) As MySqlDataReader
        Return RunSQLCommand("Select * from subcategory where ID=" & catId, SqlReturnTypes.DataReader)
    End Function

    Public Function CheckExist(ByVal Name As String, ByVal Code As String, ByVal id As Integer) As Integer
        Dim categoryid As Integer = 0
        Dim dr As MySqlDataReader
        If id = 0 Then
            dr = RunSQLCommand("Select ID FROM subcategory Where Name='" & Name & "' or Code='" & Code & "'", SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand("Select ID FROM subcategory Where Name='" & Name & "' or Code='" & Code & "'" &
                             " Except " &
                             " Select ID  FROM subcategory  Where ID =" & id, SqlReturnTypes.DataReader)
        End If
        If Not dr Is Nothing Then
            If dr.Read() Then
                categoryid = dr("ID")
            End If
            dr.Close()
        End If

        Return categoryid
    End Function

    Public Function GetSubCategory() As DataTable
        Dim str As String = ""
        str += "SELECT CT.*"
        str += " from subcategory CT order by CT.ViewOrder"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
    End Function

    Public Function GetSubCategoryName(CategoryID As Integer) As MySqlDataReader
        Return RunSQLCommand("select ID,Name from subcategory where CategoryID=" & CategoryID, SqlReturnTypes.DataReader)
    End Function

    Public Function GetAllSubCategoryName(CategoryID As Integer) As MySqlDataReader
        Return RunSQLCommand("select ID,Name from subcategory where CategoryID=" & CategoryID & " order by ViewOrder", SqlReturnTypes.DataReader)
    End Function

    Public Function GetSubCategoryName() As MySqlDataReader
        Return RunSQLCommand("select ID,Name from subcategory where Status=1", SqlReturnTypes.DataReader)
    End Function

    Public Function GetAllSubCategoryName() As MySqlDataReader
        Return RunSQLCommand("select ID,Name from subcategory order by ViewOrder", SqlReturnTypes.DataReader)
    End Function

    Public Function GetSubCategories() As MySqlDataReader
        Return RunSQLCommand("select ID,Name from subcategory", SqlReturnTypes.DataReader)
    End Function


    Public Function GetSubCategory(CategoryID As Integer) As DataTable
        Dim str As String = ""
        str += "select * from subcategory where CategoryID=" & CategoryID & " order by ViewOrder"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
    End Function

End Class
