﻿
'------------------------------------------------------------
'created for table [devicetype]  on 15-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class devicetype
    Inherits UneeshHelper
    Public ID As Integer
    Public Name As String
    Public NameAlt As String
    Public Description As String
    Public DescriptionAlt As String
    Public Status As Byte
    Public LastDataSyncDate As DateTime

    Public Sub New(Optional ByVal devicetypeID As Integer = 0)
        MyBase.New(devicetypeID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertdevicetype"
        IUDProcedures.UpdateProcedure = "Updatedevicetype"
        IUDProcedures.DeleteProcedure = "Deletedevicetype"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from devicetype where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Description = IsNullValue(dr("Description"), "")
            DescriptionAlt = IsNullValue(dr("DescriptionAlt"), "")
            Status = IsNullValue(dr("Status"), 0)
            LastDataSyncDate = IsNullValue(dr("LastDataSyncDate"), New Date(0))
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 50), _
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50), _
        New MySqlParameter("@Description", MySqlDbType.String, 250), _
        New MySqlParameter("@DescriptionAlt", MySqlDbType.String, 250), _
        New MySqlParameter("@Status", MySqlDbType.byte), _
        New MySqlParameter("@LastDataSyncDate", MySqlDbType.DateTime)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Name, DBNull.Value)
        sp_params(2).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(3).Value = IsNullValue(Description, DBNull.Value)
        sp_params(4).Value = IsNullValue(DescriptionAlt, DBNull.Value)
        sp_params(5).Value = IsNullValue(Status, 0)
        sp_params(6).Value = dbDate(LastDataSyncDate)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function CheckExist(ByVal Name As String, ByVal ID As Integer) As Integer
        Dim dr As MySqlDataReader
        Dim Deviceid As Integer = 0
        If ID = 0 Then
            dr = RunSQLCommand("Select ID FROM DeviceType Where Name='" & Name & "'", SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand("Select ID FROM DeviceType Where Name='" & Name & "' except " & _
                               "Select ID FROM DeviceType Where ID=" & ID, SqlReturnTypes.DataReader)
        End If

        If dr.Read() Then
            Deviceid = dr("ID")
        End If
        dr.Close()
        Return Deviceid
    End Function

    Public Function GetDeviceTypeforEdit(DeviceId As Integer) As MySqlDataReader
        Dim str As String = ""
        str += " Select * from DeviceType where ID=" & DeviceId
        Return RunSQLCommand(str, SqlReturnTypes.DataReader)
    End Function

    Public Function GetDeviceType() As DataTable
        Dim str As String = ""
        str += "Select ID,Name,Status from devicetype order by ID"
        'str += "Select row_number() over(order by DT.ID ) as SrNo,DT.*"
        'str += " from DeviceType DT order by DT.ID "
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
    End Function

    Public Function GetDevice() As MySqlDataReader
        Dim str As String = ""
        str += "Select ID,Name,status from DeviceType order by ID"
        Return RunSQLCommand(str, SqlReturnTypes.DataReader)
    End Function

End Class
