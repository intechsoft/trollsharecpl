﻿'------------------------------------------------------------
'created for table [mobileinstalllog]  on 15-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class mobileinstalllog
    Inherits UneeshHelper
    Public ID As Integer
    Public MobileNumber As String
    Public EmailID As String
    Public CivilID As String
    Public EName As String
    Public AName As String
    Public Age As Byte
    Public Gender As Byte
    Public DeviceType As Integer
    Public DeviceData As String
    Public VerificationKey As String
    Public InstallDate As DateTime
    Public DeviceKeyID As String
    Public Status As Byte
    Public ServiceProfileID As Integer

    Public Sub New(Optional ByVal mobileinstalllogID As Integer = 0)
        MyBase.New(mobileinstalllogID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertmobileinstalllog"
        IUDProcedures.UpdateProcedure = "Updatemobileinstalllog"
        IUDProcedures.DeleteProcedure = "Deletemobileinstalllog"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from mobileinstalllog where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            MobileNumber = IsNullValue(dr("MobileNumber"), "")
            EmailID = IsNullValue(dr("EmailID"), "")
            CivilID = IsNullValue(dr("CivilID"), "")
            EName = IsNullValue(dr("EName"), "")
            AName = IsNullValue(dr("AName"), "")
            Age = IsNullValue(dr("Age"), 0)
            Gender = IsNullValue(dr("Gender"), 0)
            DeviceType = IsNullValue(dr("DeviceType"), 0)
            DeviceData = IsNullValue(dr("DeviceData"), "")
            VerificationKey = IsNullValue(dr("VerificationKey"), "")
            InstallDate = IsNullValue(dr("InstallDate"), New Date(0))
            DeviceKeyID = IsNullValue(dr("DeviceKeyID"), "")
            Status = IsNullValue(dr("Status"), 0)
            ServiceProfileID = IsNullValue(dr("ServiceProfileID"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@MobileNumber", MySqlDbType.String, 500), _
        New MySqlParameter("@EmailID", MySqlDbType.String, 50), _
        New MySqlParameter("@CivilID", MySqlDbType.String, 50), _
        New MySqlParameter("@EName", MySqlDbType.String, 250), _
        New MySqlParameter("@AName", MySqlDbType.String, 250), _
        New MySqlParameter("@Age", MySqlDbType.byte), _
        New MySqlParameter("@Gender", MySqlDbType.byte), _
        New MySqlParameter("@DeviceType", MySqlDbType.Int16), _
        New MySqlParameter("@DeviceData", MySqlDbType.String, 500), _
        New MySqlParameter("@VerificationKey", MySqlDbType.String, 500), _
        New MySqlParameter("@InstallDate", MySqlDbType.DateTime), _
        New MySqlParameter("@DeviceKeyID", MySqlDbType.String, 500), _
        New MySqlParameter("@Status", MySqlDbType.byte), _
        New MySqlParameter("@ServiceProfileID", MySqlDbType.Int16)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(MobileNumber, DBNull.Value)
        sp_params(2).Value = IsNullValue(EmailID, DBNull.Value)
        sp_params(3).Value = IsNullValue(CivilID, DBNull.Value)
        sp_params(4).Value = IsNullValue(EName, DBNull.Value)
        sp_params(5).Value = IsNullValue(AName, DBNull.Value)
        sp_params(6).Value = IsNullValue(Age, 0)
        sp_params(7).Value = IsNullValue(Gender, 0)
        sp_params(8).Value = IsNullValue(DeviceType, 0)
        sp_params(9).Value = IsNullValue(DeviceData, DBNull.Value)
        sp_params(10).Value = IsNullValue(VerificationKey, DBNull.Value)
        sp_params(11).Value = dbDate(InstallDate)
        sp_params(12).Value = IsNullValue(DeviceKeyID, DBNull.Value)
        sp_params(13).Value = IsNullValue(Status, 0)
        sp_params(14).Value = IsNullValue(ServiceProfileID, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input
        sp_params(10).Direction = ParameterDirection.Input
        sp_params(11).Direction = ParameterDirection.Input
        sp_params(12).Direction = ParameterDirection.Input
        sp_params(13).Direction = ParameterDirection.Input
        sp_params(14).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function GetMobileInstallLog() As DataTable

        Return RunSQLCommand("SELECT dbo.ServiceProfileMST.Name as ServiceProfileName, dbo.MobileInstallLog.* FROM dbo.MobileInstallLog INNER JOIN dbo.ServiceProfileMST ON dbo.MobileInstallLog.ServiceProfileID = dbo.ServiceProfileMST.ID", SqlReturnTypes.DatatTable)

    End Function



    Public Function GetMobileforEdit(ByVal MobileID As Integer) As DataTable

        Return RunSQLCommand("SELECT dbo.ServiceProfileMST.Name as ServiceProfileName, dbo.MobileInstallLog.* FROM dbo.MobileInstallLog INNER JOIN dbo.ServiceProfileMST ON dbo.MobileInstallLog.ServiceProfileID = dbo.ServiceProfileMST.ID where MobileInstallLog.ID=" & MobileID & " ", SqlReturnTypes.DatatTable)

    End Function


End Class
