﻿
'------------------------------------------------------------
'created for table [ applicationtype ]  on 15-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class applicationtype
    Inherits UneeshHelper
    Public ID As Integer
    Public Name As String
    Public NameAlt As String
    Public Status As Byte

    Public Sub New(Optional ByVal applicationtypeID As Integer = 0)
        MyBase.New(applicationtypeID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertapplicationtype"
        IUDProcedures.UpdateProcedure = "Updateapplicationtype"
        IUDProcedures.DeleteProcedure = "Deleteapplicationtype"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from  applicationtype  where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Status = IsNullValue(dr("Status"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 50), _
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50), _
        New MySqlParameter("@Status", MySqlDbType.Byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Name, DBNull.Value)
        sp_params(2).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(3).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function GetWebPages(ByVal ApplicationTypeID As Integer) As MySqlDataReader
        Return RunSQLCommand("select WP.ID,S.Name, WP.Title, WP.FileName, WP.FilePath, WP.SubSection, WP.PageLevel," & _
                             "      WP.Section, WP.Status ,WP.Parent, WP.ViewOrder" & _
                             "  from WebPages WP inner join webpagesection S on WP.Section =S.ID where S.ApplicationType=" & ApplicationTypeID & _
                             "  order by WP.Section, WP.ViewOrder", SqlReturnTypes.DataReader)
    End Function


    Public Function GetApplicationTypeList() As MySqlDataReader
        Return RunSQLCommand("select * from applicationtype where status=1 Order by Name", SqlReturnTypes.DataReader)
    End Function

    Public Function CheckExist(ByVal Name As String, ByVal AppID As Integer) As Integer
        Dim id As Integer = 0
        Dim dr As MySqlDataReader
        If AppID = 0 Then
            dr = RunSQLCommand("Select ID from applicationtype Where Name ='" & Name & "'", SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand(" select ID from applicationtype where Name ='" & Name & "'" & _
                                 " Except " & _
                                 " select ID from applicationtype where Name = '" & Name & "' and ID =" & AppID, SqlReturnTypes.DataReader)
        End If
        If Not dr Is Nothing Then
            If dr.Read Then
                id = dr("ID")
            End If
            dr.Close()
        End If

        Return id
    End Function

    Public Function GetAppTypeforEdit(appid As Integer) As MySqlDataReader

        Dim str As String = ""

        str += "select Name,NameAlt,Status FROM applicationtype where ID = " & appid

        Return RunSQLCommand(str, SqlReturnTypes.DataReader)

    End Function

    Public Function GetApplicationType() As DataTable

        'Dim str As String = ""
        'str += "Select  row_number() over(order by AT.ID ) as SrNo,AT.* "
        'str += " from applicationtype AT order by AT.ID "
        'Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
        Dim str As String = ""
        str += "SELECT AT.*"
        str += " from applicationtype AT order by AT.ID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

    Public Function GetApplicationName() As MySqlDataReader
        Return RunSQLCommand("select ID,Name from applicationtype where Status=1", SqlReturnTypes.DataReader)
    End Function

    

 
End Class
