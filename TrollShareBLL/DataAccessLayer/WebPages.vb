﻿'------------------------------------------------------------
'created for table [webpages]  on 15-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class webpages
    Inherits UneeshHelper
    Public ID As Integer
    Public Title As String
    Public TitleAlt As String
    Public FileName As String
    Public FilePath As String
    Public SectionID As Integer
    Public SubSection As Integer
    Public PageLevel As Byte
    Public Status As Byte
    Public ViewOrder As Integer
    Public Parent As Integer

    Public Sub New(Optional ByVal webpagesID As Integer = 0)
        MyBase.New(webpagesID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertwebpages"
        IUDProcedures.UpdateProcedure = "Updatewebpages"
        IUDProcedures.DeleteProcedure = "Deletewebpages"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from webpages where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Title = IsNullValue(dr("Title"), "")
            TitleAlt = IsNullValue(dr("TitleAlt"), "")
            FileName = IsNullValue(dr("FileName"), "")
            FilePath = IsNullValue(dr("FilePath"), "")
            SectionID = IsNullValue(dr("SectionID"), 0)
            SubSection = IsNullValue(dr("SubSection"), 0)
            PageLevel = IsNullValue(dr("PageLevel"), 0)
            Status = IsNullValue(dr("Status"), 0)
            ViewOrder = IsNullValue(dr("ViewOrder"), 0)
            Parent = IsNullValue(dr("Parent"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@Title", MySqlDbType.String, 255), _
        New MySqlParameter("@TitleAlt", MySqlDbType.String, 255), _
        New MySqlParameter("@FileName", MySqlDbType.String, 255), _
        New MySqlParameter("@FilePath", MySqlDbType.String, 255), _
        New MySqlParameter("@SectionID", MySqlDbType.Int16), _
        New MySqlParameter("@SubSection", MySqlDbType.Int16), _
        New MySqlParameter("@PageLevel", MySqlDbType.byte), _
        New MySqlParameter("@Status", MySqlDbType.byte), _
        New MySqlParameter("@ViewOrder", MySqlDbType.Int16), _
        New MySqlParameter("@Parent", MySqlDbType.Int16)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Title, DBNull.Value)
        sp_params(2).Value = IsNullValue(TitleAlt, DBNull.Value)
        sp_params(3).Value = IsNullValue(FileName, DBNull.Value)
        sp_params(4).Value = IsNullValue(FilePath, DBNull.Value)
        sp_params(5).Value = IsNullValue(SectionID, 0)
        sp_params(6).Value = IsNullValue(SubSection, 0)
        sp_params(7).Value = IsNullValue(PageLevel, 0)
        sp_params(8).Value = IsNullValue(Status, 0)
        sp_params(9).Value = IsNullValue(ViewOrder, 0)
        sp_params(10).Value = IsNullValue(Parent, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input
        sp_params(10).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function GetWebPages(ByVal ApplicationTypeID As Integer) As MySqlDataReader
        Return RunSQLCommand("select WP.ID,S.Name, WP.Title, WP.FileName, WP.FilePath, WP.SubSection, WP.PageLevel," & _
                             "      WP.SectionID, WP.Status ,WP.Parent, WP.ViewOrder" & _
                             "  from webpages WP inner join webpagesection S on WP.SectionID =S.ID where S.ApplicationType=" & ApplicationTypeID & _
                             "  order by WP.SectionID, WP.ViewOrder", SqlReturnTypes.DataReader)
    End Function

    'Public Function GetWebPages(Optional ByVal ApplicationTypeID As Integer = 0, Optional ByVal Sectionid As Integer = 0, Optional ByVal Lang As String = "en") As DataTable

    '    If ApplicationTypeID <> 0 And Sectionid <> 0 Then

    '        If Lang = "en" Then
    '            Return RunSQLCommand("select row_number() over(order by WP.ID ) as SrNo,WP.ID,S.Name, WP.Title, WP.FileName, WP.FilePath, WP.SubSection, WP.PageLevel," & _
    '                                        "      WP.Section, WP.Status ,WP.Parent, WP.ViewOrder" & _
    '                                        "  from WebPages WP inner join webpagesection S on WP.Section =S.ID where S.ApplicationType=" & ApplicationTypeID & _
    '                                        " and  WP.Section= " & Sectionid & " order by WP.ViewOrder", 0, 0).Tables(0)
    '        Else
    '            Return RunSQLCommand("select row_number() over(order by WP.ID ) as SrNo,WP.ID,S.Name, WP.TitleAr [Title], WP.FileName, WP.FilePath, WP.SubSection, WP.PageLevel," & _
    '                                    "      WP.Section, WP.Status ,WP.Parent, WP.ViewOrder" & _
    '                                    "  from WebPages WP inner join webpagesection S on WP.Section =S.ID where S.ApplicationType=" & ApplicationTypeID & _
    '                                    " and  WP.Section= " & Sectionid & " order by WP.ViewOrder", 0, 0).Tables(0)
    '        End If

    '    Else

    '        Return RunSQLCommand("select row_number() over(order by WP.ID ) as SrNo,WP.ID,S.Name, WP.Title, WP.FileName, WP.FilePath, WP.SubSection, WP.PageLevel," & _
    '                                        "      WP.Section, WP.Status ,WP.Parent, WP.ViewOrder" & _
    '                                        "  from WebPages WP inner join webpagesection S on WP.Section =S.ID  order by WP.ViewOrder", 0, 0).Tables(0)


    '    End If

    'End Function

    Public Function GetWebPages(Optional ByVal ApplicationTypeID As Integer = 0, Optional ByVal Sectionid As Integer = 0, Optional ByVal Lang As String = "en") As DataTable
        Dim str As String = ""
        If Lang = "en" Then
            str += "select 1 as SrNo,WP.ID,S.Name, WP.Title, WP.FileName, WP.FilePath, WP.SubSection, WP.PageLevel,"
            str += " WP.SectionID, WP.Status ,WP.Parent, WP.ViewOrder"
            str += " from webpages WP inner join webpagesection S on WP.SectionID =S.ID where WP.Status = 1 and (S.ApplicationType=" & ApplicationTypeID & " or " & ApplicationTypeID & "=0)"
            str += " and ( WP.SectionID= " & Sectionid & " or " & Sectionid & " =0) order by WP.ViewOrder"
            Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
        Else
            str += "select 1 as SrNo,WP.ID,S.Name, WP.TitleAr [Title], WP.FileName, WP.FilePath, WP.SubSection, WP.PageLevel,"
            str += " WP.SectionID, WP.Status ,WP.Parent, WP.ViewOrder"
            str += " from webpages WP inner join webpagesection S on WP.SectionID =S.ID where WP.Status = 1 and (S.ApplicationType=" & ApplicationTypeID & " or " & ApplicationTypeID & "=0)"
            str += " and  ( WP.SectionID= " & Sectionid & " or " & Sectionid & " =0) order by WP.ViewOrder"
            Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
        End If

    End Function

    Public Function CheckExist(ByVal Title As String, ByVal ID As Integer, ByVal ApplicationTypeID As Integer) As Integer
        Dim webId As Integer = 0
        Dim dr As MySqlDataReader
        If ID = 0 Then
            dr = RunSQLCommand("Select webpages.ID  FROM    WebPages INNER JOIN     webpagesection ON webpages.SectionID =WebPageSection.ID Where webpages.Title ='" & Title & "' and WebPageSection.ApplicationType =" & ApplicationTypeID, SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand(" select webpages.ID  FROM    WebPages INNER JOIN     webpagesection ON webpages.SectionID =WebPageSection.ID where webpages.Title ='" & Title & "' and WebPageSection.ApplicationType =" & ApplicationTypeID & _
                                 " Except " & _
                                 " select webpages.ID  FROM    WebPages INNER JOIN     webpagesection ON webpages.SectionID =WebPageSection.ID where webpages.Title = '" & Title & "' and  webpages.ID =" & ID & " and WebPageSection.ApplicationType =" & ApplicationTypeID, SqlReturnTypes.DataReader)
        End If
        If dr.Read() Then
            webId = dr("ID")
        End If
        dr.Close()
        Return webId
    End Function

    Public Function GetParentWebPage(ByVal varSection As Integer, ByVal ApplicationTypeID As Integer) As MySqlDataReader
        Return RunSQLCommand("select webpages.ID, webpages.Title   FROM    WebPages INNER JOIN  webpagesection ON webpages.SectionID = WebPageSection.ID   Where webpages.SectionID=" & varSection & " andapplicationtype=" & ApplicationTypeID & _
                             " and webpages.Parent =0  order by webpages.Title", SqlReturnTypes.DataReader)
    End Function

    Public Function GetWebPageforEdit(ByVal WebPageID As Integer) As DataTable
        Dim str As String = ""
        str += " SELECT WP.ID,WP.Title,WP.FileName,WP.FilePath,WP.SectionID,WP.SubSection,WP.PageLevel,WP.ViewOrder,WP.Parent,WP.Status,WPS.ApplicationType"
        str += " FROM  WebPages WP "
        str += " INNER JOIN webpagesection WPS ON WP.SectionID=WPS.ID"
        str += " WHERE WP.ID= " & WebPageID & " ORDER BY WP.ViewOrder"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

End Class

