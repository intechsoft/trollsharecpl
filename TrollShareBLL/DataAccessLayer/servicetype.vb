﻿
'------------------------------------------------------------
'created for table [servicetype]  on 16-Oct-2017
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class servicetype
    Inherits UneeshHelper
    Public ID As Integer
    Public Name As String
    Public NameAlt As String
    Public AppTypeID As Byte
    Public Status As Byte

    Public Sub New(Optional ByVal servicetypeID As Integer = 0)
        MyBase.New(servicetypeID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertservicetype"
        IUDProcedures.UpdateProcedure = "Updateservicetype"
        IUDProcedures.DeleteProcedure = "Deleteservicetype"
    End Sub
    Protected Overrides Sub getData()
        If ObjId = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from servicetype where ID=" & ObjId, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            AppTypeID = IsNullValue(dr("AppTypeID"), 0)
            Status = IsNullValue(dr("Status"), 0)
        Else
            ObjId = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = {
        New MySqlParameter("@ID", MySqlDbType.Int16),
        New MySqlParameter("@Name", MySqlDbType.String, 50),
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50),
        New MySqlParameter("@AppTypeID", MySqlDbType.Byte),
        New MySqlParameter("@Status", MySqlDbType.Byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Name, DBNull.Value)
        sp_params(2).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(3).Value = IsNullValue(AppTypeID, 0)
        sp_params(4).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function GetServiceTypes() As DataTable
        Dim sql As String = ""
        sql = "Select S.ID,S.Name,S.NameAlt,S.Status,A.Name AppName from servicetype S inner join applicationtype A on S.AppTypeID=A.ID order by S.Name"
        Return RunSQLCommand(sql, SqlReturnTypes.DatatTable)
    End Function

    Public Function GetServiceTypeList() As MySqlDataReader
        Return RunSQLCommand("select * from servicetype where status=1 Order by Name", SqlReturnTypes.DataReader)
    End Function

    Public Function GetServiceTypeforEdit(ServiceTypeId As Integer) As MySqlDataReader
        Return RunSQLCommand("select ID,Name from servicetype where ID=" & ServiceTypeId, SqlReturnTypes.DataReader)
    End Function

    Public Function CheckExist(ByVal Name As String, ByVal AppTypeID As String, ByVal id As Integer) As Integer
        Dim categoryid As Integer = 0
        Dim dr As MySqlDataReader
        If id = 0 Then
            dr = RunSQLCommand("Select ID FROM servicetype Where Name='" & Name & "' and AppTypeID='" & AppTypeID & "'", SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand("Select ID FROM servicetype Where Name='" & Name & "' and AppTypeID='" & AppTypeID & "'" &
                             " Except " &
                             " Select ID  FROM servicetype  Where ID =" & id, SqlReturnTypes.DataReader)
        End If
        If Not dr Is Nothing Then
            If dr.Read() Then
                categoryid = dr("ID")
            End If
            dr.Close()
        End If

        Return categoryid
    End Function
    
   

End Class