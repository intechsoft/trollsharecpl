﻿
'------------------------------------------------------------
'created for table [apphistory]  on 29-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class apphistory
    Inherits UneeshHelper
    Public ID As Integer
    Public AppID As Integer
    Public Name As String
    Public VersionName As String
    Public VersionCode As String
    Public StartDate As DateTime
    Public EndDate As DateTime
    Public Descriptions As String
    Public LastUpdate As DateTime
    Public Status As Byte

    Public Sub New(Optional ByVal apphistoryID As Integer = 0)
        MyBase.New(apphistoryID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertapphistory"
        IUDProcedures.UpdateProcedure = "Updateapphistory"
        IUDProcedures.DeleteProcedure = "Deleteapphistory"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from apphistory where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            AppID = IsNullValue(dr("AppID"), 0)
            Name = IsNullValue(dr("Name"), "")
            VersionName = IsNullValue(dr("VersionName"), "")
            VersionCode = IsNullValue(dr("VersionCode"), "")
            StartDate = IsNullValue(dr("StartDate"), New Date(0))
            EndDate = IsNullValue(dr("EndDate"), New Date(0))
            Descriptions = IsNullValue(dr("Descriptions"), "")
            LastUpdate = IsNullValue(dr("LastUpdate"), New Date(0))
            Status = IsNullValue(dr("Status"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@AppID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 50), _
        New MySqlParameter("@VersionName", MySqlDbType.String, 50), _
        New MySqlParameter("@VersionCode", MySqlDbType.String, 50), _
        New MySqlParameter("@StartDate", MySqlDbType.DateTime), _
        New MySqlParameter("@EndDate", MySqlDbType.DateTime), _
        New MySqlParameter("@Descriptions", MySqlDbType.String, 500), _
        New MySqlParameter("@LastUpdate", MySqlDbType.DateTime), _
        New MySqlParameter("@Status", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(AppID, 0)
        sp_params(2).Value = IsNullValue(Name, DBNull.Value)
        sp_params(3).Value = IsNullValue(VersionName, DBNull.Value)
        sp_params(4).Value = IsNullValue(VersionCode, DBNull.Value)
        sp_params(5).Value = dbDate(StartDate)
        sp_params(6).Value = dbDate(EndDate)
        sp_params(7).Value = IsNullValue(Descriptions, DBNull.Value)
        sp_params(8).Value = dbDate(LastUpdate)
        sp_params(9).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input

        Return sp_params
    End Function
End Class
