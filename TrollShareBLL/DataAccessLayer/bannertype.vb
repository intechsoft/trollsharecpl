﻿'####################################### VB.NET COMPONENT ###################################################



'------------------------------------------------------------
'created for table [bannertype]  on 08-Nov-2017
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary

Public Class bannertype
    Inherits UneeshHelper
    Public ID As Integer
    Public Name As String
    Public NameAlt As String
    Public Status As Byte

    Public Sub New(Optional ByVal bannertypeID As Integer = 0)
        MyBase.New(bannertypeID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertbannertype"
        IUDProcedures.UpdateProcedure = "Updatebannertype"
        IUDProcedures.DeleteProcedure = "Deletebannertype"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from bannertype where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Status = IsNullValue(dr("Status"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 50), _
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50), _
        New MySqlParameter("@Status", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Name, DBNull.Value)
        sp_params(2).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(3).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function getBannerType() As DataTable
        Return RunSQLCommand("select * from bannertype where status=1 Order by ID", SqlReturnTypes.DatatTable)
    End Function
End Class
