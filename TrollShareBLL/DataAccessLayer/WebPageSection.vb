﻿
'------------------------------------------------------------
'created for table [webpagesection]  on 15-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class webpagesection
    Inherits UneeshHelper
    Public ID As Integer
    Public Name As String
    Public NameAlt As String
    Public Status As Byte
    Public ViewOrder As Integer
    Public applicationtype As Integer

    Public Sub New(Optional ByVal webpagesectionID As Integer = 0)
        MyBase.New(webpagesectionID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertwebpagesection"
        IUDProcedures.UpdateProcedure = "Updatewebpagesection"
        IUDProcedures.DeleteProcedure = "Deletewebpagesection"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from webpagesection where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Status = IsNullValue(dr("Status"), 0)
            ViewOrder = IsNullValue(dr("ViewOrder"), 0)
            applicationtype = IsNullValue(dr("ApplicationType"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 255), _
        New MySqlParameter("@NameAlt", MySqlDbType.String, 255), _
        New MySqlParameter("@Status", MySqlDbType.byte), _
        New MySqlParameter("@ViewOrder", MySqlDbType.Int16), _
        New MySqlParameter("@ApplicationType", MySqlDbType.Int16)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Name, DBNull.Value)
        sp_params(2).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(3).Value = IsNullValue(Status, 0)
        sp_params(4).Value = IsNullValue(ViewOrder, 0)
        sp_params(5).Value = IsNullValue(ApplicationType, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function CheckExist(ByVal Name As String, ByVal ID As Integer, ByVal ApplicationTypeID As Integer) As Integer
        Dim webId As Integer = 0
        Dim dr As MySqlDataReader
        If ID = 0 Then
            dr = RunSQLCommand("Select ID from webpagesection Where Name ='" & Name & "' andapplicationtype=" & ApplicationTypeID, SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand(" select ID from webpagesection where Name ='" & Name & "' andapplicationtype=" & ApplicationTypeID & _
                                 " Except " & _
                                 " select ID from webpagesection where Name = '" & Name & "' and ID =" & ID & " andapplicationtype=" & ApplicationTypeID, SqlReturnTypes.DataReader)
        End If
        If dr.Read() Then
            webId = dr("ID")
        End If
        dr.Close()
        Return webId
    End Function
    Public Function GetActiveWebPageSections(ByVal ApplicationTypeID As Integer, Optional ByVal Lang As String = "en") As MySqlDataReader
        If Lang = "en" Then
            Return RunSQLCommand("SELECT ID, Name,Status  FROM webpagesection  Where Status =1 " & " and applicationtype=" & ApplicationTypeID & " order by ViewOrder", SqlReturnTypes.DataReader)
        Else
            Return RunSQLCommand("SELECT ID, NameAlt [Name],Status  FROM webpagesection  Where Status =1 " & " and applicationtype=" & ApplicationTypeID & " order by ViewOrder", SqlReturnTypes.DataReader)
        End If
    End Function

    Public Function GetWebPageSection(ByVal ApplicationTypeID As Integer) As MySqlDataReader
        Dim query As String = ""
        query += " Select WPS.ID,WPS.Name from webpagesection WPS "
        query += " inner joinapplicationtypeAT on WPS.ApplicationType=AT.ID"
        query += " where (AT.ID=" & ApplicationTypeID & " or " & ApplicationTypeID & "=0)"
        Return RunSQLCommand(query, SqlReturnTypes.DataReader)
    End Function

    Public Function GetWebPageSections(Optional ByVal ApplicationTypeID As Integer = -1) As DataTable
        Dim str As String = ""
        str += "SELECT row_number() over(order by WPS.ViewOrder) as SrNo,WPS.ID, WPS.Name, WPS.ViewOrder,  WPS.status,AT.Name as ApplicationTypeName "
        str += " FromapplicationtypeAT"
        str += " INNER JOIN   webpagesection WPS ON AT.ID =WPS.ApplicationType "
        str += " where (WPS.ApplicationType =" & ApplicationTypeID & " or " & ApplicationTypeID & "=-1)"
        str += " order by WPS.ViewOrder "
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
    End Function

    Public Function getWebPageSectionforEdit(secId As Integer) As MySqlDataReader
        Return RunSQLCommand("Select * from webpagesection where ID=" & secId, SqlReturnTypes.DataReader)
    End Function

End Class
