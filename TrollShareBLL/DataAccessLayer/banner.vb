﻿'------------------------------------------------------------
'created for table [banner]  on 07-Nov-2017
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class banner
    Inherits UneeshHelper
    Public ID As Integer
    Public BannerType As Byte
    Public BannerOwnerID As Integer
    Public CategoryID As Integer
    Public SubCategoryID As Integer
    Public Name As String
    Public NameAlt As String
    Public ImageUrl As String
    Public BannerData As String
    Public ViewOrder As Integer
    Public ViewCount As Integer
    Public Website As String
    Public PhoneNumber As String
    Public EmailID As String
    Public LastAccessDate As DateTime
    Public LastSyncDate As DateTime
    Public Status As Byte
    Public DisplayStatus As Byte

    Public Sub New(Optional ByVal bannerID As Integer = 0)
        MyBase.New(bannerID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertbanner"
        IUDProcedures.UpdateProcedure = "Updatebanner"
        IUDProcedures.DeleteProcedure = "Deletebanner"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from banner where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            BannerType = IsNullValue(dr("BannerType"), 0)
            BannerOwnerID = IsNullValue(dr("BannerOwnerID"), 0)
            CategoryID = IsNullValue(dr("CategoryID"), 0)
            SubCategoryID = IsNullValue(dr("SubCategoryID"), 0)
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            ImageUrl = IsNullValue(dr("ImageUrl"), "")
            BannerData = IsNullValue(dr("BannerData"), "")
            ViewOrder = IsNullValue(dr("ViewOrder"), 0)
            ViewCount = IsNullValue(dr("ViewCount"), 0)
            Website = IsNullValue(dr("Website"), "")
            PhoneNumber = IsNullValue(dr("PhoneNumber"), "")
            EmailID = IsNullValue(dr("EmailID"), "")
            LastAccessDate = IsNullValue(dr("LastAccessDate"), New Date(0))
            LastSyncDate = IsNullValue(dr("LastSyncDate"), New Date(0))
            Status = IsNullValue(dr("Status"), 0)
            DisplayStatus = IsNullValue(dr("DisplayStatus"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@BannerType", MySqlDbType.byte), _
        New MySqlParameter("@BannerOwnerID", MySqlDbType.Int16), _
        New MySqlParameter("@CategoryID", MySqlDbType.Int16), _
        New MySqlParameter("@SubCategoryID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 250), _
        New MySqlParameter("@NameAlt", MySqlDbType.String), _
        New MySqlParameter("@ImageUrl", MySqlDbType.String, 500), _
        New MySqlParameter("@BannerData", MySqlDbType.String, 5000), _
        New MySqlParameter("@ViewOrder", MySqlDbType.Int16), _
        New MySqlParameter("@ViewCount", MySqlDbType.Int16), _
        New MySqlParameter("@Website", MySqlDbType.String, 500), _
        New MySqlParameter("@PhoneNumber", MySqlDbType.String, 50), _
        New MySqlParameter("@EmailID", MySqlDbType.String, 50), _
        New MySqlParameter("@LastAccessDate", MySqlDbType.DateTime), _
        New MySqlParameter("@LastSyncDate", MySqlDbType.DateTime), _
        New MySqlParameter("@Status", MySqlDbType.byte), _
        New MySqlParameter("@DisplayStatus", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(BannerType, 0)
        sp_params(2).Value = IsNullValue(BannerOwnerID, 0)
        sp_params(3).Value = IsNullValue(CategoryID, 0)
        sp_params(4).Value = IsNullValue(SubCategoryID, 0)
        sp_params(5).Value = IsNullValue(Name, DBNull.Value)
        sp_params(6).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(7).Value = IsNullValue(ImageUrl, DBNull.Value)
        sp_params(8).Value = IsNullValue(BannerData, DBNull.Value)
        sp_params(9).Value = IsNullValue(ViewOrder, 0)
        sp_params(10).Value = IsNullValue(ViewCount, 0)
        sp_params(11).Value = IsNullValue(Website, DBNull.Value)
        sp_params(12).Value = IsNullValue(PhoneNumber, DBNull.Value)
        sp_params(13).Value = IsNullValue(EmailID, DBNull.Value)
        sp_params(14).Value = dbDate(LastAccessDate)
        sp_params(15).Value = dbDate(LastSyncDate)
        sp_params(16).Value = IsNullValue(Status, 0)
        sp_params(17).Value = IsNullValue(DisplayStatus, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input
        sp_params(10).Direction = ParameterDirection.Input
        sp_params(11).Direction = ParameterDirection.Input
        sp_params(12).Direction = ParameterDirection.Input
        sp_params(13).Direction = ParameterDirection.Input
        sp_params(14).Direction = ParameterDirection.Input
        sp_params(15).Direction = ParameterDirection.Input
        sp_params(16).Direction = ParameterDirection.Input
        sp_params(17).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function getBannerInfo(Authentication As MobileAuthHeader) As DataSet
        Dim str As String = ""
        str += " Select BO.ID as BannerOwnerID,BO.Name as BannerOwnerName,BO.Status as BannerOwnerStatus,"
        str += " B.ID as BannerID,B.BannerType,B.CategoryID,B.SubCategoryID,B.ImageUrl,B.BannerData,B.ViewOrder,B.ViewCount,B.Website,B.PhoneNumber,B.EmailID,B.Status"
        str += " from bannerowner BO inner join banner B on BO.ID=B.BannerOwnerID where Bo.Status=1 and B.Status=1 order by ViewOrder"
        Return RunSQLCommand(str, SqlReturnTypes.DataSet)
    End Function

    Public Function getBanner(whereClause As String) As DataTable
        Dim sql As String = ""
        sql += " SELECT b.ID,bo.Name,b.PhoneNumber,b.EmailID from banner b"
        sql += " inner join subcategory s on b.SubCategoryID=s.ID"
        sql += " inner join categorymst c on s.CategoryID=c.ID"
        sql += " inner join bannerowner bo on b.BannerOwnerID=bo.ID"
        sql += " WHERE 1=1 " & whereClause & ";"
        Return RunSQLCommand(sql, SqlReturnTypes.DatatTable)
    End Function

    Public Function getBanner() As DataTable
        Return RunSQLCommand("select ID,Name from banner order by ID", SqlReturnTypes.DatatTable)
    End Function

End Class
