﻿
'------------------------------------------------------------
'created for table [uploadedfiles]  on 19-Dec-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class uploadedfiles
    Inherits UneeshHelper
    Public ID As Integer
    Public FileName As String
    Public FilePath As String
    Public UserID As Integer
    Public FileType As Integer
    Public IpAddress As String
    Public Status As Byte
    Public UploadedDate As DateTime

    Public Sub New(Optional ByVal uploadedfilesID As Integer = 0)
        MyBase.New(uploadedfilesID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertuploadedfiles"
        IUDProcedures.UpdateProcedure = "Updateuploadedfiles"
        IUDProcedures.DeleteProcedure = "Deleteuploadedfiles"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from uploadedfiles where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            FileName = IsNullValue(dr("FileName"), "")
            FilePath = IsNullValue(dr("FilePath"), "")
            UserID = IsNullValue(dr("UserID"), 0)
            FileType = IsNullValue(dr("FileType"), 0)
            IpAddress = IsNullValue(dr("IpAddress"), "")
            Status = IsNullValue(dr("Status"), 0)
            UploadedDate = IsNullValue(dr("UploadedDate"), New Date(0))
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = {
        New MySqlParameter("@ID", MySqlDbType.Int16),
        New MySqlParameter("@FileName", MySqlDbType.String, 500),
        New MySqlParameter("@FilePath", MySqlDbType.String, 500),
        New MySqlParameter("@UserID", MySqlDbType.Int16),
        New MySqlParameter("@FileType", MySqlDbType.Int16),
        New MySqlParameter("@IpAddress", MySqlDbType.String, 50),
        New MySqlParameter("@Status", MySqlDbType.byte),
        New MySqlParameter("@UploadedDate", MySqlDbType.DateTime)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(FileName, DBNull.Value)
        sp_params(2).Value = IsNullValue(FilePath, DBNull.Value)
        sp_params(3).Value = IsNullValue(UserID, 0)
        sp_params(4).Value = IsNullValue(FileType, 0)
        sp_params(5).Value = IsNullValue(IpAddress, DBNull.Value)
        sp_params(6).Value = IsNullValue(Status, 0)
        sp_params(7).Value = dbDate(UploadedDate)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function CheckExist(ByVal Name As String, ByVal Code As String, ByVal id As Integer) As Integer
        Dim categoryid As Integer = 0
        Dim dr As MySqlDataReader
        If id = 0 Then
            dr = RunSQLCommand("Select ID FROM uploadedfiles Where FileName='" & Name & "' or FileType='" & Code & "'", SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand("Select ID FROM uploadedfiles Where fileName='" & Name & "' or FilePath='" & Code & "'" & _
                             " Except " & _
                             " Select ID  FROM uploadedfiles  Where ID =" & id, SqlReturnTypes.DataReader)
        End If
        If Not dr Is Nothing Then
            If dr.Read() Then
                categoryid = dr("ID")
            End If
            dr.Close()
        End If

        Return categoryid
    End Function

    Public Function GetUploadedfilesforEdit(fileId As Integer) As MySqlDataReader

        Return RunSQLCommand("Select * from uploadedfiles where ID=" & fileId, SqlReturnTypes.DataReader)

    End Function

    Public Function GetUploadedfile() As DataTable

        Dim str As String = ""
        str += " select f.ID,u.UserName,f.fileName,f.FilePath,f.FileType,f.Status from uploadedfiles f"
        str += " inner join userlogins u on f.UserID=u.ID"
        str += " order by f.ID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

    Public Function GetUploadedfile(UserID As Integer) As DataTable

        Dim str As String = ""
        str += " select f.ID,u.UserName,f.fileName,f.FilePath,f.FileType,f.Status from uploadedfiles f"
        str += " inner join userlogins u on f.UserID=u.ID"
        str += " where f.UserID = " & UserID & " order by f.ID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

End Class
