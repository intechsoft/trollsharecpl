﻿
'------------------------------------------------------------
'created for table [notificationlog]  on 29-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class notificationlog
    Inherits UneeshHelper
    Public ID As Integer
    Public UserID As Integer
    Public CustomerIDs As String
    Public Message As String
    Public LogTime As DateTime
    Public FromIP As String

    Public Sub New(Optional ByVal notificationlogID As Integer = 0)
        MyBase.New(notificationlogID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertnotificationlog"
        IUDProcedures.UpdateProcedure = "Updatenotificationlog"
        IUDProcedures.DeleteProcedure = "Deletenotificationlog"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from notificationlog where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            UserID = IsNullValue(dr("UserID"), 0)
            CustomerIDs = IsNullValue(dr("CustomerIDs"), "")
            Message = IsNullValue(dr("Message"), "")
            LogTime = IsNullValue(dr("LogTime"), New Date(0))
            FromIP = IsNullValue(dr("FromIP"), "")
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@UserID", MySqlDbType.Int16), _
        New MySqlParameter("@CustomerIDs", MySqlDbType.String, 500), _
        New MySqlParameter("@Message", MySqlDbType.String, 500), _
        New MySqlParameter("@LogTime", MySqlDbType.DateTime), _
        New MySqlParameter("@FromIP", MySqlDbType.String, 50)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(UserID, 0)
        sp_params(2).Value = IsNullValue(CustomerIDs, DBNull.Value)
        sp_params(3).Value = IsNullValue(Message, DBNull.Value)
        sp_params(4).Value = dbDate(LogTime)
        sp_params(5).Value = IsNullValue(FromIP, DBNull.Value)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input

        Return sp_params
    End Function
End Class
