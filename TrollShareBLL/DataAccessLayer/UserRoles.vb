﻿
'------------------------------------------------------------
'created for table [userroles]  on 15-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class userroles
    Inherits UneeshHelper
    Public ID As Integer
    Public Name As String
    Public NameAlt As String
    Public Privilege As String
    Public Status As Byte

    Public Sub New(Optional ByVal userrolesID As Integer = 0)
        MyBase.New(userrolesID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertuserroles"
        IUDProcedures.UpdateProcedure = "Updateuserroles"
        IUDProcedures.DeleteProcedure = "Deleteuserroles"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from userroles where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Privilege = IsNullValue(dr("Privilege"), "")
            Status = IsNullValue(dr("Status"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 50), _
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50), _
        New MySqlParameter("@Privilege", MySqlDbType.String, 100), _
        New MySqlParameter("@Status", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Name, DBNull.Value)
        sp_params(2).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(3).Value = IsNullValue(Privilege, DBNull.Value)
        sp_params(4).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function CheckExist(ByVal Name As String, ByVal RID As Integer) As Integer
        Dim id As Integer = 0
        Dim dr As MySqlDataReader
        If RID = 0 Then
            dr = RunSQLCommand("Select ID from UserRoles Where Name ='" & Name & "'", SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand(" select ID from UserRoles where Name ='" & Name & "'" & _
                                 " Except " & _
                                 " select ID from UserRoles where Name = '" & Name & "' and ID =" & id, SqlReturnTypes.DataReader)
        End If
        If Not dr Is Nothing Then
            If dr.Read() Then
                id = dr("ID")
            End If
            dr.Close()
        End If
        Return id
    End Function

    Public Function GetUserRoles() As MySqlDataReader
        Dim str As String = ""
        str += "select row_number() over(order by ID ) as SrNo,ID,Name,Status FROM UserRoles where Status = 1 order by ID "
        Return RunSQLCommand(str, SqlReturnTypes.DataReader)
    End Function

    Public Function GetUserRole() As DataTable
        'Dim str As String = ""
        'str += "select row_number() over(order by ID ) as SrNo,ID,Name,Status FROM UserRoles where Status = 1 order by ID "
        'Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
        Dim str As String = ""
        str += "SELECT u.*"
        str += " from userroles u order by u.ID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
    End Function

    Public Function GetUserRoles(ByVal applicationtype As Integer) As DataTable
        Dim str As String = ""
        str += "SELECT UR.*,AT.Name AS ApplicationType,AT.ID AS ApplicationTypeID "
        str += " FROM UserRoles UR INNER JOIN"
        str += "applicationtypeAT ON AT.ID=UR.ApplicationType"
        str += " WHERE (UR.ApplicationType=" & applicationtype & " OR " & applicationtype & "=0)"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)
    End Function

    Public Function GetUserRoles(ByVal id As Integer, ByVal applicationtype As Integer) As MySqlDataReader
        If applicationtype <> 0 Then
            ' Return RunSQLCommand("select UserRoles.ID,UserRoles.Name,UserRoles.Status,UserRoles.Privilege,ApplicationType.Name as ApplicationType,ApplicationType.ID as ApplicationTypeID  FROM     UserRoles INNER JOIN  applicationtypeON ApplicationType.ID = UserRoles.ApplicationType where  UserRoles.ApplicationType=" & applicationtype & " and   UserRoles.ID =" & id)
            Return RunSQLCommand("SELECT UR.ID, UR.Name, AR.ID as AppTypeRoleID , UR.Status, AR.Privilege, AT.Name ApplicationType, AT.ID ApplicationTypeID" & _
            "    FROM UserRoles UR INNER JOIN applicationtyperoles AR ON AR.RoleID = UR.ID INNER JOINapplicationtypeAT ON AT.ID = AR.ApplicationType" & _
            "   WHERE AT.ID = " & applicationtype & " And UR.ID = " & id, SqlReturnTypes.DataReader)
        Else
            Return RunSQLCommand("select UserRoles.ID,UserRoles.Name,UserRoles.Status,UserRoles.Privilege,ApplicationType.Name as ApplicationType,ApplicationType.ID as ApplicationTypeID  FROM     UserRoles INNER JOIN  applicationtypeON ApplicationType.ID = UserRoles.ApplicationType where  UserRoles.ID =" & id, SqlReturnTypes.DataReader)
        End If
    End Function
    Public Function GetUserRoleforEdit(roleId As Integer) As MySqlDataReader
        Return RunSQLCommand("select * FROM UserRoles where ID = " & roleId, SqlReturnTypes.DataReader)
    End Function

End Class

