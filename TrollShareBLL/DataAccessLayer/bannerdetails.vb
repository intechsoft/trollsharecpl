﻿
'------------------------------------------------------------
'created for table [bannerdetails]  on 29-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class bannerdetails
    Inherits UneeshHelper
    Public ID As Integer
    Public LocalBannerID As Integer
    Public BannerID As Integer
    Public CustomerID As Integer
    Public LastViewDate As DateTime
    Public LastViewIP As String
    Public SyncDate As DateTime

    Public Sub New(Optional ByVal bannerdetailsID As Integer = 0)
        MyBase.New(bannerdetailsID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertbannerdetails"
        IUDProcedures.UpdateProcedure = "Updatebannerdetails"
        IUDProcedures.DeleteProcedure = "Deletebannerdetails"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from bannerdetails where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            LocalBannerID = IsNullValue(dr("LocalBannerID"), 0)
            BannerID = IsNullValue(dr("BannerID"), 0)
            CustomerID = IsNullValue(dr("CustomerID"), 0)
            LastViewDate = IsNullValue(dr("LastViewDate"), New Date(0))
            LastViewIP = IsNullValue(dr("LastViewIP"), "")
            SyncDate = IsNullValue(dr("SyncDate"), New Date(0))
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@LocalBannerID", MySqlDbType.Int16), _
        New MySqlParameter("@BannerID", MySqlDbType.Int16), _
        New MySqlParameter("@CustomerID", MySqlDbType.Int16), _
        New MySqlParameter("@LastViewDate", MySqlDbType.DateTime), _
        New MySqlParameter("@LastViewIP", MySqlDbType.String, 50), _
        New MySqlParameter("@SyncDate", MySqlDbType.DateTime)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(LocalBannerID, 0)
        sp_params(2).Value = IsNullValue(BannerID, 0)
        sp_params(3).Value = IsNullValue(CustomerID, 0)
        sp_params(4).Value = dbDate(LastViewDate)
        sp_params(5).Value = IsNullValue(LastViewIP, DBNull.Value)
        sp_params(6).Value = dbDate(SyncDate)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function getBannerDetails(whereClause As String) As DataTable
        Dim sql As String = ""
        sql += " select bd.ID,c.Name,b.BannerData from bannerdetails bd "
        sql += " inner join banner b on bd.BannerID=b.ID"
        sql += " inner join categorymst c on b.CategoryID=c.ID"
        sql += " WHERE 1=1 " & whereClause & ";"
        Return RunSQLCommand(sql, SqlReturnTypes.DatatTable)
    End Function

    Public Function getBannerDetails() As DataTable
        Dim sql As String = ""
        sql += " select bd.ID,c.Name,b.BannerData from bannerdetails bd "
        sql += " inner join banner b on bd.BannerID=b.ID"
        sql += " inner join categorymst c on b.CategoryID=c.ID"
        sql += " order by bd.ID"
        Return RunSQLCommand(sql, SqlReturnTypes.DatatTable)
    End Function

    Public Function getBanner() As MySqlDataReader
        Return RunSQLCommand("select ID,Name from banner order by ID", SqlReturnTypes.DataReader)
    End Function


    Public Function GetBannerByCatagory(CategoryID As Integer) As MySqlDataReader
        Dim sql As String = ""
        sql += " select b.Name,b.ID from banner b"
        sql += " inner join subcategory s on b.SubCategoryID=s.ID"
        sql += " inner join categorymst c on s.CategoryID=c.ID "
        sql += " where s.CategoryID=" & CategoryID & " order by b.name;"
        Return RunSQLCommand(sql, SqlReturnTypes.DataReader)
    End Function

    Public Function GetBannerBySubCategory(SubCategoryID As Integer) As MySqlDataReader
        Return RunSQLCommand("select ID,Name from banner where SubCategoryID=" & SubCategoryID, SqlReturnTypes.DataReader)
    End Function








End Class
