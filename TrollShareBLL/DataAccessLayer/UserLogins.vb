﻿
'------------------------------------------------------------
'created for table [userlogins]  on 12-Dec-2015
'------------------------------------------------------------
Imports System.Web
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class userlogins
    Inherits UneeshHelper
    Public ID As Integer
    Public CustomerID As Integer
    Public EmployeeID As Integer
    Public CompanyID As Integer
    Public UserName As String
    Public UserPwd As String
    Public UserActualName As String
    Public UserTypeId As Integer
    Public LanguageOption As String
    Public Status As Byte
    Public RoleID As Integer
    Public PasswordChanged As Byte
    Public LastPasswordChangeDate As DateTime
    Public UserKey As String
    Public LoginCount As Byte
    Public LastLoginDate As DateTime
    Public LastLoginIP As String
    Public LastLoginAttempt As DateTime
    Public LastLoginAttemptIP As String
    Public AppVersion As String

    Public Sub New(Optional ByVal userloginsID As Integer = 0)
        MyBase.New(userloginsID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertuserlogins"
        IUDProcedures.UpdateProcedure = "Updateuserlogins"
        IUDProcedures.DeleteProcedure = "Deleteuserlogins"
    End Sub
    Protected Overrides Sub getData()
        If ObjId = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from userlogins where ID=" & ObjId, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            CustomerID = dr("CustomerID")
            EmployeeID = dr("EmployeeID")
            CompanyID = dr("CompanyID")
            UserName = dr("UserName")
            UserPwd = dr("UserPwd")
            UserActualName = IsNullValue(dr("UserActualName"), "")
            UserTypeId = dr("UserTypeId")
            LanguageOption = IsNullValue(dr("LanguageOption"), "")
            Status = dr("Status")
            RoleID = dr("RoleID")
            PasswordChanged = dr("PasswordChanged")
            LastPasswordChangeDate = IsNullValue(dr("LastPasswordChangeDate"), New Date(0))
            UserKey = IsNullValue(dr("UserKey"), "")
            LoginCount = IsNullValue(dr("LoginCount"), 0)
            LastLoginDate = IsNullValue(dr("LastLoginDate"), New Date(0))
            LastLoginIP = IsNullValue(dr("LastLoginIP"), "")
            LastLoginAttempt = IsNullValue(dr("LastLoginAttempt"), New Date(0))
            LastLoginAttemptIP = IsNullValue(dr("LastLoginAttemptIP"), "")
            AppVersion = IsNullValue(dr("AppVersion"), "")
        Else
            ObjId = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = {
        New MySqlParameter("@ID", MySqlDbType.Int16),
        New MySqlParameter("@CustomerID", MySqlDbType.Int16),
        New MySqlParameter("@EmployeeID", MySqlDbType.Int16),
        New MySqlParameter("@CompanyID", MySqlDbType.Int16),
        New MySqlParameter("@UserName", MySqlDbType.String, 50),
        New MySqlParameter("@UserPwd", MySqlDbType.String, 24),
        New MySqlParameter("@UserActualName", MySqlDbType.String, 50),
        New MySqlParameter("@UserTypeId", MySqlDbType.Int16),
        New MySqlParameter("@LanguageOption", MySqlDbType.String, 10),
        New MySqlParameter("@Status", MySqlDbType.Byte),
        New MySqlParameter("@RoleID", MySqlDbType.Int16),
        New MySqlParameter("@PasswordChanged", MySqlDbType.Byte),
        New MySqlParameter("@LastPasswordChangeDate", MySqlDbType.DateTime),
        New MySqlParameter("@UserKey", MySqlDbType.String, 50),
        New MySqlParameter("@LoginCount", MySqlDbType.Byte),
        New MySqlParameter("@LastLoginDate", MySqlDbType.DateTime),
        New MySqlParameter("@LastLoginIP", MySqlDbType.String, 50),
        New MySqlParameter("@LastLoginAttempt", MySqlDbType.DateTime),
        New MySqlParameter("@LastLoginAttemptIP", MySqlDbType.String, 50),
        New MySqlParameter("@AppVersion", MySqlDbType.String, 50)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = CustomerID
        sp_params(2).Value = EmployeeID
        sp_params(3).Value = CompanyID
        sp_params(4).Value = UserName
        sp_params(5).Value = UserPwd
        sp_params(6).Value = IsNullValue(UserActualName, DBNull.Value)
        sp_params(7).Value = UserTypeId
        sp_params(8).Value = IsNullValue(LanguageOption, DBNull.Value)
        sp_params(9).Value = Status
        sp_params(10).Value = RoleID
        sp_params(11).Value = PasswordChanged
        sp_params(12).Value = dbDate(LastPasswordChangeDate)
        sp_params(13).Value = IsNullValue(UserKey, DBNull.Value)
        sp_params(14).Value = IsNullValue(LoginCount, 0)
        sp_params(15).Value = dbDate(LastLoginDate)
        sp_params(16).Value = IsNullValue(LastLoginIP, DBNull.Value)
        sp_params(17).Value = dbDate(LastLoginAttempt)
        sp_params(18).Value = IsNullValue(LastLoginAttemptIP, DBNull.Value)
        sp_params(19).Value = IsNullValue(AppVersion, DBNull.Value)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input
        sp_params(10).Direction = ParameterDirection.Input
        sp_params(11).Direction = ParameterDirection.Input
        sp_params(12).Direction = ParameterDirection.Input
        sp_params(13).Direction = ParameterDirection.Input
        sp_params(14).Direction = ParameterDirection.Input
        sp_params(15).Direction = ParameterDirection.Input
        sp_params(16).Direction = ParameterDirection.Input
        sp_params(17).Direction = ParameterDirection.Input
        sp_params(18).Direction = ParameterDirection.Input
        sp_params(19).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function LoginUser(ByVal User As String, ByVal Pass As String) As Integer

        LoginUser = LoginStatus.InValidUser
        Dim f As Integer = 0
        If User <> "" And Pass <> "" Then
            Dim strUserName As String = User
            strUserName = strUserName.Replace("'", "''").Replace("--", "")
            Pass = Pass.Replace("'", "").ToString().Replace("--", "")
            Dim strEncPass As String = encryptPassword(strUserName, Pass)

            'APPType 0 for cpl and 1 for reporting
            'SourceType 1 DealerLogin, 2 reporting,  3 cpl, 4 PinUpload, 5 FTI
            Dim I As Integer
            Dim dt As DataTable
            Dim sqlParm As MySqlParameterCollection

            Dim sp_params As MySqlParameter() = {
                New MySqlParameter("@UserName", MySqlDbType.String, 50),
                New MySqlParameter("@UserPwd", MySqlDbType.String, 24),
                New MySqlParameter("@ENPassword", MySqlDbType.String, 50),
                New MySqlParameter("@IPAddress", MySqlDbType.String, 50),
                New MySqlParameter("@AppType", MySqlDbType.Byte),
                New MySqlParameter("@UserID", MySqlDbType.Int16),
                New MySqlParameter("@EmployeeID", MySqlDbType.Int16),
                New MySqlParameter("@CompanyID", MySqlDbType.Int16),
                New MySqlParameter("@MasterCustomerID", MySqlDbType.Int16),
                New MySqlParameter("@UserActualName", MySqlDbType.String, 50),
                New MySqlParameter("@UserTypeId", MySqlDbType.Int16),
                New MySqlParameter("@LanguageOption", MySqlDbType.VarChar, 10),
                New MySqlParameter("@Status", MySqlDbType.Byte),
                New MySqlParameter("@PasswordChanged", MySqlDbType.Byte),
                New MySqlParameter("@LastPasswordChangeDate", MySqlDbType.String),
                New MySqlParameter("@UserKey", MySqlDbType.String, 50),
                New MySqlParameter("@LoginCount", MySqlDbType.Byte),
                New MySqlParameter("@LastLoginDate", MySqlDbType.String),
                New MySqlParameter("@LastLoginIP", MySqlDbType.String, 50),
                New MySqlParameter("@LastLoginAttempt", MySqlDbType.String),
                New MySqlParameter("@LastLoginAttemptIP", MySqlDbType.String, 50),
                New MySqlParameter("@RoleID", MySqlDbType.Int16),
                New MySqlParameter("@Privilege", MySqlDbType.String, -1),
                New MySqlParameter("@ApplicationType", MySqlDbType.Int16),
                New MySqlParameter("@SucessFlag", MySqlDbType.Int16),
                New MySqlParameter("@ProfileID", MySqlDbType.Int16)}

            sp_params(0).Value = Trim(strUserName)
            sp_params(1).Value = Trim(Pass)
            sp_params(2).Value = strEncPass
            sp_params(3).Value = Web.HttpContext.Current.Request.UserHostAddress.ToString
            sp_params(4).Value = Val(GetAppSetings("AppID"))
            sp_params(5).Value = 0
            sp_params(6).Value = 0
            sp_params(7).Value = 0
            sp_params(8).Value = 0
            sp_params(9).Value = "aaa"
            sp_params(10).Value = 0
            sp_params(11).Value = "aaa"
            sp_params(12).Value = 0
            sp_params(13).Value = 0
            sp_params(14).Value = "aaa"
            sp_params(15).Value = "aaa"
            sp_params(16).Value = 0
            sp_params(17).Value = "aaa"
            sp_params(18).Value = "aaa"
            sp_params(19).Value = "aaa"
            sp_params(20).Value = "aaa"
            sp_params(21).Value = 0
            sp_params(22).Value = "aaa"
            sp_params(23).Value = 0
            sp_params(24).Value = 0
            sp_params(25).Value = 0

            sp_params(0).Direction = ParameterDirection.Input
            sp_params(1).Direction = ParameterDirection.Input
            sp_params(2).Direction = ParameterDirection.Input
            sp_params(3).Direction = ParameterDirection.Input
            sp_params(4).Direction = ParameterDirection.Input
            sp_params(5).Direction = ParameterDirection.Output
            sp_params(6).Direction = ParameterDirection.Output
            sp_params(7).Direction = ParameterDirection.Output
            sp_params(8).Direction = ParameterDirection.Output
            sp_params(9).Direction = ParameterDirection.Output
            sp_params(10).Direction = ParameterDirection.Output
            sp_params(11).Direction = ParameterDirection.Output
            sp_params(12).Direction = ParameterDirection.Output
            sp_params(13).Direction = ParameterDirection.Output
            sp_params(14).Direction = ParameterDirection.Output
            sp_params(15).Direction = ParameterDirection.Output
            sp_params(16).Direction = ParameterDirection.Output
            sp_params(17).Direction = ParameterDirection.Output
            sp_params(18).Direction = ParameterDirection.Output
            sp_params(19).Direction = ParameterDirection.Output
            sp_params(20).Direction = ParameterDirection.Output
            sp_params(21).Direction = ParameterDirection.Output
            sp_params(22).Direction = ParameterDirection.Output
            sp_params(23).Direction = ParameterDirection.Output
            sp_params(24).Direction = ParameterDirection.Output
            sp_params(25).Direction = ParameterDirection.Output

            dt = RunProcedure("PROCLogin", sp_params, SqlReturnTypes.DatatTable)
            If dt(0)("SucessFlag") = 1 Then
                HttpContext.Current.Session.Add("MasterCustomerID", IsNullValue(dt(0)("MasterCustomerID"), 0))
                HttpContext.Current.Session.Add("EmployeeID", IsNullValue(dt(0)("EmployeeID"), 0))
                HttpContext.Current.Session.Add("CompanyID", IsNullValue(dt(0)("CompanyID"), ""))
                HttpContext.Current.Session.Add("Privilege", IsNullValue(dt(0)("Privilege"), ""))
                HttpContext.Current.Session.Add("UserID", IsNullValue(dt(0)("UserID"), 0))
                HttpContext.Current.Session.Add("LastLoginDate", IsNullValue(dt(0)("LastLoginDate"), Now))
                HttpContext.Current.Session.Add("PasswordChanged", IsNullValue(dt(0)("PasswordChanged"), ""))
                HttpContext.Current.Session.Add("UserActualName", IsNullValue(dt(0)("UserActualName"), ""))
                HttpContext.Current.Session.Add("LastLoginIP", IsNullValue(dt(0)("LastLoginIP"), ""))
                HttpContext.Current.Session.Add("LastLoginAttempt", IsNullValue(dt(0)("LastLoginAttempt"), Now))
                HttpContext.Current.Session.Add("LastLoginAttemptIP", IsNullValue(dt(0)("LastLoginAttemptIP"), ""))
                HttpContext.Current.Session.Add("LastPasswordChangeDate", IsNullValue(dt(0)("LastPasswordChangeDate"), Now))
                HttpContext.Current.Session.Add("UserTypeId", IsNullValue(dt(0)("UserTypeId"), 0))
                HttpContext.Current.Session.Add("LanguageOption", IsNullValue(dt(0)("LanguageOption"), ""))
                HttpContext.Current.Session.Add("LoginTime", Now)
                HttpContext.Current.Session.Add("RoleID", IsNullValue(dt(0)("RoleID"), 0))

                If HttpContext.Current.Session("UICulture") Is Nothing Then
                    HttpContext.Current.Session.Add("UICulture", IsNullValue(dt(0)("LanguageOption"), ""))
                Else
                    HttpContext.Current.Session("UICulture") = IsNullValue(dt(0)("LanguageOption"), "")
                End If

                LoginUser = LoginStatus.ValidUser
            ElseIf dt(0)("SucessFlag") = 2 Then
                LoginUser = LoginStatus.FailAttemptExec
            ElseIf dt(0)("SucessFlag") = 0 Then
                LoginUser = LoginStatus.InValidUser
            End If
        End If
    End Function

    Public Function GetUserLoginDetails() As MySqlDataReader

        Return RunSQLCommand(" SELECT  UserLogins.ID, UserType.Name AS UserTypeName, Dealer.Name AS DealerName," &
       " ApplicationType.Name AS ApplicationTypeName,UserRole.Name AS UserRoleName,Employee.Name AS EmployeeName," &
       " UserLogins.UserName,UserLogins.UserKey,UserLogins.LoginCount,UserLogins.LastLoginDate," &
       " UserLogins.Status,UserLogins.RoleID,UserLogins.UserActualName" &
       " FROM  UserLogins INNER JOIN " &
        " UserType ON UserLogins.UserTypeId = UserType.ID INNER JOIN " &
        " UserRole ON UserLogins.RoleID = UserRole.ID INNER JOIN " &
        "applicationtypeON UserRole.ApplicationType = ApplicationType.ID INNER JOIN " &
        " Employee ON UserLogins.EmployeeID = Employee.ID LEFT OUTER JOIN " &
        " Dealer ON UserLogins.DealerID = Dealer.ID ", SqlReturnTypes.DataReader)


    End Function

    Public Function GetUserLoginDetails(ByVal UserTypeId As Integer, ByVal DealerID As Integer) As MySqlDataReader
        Dim Qstr As String
        Qstr = "SELECT  UserLogins.ID, UserType.Name AS UserTypeName, Dealer.Name AS DealerName," &
        " ApplicationType.Name AS ApplicationTypeName,UserRole.Name AS UserRoleName,Employee.Name AS EmployeeName," &
        " UserLogins.UserName,UserLogins.UserKey,UserLogins.LoginCount,UserLogins.LastLoginDate," &
        " UserLogins.Status, UserLogins.RoleID,UserLogins.UserActualName" &
        " FROM  UserLogins INNER JOIN " &
        " UserType ON UserLogins.UserTypeId = UserType.ID INNER JOIN " &
        " UserRole ON UserLogins.RoleID = UserRole.ID INNER JOIN " &
        "applicationtypeON UserRole.ApplicationType = ApplicationType.ID INNER JOIN " &
        " Employee ON UserLogins.EmployeeID = Employee.ID LEFT OUTER JOIN " &
        " Dealer ON UserLogins.DealerID = Dealer.ID " &
        " Where 1=1 "
        If UserTypeId <> 0 Then
            Qstr += " And UserLogins.UserTypeId=" & UserTypeId
        End If
        If DealerID <> 0 Then
            Qstr += " And UserLogins.DealerID=" & DealerID
        End If
        Qstr += "  order by UserLogins.UserName"

        Return RunSQLCommand(Qstr, SqlReturnTypes.DataReader)

    End Function
    Public Function CheckExist(ByVal UserName As String, ByVal UID As Integer) As Integer
        Dim userId As Integer = 0
        Dim dr As MySqlDataReader
        If UID = 0 Then
            dr = RunSQLCommand("Select ID from UserLogins Where UserName ='" & UserName & "'", SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand("Select ID from UserLogins where UserName ='" & UserName & "'" &
                                 " Except " &
                                 " select ID from UserLogins where UserName = '" & UserName & "' and ID =" & UID, SqlReturnTypes.DataReader)
        End If
        If dr Is Nothing Then
            userId = 0
        Else
            If dr.Read() Then
                userId = dr("ID")
            End If
            dr.Close()
        End If

        Return userId
    End Function
    Public Function GetUserLoginDetails(ByVal ID As Integer) As MySqlDataReader
        Return RunSQLCommand("select * from ClientUsers where ID =" & ID, SqlReturnTypes.DataReader)
    End Function

    Public Function GetAllUser() As MySqlDataReader
        Return RunSQLCommand("select * from UserLogins where UserTypeId <> 1 and Status = 1", SqlReturnTypes.DataReader)
    End Function

    Public Function GetLoginUsersName(ByVal DealerID As String) As MySqlDataReader
        If Val(DealerID) <= 1 Then
            Return RunSQLCommand("select * from UserLogins where RoleID=2 and DealerID=0", SqlReturnTypes.DataReader)
        ElseIf Val(DealerID) > 1 Then
            '   Return RunSQLCommand("select * from UserLogins where (UserTypeID=2 or UserTypeID=5) and DealerID=" & DealerID) 'UserTypeID=5 and
            Return RunSQLCommand("select * from Branch where DealerID=" & DealerID, SqlReturnTypes.DataReader)
        Else
            Return RunSQLCommand("select EmployeeID,UserActualName from UserLogins where UserTypeID=2 and DealerID=1", SqlReturnTypes.DataReader)

        End If

    End Function

    Public Function ChangeMyPassword(ByVal OldPassword As String, ByVal NewPassord As String) As Boolean

        Dim result As Integer = -1
        Dim RowsEffected As Integer = 0
        Dim sqlParm As MySqlParameterCollection

        Dim sp_params As MySqlParameter() = {
       New MySqlParameter("@UserName", MySqlDbType.String, 50),
       New MySqlParameter("@OldPassword", MySqlDbType.String, 24),
       New MySqlParameter("@NewPassword", MySqlDbType.String, 24)}


        If Not HttpContext.Current.Session("UserName") Is Nothing Then
            sp_params(0).Value = HttpContext.Current.Session("UserName").ToString
        Else
            HttpContext.Current.Response.Redirect("Login.aspx")
        End If

        sp_params(1).Value = OldPassword
        sp_params(2).Value = NewPassord


        result = RunProcedure("ChangePassword", sp_params, RowsEffected)

        If RowsEffected > 0 Then
            HttpContext.Current.Session.Abandon()
            HttpContext.Current.Response.Redirect("~/Login.aspx")

        End If

        If result = 0 Then

            Return True

        Else
            Return False

        End If




    End Function


    Public Function GetLoginUsersNameByProfile(ByVal UserRoleID As Integer, Optional ByVal ProfileID As Integer = 0) As MySqlDataReader

        Dim str As String = ""
        str += " select * from UserLogins where RoleID=" & UserRoleID
        If ProfileID > 0 Then
            str += " and ProfileID=" & ProfileID
        End If

        Return RunSQLCommand(str, SqlReturnTypes.DataReader)

    End Function

    Public Function GetUserforEdit(ByVal uid As Integer) As MySqlDataReader
        Return RunSQLCommand("select * from UserLogins where ID=" & uid, SqlReturnTypes.DataReader)
    End Function

    Public Function GetAllUserDetails(Optional where As String = "") As DataTable
        Dim str = ""
        str += " select row_number() over(order by U.ID ) as SrNo,U.ID,U.UserName,U.[Status],U.UserActualName,UR.Name as [Role],"
        str += " IIF(U.LanguageOption='en','English','Arabic') as LanguageOption,"
        str += " UT.Name as UserType,ISNULL(CC.Name,'---') as Company,ISNULL(EM.Name,'---') as EmployeeName "
        str += " from UserLogins U inner join"
        str += " UserRoles UR on U.RoleID=UR.ID inner join"
        str += " UserType UT on U.UserTypeId=UT.ID left join"
        str += " CountryWiseCompanyMST CC on U.CompanyID=CC.ID left join"
        str += " EmployeeMST EM on U.EmployeeID=EM.ID"
        str += " where 1=1 " & where
        str += " order by U.ID"

        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

    Public Function getAllUsers() As MySqlDataReader
        Return RunSQLCommand("select ID,UserName from userlogins ORDER by ID", SqlReturnTypes.DataReader)
    End Function

End Class
