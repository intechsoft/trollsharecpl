﻿'------------------------------------------------------------
'created for table [item]  on 21-Oct-2017
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class item
    Inherits UneeshHelper
    Public ID As Integer
    Public SubCategoryID As Integer
    Public Code As String
    Public Name As String
    Public NameAlt As String
    Public PhoneNumber As String
    Public Description As String
    Public DescriptionAlt As String
    Public ImageURL As String
    Public ImageURLAlt As String
    Public ViewOrder As Byte
    Public Status As Byte
    Public DisplayStatus As Byte
    Public ServiceTypeID As Integer
    Public AddedBy As Integer
    Public AddedDate As DateTime
    Public AddeddIpAddress As String
    Public UpdatedBy As Integer
    Public UpdatedDate As DateTime
    Public UpdatedIpAddress As String
    Public EmailID As String

    Public Sub New(Optional ByVal itemID As Integer = 0)
        MyBase.New(itemID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertitem"
        IUDProcedures.UpdateProcedure = "Updateitem"
        IUDProcedures.DeleteProcedure = "Deleteitem"
    End Sub
    Protected Overrides Sub getData()
        If ObjId = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from item where ID=" & ObjId, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            SubCategoryID = IsNullValue(dr("SubCategoryID"), 0)
            Code = IsNullValue(dr("Code"), "")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            PhoneNumber = IsNullValue(dr("PhoneNumber"), "")
            Description = IsNullValue(dr("Description"), "")
            DescriptionAlt = IsNullValue(dr("DescriptionAlt"), "")
            ImageURL = IsNullValue(dr("ImageURL"), "")
            ImageURLAlt = IsNullValue(dr("ImageURLAlt"), "")
            ViewOrder = IsNullValue(dr("ViewOrder"), 0)
            Status = IsNullValue(dr("Status"), 0)
            DisplayStatus = IsNullValue(dr("DisplayStatus"), 0)
            ServiceTypeID = IsNullValue(dr("ServiceTypeID"), 0)
            AddedBy = IsNullValue(dr("AddedBy"), 0)
            AddedDate = IsNullValue(dr("AddedDate"), New Date(0))
            AddeddIpAddress = IsNullValue(dr("AddeddIpAddress"), "")
            UpdatedBy = IsNullValue(dr("UpdatedBy"), 0)
            UpdatedDate = IsNullValue(dr("UpdatedDate"), New Date(0))
            UpdatedIpAddress = IsNullValue(dr("UpdatedIpAddress"), "")
            EmailID = IsNullValue(dr("EmailID"), "")
        Else
            ObjId = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = {
        New MySqlParameter("@ID", MySqlDbType.Int16),
        New MySqlParameter("@SubCategoryID", MySqlDbType.Int16),
        New MySqlParameter("@Code", MySqlDbType.String, 50),
        New MySqlParameter("@Name", MySqlDbType.String, 250),
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50),
        New MySqlParameter("@PhoneNumber", MySqlDbType.String, 50),
        New MySqlParameter("@Description", MySqlDbType.String, 50),
        New MySqlParameter("@DescriptionAlt", MySqlDbType.String, 250),
        New MySqlParameter("@ImageURL", MySqlDbType.String, 250),
        New MySqlParameter("@ImageURLAlt", MySqlDbType.String, 250),
        New MySqlParameter("@ViewOrder", MySqlDbType.Byte),
        New MySqlParameter("@Status", MySqlDbType.Byte),
        New MySqlParameter("@DisplayStatus", MySqlDbType.Byte),
        New MySqlParameter("@ServiceTypeID", MySqlDbType.Int16),
        New MySqlParameter("@AddedBy", MySqlDbType.Int16),
        New MySqlParameter("@AddedDate", MySqlDbType.DateTime),
        New MySqlParameter("@AddeddIpAddress", MySqlDbType.String, 50),
        New MySqlParameter("@UpdatedBy", MySqlDbType.Int16),
        New MySqlParameter("@UpdatedDate", MySqlDbType.DateTime),
        New MySqlParameter("@UpdatedIpAddress", MySqlDbType.String, 50),
        New MySqlParameter("@EmailID", MySqlDbType.String, 500)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(SubCategoryID, 0)
        sp_params(2).Value = IsNullValue(Code, DBNull.Value)
        sp_params(3).Value = IsNullValue(Name, DBNull.Value)
        sp_params(4).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(5).Value = IsNullValue(PhoneNumber, DBNull.Value)
        sp_params(6).Value = IsNullValue(Description, DBNull.Value)
        sp_params(7).Value = IsNullValue(DescriptionAlt, DBNull.Value)
        sp_params(8).Value = IsNullValue(ImageURL, DBNull.Value)
        sp_params(9).Value = IsNullValue(ImageURLAlt, DBNull.Value)
        sp_params(10).Value = IsNullValue(ViewOrder, 0)
        sp_params(11).Value = IsNullValue(Status, 0)
        sp_params(12).Value = IsNullValue(DisplayStatus, 0)
        sp_params(13).Value = IsNullValue(ServiceTypeID, 0)
        sp_params(14).Value = IsNullValue(AddedBy, 0)
        sp_params(15).Value = dbDate(AddedDate)
        sp_params(16).Value = IsNullValue(AddeddIpAddress, DBNull.Value)
        sp_params(17).Value = IsNullValue(UpdatedBy, 0)
        sp_params(18).Value = dbDate(UpdatedDate)
        sp_params(19).Value = IsNullValue(UpdatedIpAddress, DBNull.Value)
        sp_params(20).Value = IsNullValue(EmailID, DBNull.Value)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input
        sp_params(9).Direction = ParameterDirection.Input
        sp_params(10).Direction = ParameterDirection.Input
        sp_params(11).Direction = ParameterDirection.Input
        sp_params(12).Direction = ParameterDirection.Input
        sp_params(13).Direction = ParameterDirection.Input
        sp_params(14).Direction = ParameterDirection.Input
        sp_params(15).Direction = ParameterDirection.Input
        sp_params(16).Direction = ParameterDirection.Input
        sp_params(17).Direction = ParameterDirection.Input
        sp_params(18).Direction = ParameterDirection.Input
        sp_params(19).Direction = ParameterDirection.Input
        sp_params(20).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function getitemlist() As DataTable
        Dim sql As String = ""
        sql += " Select Code,Name,Status from item where status=1 order by Name; "
        Return RunSQLCommand(sql, SqlReturnTypes.DatatTable)
    End Function

    Public Function getItem() As MySqlDataReader
        Dim sql As String = ""
        sql += " Select * from item where status=1 order by Name; "
        Return RunSQLCommand(sql, SqlReturnTypes.DataReader)
    End Function

    Public Function GetServiceTypeList() As MySqlDataReader
        Return RunSQLCommand("select * from servicetype where status=1 Order by Name", SqlReturnTypes.DataReader)
    End Function

    Public Function getitemlist(whereClause As String) As DataTable
        Dim sql As String = ""
        sql += " select i.* from item i"
        sql += " inner join subcategory s on i.SubCategoryID=s.ID"
        sql += " inner join categorymst c on s.CategoryID=c.ID"
        sql += " inner join servicetype st on i.ServiceTypeID=st.ID"
        sql += " WHERE 1=1 " & whereClause & " order by i.ID desc;"
        Return RunSQLCommand(sql, SqlReturnTypes.DatatTable)
    End Function

    Public Function GetAllItemList() As MySqlDataReader
        Return RunSQLCommand("select * from Item Order by Name", SqlReturnTypes.DataReader)
    End Function

    Public Function GetItemListByCategoryID(CategoryID As Integer) As MySqlDataReader
        Dim sql As String = ""
        sql += " select i.Name,i.ID from Item i"
        sql += " inner join subcategory s on i.SubCategoryID=s.ID"
        sql += " inner join categorymst c on s.CategoryID=c.ID "
        sql += " where s.CategoryID=" & CategoryID & " order by i.name;"
        Return RunSQLCommand(sql, SqlReturnTypes.DataReader)
    End Function

    Public Function GetItemListBySubCategory(SubCategoryID As Integer) As MySqlDataReader
        Return RunSQLCommand("select ID,Name from item where SubCategoryID=" & SubCategoryID, SqlReturnTypes.DataReader)
    End Function

    Public Function GetItemListByServiceType(ServiceTypeID As Integer) As MySqlDataReader
        Return RunSQLCommand("select ID,Name from item where ServiceTypeID=" & ServiceTypeID, SqlReturnTypes.DataReader)
    End Function

    Public Function getDashboardDetails(AuthHeader As MobileAuthHeader) As DataSet
        Dim str As String = ""
        str += " Select C.ID as CategoryID,C.Code  as CategoryCode,C.Name as CategoryName,C.ImageURL as CategoryImage,C.ViewOrder as CategoryViewOrder,C.Status as CategoryStatus,C.DisplayStatus as CategoryDisplayStatus,"
        str += " S.ID as SubCategoryID,S.Code  as SubCategoryCode,S.Name as SubCategoryName,S.ImageURL as SubCategoryImage,S.ViewOrder as SubCategoryViewOrder,S.Status as SubCategoryStatus,S.DisplayStatus as SubCategoryDisplayStatus,"
        str += " I.ID as ItemID,I.Code as ItemCode,I.Name as ItemName,I.ImageURL as ItemImage,I.ViewOrder as ItemViewOrder,I.Status as ItemStatus,I.DisplayStatus as ItemDisplayStatus"
        str += " from categorymst C "
        str += " inner join subcategory S on C.ID=S.CategoryID "
        str += " inner join item I on S.ID=I.SubCategoryID "
        str += " where C.Status=1 and S.Status=1 and I.Status=1"
        str += " order by CategoryViewOrder,SubCategoryViewOrder,ItemViewOrder"
        Return RunSQLCommand(str, SqlReturnTypes.DataSet)
    End Function

End Class
