﻿
'------------------------------------------------------------
'created for table [itemdetails]  on 29-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class itemdetails
    Inherits UneeshHelper
    Public ID As Integer
    Public ItemID As Integer
    Public Description As String
    Public DescriptionAlt As String
    Public Status As Byte
    Public DisplayStatus As Byte

    Public Sub New(Optional ByVal itemdetailsID As Integer = 0)
        MyBase.New(itemdetailsID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertitemdetails"
        IUDProcedures.UpdateProcedure = "Updateitemdetails"
        IUDProcedures.DeleteProcedure = "Deleteitemdetails"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from itemdetails where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            ItemID = IsNullValue(dr("ItemID"), 0)
            Description = IsNullValue(dr("Description"), "")
            DescriptionAlt = IsNullValue(dr("DescriptionAlt"), "")
            Status = IsNullValue(dr("Status"), 0)
            DisplayStatus = IsNullValue(dr("DisplayStatus"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@ItemID", MySqlDbType.Int16), _
        New MySqlParameter("@Description", MySqlDbType.String, 500), _
        New MySqlParameter("@DescriptionAlt", MySqlDbType.String, 500), _
        New MySqlParameter("@Status", MySqlDbType.byte), _
        New MySqlParameter("@DisplayStatus", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(ItemID, 0)
        sp_params(2).Value = IsNullValue(Description, DBNull.Value)
        sp_params(3).Value = IsNullValue(DescriptionAlt, DBNull.Value)
        sp_params(4).Value = IsNullValue(Status, 0)
        sp_params(5).Value = IsNullValue(DisplayStatus, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function getitemlistdetails() As DataTable
        Dim sql As String = ""
        sql += " Select it.ID,i.Name,it.Description,it.status from itemdetails it"
        sql += " inner join item i on it.ItemID=i.ID order by it.ID"
        Return RunSQLCommand(sql, SqlReturnTypes.DatatTable)
    End Function

    Public Function getitemlistdetails(whereClause As String) As DataTable
        Dim sql As String = ""
        sql += " Select it.ID,i.Name,it.Description,it.status from itemdetails it"
        sql += " inner join item i on it.ItemID=i.ID"
        sql += " inner join subcategory s on i.SubCategoryID=s.ID"
        sql += " inner join categorymst c on s.CategoryID=c.ID"
        sql += " inner join servicetype st on i.ServiceTypeID=st.ID"
        sql += " WHERE 1=1 " & whereClause & ";"
        Return RunSQLCommand(sql, SqlReturnTypes.DatatTable)
    End Function

End Class
