﻿
'------------------------------------------------------------
'created for table [bannerowner]  on 29-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class bannerowner
    Inherits UneeshHelper
    Public ID As Integer
    Public Name As String
    Public NameAlt As String
    Public Status As Byte

    Public Sub New(Optional ByVal bannerownerID As Integer = 0)
        MyBase.New(bannerownerID, "dbconnect")
    End Sub

    Public Function GetAllBannerOwner() As MySqlDataReader
        Return RunSQLCommand("select ID,Name from bannerowner order by Status", SqlReturnTypes.DataReader)
    End Function

    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertbannerowner"
        IUDProcedures.UpdateProcedure = "Updatebannerowner"
        IUDProcedures.DeleteProcedure = "Deletebannerowner"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from bannerowner where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Status = IsNullValue(dr("Status"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 50), _
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50), _
        New MySqlParameter("@Status", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Name, DBNull.Value)
        sp_params(2).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(3).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function getBannerDetails(BannerOwnerID As Integer) As MySqlDataReader
        Dim sql As String = ""
        sql += " select bd.ID,c.Name from bannerdetails bd"
        sql += " inner join customermst c on bd.CustomerID=c.ID order by bd.ID"
        Return RunSQLCommand(sql, SqlReturnTypes.DataReader)
    End Function
End Class
