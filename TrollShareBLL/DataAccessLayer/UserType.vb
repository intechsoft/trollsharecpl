﻿'------------------------------------------------------------
'created for table [usertype]  on 15-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class usertype
    Inherits UneeshHelper
    Public ID As Integer
    Public Name As String
    Public NameAlt As String
    Public Status As Byte

    Public Sub New(Optional ByVal usertypeID As Integer = 0)
        MyBase.New(usertypeID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertusertype"
        IUDProcedures.UpdateProcedure = "Updateusertype"
        IUDProcedures.DeleteProcedure = "Deleteusertype"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from usertype where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            Name = IsNullValue(dr("Name"), "")
            NameAlt = IsNullValue(dr("NameAlt"), "")
            Status = IsNullValue(dr("Status"), 0)
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@Name", MySqlDbType.String, 50), _
        New MySqlParameter("@NameAlt", MySqlDbType.String, 50), _
        New MySqlParameter("@Status", MySqlDbType.byte)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(Name, DBNull.Value)
        sp_params(2).Value = IsNullValue(NameAlt, DBNull.Value)
        sp_params(3).Value = IsNullValue(Status, 0)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input

        Return sp_params
    End Function

    Public Function CheckExist(ByVal Name As String, ByVal TID As Integer) As Integer
        Dim id As Integer = 0
        Dim dr As MySqlDataReader
        If TID = 0 Then
            dr = RunSQLCommand("Select ID from UserType Where Name ='" & Name & "'", SqlReturnTypes.DataReader)
        Else
            dr = RunSQLCommand(" select ID from UserType where Name ='" & Name & "'" & _
                                 " Except " & _
                                 " select ID from UserType where Name = '" & Name & "' and ID =" & TID, SqlReturnTypes.DataReader)
        End If
        If Not dr Is Nothing Then
            If dr.Read() Then
                id = dr("ID")
            End If
            dr.Close()
        End If
        Return id
    End Function

    Public Function GetUserTypeforEdit(typeid As Integer) As MySqlDataReader
        Dim str As String = ""
        str += "select * FROM UserType where ID = " & typeid
        Return RunSQLCommand(str, SqlReturnTypes.DataReader)
    End Function

    Public Function GetUserType() As DataTable
        Dim str As String = ""
        'str += "select row_number() over(order by ID ) as SrNo,ID,Name,NameAlt,Status FROM UserType order by ID"
        'Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

        str += "SELECT UT.*"
        str += " from usertype UT order by UT.ID"
        Return RunSQLCommand(str, SqlReturnTypes.DatatTable)

    End Function

End Class
