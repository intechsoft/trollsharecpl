﻿
'------------------------------------------------------------
'created for table [cplactivitieslog]  on 15-Nov-2015
'------------------------------------------------------------
Imports MySql.Data.MySqlClient
Imports UneeshLibrary
Public Class cplactivitieslog
    Inherits UneeshHelper
    Public ID As Integer
    Public AppTypeID As Integer
    Public WebPageName As String
    Public ActionMode As Byte
    Public BeforeUpdate As String
    Public AfterUpdate As String
    Public UserID As Integer
    Public IPAddress As String
    Public ActionTime As DateTime

    Public Sub New(Optional ByVal cplactivitieslogID As Integer = 0)
        MyBase.New(cplactivitieslogID, "dbconnect")
    End Sub
    Protected Overrides Sub Init()
        IUDProcedures.InsertProcedure = "Insertcplactivitieslog"
        IUDProcedures.UpdateProcedure = "Updatecplactivitieslog"
        IUDProcedures.DeleteProcedure = "Deletecplactivitieslog"
    End Sub
    Protected Overrides Sub getData()
        If objID = 0 Then Exit Sub
        Dim dr As MySqlDataReader = RunSQLCommand("Select * from cplactivitieslog where ID=" & objID, SqlReturnTypes.DataReader)
        If dr.Read() Then
            ID = dr("ID")
            AppTypeID = IsNullValue(dr("AppTypeID"), 0)
            WebPageName = IsNullValue(dr("WebPageName"), "")
            ActionMode = IsNullValue(dr("ActionMode"), 0)
            BeforeUpdate = IsNullValue(dr("BeforeUpdate"), "")
            AfterUpdate = IsNullValue(dr("AfterUpdate"), "")
            UserID = IsNullValue(dr("UserID"), 0)
            IPAddress = IsNullValue(dr("IPAddress"), "")
            ActionTime = IsNullValue(dr("ActionTime"), New Date(0))
        Else
            objID = 0
        End If
        dr.Close()
    End Sub
    Protected Overrides Function CreateCommandParameters() As IDataParameter()
        Dim sp_params As MySqlParameter() = { _
        New MySqlParameter("@ID", MySqlDbType.Int16), _
        New MySqlParameter("@AppTypeID", MySqlDbType.Int16), _
        New MySqlParameter("@WebPageName", MySqlDbType.String, 50), _
        New MySqlParameter("@ActionMode", MySqlDbType.byte), _
        New MySqlParameter("@BeforeUpdate", MySqlDbType.String, 500), _
        New MySqlParameter("@AfterUpdate", MySqlDbType.String, 500), _
        New MySqlParameter("@UserID", MySqlDbType.Int16), _
        New MySqlParameter("@IPAddress", MySqlDbType.String, 50), _
        New MySqlParameter("@ActionTime", MySqlDbType.DateTime)}
        sp_params(0).Value = ObjId
        sp_params(1).Value = IsNullValue(AppTypeID, 0)
        sp_params(2).Value = IsNullValue(WebPageName, DBNull.Value)
        sp_params(3).Value = IsNullValue(ActionMode, 0)
        sp_params(4).Value = IsNullValue(BeforeUpdate, DBNull.Value)
        sp_params(5).Value = IsNullValue(AfterUpdate, DBNull.Value)
        sp_params(6).Value = IsNullValue(UserID, 0)
        sp_params(7).Value = IsNullValue(IPAddress, DBNull.Value)
        sp_params(8).Value = dbDate(ActionTime)

        sp_params(0).Direction = ParameterDirection.InputOutput
        sp_params(1).Direction = ParameterDirection.Input
        sp_params(2).Direction = ParameterDirection.Input
        sp_params(3).Direction = ParameterDirection.Input
        sp_params(4).Direction = ParameterDirection.Input
        sp_params(5).Direction = ParameterDirection.Input
        sp_params(6).Direction = ParameterDirection.Input
        sp_params(7).Direction = ParameterDirection.Input
        sp_params(8).Direction = ParameterDirection.Input

        Return sp_params
    End Function

End Class
