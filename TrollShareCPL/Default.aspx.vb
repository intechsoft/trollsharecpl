﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class _Default
    Inherits BaseUIPage

#Region "Variables"

    Private dt As DataTable
    Private dr As MySqlDataReader
    Private objCustomer As CustomerMST

#End Region

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Application("SitePath") Is Nothing Then Application("SitePath") = CurrentPagePath()
    End Sub
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsSessionValid() Then
            Response.Redirect(LoginSitePath())
        Else
            LblUserActualName.Text = IsNullValue(HttpContext.Current.Session("UserActualName"), "")
            LblLastLoginDate.Text = IsNullValue(HttpContext.Current.Session("LastLoginDate"), "")
            LblLastLoginIP.Text = IsNullValue(HttpContext.Current.Session("LastLoginIP"), "")
            LblLastLoginAttempt.Text = IsNullValue(HttpContext.Current.Session("LastLoginAttempt"), "")
            LblLastLoginAttemptIP.Text = IsNullValue(HttpContext.Current.Session("LastLoginAttemptIP"), "")
            LblLastPasswordChangeDate.Text = IsNullValue(HttpContext.Current.Session("LastPasswordChangeDate"), "")
            'LblUserActualName.Text = HttpContext.Current.Session("UserActualName")
        End If
    End Sub

#Region "Procedures"
     
#End Region
 
End Class