﻿Imports TrollShareBLL
Public Class DashBoard
    Inherits BaseUIPage

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Application("SitePath") Is Nothing Then Application("SitePath") = CurrentPagePath()
    End Sub
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsSessionValid() Then
            Response.Redirect(LoginSitePath())
        End If

    End Sub

End Class