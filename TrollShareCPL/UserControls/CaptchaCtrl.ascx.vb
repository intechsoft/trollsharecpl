﻿Imports System.Drawing
Imports System.IO
Imports System.Drawing.Text
Imports System.Drawing.Imaging

Public Class CaptchaCtrl
    Inherits System.Web.UI.UserControl



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Session("LoginCount") Is Nothing Then
            If Val(Session("LoginCount")) >= 2 Then 
                CreateImage() 
            End If
        End If


    End Sub




    Public Function GeneratePassword() As String
        ' Below code describes how to create random numbers.some of the digits and letters
        ' are ommited because they look same like "i","o","1","0","I","O".
        Dim allowedChars As String = "a,b,c,d,e,f,g,h,j,k,m,n,p,q,r,s,t,u,v,w,x,y,z,"
        allowedChars += "A,B,C,D,E,F,G,H,J,K,L,M,N,P,Q,R,S,T,U,V,W,X,Y,Z,"
        allowedChars += "2,3,4,5,6,7,8,9"

        Dim sep As Char() = {","c}
        Dim arr As String() = allowedChars.Split(sep)

        Dim passwordString As String = ""
        Dim temp As String
        Dim rand As New Random()
        For i As Integer = 0 To 5
            temp = arr(rand.[Next](0, arr.Length))
            passwordString += temp
        Next
        Return passwordString
    End Function



    Public ReadOnly Property lblError() As Label
        Get
            Return lblErrorMsg
        End Get
    End Property

    Public ReadOnly Property txtSecurity() As TextBox
        Get
            Return txtSecurityCode
        End Get
    End Property

    Public Sub CreateImage(Optional ByVal randomStr As String = "")
        Dim objBMP As New Bitmap(100, 35)
        Dim objGraphics As Graphics = Graphics.FromImage(objBMP)
        objGraphics.Clear(Color.LightBlue)
        objGraphics.TextRenderingHint = TextRenderingHint.AntiAlias
        Dim objFont As New Font("Gabriola", 20, FontStyle.Italic)

        If objFont Is Nothing Then
            objFont = New Font("Arial", 18, FontStyle.Italic)
        End If

        If randomStr = "" Then
            randomStr = GeneratePassword()
        End If

        If Session("CaptchaText") Is Nothing Then
            Session.Add("CaptchaText", randomStr)
        Else
            Session("CaptchaText") = randomStr
        End If

        objGraphics.DrawString(randomStr, objFont, Brushes.Black, 2, 2)
        ' Me.ContentType = "image/GIF"
        'objBMP.Save(Response.OutputStream, ImageFormat.Gif)

        Dim ms As New MemoryStream()
        objBMP.Save(ms, ImageFormat.Gif)
        Dim base64Data = Convert.ToBase64String(ms.ToArray())
        Image1.ImageUrl = "data:image/gif;base64," + base64Data

        objFont.Dispose()
        objGraphics.Dispose()
        objBMP.Dispose()
    End Sub


End Class