﻿function confirmDelete() {
    if (confirm("Are you sure you want to delete this?") == true)
        return true;
    else
        return false;
}

function confirmUpdate() {
    if (confirm("Are you sure you want to update this record?") == true)
        return true;
    else
        return false;
}

function inputMask(ctlValue, sFormat) {
    var nKey;
    nKey = event.keyCode;

    var returnCode = true;

    if (sFormat == 'N') {
        if (nKey > 47 && nKey < 58) // numbers
        {
            returnCode = true;
        }
        else if (nKey == 46 && ctlValue.indexOf('.') == -1) {
            returnCode = true;
        }
        else {
            returnCode = false;
            event.keyCode = 0;
        }
    }

    if (sFormat == 'U') {
        if (nKey >= 97 && nKey <= 122) // upper
        {
            event.keyCode = nKey - 32;
            returnCode = true;
        }
        else
            return;
    }
    if (sFormat == 'L') {
        if (nKey >= 65 && nKey <= 90) // lower
        {
            event.keyCode = nKey + 32;
            returnCode = true;
        }
        else
            return;
    }

    event.returnValue = returnCode;

}


      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
     }
