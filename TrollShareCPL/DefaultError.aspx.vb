﻿Public Class DefaultError
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim QryString As String = ""
        If IsPostBack = False Then
            QryString = Request.QueryString("Err")
            Select Case QryString
                Case "Page Not Available"
                    ImgErr.ImageUrl = "images/PageNotFound.jpg"
                Case ("Access Denied")
                    ImgErr.ImageUrl = "images/AccessDenied.jpg"
                Case "UnExpected Error"
                    ImgErr.ImageUrl = "images/oops.jpg"
                Case Else
                    ImgErr.ImageUrl = "images/oops.jpg"
            End Select
        End If
    End Sub

End Class