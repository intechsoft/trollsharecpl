﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddArea
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private objArea As area

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillForm()
            'FillDropDowns()
        End If
    End Sub
    'Private Sub cmbAppType_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAppType.DataBound
    '    cmbAppType.Items.Insert(0, New ListItem("-------Select a Application Type -------", 0))
    'End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim CategoryId As Integer
        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then ' Checking adding privilage of current user
                objArea = New area
                With objArea
                    .Code = txtCode.Text.Trim
                    .Name = txtName.Text.Trim
                    .NameAlt = txtNameAlt.Text.Trim
                    '.AppTypeID = Val(cmbAppType.SelectedValue)
                    .Status = Val(cmbStatus.SelectedValue())
                    CategoryId = .CheckExist(.Name, .Code, 0)

                    If CategoryId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                        Exit Sub
                    Else
                        Dim BeforeText As String = ""
                        Dim AfterText As String = "Code =" & txtCode.Text
                        AfterText += "|Name =" & txtName.Text
                        AfterText += "|NameAlt =" & txtNameAlt.Text
                        'AfterText += "|AppTypeID =" & Val(cmbAppType.SelectedValue())
                        AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                        AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            .insertMember()
                        Else
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = "Add category failed"
                            Exit Sub
                        End If

                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Service Type added successfully"
                            Response.Redirect("ManageArea.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Service Type added successfully"
                        Response.Redirect("Manage.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else

            If EditPrivilage Then

                objArea = New area(Val(hdnAreaID.Value))

                Dim BeforeText As String = ""

                With objArea
                    BeforeText += "Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    'BeforeText += "|AppTypeID=" & .AppTypeID
                    BeforeText += "|Status=" & .Status
                    .Code = txtCode.Text.Trim
                    .Name = txtName.Text.Trim
                    .NameAlt = txtNameAlt.Text.Trim
                    '.AppTypeID = Val(cmbAppType.SelectedValue)
                    .Status = Val(cmbStatus.SelectedValue)
                    CategoryId = .CheckExist(.Name, .Code, Val(hdnAreaID.Value))
                    If CategoryId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else

                        Dim AfterText As String = "Code" & txtCode.Text
                        AfterText += "|Name =" & txtName.Text
                        AfterText += "|NameAlt =" & txtName.Text
                        'AfterText += "|AppType =" & Val(cmbAppType.SelectedValue())
                        AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                        AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())
                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            .update()
                        Else
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = "Update area failed"
                            Exit Sub
                        End If

                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Area Updated successfully"
                            Response.Redirect("ManageArea.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Area Updated successfully"
                        Response.Redirect("ManageArea.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated
            End If
        End If
    End Sub

    'Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

    '    If Val(hdnCategoryId.Value) = 0 Then
    '        ClearForm()
    '        lblMsg.Text = ""
    '    Else
    '        Response.Redirect("Managecategorymst.aspx")
    '    End If
    'End Sub

#End Region

#Region "Functions"

    Private Sub FillForm()
        Dim queryStr As String = ""
        Dim AreaID As Integer
        objArea = New area
        Dim qsEnc As New AbhiQsEnc.AbhiQs

        queryStr = Request.QueryString("Qs")
        AreaID = Val(qsEnc.ReadQS(queryStr, "id"))   'decrypt CategoryId from the query 
        hdnAreaID.Value = AreaID                  'Keep CategoryId in a hidden field for later use

        dr = objArea.GetAreaforEdit(AreaID)
        If Not dr Is Nothing Then
            If dr.Read() Then
                txtName.Text = IsNullValue(dr("Name").ToString, "")
                txtNameAlt.Text = IsNullValue(dr("NameAlt").ToString, "")
                'cmbAppType.SelectedValue = dr("AppTypeID")
                cmbStatus.SelectedValue() = dr("Status")
                'cmbDisplayStatus.SelectedValue() = dr("DisplayStatus").ToString
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Service Type"
            End If
            dr.Close()
        End If

    End Sub

    Private Sub ClearForm()
        txtName.Text = ""
        txtNameAlt.Text = ""
        'cmbAppType.SelectedValue = 0
        cmbStatus.SelectedIndex = 0
        'cmbDisplayStatus.SelectedIndex() = 0
    End Sub
    'Private Sub FillDropDowns()
    '    Dim objApp As New applicationtype
    '    dr = objApp.GetApplicationTypeList()
    '    cmbAppType.DataSource = dr
    '    cmbAppType.DataValueField = "ID"
    '    cmbAppType.DataTextField = "Name"
    '    cmbAppType.DataBind()
    '    dr.Close()
    'End Sub

#End Region

End Class