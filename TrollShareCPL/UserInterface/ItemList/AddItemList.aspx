﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="AddItemList.aspx.vb" Inherits="TrollShareCPL.AddItemList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdnId" runat="server" Value="0" />
    <div class="row">
        <div class="col-lg-4" style="width: 500px; margin-left: 150px; margin-right: auto; margin-top: 10px; margin-bottom: auto; top: 0px; left: 3px;">
            <div class="panel panel-primary">
                 <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
                    <asp:Label ID="lblHeader" runat="server" Text="Add New Product"></asp:Label>
                    <a href="ManageItemList.aspx">&nbsp;>>Manage Product</a>

                </div>
                <div class="panel-heading" style="height: 50px">
                    <h4 style="text-align: center"><strong>
                        <asp:Label ID="lblHeadText" runat="server" Text="Add New Product"></asp:Label>
                    </strong></h4>
                </div>
                <div id="additemlist">
                    <table style="width: 100%; margin: 30px auto; width: 400px; line-height: 40px;">
                        <tr>
                            <td>Servce Type
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbServiceType" runat="server" class="form-control" Style="width: auto; min-width: 250px; height: 30px" AutoPostBack="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>Category
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbCategory" runat="server" class="form-control" Style="width: auto; min-width: 250px; height: 30px" AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>Sub Category</td>
                            <td>
                                <asp:DropDownList ID="cmbSubCategory" runat="server" class="form-control" Style="width: auto; min-width: 250px; height: 30px" AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>Code

                            </td>
                            <td>

                                <asp:TextBox ID="txtCode" runat="server" placeholder="Enter Code" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Name </td>
                            <td>

                                <asp:TextBox ID="txtName" runat="server" placeholder="Enter Name" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Name Alt</td>
                            <td>

                                <asp:TextBox ID="txtNameAlt" runat="server" placeholder="Karaoke" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Contact Number </td>
                            <td>

                                <asp:TextBox ID="txtPhoneNumber" runat="server" placeholder="Enter Contact Number" class="form-control" Style="width: 250px;" MaxLength="50" required="required"
                                    Text="1234567890"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Description </td>
                            <td>

                                <asp:TextBox ID="txtDescription" TextMode="MultiLine" Rows="3" runat="server" placeholder="Enter Description" class="form-control" Style="width: 250px;"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Description Alt</td>
                            <td>

                                <asp:TextBox ID="txtDescriptionAlt" TextMode="MultiLine" Rows="3" runat="server" placeholder="Mp3 URL Link" class="form-control" Style="width: 250px;"
                                    ></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>View Order </td>
                            <td>

                                <asp:TextBox ID="txtViewOrder" runat="server" placeholder="Enter View Order" class="form-control" Style="width: 250px;" MaxLength="50" required="required"
                                    Text="1"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Image URL </td>
                            <td>

                                <asp:FileUpload ID="FileUpload1" runat="server" class="form-control" Style="width: 250px;"  />

                            </td>
                        </tr>
                        <tr>
                            <td>Image URL Alt</td>
                            <td>

                                <asp:TextBox ID="txtImageURLAlt" runat="server" placeholder="Enter Youtube link" class="form-control" Style="width: 250px;" MaxLength="50" required="required"
                                    ></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>

                                <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" class="form-control" Style="width: 250px;" MaxLength="50" required="required"
                                    Text="teamintechsoft@gmail.com"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>DisplayStatus </td>
                            <td>

                                <asp:DropDownList ID="cmbDisplayStatus" runat="server" class="form-control" Style="width: auto; min-width: 250px; height: 30px" required="required">
                                    <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                    <asp:ListItem Text="On" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Off" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                            </td>
                        </tr>
                        <tr>
                            <td>Status </td>
                            <td>

                                <asp:DropDownList ID="cmbStatus" runat="server" class="form-control" Style="width: auto; min-width: 250px; height: 30px" required="required">
                                    <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Active" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Suspended" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Stopped" Value="3"></asp:ListItem>
                                </asp:DropDownList>

                            </td>
                        </tr>
                        <tr style="height: 20px">
                            <td colspan="2">

                                <asp:Label ID="lblMsg" runat="server" Font-Size="Medium" Text="" CssClass="warning"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div style="text-align: center; margin: 10px auto; height: 50px; width: 30%;">

                        <div style="float: left; width: 100%; margin: 0px auto;">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-outline btn-success" />
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
