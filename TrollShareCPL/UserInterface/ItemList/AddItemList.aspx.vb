﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL
Public Class AddItemList
    Inherits BaseUIPage

#Region "Variables"
    Private dr As MySqlDataReader
#End Region

    Private UPLOADFOLDER As String = "../FileStore/Products"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillForm()
        End If
    End Sub

    Private Sub cmbCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCategory.DataBound
        cmbCategory.Items.Insert(0, New ListItem("-------Select A Category -------", 0))
    End Sub

    Private Sub cmbSubCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubCategory.DataBound
        cmbSubCategory.Items.Insert(0, New ListItem("----Select A Sub Category----", 0))
    End Sub

    Private Sub cmbServiceType_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbServiceType.DataBound
        cmbServiceType.Items.Insert(0, New ListItem("----Select A Service Type----", 0))
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        If btnAdd.Text.Trim.ToUpper = "ADD" Then
            If AddPrivilage Then

                Dim objitemlist As New item
                With objitemlist
                    .SubCategoryID = cmbSubCategory.SelectedValue
                    .ServiceTypeID = cmbServiceType.SelectedValue
                    .Code = txtCode.Text
                    .Name = txtName.Text
                    .NameAlt = txtNameAlt.Text
                    .PhoneNumber = txtPhoneNumber.Text
                    .Description = txtDescription.Text
                    .DescriptionAlt = txtDescriptionAlt.Text
                    '.ImageURL = txtImageURL.Text
                    .ImageURLAlt = txtImageURLAlt.Text
                    .ViewOrder = txtViewOrder.Text
                    .Status = cmbStatus.SelectedValue
                    .DisplayStatus = cmbDisplayStatus.SelectedValue
                    .EmailID = txtEmail.Text
                    .AddedBy = Val(Session("UserID").ToString)
                    .AddedDate = Now
                    .AddeddIpAddress = Request.UserHostAddress.ToString
                    .UpdatedBy = Val(Session("UserID").ToString)
                    .UpdatedDate = Now
                    .UpdatedIpAddress = Request.UserHostAddress.ToString
                End With

                Dim BeforeText As String = ""
                Dim AfterText As String = "Name =" & txtName.Text
                AfterText += "|NameAlt =" & txtNameAlt.Text
                AfterText += "|Code = " & txtCode.Text
                AfterText += "|Description =" & txtDescription.Text
                AfterText += "|DescriptionAlt =" & txtDescriptionAlt.Text
                'AfterText += "|ImageURL =" & txtImageURL.Text
                AfterText += "|ImageURLAlt = " & txtImageURLAlt.Text
                AfterText += "|ViewOrder =" & txtViewOrder.Text
                AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                    With objitemlist
                        If FileUpload1.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = FileUpload1.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(Me.FileUpload1.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            'Path.GetFileName(Me.FileUpload1.PostedFile.FileName)
                            upldfile.FileType = UploadFileTypes.Product
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (FileUpload1.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .ImageURL = upldfile.insertMember()
                            upldfile = New uploadedfiles(.ImageURL)
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .ImageURL, ext)
                            upldfile.update()
                            If Val(IsNullValue(.ImageURL.Trim, "0")) > 0 Then
                                If UploadFile(.ImageURL, ext) Then
                                    .ImageURL = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add item failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add item failed"
                                Exit Sub
                            End If
                        End If
                        .insert()
                        If Not .ErrorMsg Is Nothing Then
                            If .ErrorMsg.Trim.Length > 0 Then
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = .ErrorMsg
                            Else
                                lblMsg.ForeColor = Drawing.Color.Green
                                lblMsg.Text = "Added successfully"
                                Response.Redirect("ManageItemList.aspx")
                            End If
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Added successfully"
                            Response.Redirect("ManageItemList.aspx")
                        End If
                    End With
                Else
                    lblMsg.ForeColor = Drawing.Color.Red
                    lblMsg.Text = "Add item failed"
                    Exit Sub
                End If
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then

                Dim BeforeText As String = ""
                Dim objitemlist As New item(Val(hdnId.Value))
                With objitemlist
                    BeforeText += "Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Code=" & .Code
                    BeforeText += "|Description=" & .Description
                    BeforeText += "|DescriptionAlt=" & .DescriptionAlt
                    BeforeText += "|ImageURL=" & .ImageURL
                    BeforeText += "|ImageURLAlt=" & .ImageURLAlt
                    BeforeText += "|ViewOrder=" & .ViewOrder
                    BeforeText += "|Status=" & .Status
                    BeforeText += "|DisplayStatus=" & .DisplayStatus

                    .Name = txtName.Text.Trim
                    .NameAlt = txtNameAlt.Text.Trim
                    .Code = txtCode.Text.Trim
                    .Description = txtDescription.Text.Trim
                    .DescriptionAlt = txtDescriptionAlt.Text.Trim
                    '.ImageURL = txtImageURL.Text.Trim
                    .ImageURLAlt = txtImageURLAlt.Text.Trim
                    .ViewOrder = txtViewOrder.Text.Trim
                    .Status = Val(cmbStatus.SelectedValue)
                    .DisplayStatus = 1 'Val(cmbDisplayStatus.SelectedValue())
                    .EmailID = txtEmail.Text
                    .UpdatedBy = Val(Session("UserID").ToString)
                    .UpdatedDate = Now
                    .UpdatedIpAddress = Request.UserHostAddress.ToString
                    'CategoryId = .CheckExist(.Name, .Code, Val(hdnCategoryId.Value))
                    'If CategoryId <> 0 Then
                    '    .ErrorMsg = .Name & " or " & .Code & " Already Exist"
                    'Else

                    Dim AfterText As String = "Name" & txtName.Text
                    AfterText += "|NameAlt =" & txtNameAlt.Text
                    AfterText += "|Code = " & txtCode.Text
                    AfterText += "|Description =" & txtDescription.Text
                    AfterText += "|DescriptionAlt =" & txtDescriptionAlt.Text
                    'AfterText += "|ImageURL =" & txtImageURL.Text
                    AfterText += "|ImageURLAlt = " & txtImageURLAlt.Text
                    AfterText += "|ViewOrder =" & txtViewOrder.Text
                    AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                    AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                    If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                        If FileUpload1.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = FileUpload1.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(Me.FileUpload1.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            upldfile.FileType = UploadFileTypes.Product
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (FileUpload1.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .ImageURL = upldfile.insertMember()
                            upldfile = New uploadedfiles(.ImageURL)
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .ImageURL, ext)
                            upldfile.update()
                            If Val(IsNullValue(.ImageURL.Trim, "0")) > 0 Then
                                If UploadFile(.ImageURL, ext) Then
                                    .ImageURL = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add item failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add item failed"
                                Exit Sub
                            End If
                        End If
                        .update()
                    Else
                        lblMsg.ForeColor = Drawing.Color.Red
                        lblMsg.Text = "Update item failed"
                        Exit Sub
                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Update successfully"
                            Response.Redirect("ManageItemList.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Update successfully"
                        Response.Redirect("ManageItemList.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        End If
    End Sub

#Region "Functions"

    Private Sub FillDropDowns()
        Dim objCategory As New categorymst
        Dim dr As MySqlDataReader = objCategory.GetAllCategoryName()
        cmbCategory.DataSource = dr
        cmbCategory.DataValueField = "ID"
        cmbCategory.DataTextField = "Name"
        cmbCategory.DataBind()
        dr.Close()
        Dim objSubcategory As New subcategory
        dr = objSubcategory.GetSubCategories()
        cmbSubCategory.DataSource = dr
        cmbSubCategory.DataValueField = "ID"
        cmbSubCategory.DataTextField = "Name"
        cmbSubCategory.DataBind()
        dr.Close()
        Dim objServiceType As New servicetype
        dr = objServiceType.GetServiceTypeList()
        cmbServiceType.DataSource = dr
        cmbServiceType.DataValueField = "ID"
        cmbServiceType.DataTextField = "Name"
        cmbServiceType.DataBind()
        dr.Close()
    End Sub

    Private Sub FillForm()

        Dim queryStr As String = ""
        Dim itemid As Integer
        Dim qsEnc As New AbhiQsEnc.AbhiQs

        queryStr = Request.QueryString("Qs")   ' read query string 
        If Not queryStr Is Nothing Then
            If queryStr.Trim.Length > 0 Then
                itemid = Val(qsEnc.ReadQS(queryStr, "id"))
                hdnId.Value = itemid
                Dim objitemlist As New item(itemid)
                Dim objsubcategory As New subcategory(objitemlist.SubCategoryID)
                With objitemlist
                    cmbCategory.SelectedValue = objsubcategory.CategoryID
                    cmbSubCategory.SelectedValue = .SubCategoryID
                    cmbServiceType.SelectedValue = .ServiceTypeID
                    txtCode.Text = .Code
                    txtName.Text = .Name
                    txtNameAlt.Text = .NameAlt
                    txtPhoneNumber.Text = .PhoneNumber
                    txtDescription.Text = .Description
                    txtDescriptionAlt.Text = .DescriptionAlt
                    txtImageURLAlt.Text = .ImageURLAlt
                    txtViewOrder.Text = .ViewOrder
                    cmbStatus.SelectedValue = .Status
                    cmbDisplayStatus.SelectedValue = .DisplayStatus
                    cmbStatus.SelectedValue = .Status
                    txtEmail.Text = .EmailID
                    If hdnId.Value > 0 Then
                        btnAdd.Text = "UPDATE"
                        lblHeadText.Text = "Update Item"
                    Else
                        btnAdd.Text = "ADD"
                    End If
                End With
            End If
        End If
    End Sub

    Public Function UploadFile(ByVal SaveFileName As String, ext As String()) As Boolean
        If FileUpload1.HasFile Then
            Try
                FileUpload1.SaveAs(getServerFilePath(UPLOADFOLDER, SaveFileName, ext))
                Return True
            Catch ex As Exception
                lblMsg.Text = "Error in File Upload ..."
            End Try
        Else

        End If
        Return False
    End Function

#End Region

    Private Sub cmbCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCategory.SelectedIndexChanged

        Dim objsubcategory As New subcategory
        dr = objsubcategory.GetSubCategoryName(cmbCategory.SelectedValue)
        cmbSubCategory.DataSource = dr
        cmbSubCategory.DataValueField = "ID"
        cmbSubCategory.DataTextField = "Name"
        cmbSubCategory.DataBind()
        dr.Close()

    End Sub

End Class