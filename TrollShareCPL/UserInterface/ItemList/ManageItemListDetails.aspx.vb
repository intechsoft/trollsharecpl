﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageItemListDetails
     Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objServicetype As servicetype
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            FillDropDowns()
            FillGridView()
        End If
    End Sub

    Private Sub cmbCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCategory.DataBound
        cmbCategory.Items.Insert(0, New ListItem("-------Select a Category -------", 0))
    End Sub

    Private Sub cmbServicetype_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbServicetype.DataBound
        cmbServicetype.Items.Insert(0, New ListItem("-------Select a Service Type -------", 0))
    End Sub

    Private Sub cmbSubcategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubcategory.DataBound
        cmbSubcategory.Items.Insert(0, New ListItem("-------Select a Sub Category -------", 0))
    End Sub

    Private Sub cmbItem_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbItem.DataBound
        cmbItem.Items.Insert(0, New ListItem("-------Select a Iem -------", 0))
    End Sub

    Private Sub grditemlistdetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grditemlistdetails.PageIndexChanging
        grditemlistdetails.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub

    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub

    Private Sub grditemlistdetails_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grditemlistdetails.RowDataBound
        If grditemlistdetails.Columns.Count > 0 Then
            If DeletePrivilage Then
                grditemlistdetails.Columns(grditemlistdetails.Columns.Count - 1).Visible = True
            Else
                grditemlistdetails.Columns(grditemlistdetails.Columns.Count - 1).Visible = False
            End If
        End If
    End Sub

    Private Sub grditemlistdetails_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grditemlistdetails.RowCommand
        Dim BeforeText As String = ""
        Dim AfterText As String = ""
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Enc As New AbhiQsEnc.AbhiQs
                Dim Encrypted As String = Enc.WriteQS("id=" & id)
                Response.Redirect("AddItemListDetails.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                Dim objitemlist As New item(id)
                objitemlist.delete()
                With objitemlist
                    BeforeText += " Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
                FillGridView()
            End If
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        Dim whereClause As String = ""
        If cmbCategory.SelectedValue > 0 Then
            whereClause += " and c.ID=" & cmbCategory.SelectedValue
        End If

        If cmbSubcategory.SelectedValue > 0 Then
            whereClause += " and s.ID=" & cmbSubcategory.SelectedValue
        End If

        If cmbServicetype.SelectedValue > 0 Then
            whereClause += " and st.ID=" & cmbServicetype.SelectedValue
        End If

        If cmbItem.SelectedValue > 0 Then
            whereClause += " and it.ID=" & cmbItem.SelectedValue
        End If

        Dim dt As DataTable
        Dim objitemlistdetails As New itemdetails
        dt = objitemlistdetails.getitemlistdetails(whereClause)
        If chkPaging.Checked = True Then
            grditemlistdetails.AllowPaging = True
        End If
        grditemlistdetails.DataSource = dt
        grditemlistdetails.DataBind()

    End Sub

    Private Sub cmbCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCategory.SelectedIndexChanged
        If cmbCategory.SelectedValue > 0 Then

            Dim objsubcategory As New subcategory
            dr = objsubcategory.GetSubCategoryName(cmbCategory.SelectedValue)
            cmbSubcategory.DataSource = dr
            cmbSubcategory.DataValueField = "ID"
            cmbSubcategory.DataTextField = "Name"
            cmbSubcategory.DataBind()
            dr.Close()

            Dim objItemDetails As New item
            dr = objItemDetails.GetItemListByCategoryID(cmbCategory.SelectedValue)
            cmbItem.DataSource = dr
            cmbItem.DataValueField = "ID"
            cmbItem.DataTextField = "Name"
            cmbItem.DataBind()
            dr.Close()
        Else
            Dim objItemDetails As New item
            dr = objItemDetails.GetAllItemList()
            cmbItem.DataSource = dr
            cmbItem.DataValueField = "ID"
            cmbItem.DataTextField = "Name"
            cmbItem.DataBind()
            dr.Close()

            Dim objsubcategory As New subcategory
            dr = objsubcategory.GetSubCategoryName()
            cmbSubcategory.DataSource = dr
            cmbSubcategory.DataValueField = "ID"
            cmbSubcategory.DataTextField = "Name"
            cmbSubcategory.DataBind()
            dr.Close()
        End If

    End Sub

    Private Sub cmbSubcategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSubcategory.SelectedIndexChanged

        If cmbSubcategory.SelectedValue > 0 Then

            Dim objItemDetails As New item
            dr = objItemDetails.GetItemListBySubCategory(cmbSubcategory.SelectedValue)
            cmbItem.DataSource = dr
            cmbItem.DataValueField = "ID"
            cmbItem.DataTextField = "Name"
            cmbItem.DataBind()
            dr.Close()
        Else
            If cmbCategory.SelectedValue > 0 Then
                Dim objItemDetails As New item
                dr = objItemDetails.GetItemListByCategoryID(cmbCategory.SelectedValue)
                cmbItem.DataSource = dr
                cmbItem.DataValueField = "ID"
                cmbItem.DataTextField = "Name"
                cmbItem.DataBind()
                dr.Close()
            Else
                Dim objItemDetails As New item
                dr = objItemDetails.GetAllItemList()
                cmbItem.DataSource = dr
                cmbItem.DataValueField = "ID"
                cmbItem.DataTextField = "Name"
                cmbItem.DataBind()
                dr.Close()
            End If

        End If

    End Sub

    Private Sub cmbServicetype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbServicetype.SelectedIndexChanged

        If cmbServicetype.SelectedValue > 0 Then

            Dim objItemDetails As New item
            dr = objItemDetails.GetItemListByServiceType(cmbServicetype.SelectedValue)
            cmbItem.DataSource = dr
            cmbItem.DataValueField = "ID"
            cmbItem.DataTextField = "Name"
            cmbItem.DataBind()
            dr.Close()
        Else
            Dim objItemDetails As New item
            dr = objItemDetails.GetAllItemList()
            cmbItem.DataSource = dr
            cmbItem.DataValueField = "ID"
            cmbItem.DataTextField = "Name"
            cmbItem.DataBind()
            dr.Close()
        End If

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()
        Dim dt As DataTable
        Dim objitemlistdetails As New itemdetails
        dt = objitemlistdetails.getitemlistdetails()
        If chkPaging.Checked = True Then
            grditemlistdetails.AllowPaging = True
        End If
        grditemlistdetails.DataSource = dt
        grditemlistdetails.DataBind()
    End Sub

    Private Sub FillDropDowns()
        Dim objCategorymst As New categorymst
        dr = objCategorymst.GetAllCategoryName()
        cmbCategory.DataSource = dr
        cmbCategory.DataValueField = "ID"
        cmbCategory.DataTextField = "Name"
        cmbCategory.DataBind()
        dr.Close()


        Dim objservicetype As New servicetype
        dr = objservicetype.GetServiceTypeList()
        cmbServicetype.DataSource = dr
        cmbServicetype.DataValueField = "ID"
        cmbServicetype.DataTextField = "Name"
        cmbServicetype.DataBind()
        dr.Close()

        Dim objItemDetails As New item
        dr = objItemDetails.GetAllItemList()
        cmbItem.DataSource = dr
        cmbItem.DataValueField = "ID"
        cmbItem.DataTextField = "Name"
        cmbItem.DataBind()
        dr.Close()

        Dim objsubcategory As New subcategory
        dr = objsubcategory.GetSubCategoryName()
        cmbSubcategory.DataSource = dr
        cmbSubcategory.DataValueField = "ID"
        cmbSubcategory.DataTextField = "Name"
        cmbSubcategory.DataBind()
        dr.Close()
    End Sub

#End Region

End Class