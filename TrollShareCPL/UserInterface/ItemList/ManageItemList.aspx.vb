﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageItemList
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objServicetype As servicetype
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            FillDropDowns()
            FillGridView()
        End If
    End Sub

    Private Sub cmbCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCategory.DataBound
        cmbCategory.Items.Insert(0, New ListItem("-------Select a Category -------", 0))
    End Sub

    Private Sub cmbServicetype_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbServicetype.DataBound
        cmbServicetype.Items.Insert(0, New ListItem("-------Select a Service Type -------", 0))
    End Sub

    Private Sub cmbSubcategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubcategory.DataBound
        cmbSubcategory.Items.Insert(0, New ListItem("-------Select a Sub Category -------", 0))
    End Sub

    Private Sub grditemlist_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grditemlist.PageIndexChanging
        grditemlist.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub

    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub

    Private Sub grditemlist_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grditemlist.RowDataBound
        If grditemlist.Columns.Count > 0 Then
            If DeletePrivilage Then
                grditemlist.Columns(grditemlist.Columns.Count - 1).Visible = True
            Else
                grditemlist.Columns(grditemlist.Columns.Count - 1).Visible = False
            End If
        End If
    End Sub

    Private Sub grditemlist_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grditemlist.RowCommand
        Dim BeforeText As String = ""
        Dim AfterText As String = ""
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Enc As New AbhiQsEnc.AbhiQs
                Dim Encrypted As String = Enc.WriteQS("id=" & id)
                Response.Redirect("AddItemList.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                Dim objitemlist As New item(id)
                objitemlist.delete()
                With objitemlist
                    BeforeText += " Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
                FillGridView()
            End If
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        Dim whereClause As String = ""
        If cmbCategory.SelectedValue > 0 Then
            whereClause += " and c.ID=" & cmbCategory.SelectedValue
        End If

        If cmbSubcategory.SelectedValue > 0 Then
            whereClause += " and s.ID=" & cmbSubcategory.SelectedValue
        End If

        If cmbServicetype.SelectedValue > 0 Then
            whereClause += " and st.ID=" & cmbServicetype.SelectedValue
        End If

        Dim dt As DataTable
        Dim objitemlist As New item
        dt = objitemlist.getitemlist(whereClause)
        If chkPaging.Checked = True Then
            grditemlist.AllowPaging = True
        Else
            grditemlist.AllowPaging = False
        End If
        grditemlist.DataSource = dt
        grditemlist.DataBind()

    End Sub

    Private Sub cmbCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCategory.SelectedIndexChanged

        Dim objsubcategory As New subcategory
        dr = objsubcategory.GetSubCategoryName(cmbCategory.SelectedValue)
        cmbSubcategory.DataSource = dr
        cmbSubcategory.DataValueField = "ID"
        cmbSubcategory.DataTextField = "Name"
        cmbSubcategory.DataBind()
        dr.Close()

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()
        Dim dt As DataTable
        Dim objitemlist As New item
        dt = objitemlist.getitemlist("")
        If chkPaging.Checked = True Then
            grditemlist.AllowPaging = True
        Else
            grditemlist.AllowPaging = False
        End If
        grditemlist.DataSource = dt
        grditemlist.DataBind()
    End Sub

    Private Sub FillDropDowns()
        Dim objCategorymst As New categorymst
        dr = objCategorymst.GetAllCategoryName()
        cmbCategory.DataSource = dr
        cmbCategory.DataValueField = "ID"
        cmbCategory.DataTextField = "Name"
        cmbCategory.DataBind()
        dr.Close()


        Dim objservicetype As New servicetype
        dr = objservicetype.GetServiceTypeList()
        cmbServicetype.DataSource = dr
        cmbServicetype.DataValueField = "ID"
        cmbServicetype.DataTextField = "Name"
        cmbServicetype.DataBind()
        dr.Close()

        Dim objsubcategory As New subcategory
        dr = objsubcategory.GetSubCategoryName()
        cmbSubcategory.DataSource = dr
        cmbSubcategory.DataValueField = "ID"
        cmbSubcategory.DataTextField = "Name"
        cmbSubcategory.DataBind()
        dr.Close()
    End Sub

#End Region

End Class