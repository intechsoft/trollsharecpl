﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL
Public Class AddSongsToPaattuPusthakam
    Inherits BaseUIPage

#Region "Variables"
    Private dr As MySqlDataReader
#End Region

    Private UPLOADFOLDER As String = "../FileStore/Products"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillForm()
        End If
    End Sub

    Private Sub cmbSubCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubCategory.DataBound
        cmbSubCategory.Items.Insert(0, New ListItem("----Select A Sub Category----", 0))
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        If btnAdd.Text.Trim.ToUpper = "ADD" Then
            If AddPrivilage Then

                Dim objitemlist As New item
                With objitemlist
                    .SubCategoryID = cmbSubCategory.SelectedValue
                    .ServiceTypeID = ServiceTypeList.Paattu_Pusthakam
                    '.Code = txtCode.Text Thumbnil
                    .Name = txtName.Text
                    '.NameAlt = txtNameAlt.Text Karaoke
                    .PhoneNumber = txtYear.Text 'Year
                    .Description = txtDescription.Text
                    '.DescriptionAlt = txtDescriptionAlt.Text Mp3
                    '.ImageURL = txtImageURL.Text Lyrics
                    .ImageURLAlt = txtImageURLAlt.Text
                    .ViewOrder = txtViewOrder.Text
                    .Status = cmbStatus.SelectedValue
                    .DisplayStatus = cmbDisplayStatus.SelectedValue
                    .EmailID = txtCast.Text 'Cast
                    .AddedBy = Val(Session("UserID").ToString)
                    .AddedDate = Now
                    .AddeddIpAddress = Request.UserHostAddress.ToString
                    .UpdatedBy = Val(Session("UserID").ToString)
                    .UpdatedDate = Now
                    .UpdatedIpAddress = Request.UserHostAddress.ToString
                End With

                Dim BeforeText As String = ""
                Dim AfterText As String = "Name =" & txtName.Text
                AfterText += "|NameAlt =" & fileKaraoke.FileName
                AfterText += "|Code = " & fileThumbnil.FileName
                AfterText += "|Description =" & txtDescription.Text
                AfterText += "|DescriptionAlt =" & fileMp3.FileName
                'AfterText += "|ImageURL =" & txtImageURL.Text
                AfterText += "|ImageURLAlt = " & txtImageURLAlt.Text
                AfterText += "|ViewOrder =" & txtViewOrder.Text
                AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                    With objitemlist

                        ' Upload Lyrics file
                        If fileLyrics.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = fileLyrics.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(Me.fileLyrics.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            upldfile.FileType = UploadFileTypes.Lyrics
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (fileLyrics.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .ImageURL = upldfile.insertMember()
                            upldfile = New uploadedfiles(.ImageURL)
                            UPLOADFOLDER = UPLOADFOLDER & "/Lyrics"
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .ImageURL, ext)
                            upldfile.update()
                            If Val(IsNullValue(.ImageURL.Trim, "0")) > 0 Then
                                If UploadFile(UploadFileTypes.Lyrics, .ImageURL, ext) Then
                                    .ImageURL = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add Lyrics failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add Lyrics failed"
                                Exit Sub
                            End If
                        End If

                        ' Upload Thumbnil file
                        If fileThumbnil.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = fileThumbnil.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(fileThumbnil.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            upldfile.FileType = UploadFileTypes.Thumbnil
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (fileThumbnil.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .Code = upldfile.insertMember()
                            upldfile = New uploadedfiles(.Code)
                            UPLOADFOLDER = UPLOADFOLDER & "/Thumbnil"
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .Code, ext)
                            upldfile.update()
                            If Val(IsNullValue(.Code.Trim, "0")) > 0 Then
                                If UploadFile(UploadFileTypes.Thumbnil, .Code, ext) Then
                                    .Code = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add thumbnil failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add thumbnil failed"
                                Exit Sub
                            End If
                        End If

                        ' Upload Karaoke file
                        If fileKaraoke.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = fileKaraoke.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(fileKaraoke.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            upldfile.FileType = UploadFileTypes.Karaoke
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (fileKaraoke.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .NameAlt = upldfile.insertMember()
                            upldfile = New uploadedfiles(.NameAlt)
                            UPLOADFOLDER = UPLOADFOLDER & "/Karaoke"
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .NameAlt, ext)
                            upldfile.update()
                            If Val(IsNullValue(.NameAlt.Trim, "0")) > 0 Then
                                If UploadFile(UploadFileTypes.Karaoke, .NameAlt, ext) Then
                                    .NameAlt = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add karaoke failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add karaoke failed"
                                Exit Sub
                            End If
                        End If

                        ' Upload Mp3 file
                        If fileMp3.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = fileMp3.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(fileMp3.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            upldfile.FileType = UploadFileTypes.Mp3
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (fileMp3.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .DescriptionAlt = upldfile.insertMember()
                            upldfile = New uploadedfiles(.DescriptionAlt)
                            UPLOADFOLDER = UPLOADFOLDER & "/Mp3"
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .DescriptionAlt, ext)
                            upldfile.update()
                            If Val(IsNullValue(.DescriptionAlt.Trim, "0")) > 0 Then
                                If UploadFile(UploadFileTypes.Mp3, .DescriptionAlt, ext) Then
                                    .DescriptionAlt = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add mp3 failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add mp3 failed"
                                Exit Sub
                            End If
                        End If

                        .insert()

                        If Not .ErrorMsg Is Nothing Then
                            If .ErrorMsg.Trim.Length > 0 Then
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = .ErrorMsg
                            Else
                                lblMsg.ForeColor = Drawing.Color.Green
                                lblMsg.Text = "Added successfully"
                                Response.Redirect("ManagePaattuPusthakam.aspx")
                            End If
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Added successfully"
                            Response.Redirect("ManagePaattuPusthakam.aspx")
                        End If
                    End With
                Else
                    lblMsg.ForeColor = Drawing.Color.Red
                    lblMsg.Text = "Add item failed"
                    Exit Sub
                End If
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then

                Dim BeforeText As String = ""
                Dim objitemlist As New item(Val(hdnId.Value))
                With objitemlist
                    BeforeText += "Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Code=" & .Code
                    BeforeText += "|Description=" & .Description
                    BeforeText += "|DescriptionAlt=" & .DescriptionAlt
                    BeforeText += "|ImageURL=" & .ImageURL
                    BeforeText += "|ImageURLAlt=" & .ImageURLAlt
                    BeforeText += "|ViewOrder=" & .ViewOrder
                    BeforeText += "|Status=" & .Status
                    BeforeText += "|DisplayStatus=" & .DisplayStatus

                    .Name = txtName.Text.Trim
                    '.NameAlt = fileKaraoke.FileName
                    '.Code = fileThumbnil.FileName
                    .Description = txtDescription.Text.Trim
                    '.DescriptionAlt = fileMp3.FileName
                    .ImageURLAlt = txtImageURLAlt.Text.Trim
                    .ViewOrder = txtViewOrder.Text.Trim
                    .Status = Val(cmbStatus.SelectedValue)
                    .DisplayStatus = Val(cmbDisplayStatus.SelectedValue())
                    .EmailID = txtCast.Text.Trim
                    .UpdatedBy = Val(Session("UserID").ToString)
                    .UpdatedDate = Now
                    .UpdatedIpAddress = Request.UserHostAddress.ToString
                    'CategoryId = .CheckExist(.Name, .Code, Val(hdnCategoryId.Value))
                    'If CategoryId <> 0 Then
                    '    .ErrorMsg = .Name & " or " & .Code & " Already Exist"
                    'Else

                    Dim AfterText As String = "Name" & txtName.Text
                    AfterText += "|NameAlt =" & fileKaraoke.FileName
                    AfterText += "|Code = " & fileThumbnil.FileName
                    AfterText += "|Description =" & txtDescription.Text
                    AfterText += "|DescriptionAlt =" & fileMp3.FileName
                    'AfterText += "|ImageURL =" & txtImageURL.Text
                    AfterText += "|ImageURLAlt = " & txtImageURLAlt.Text
                    AfterText += "|ViewOrder =" & txtViewOrder.Text
                    AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                    AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                    If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then

                        ' Update Lyrics
                        If fileLyrics.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = fileLyrics.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(fileLyrics.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            upldfile.FileType = UploadFileTypes.Lyrics
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (fileLyrics.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .ImageURL = upldfile.insertMember()
                            upldfile = New uploadedfiles(.ImageURL)
                            UPLOADFOLDER = UPLOADFOLDER & "/Lyrics"
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .ImageURL, ext)
                            upldfile.update()
                            If Val(IsNullValue(.ImageURL.Trim, "0")) > 0 Then
                                If UploadFile(UploadFileTypes.Lyrics, .ImageURL, ext) Then
                                    .ImageURL = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Update lyrics failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Update lyrics failed"
                                Exit Sub
                            End If
                        End If

                        ' Update Thumbnil
                        If fileThumbnil.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = fileThumbnil.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(fileThumbnil.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            upldfile.FileType = UploadFileTypes.Thumbnil
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (fileThumbnil.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .Code = upldfile.insertMember()
                            upldfile = New uploadedfiles(.Code)
                            UPLOADFOLDER = UPLOADFOLDER & "/Thumbnil"
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .Code, ext)
                            upldfile.update()
                            If Val(IsNullValue(.Code.Trim, "0")) > 0 Then
                                If UploadFile(UploadFileTypes.Thumbnil, .Code, ext) Then
                                    .Code = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Update thumbnil failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Update thumbnil failed"
                                Exit Sub
                            End If
                        End If

                        ' Update Karaoke
                        If fileKaraoke.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = fileKaraoke.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(fileKaraoke.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            upldfile.FileType = UploadFileTypes.Karaoke
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (fileKaraoke.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .NameAlt = upldfile.insertMember()
                            upldfile = New uploadedfiles(.NameAlt)
                            UPLOADFOLDER = UPLOADFOLDER & "/Karaoke"
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .NameAlt, ext)
                            upldfile.update()
                            If Val(IsNullValue(.NameAlt.Trim, "0")) > 0 Then
                                If UploadFile(UploadFileTypes.Karaoke, .NameAlt, ext) Then
                                    .NameAlt = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Update karaoke failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Update karaoke failed"
                                Exit Sub
                            End If
                        End If

                        ' Update Mp3
                        If fileMp3.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = fileMp3.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(fileMp3.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            upldfile.FileType = UploadFileTypes.Mp3
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (fileMp3.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .DescriptionAlt = upldfile.insertMember()
                            upldfile = New uploadedfiles(.DescriptionAlt)
                            UPLOADFOLDER = UPLOADFOLDER & "/Mp3"
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .DescriptionAlt, ext)
                            upldfile.update()
                            If Val(IsNullValue(.DescriptionAlt.Trim, "0")) > 0 Then
                                If UploadFile(UploadFileTypes.Mp3, .DescriptionAlt, ext) Then
                                    .DescriptionAlt = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Update mp3 failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Update mp3 failed"
                                Exit Sub
                            End If
                        End If

                        .update()

                    Else
                        lblMsg.ForeColor = Drawing.Color.Red
                        lblMsg.Text = "Update item failed"
                        Exit Sub
                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Update successfully"
                            Response.Redirect("ManagePaattuPusthakam.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Update successfully"
                        Response.Redirect("ManagePaattuPusthakam.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        End If
    End Sub

#Region "Functions"

    Private Sub FillDropDowns()
        Dim objSubcategory As New subcategory
        dr = objSubcategory.GetSubCategories()
        cmbSubCategory.DataSource = dr
        cmbSubCategory.DataValueField = "ID"
        cmbSubCategory.DataTextField = "Name"
        cmbSubCategory.DataBind()
        dr.Close()
    End Sub

    Private Sub FillForm()

        Dim queryStr As String = ""
        Dim itemid As Integer
        Dim qsEnc As New AbhiQsEnc.AbhiQs

        queryStr = Request.QueryString("Qs")   ' read query string 
        If Not queryStr Is Nothing Then
            If queryStr.Trim.Length > 0 Then
                itemid = Val(qsEnc.ReadQS(queryStr, "id"))
                hdnId.Value = itemid
                Dim objitemlist As New item(itemid)
                Dim objsubcategory As New subcategory(objitemlist.SubCategoryID)
                With objitemlist
                    cmbSubCategory.SelectedValue = .SubCategoryID
                    hlnkThumbnil.Text = "View"
                    hlnkThumbnil.NavigateUrl = .Code
                    txtName.Text = .Name
                    hlnkKaraoke.Text = "Download"
                    hlnkKaraoke.NavigateUrl = .NameAlt
                    txtYear.Text = .PhoneNumber
                    txtDescription.Text = .Description
                    hlnkMp3.Text = "Download"
                    hlnkMp3.NavigateUrl = .DescriptionAlt
                    hlnkImage.Text = "View"
                    hlnkImage.NavigateUrl = .ImageURL
                    txtImageURLAlt.Text = .ImageURLAlt
                    txtViewOrder.Text = .ViewOrder
                    cmbStatus.SelectedValue = .Status
                    cmbDisplayStatus.SelectedValue = .DisplayStatus
                    cmbStatus.SelectedValue = .Status
                    txtCast.Text = .EmailID
                    If hdnId.Value > 0 Then
                        btnAdd.Text = "UPDATE"
                        lblHeadText.Text = "Update Item"
                    Else
                        btnAdd.Text = "ADD"
                    End If
                End With
            End If
        End If
    End Sub

    Public Function UploadFile(type As UploadFileTypes, ByVal SaveFileName As String, ext As String()) As Boolean
        Select Case type
            Case UploadFileTypes.Lyrics
                If fileLyrics.HasFile Then
                    Try
                        fileLyrics.SaveAs(getServerFilePath(UPLOADFOLDER, SaveFileName, ext))
                        Return True
                    Catch ex As Exception
                        lblMsg.Text = "Error in File Upload ..."
                    End Try
                Else

                End If
            Case UploadFileTypes.Karaoke
                If fileKaraoke.HasFile Then
                    Try
                        fileKaraoke.SaveAs(getServerFilePath(UPLOADFOLDER, SaveFileName, ext))
                        Return True
                    Catch ex As Exception
                        lblMsg.Text = "Error in File Upload ..."
                    End Try
                Else

                End If
            Case UploadFileTypes.Mp3
                If fileMp3.HasFile Then
                    Try
                        fileMp3.SaveAs(getServerFilePath(UPLOADFOLDER, SaveFileName, ext))
                        Return True
                    Catch ex As Exception
                        lblMsg.Text = "Error in File Upload ..."
                    End Try
                Else

                End If

            Case UploadFileTypes.Thumbnil
                If fileThumbnil.HasFile Then
                    Try
                        fileThumbnil.SaveAs(getServerFilePath(UPLOADFOLDER, SaveFileName, ext))
                        Return True
                    Catch ex As Exception
                        lblMsg.Text = "Error in File Upload ..."
                    End Try
                Else

                End If

        End Select
        Return False
    End Function

#End Region

End Class