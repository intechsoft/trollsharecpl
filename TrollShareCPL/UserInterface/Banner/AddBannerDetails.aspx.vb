﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddBannerDetails
    Inherits BaseUIPage

    Private UPLOADFOLDER As String = "../FileStore/Products"

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objServicetype As servicetype
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region


    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillForm()
        End If
    End Sub

    Private Sub cmbBanner_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBanner.DataBound
        cmbBanner.Items.Insert(0, New ListItem("-------Select A Banner -------", 0))
    End Sub

    Private Sub cmbCustomer_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCustomer.DataBound
        cmbCustomer.Items.Insert(0, New ListItem("-------Select A Customer -------", 0))
    End Sub

    'Private Sub cmbItem_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbItem.DataBound
    '    cmbItem.Items.Insert(0, New ListItem("-------Select A Item -------", 0))
    'End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        If btnAdd.Text.Trim.ToUpper = "ADD" Then
            If AddPrivilage Then

                Dim objbannerdetails As New bannerdetails
                With objbannerdetails
                    .BannerID = cmbBanner.SelectedValue
                    .CustomerID = cmbCustomer.SelectedValue
                    .LastViewDate = txtLastViewDate.Text
                    .LastViewIP = txtLastViewIP.Text
                    '.NameAlt = txtName.Text
                    '.PhoneNumber = txtPhoneNumber.Text
                    '.Description = txtDescription.Text
                    '.DescriptionAlt = txtDescription.Text
                    '.ImageURL = txtImageURL.Text
                    '.ImageURLAlt = txtImageURL.Text
                    '.ViewOrder = txtViewOrder.Text
                    '.Status = cmbStatus.SelectedValue
                    '.DisplayStatus = cmbDisplayStatus.SelectedValue
                End With

                Dim BeforeText As String = ""
                Dim AfterText As String = ""
                AfterText += "|LastViewDate =" & txtLastViewDate.Text
                AfterText += "|LastViewIP =" & txtLastViewIP.Text
                'AfterText += "|Code = " & txtCode.Text
                'AfterText += "|Description =" & txtDescription.Text
                'AfterText += "|DescriptionAlt =" & txtDescription.Text
                'AfterText += "|ImageURL =" & txtImageURL.Text
                'AfterText += "|ImageURLAlt = " & txtImageURL.Text
                'AfterText += "|ViewOrder =" & txtViewOrder.Text
                'AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                'AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                    With objbannerdetails

                        .insert()
                        If Not .ErrorMsg Is Nothing Then
                            If .ErrorMsg.Trim.Length > 0 Then
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = .ErrorMsg
                            Else
                                lblMsg.ForeColor = Drawing.Color.Green
                                lblMsg.Text = "Added successfully"
                                Response.Redirect("ManageBannerDetails.aspx")
                            End If
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Added successfully"
                            Response.Redirect("ManageBannerDetails.aspx")
                        End If
                    End With
                Else
                    lblMsg.ForeColor = Drawing.Color.Red
                    lblMsg.Text = "Add item failed"
                    Exit Sub
                End If
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then

                Dim BeforeText As String = ""
                Dim objbannerdetails As New bannerdetails(Val(hdnId.Value))
                With objbannerdetails
                    BeforeText += "LastViewDate = " & .LastViewDate
                    BeforeText += "|LastViewIP=" & .LastViewIP
                    'BeforeText += "|Code=" & .Code
                    'BeforeText += "|Description=" & .Description
                    'BeforeText += "|DescriptionAlt=" & .DescriptionAlt
                    'BeforeText += "|ImageURL=" & .ImageURL
                    'BeforeText += "|ImageURLAlt=" & .ImageURLAlt
                    'BeforeText += "|ViewOrder=" & .ViewOrder
                    'BeforeText += "|Status=" & .Status
                    'BeforeText += "|DisplayStatus=" & .DisplayStatus

                    '.Name = txtName.Text.Trim
                    '.NameAlt = txtName.Text.Trim
                    '.Code = txtCode.Text.Trim
                    '.Description = txtDescription.Text.Trim
                    '.DescriptionAlt = txtDescription.Text.Trim
                    '.ImageURL = txtImageURL.Text.Trim
                    '.ImageURLAlt = txtImageURL.Text.Trim
                    '.ViewOrder = txtViewOrder.Text.Trim
                    '.Status = Val(cmbStatus.SelectedValue)
                    '.DisplayStatus = 1 'Val(cmbDisplayStatus.SelectedValue())
                    'CategoryId = .CheckExist(.Name, .Code, Val(hdnCategoryId.Value))
                    'If CategoryId <> 0 Then
                    '    .ErrorMsg = .Name & " or " & .Code & " Already Exist"
                    'Else

                    Dim AfterText As String = ""
                    AfterText += "|LastViewDate =" & txtLastViewDate.Text
                    AfterText += "|LastViewIP =" & txtLastViewIP.Text
                    'AfterText += "|Code = " & txtCode.Text
                    'AfterText += "|Description =" & txtDescription.Text
                    'AfterText += "|DescriptionAlt =" & txtDescription.Text
                    'AfterText += "|ImageURL =" & txtImageURL.Text
                    ' AfterText += "|ImageURLAlt = " & txtImageURL.Text
                    'AfterText += "|ViewOrder =" & txtViewOrder.Text
                    'AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                    'AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                    If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then

                        .update()
                    Else
                        lblMsg.ForeColor = Drawing.Color.Red
                        lblMsg.Text = "Update item failed"
                        Exit Sub
                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Update successfully"
                            Response.Redirect("ManageBannerDetails.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Update successfully"
                        Response.Redirect("ManageBannerDetails.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        End If
    End Sub

#Region "Functions"

    Private Sub FillDropDowns()

        Dim objBannerOwner As New bannerowner
        Dim dr As MySqlDataReader = objBannerOwner.GetAllBannerOwner()
        cmbBanner.DataSource = dr
        cmbBanner.DataValueField = "ID"
        cmbBanner.DataTextField = "Name"
        cmbBanner.DataBind()
        dr.Close()

        Dim objCustomer As New customermst
        dr = objCustomer.GetCustomerName()
        cmbCustomer.DataSource = dr
        cmbCustomer.DataValueField = "ID"
        cmbCustomer.DataTextField = "Name"
        cmbCustomer.DataBind()
        dr.Close()

        'Dim objItem As New item
        'dr = objItem.getItem()
        'cmbItem.DataSource = dr
        'cmbItem.DataValueField = "ID"
        'cmbItem.DataTextField = "Name"
        'cmbItem.DataBind()
        'dr.Close()
    End Sub

    Private Sub FillForm()

        Dim queryStr As String = ""
        Dim ItemID As Integer
        Dim qsEnc As New AbhiQsEnc.AbhiQs

        queryStr = Request.QueryString("Qs")   ' read query string 
        ItemID = Val(qsEnc.ReadQS(queryStr, "id"))
        hdnId.Value = ItemID
        Dim objbannerdetails As New bannerdetails(ItemID)
        Dim objCustomer As New customermst(objbannerdetails.CustomerID)
        With objbannerdetails
            cmbBanner.SelectedValue = objbannerdetails.CustomerID
            cmbCustomer.SelectedValue = .CustomerID
            'cmbItem.SelectedValue = objitemlistdetails.ItemID
            txtLastViewDate.Text = .LastViewDate
            txtLastViewIP.Text = .LastViewIP
            'txtName.Text = .NameAlt
            'txtPhoneNumber.Text = .PhoneNumber
            'txtDescription.Text = .Description
            'txtImageURL.Text = .ImageURL
            'txtViewOrder.Text = .ViewOrder
            'cmbStatus.SelectedValue = .Status
            'cmbDisplayStatus.SelectedValue = .DisplayStatus
            'cmbStatus.SelectedValue = .Status
            If hdnId.Value > 0 Then
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Item"
            Else
                btnAdd.Text = "ADD"
            End If
        End With
    End Sub

#End Region



End Class