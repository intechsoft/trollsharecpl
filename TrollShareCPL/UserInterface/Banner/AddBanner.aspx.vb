﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddBanner
    Inherits BaseUIPage

#Region "Variables"
    Private dr As MySqlDataReader
#End Region

    Private UPLOADFOLDER As String = "../FileStore/Products"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillForm()
        End If
    End Sub

    Private Sub cmbBannerOwner_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBannerOwner.DataBound
        cmbBannerOwner.Items.Insert(0, New ListItem("-------Select A Banner Owner -------", 0))
    End Sub

    Private Sub cmbCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCategory.DataBound
        cmbCategory.Items.Insert(0, New ListItem("-------Select A Category -------", 0))
    End Sub

    Private Sub cmbSubCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubCategory.DataBound
        cmbSubCategory.Items.Insert(0, New ListItem("-------Select A Sub Category -------", 0))
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        If btnAdd.Text.Trim.ToUpper = "ADD" Then
            If AddPrivilage Then

                Dim objbanner As New banner
                With objbanner
                    .BannerOwnerID = cmbBannerOwner.SelectedValue
                    .CategoryID = cmbCategory.SelectedValue
                    .SubCategoryID = cmbSubCategory.SelectedValue
                    .Name = txtName.Text
                    .Website = txtWebsite.Text
                    .EmailID = txtEmailid.Text
                    '.NameAlt = txtName.Text
                    .PhoneNumber = txtPhoneNumber.Text
                    .BannerData = txtDescription.Text
                    '.DescriptionAlt = txtViewOrder.Text
                    '.ImageURL = txtImageURL.Text
                    '.ImageURLAlt = txtImageURL.Text
                    .ViewOrder = txtViewOrder.Text
                    .Status = cmbStatus.SelectedValue
                    '.DisplayStatus = cmbDisplayStatus.SelectedValue
                End With

                Dim BeforeText As String = ""
                Dim AfterText As String = "Name =" & txtName.Text
                AfterText += "|Website =" & txtWebsite.Text
                AfterText += "|EmailID =" & txtEmailid.Text
                AfterText += "|PhoneNumber = " & txtPhoneNumber.Text
                AfterText += "|BannerData =" & txtDescription.Text
                AfterText += "|DescriptionAlt =" & txtDescription.Text
                'AfterText += "|ImageURL =" & txtImageURL.Text
                'AfterText += "|ImageURLAlt = " & txtImageURL.Text
                AfterText += "|ViewOrder =" & txtViewOrder.Text
                AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                'AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                    With objbanner
                        If FileUpload1.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = FileUpload1.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(Me.FileUpload1.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            'Path.GetFileName(Me.FileUpload1.PostedFile.FileName)
                            upldfile.FileType = UploadFileTypes.Product
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (FileUpload1.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .ImageUrl = upldfile.insertMember()
                            upldfile = New uploadedfiles(.ImageUrl)
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .ImageUrl, ext)
                            upldfile.update()
                            If Val(IsNullValue(.ImageUrl.Trim, "0")) > 0 Then
                                If UploadFile(.ImageUrl, ext) Then
                                    .ImageUrl = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add item failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add item failed"
                                Exit Sub
                            End If
                        End If
                        .insert()
                        If Not .ErrorMsg Is Nothing Then
                            If .ErrorMsg.Trim.Length > 0 Then
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = .ErrorMsg
                            Else
                                lblMsg.ForeColor = Drawing.Color.Green
                                lblMsg.Text = "Added successfully"
                                Response.Redirect("ManageBanner.aspx")
                            End If
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Added successfully"
                            Response.Redirect("ManageBanner.aspx")
                        End If
                    End With
                Else
                    lblMsg.ForeColor = Drawing.Color.Red
                    lblMsg.Text = "Add item failed"
                    Exit Sub
                End If
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then

                Dim BeforeText As String = ""
                Dim objbanner As New banner(Val(hdnId.Value))
                With objbanner
                    BeforeText += "Name = " & .Name
                    BeforeText += "|Website=" & .Website
                    BeforeText += "|EmailID=" & .EmailID
                    BeforeText += "|PhoneNumber=" & .PhoneNumber
                    BeforeText += "|Description=" & .BannerData
                    BeforeText += "|ViewOrder=" & .ViewOrder
                    BeforeText += "|ImageURL=" & .ImageURL
                    'BeforeText += "|ImageURLAlt=" & .ImageURLAlt
                    BeforeText += "|Status=" & .Status
                    'BeforeText += "|DisplayStatus=" & .DisplayStatus

                    .Name = txtName.Text.Trim
                    .Website = txtWebsite.Text.Trim
                    .EmailID = txtEmailid.Text.Trim
                    .PhoneNumber = txtPhoneNumber.Text.Trim
                    .BannerData = txtDescription.Text.Trim
                    '.DescriptionAlt = txtDescription.Text.Trim
                    '.ImageURL = txtImageURL.Text.Trim
                    '.ImageURLAlt = txtImageURL.Text.Trim
                    .ViewOrder = txtViewOrder.Text.Trim
                    .Status = Val(cmbStatus.SelectedValue)
                    '.DisplayStatus = 1 'Val(cmbDisplayStatus.SelectedValue())
                    'CategoryId = .CheckExist(.Name, .Code, Val(hdnCategoryId.Value))
                    'If CategoryId <> 0 Then
                    '    .ErrorMsg = .Name & " or " & .Code & " Already Exist"
                    'Else

                    Dim AfterText As String = "Name" & txtName.Text
                    AfterText += "|Website" & txtWebsite.Text
                    AfterText += "|Emailid =" & txtEmailid.Text
                    AfterText += "|PhoneNumber = " & txtPhoneNumber.Text
                    AfterText += "|BannerData =" & txtDescription.Text
                    'AfterText += "|DescriptionAlt =" & txtDescription.Text
                    'AfterText += "|ImageURL =" & txtImageURL.Text
                    ' AfterText += "|ImageURLAlt = " & txtImageURL.Text
                    AfterText += "|ViewOrder =" & txtViewOrder.Text
                    AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                    'AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                    If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                        If FileUpload1.HasFile Then
                            Dim upldfile As New uploadedfiles
                            upldfile.FileName = FileUpload1.PostedFile.FileName
                            Dim ext As String() = IO.Path.GetFileName(Me.FileUpload1.PostedFile.FileName).Split(".")
                            upldfile.FilePath = ext(ext.Length - 1)
                            upldfile.FileType = UploadFileTypes.Product
                            upldfile.IpAddress = Request.UserHostAddress.ToString
                            upldfile.Status = 1
                            upldfile.UploadedDate = Now
                            upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                            If (FileUpload1.PostedFile.ContentLength / 1024) > 10240 Then
                                lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                lblMsg.ForeColor = Drawing.Color.Red
                                Exit Sub
                            End If
                            .ImageURL = upldfile.insertMember()
                            upldfile = New uploadedfiles(.ImageURL)
                            upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .ImageURL, ext)
                            upldfile.update()
                            If Val(IsNullValue(.ImageURL.Trim, "0")) > 0 Then
                                If UploadFile(.ImageURL, ext) Then
                                    .ImageURL = GetImageFilePath(upldfile.FilePath)
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add item failed"
                                    Exit Sub
                                End If
                            Else
                                lblMsg.ForeColor = Drawing.Color.Red
                                lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add item failed"
                                Exit Sub
                            End If
                        End If
                        .update()
                    Else
                        lblMsg.ForeColor = Drawing.Color.Red
                        lblMsg.Text = "Update item failed"
                        Exit Sub
                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Update successfully"
                            Response.Redirect("ManageBanner.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Update successfully"
                        Response.Redirect("ManageBanner.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        End If
    End Sub



#Region "Functions"

    Private Sub FillDropDowns()
        Dim objCategory As New categorymst
        Dim dr As MySqlDataReader = objCategory.GetAllCategoryName()
        cmbCategory.DataSource = dr
        cmbCategory.DataValueField = "ID"
        cmbCategory.DataTextField = "Name"
        cmbCategory.DataBind()
        dr.Close()

        Dim objSubcategory As New subcategory
        dr = objSubcategory.GetSubCategories()
        cmbSubCategory.DataSource = dr
        cmbSubCategory.DataValueField = "ID"
        cmbSubCategory.DataTextField = "Name"
        cmbSubCategory.DataBind()
        dr.Close()

        Dim objbannerowner As New bannerowner
        dr = objbannerowner.GetAllBannerOwner()
        cmbBannerOwner.DataSource = dr
        cmbBannerOwner.DataValueField = "ID"
        cmbBannerOwner.DataTextField = "Name"
        cmbBannerOwner.DataBind()
        dr.Close()

    End Sub

    Private Sub FillForm()

        Dim queryStr As String = ""
        Dim itemid As Integer
        Dim qsEnc As New AbhiQsEnc.AbhiQs

        queryStr = Request.QueryString("Qs")   ' read query string 
        itemid = Val(qsEnc.ReadQS(queryStr, "id"))
        hdnId.Value = itemid
        Dim objbanner As New banner(itemid)
        Dim objsubcategory As New subcategory(objbanner.SubCategoryID)
        With objbanner
            cmbCategory.SelectedValue = objsubcategory.CategoryID
            cmbSubCategory.SelectedValue = .SubCategoryID
            txtWebsite.Text = .Website
            txtEmailid.Text = .EmailID
            txtPhoneNumber.Text = .PhoneNumber
            txtDescription.Text = .BannerData
            'txtImageURL.Text = .ImageURL
            txtViewOrder.Text = .ViewOrder
            cmbStatus.SelectedValue = .Status
            'cmbDisplayStatus.SelectedValue = .DisplayStatus
            cmbStatus.SelectedValue = .Status
            If hdnId.Value > 0 Then
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Item"
            Else
                btnAdd.Text = "ADD"
            End If
        End With
    End Sub

    Public Function UploadFile(ByVal SaveFileName As String, ext As String()) As Boolean
        If FileUpload1.HasFile Then
            Try
                FileUpload1.SaveAs(getServerFilePath(UPLOADFOLDER, SaveFileName, ext))
                Return True
            Catch ex As Exception
                lblMsg.Text = "Error in File Upload ..."
            End Try
        Else

        End If
        Return False
    End Function

#End Region


    Private Sub cmbCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCategory.SelectedIndexChanged

        Dim objsubcategory As New subcategory
        dr = objsubcategory.GetSubCategoryName(cmbCategory.SelectedValue)
        cmbSubCategory.DataSource = dr
        cmbSubCategory.DataValueField = "ID"
        cmbSubCategory.DataTextField = "Name"
        cmbSubCategory.DataBind()
        dr.Close()

    End Sub
End Class