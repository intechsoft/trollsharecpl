﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL


Public Class ManageBannerType
    Inherits BaseUIPage
#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objServicetype As servicetype
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'FillDropDowns()
            FillGridView()
        End If
    End Sub

    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub

    Private Sub grdbannertype_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdbannertype.RowDataBound
        If grdbannertype.Columns.Count > 0 Then
            If DeletePrivilage Then
                grdbannertype.Columns(grdbannertype.Columns.Count - 1).Visible = True
            Else
                grdbannertype.Columns(grdbannertype.Columns.Count - 1).Visible = False
            End If
        End If
    End Sub

    Private Sub grdbannertype_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdbannertype.RowCommand
        Dim BeforeText As String = ""
        Dim AfterText As String = ""
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Enc As New AbhiQsEnc.AbhiQs
                Dim Encrypted As String = Enc.WriteQS("id=" & id)
                Response.Redirect("AddBannerType.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                Dim objbannertype As New bannertype(id)
                objbannertype.delete()
                With objbannertype
                    BeforeText += " Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
                FillGridView()
            End If
        End If
    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()
        Dim dt As DataTable
        Dim objbannertype As New bannertype
        dt = objbannertype.getBannerType()
        If chkPaging.Checked = True Then
            grdbannertype.AllowPaging = True
        End If
        grdbannertype.DataSource = dt
        grdbannertype.DataBind()
    End Sub

#End Region

End Class