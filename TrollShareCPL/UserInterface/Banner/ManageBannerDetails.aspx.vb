﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageBannerDetails
    Inherits BaseUIPage
#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objServicetype As servicetype
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            FillDropDowns()
            FillGridView()
        End If
    End Sub

    Private Sub cmbCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCategory.DataBound
        cmbCategory.Items.Insert(0, New ListItem("-------Select a Category -------", 0))
    End Sub


    Private Sub cmbSubcategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubcategory.DataBound
        cmbSubcategory.Items.Insert(0, New ListItem("-------Select a Sub Category -------", 0))
    End Sub

    Private Sub cmbBanner_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBanner.DataBound
        cmbBanner.Items.Insert(0, New ListItem("-------Select a Banner  -------", 0))
    End Sub

    Private Sub cmbBannerOwner_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBannerOwner.DataBound
        cmbBannerOwner.Items.Insert(0, New ListItem("-------Select a Banner Owner -------", 0))
    End Sub

    Private Sub grdBannerDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdbannerdetails.PageIndexChanging
        grdbannerdetails.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub

    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub

    Private Sub grdBannerDetails_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdbannerdetails.RowDataBound
        If grdbannerdetails.Columns.Count > 0 Then
            If DeletePrivilage Then
                grdbannerdetails.Columns(grdbannerdetails.Columns.Count - 1).Visible = True
            Else
                grdbannerdetails.Columns(grdbannerdetails.Columns.Count - 1).Visible = False
            End If
        End If
    End Sub

    Private Sub grdBannerDetails_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdbannerdetails.RowCommand
        Dim BeforeText As String = ""
        Dim AfterText As String = ""
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Enc As New AbhiQsEnc.AbhiQs
                Dim Encrypted As String = Enc.WriteQS("id=" & id)
                Response.Redirect("AddBannerDetails.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                Dim objbannerdetails As New bannerdetails(id)
                objbannerdetails.delete()
                With objbannerdetails
                    BeforeText += " LastViewDate = " & .LastViewDate
                    BeforeText += "|LastViewIP=" & .LastViewIP
                    'BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
                FillGridView()
            End If
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        Dim whereClause As String = ""
        If cmbCategory.SelectedValue > 0 Then
            whereClause += " and c.ID=" & cmbBannerOwner.SelectedValue
        End If

        If cmbSubcategory.SelectedValue > 0 Then
            whereClause += " and s.ID=" & cmbSubcategory.SelectedValue
        End If

        If cmbBanner.SelectedValue > 0 Then
            whereClause += " and b.ID=" & cmbBanner.SelectedValue
        End If

        If cmbBannerOwner.SelectedValue > 0 Then
            whereClause += " and bo.ID=" & cmbBannerOwner.SelectedValue
        End If

        Dim dt As DataTable
        Dim objbannerdetails As New bannerdetails
        dt = objbannerdetails.getBannerDetails(whereClause)
        If chkPaging.Checked = True Then
            grdbannerdetails.AllowPaging = True
        End If
        grdbannerdetails.DataSource = dt
        grdbannerdetails.DataBind()

    End Sub

    Private Sub cmbCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCategory.SelectedIndexChanged
        If cmbCategory.SelectedValue > 0 Then

            Dim objsubcategory As New subcategory
            dr = objsubcategory.GetSubCategoryName(cmbCategory.SelectedValue)
            cmbSubcategory.DataSource = dr
            cmbSubcategory.DataValueField = "ID"
            cmbSubcategory.DataTextField = "Name"
            cmbSubcategory.DataBind()
            dr.Close()

            Dim objbannerdetails As New bannerdetails
            dr = objbannerdetails.GetBannerByCatagory(cmbCategory.SelectedValue)
            cmbBanner.DataSource = dr
            cmbBanner.DataValueField = "ID"
            cmbBanner.DataTextField = "Name"
            cmbBanner.DataBind()
            dr.Close()

        Else
            Dim objbannerowner As New bannerowner
            dr = objbannerowner.GetAllBannerOwner()
            cmbBannerOwner.DataSource = dr
            cmbBannerOwner.DataValueField = "ID"
            cmbBannerOwner.DataTextField = "Name"
            cmbBannerOwner.DataBind()
            dr.Close()

            Dim objbannerdetails As New bannerdetails
            dr = objbannerdetails.getBanner()
            cmbBanner.DataSource = dr
            cmbBanner.DataValueField = "ID"
            cmbBanner.DataTextField = "Name"
            cmbBanner.DataBind()
            dr.Close()

            Dim objsubcategory As New subcategory
            dr = objsubcategory.GetSubCategoryName()
            cmbSubcategory.DataSource = dr
            cmbSubcategory.DataValueField = "ID"
            cmbSubcategory.DataTextField = "Name"
            cmbSubcategory.DataBind()
            dr.Close()
        End If

    End Sub

    Private Sub cmbSubcategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSubcategory.SelectedIndexChanged
        If cmbSubcategory.SelectedValue > 0 Then

            Dim objbannerdetails As New bannerdetails
            dr = objbannerdetails.GetBannerBySubCategory(cmbSubcategory.SelectedValue)
            cmbBanner.DataSource = dr
            cmbBanner.DataValueField = "ID"
            cmbBanner.DataTextField = "Name"
            cmbBanner.DataBind()
            dr.Close()

        Else
            If cmbCategory.SelectedValue > 0 Then
                Dim objbannerdetails As New bannerdetails
                dr = objbannerdetails.GetBannerByCatagory(cmbCategory.SelectedValue)
                cmbBanner.DataSource = dr
                cmbBanner.DataValueField = "ID"
                cmbBanner.DataTextField = "Name"
                cmbBanner.DataBind()
                dr.Close()
            Else
                Dim objbannerowner As New bannerowner
                dr = objbannerowner.GetAllBannerOwner()
                cmbBannerOwner.DataSource = dr
                cmbBannerOwner.DataValueField = "ID"
                cmbBannerOwner.DataTextField = "Name"
                cmbBannerOwner.DataBind()
                dr.Close()
            End If
        End If
    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()
        Dim dt As DataTable
        Dim objbannerdetails As New bannerdetails
        dt = objbannerdetails.getBannerDetails()
        If chkPaging.Checked = True Then
            grdbannerdetails.AllowPaging = True
        End If
        grdbannerdetails.DataSource = dt
        grdbannerdetails.DataBind()
    End Sub

    Private Sub FillDropDowns()

        Dim objCategorymst As New categorymst
        dr = objCategorymst.GetAllCategoryName()
        cmbCategory.DataSource = dr
        cmbCategory.DataValueField = "ID"
        cmbCategory.DataTextField = "Name"
        cmbCategory.DataBind()
        dr.Close()


        Dim objbannerowner As New bannerowner
        dr = objbannerowner.GetAllBannerOwner()
        cmbBannerOwner.DataSource = dr
        cmbBannerOwner.DataValueField = "ID"
        cmbBannerOwner.DataTextField = "Name"
        cmbBannerOwner.DataBind()
        dr.Close()

        Dim objbannerdetails As New bannerdetails
        dr = objbannerdetails.getBanner()
        cmbBanner.DataSource = dr
        cmbBanner.DataValueField = "ID"
        cmbBanner.DataTextField = "Name"
        cmbBanner.DataBind()
        dr.Close()

        Dim objsubcategory As New subcategory
        dr = objsubcategory.GetSubCategoryName()
        cmbSubcategory.DataSource = dr
        cmbSubcategory.DataValueField = "ID"
        cmbSubcategory.DataTextField = "Name"
        cmbSubcategory.DataBind()
        dr.Close()
    End Sub

#End Region

End Class