﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageBanner
    Inherits BaseUIPage
#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objServicetype As servicetype
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            FillDropDowns()
            FillGridView()
        End If
    End Sub

    Private Sub cmbCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCategory.DataBound
        cmbCategory.Items.Insert(0, New ListItem("-------Select a Category -------", 0))
    End Sub

    Private Sub cmbBannerOwner_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBannerOwner.DataBound
        cmbBannerOwner.Items.Insert(0, New ListItem("-------Select a Banner Owner -------", 0))
    End Sub

    Private Sub cmbSubcategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSubcategory.DataBound
        cmbSubcategory.Items.Insert(0, New ListItem("-------Select a Sub Category -------", 0))
    End Sub

    Private Sub grditemlist_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdbanner.PageIndexChanging
        grdbanner.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub

    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub

    Private Sub grdbanner_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdbanner.RowDataBound
        If grdbanner.Columns.Count > 0 Then
            If DeletePrivilage Then
                grdbanner.Columns(grdbanner.Columns.Count - 1).Visible = True
            Else
                grdbanner.Columns(grdbanner.Columns.Count - 1).Visible = False
            End If
        End If
    End Sub

    Private Sub grdbanner_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdbanner.RowCommand
        Dim BeforeText As String = ""
        Dim AfterText As String = ""
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Enc As New AbhiQsEnc.AbhiQs
                Dim Encrypted As String = Enc.WriteQS("id=" & id)
                Response.Redirect("AddBanner.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                Dim objbanner As New banner(id)
                objbanner.delete()
                With objbanner
                    BeforeText += " Website = " & .Website
                    BeforeText += "|EmailID=" & .EmailID
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
                FillGridView()
            End If
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        Dim whereClause As String = ""
        If cmbCategory.SelectedValue > 0 Then
            whereClause += " and c.ID=" & cmbCategory.SelectedValue
        End If

        If cmbSubcategory.SelectedValue > 0 Then
            whereClause += " and s.ID=" & cmbSubcategory.SelectedValue
        End If

        If cmbBannerOwner.SelectedValue > 0 Then
            whereClause += " and bo.ID=" & cmbBannerOwner.SelectedValue
        End If

        Dim dt As DataTable
        Dim objbanner As New banner
        dt = objbanner.getBanner(whereClause)
        If chkPaging.Checked = True Then
            grdbanner.AllowPaging = True
        End If
        grdbanner.DataSource = dt
        grdbanner.DataBind()

    End Sub

    Private Sub cmbCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCategory.SelectedIndexChanged

        Dim objsubcategory As New subcategory
        dr = objsubcategory.GetSubCategoryName(cmbCategory.SelectedValue)
        cmbSubcategory.DataSource = dr
        cmbSubcategory.DataValueField = "ID"
        cmbSubcategory.DataTextField = "Name"
        cmbSubcategory.DataBind()
        dr.Close()

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()
        Dim dt As DataTable
        Dim objbanner As New banner
        dt = objbanner.getBanner()
        If chkPaging.Checked = True Then
            grdbanner.AllowPaging = True
        End If
        grdbanner.DataSource = dt
        grdbanner.DataBind()
    End Sub

    Private Sub FillDropDowns()
        Dim objCategorymst As New categorymst
        dr = objCategorymst.GetAllCategoryName()
        cmbCategory.DataSource = dr
        cmbCategory.DataValueField = "ID"
        cmbCategory.DataTextField = "Name"
        cmbCategory.DataBind()
        dr.Close()


        Dim objbannerowner As New bannerowner
        dr = objbannerowner.GetAllBannerOwner()
        cmbBannerOwner.DataSource = dr
        cmbBannerOwner.DataValueField = "ID"
        cmbBannerOwner.DataTextField = "Name"
        cmbBannerOwner.DataBind()
        dr.Close()

        Dim objsubcategory As New subcategory
        dr = objsubcategory.GetSubCategoryName()
        cmbSubcategory.DataSource = dr
        cmbSubcategory.DataValueField = "ID"
        cmbSubcategory.DataTextField = "Name"
        cmbSubcategory.DataBind()
        dr.Close()
    End Sub

#End Region

End Class