﻿<%@ Page title="" Language="vb" AutoEventWireup="false" masterpagefile="~/AppMaster.Master" CodeBehind="AddBannerDetails.aspx.vb" Inherits="TrollShareCPL.AddBannerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdnId" runat="server" Value="0" />
    <div class="row">
        <div class="col-lg-4" style="width: 500px; margin-left: 150px; margin-right: auto; margin-top: 10px; margin-bottom: auto; top: 0px; left: 3px;">
            <div class="panel panel-primary">
                <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
                    <asp:Label ID="lblHeader" runat="server" Text="Add New Category"></asp:Label>
                    <a href="ManageBannerDetails.aspx">&nbsp;>>Manage Banner Details</a>

                </div>
                   <div class="panel-heading" style="height: 50px">
                    <h4 style="text-align: center"><strong>
                        <asp:Label ID="lblHeadText" runat="server" Text="Add Banner Details"></asp:Label>
                    </strong></h4>
                </div>
                <div id="addbannerlist">
                    <table style="width: 100%; margin: 30px auto; width: 400px; line-height: 50px;">
                        <tr>
                            <td>Banner Owner</td>
                            <td>
                                <asp:DropDownList ID="cmbBanner" runat="server" class="form-control" Style="width: auto; min-width: 250px; height: 30px" required="required" />
                            </td>
                        </tr>
                        <tr>
                            <td>Customer</td>
                            <td>
                                <asp:DropDownList ID="cmbCustomer" runat="server" class="form-control" Style="width: auto; min-width: 250px; height: 30px" required="required" />
                            </td>
                        </tr>
                       <%-- <tr>
                            <td>Item</td>
                            <td>
                                <asp:DropDownList ID="cmbItem" runat="server" class="form-control" Style="width: auto; min-width: 250px; height: 30px" required="required" />
                            </td>
                        </tr>--%>
                         <tr>
                            <td>Last View Date

                            </td>
                            <td>

                                <asp:TextBox ID="txtLastViewDate" runat="server" placeholder="Enter Code" class="form-control" Style="width: 250px;" MaxLength="50" AutoPostBack="true"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Last View IP </td>
                            <td>

                                <asp:TextBox ID="txtLastViewIP" runat="server" placeholder="Enter Name" class="form-control" Style="width: 250px;" MaxLength="50" AutoPostBack="true"></asp:TextBox>

                            </td>
                        </tr>
                        <%--<tr>
                            <td>Contact Number </td>
                            <td>

                                <asp:TextBox ID="txtPhoneNumber" runat="server" placeholder="Enter Contact Number" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>Description </td>
                            <td>

                                <asp:TextBox ID="txtDescription" TextMode="MultiLine" Rows="3" runat="server" placeholder="Enter Description" class="form-control" Style="width: 250px;"></asp:TextBox>

                            </td>
                        </tr>--%>
                       <%-- <tr>
                            <td>View Order </td>
                            <td>

                                <asp:TextBox ID="txtViewOrder" runat="server" placeholder="Enter View Order" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>

                            </td>
                        </tr>--%>
                      <%-- <tr>
                            <td>Image URL </td>
                            <td>

                                <asp:FileUpload ID="FileUpload1" runat="server" class="form-control" Style="width: 250px;"  />

                            </td>
                        </tr>--%>
                        <%--<tr>
                            <td>DisplayStatus </td>
                            <td>

                                <asp:DropDownList ID="cmbDisplayStatus" runat="server" class="form-control" Style="width: auto; min-width: 250px; height: 30px" required="required">
                                    <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                    <asp:ListItem Text="On" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Off" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                            </td>
                        </tr>
                        <tr>
                            <td>Status </td>
                            <td>

                                <asp:DropDownList ID="cmbStatus" runat="server" class="form-control" Style="width: auto; min-width: 250px; height: 30px" required="required">
                                    <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Suspended" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Stopped" Value="3"></asp:ListItem>
                                </asp:DropDownList>

                            </td>
                        </tr>--%>
                        <tr style="height: 20px">
                            <td colspan="2">

                                <asp:Label ID="lblMsg" runat="server" Font-Size="Medium" Text="" CssClass="warning"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div style="text-align: center; margin: 10px auto; height: 50px; width: 30%;">

                        <div style="float: left; width: 100%; margin: 0px auto;">
                            <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-outline btn-success" />
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

