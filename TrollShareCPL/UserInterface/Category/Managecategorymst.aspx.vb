﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class Managecategorymst
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objCategory As categorymst
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillGridView()
        End If
    End Sub

    Private Sub grdCategory_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdCategory.PageIndexChanging
        grdCategory.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub
    Private Sub grdCategory_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdCategory.RowDataBound

        If DeletePrivilage Then
            grdCategory.Columns(10).Visible = True
        Else
            grdCategory.Columns(10).Visible = False
        End If
    End Sub

    Private Sub grdCategory_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCategory.RowCommand

        Dim BeforeText As String = ""
        Dim AfterText As String = ""

        id = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = Enc.WriteQS("id=" & id)                ' code for encrytpting a strings
                Response.Redirect("Addcategorymst.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objCategory = New categorymst(id)
                With objCategory

                    BeforeText += "Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Code=" & .Code
                    BeforeText += "|Description=" & .Description
                    BeforeText += "|DescriptionAlt=" & .DescriptionAlt
                    BeforeText += "|ImageURL=" & .ImageURL
                    BeforeText += "|ImageURLAlt=" & .ImageURLAlt
                    BeforeText += "|ViewOrder=" & .ViewOrder
                    BeforeText += "|Status=" & .Status
                    BeforeText += "|DisplayStatus=" & .DisplayStatus
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
            End If
            FillGridView()
        End If

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()

        If chkPaging.Checked = True Then
            grdCategory.AllowPaging = True
        Else
            grdCategory.AllowPaging = False
        End If
        objCategory = New categorymst
        dt = objCategory.GetCategory()
        grdCategory.DataSource = dt
        grdCategory.DataBind()

    End Sub

#End Region

End Class