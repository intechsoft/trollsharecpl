﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class Managesubcategory
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objSubCategory As subcategory
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillGridView()
        End If
    End Sub

    Private Sub cmbCategory_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCategory.DataBound
        cmbCategory.Items.Insert(0, New ListItem("-------Select a Category -------", 0))
    End Sub

    Private Sub grdCategory_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdCategory.PageIndexChanging
        grdCategory.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub
    Private Sub grdCategory_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdCategory.RowDataBound

        If DeletePrivilage Then
            grdCategory.Columns(10).Visible = True
        Else
            grdCategory.Columns(10).Visible = False
        End If
    End Sub

    Private Sub grdCategory_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCategory.RowCommand

        Dim BeforeText As String = ""
        Dim AfterText As String = ""

        id = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = Enc.WriteQS("id=" & id)                ' code for encrytpting a strings
                Response.Redirect("Addsubcategory.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objSubCategory = New subcategory(id)
                With objSubCategory

                    BeforeText += "Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Code=" & .Code
                    BeforeText += "|Description=" & .Description
                    BeforeText += "|DescriptionAlt=" & .DescriptionAlt
                    BeforeText += "|ImageURL=" & .ImageURL
                    BeforeText += "|ImageURLAlt=" & .ImageURLAlt
                    BeforeText += "|ViewOrder=" & .ViewOrder
                    BeforeText += "|Status=" & .Status
                    BeforeText += "|DisplayStatus=" & .DisplayStatus
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
            End If
            FillGridView()
        End If

    End Sub

    Private Sub cmbCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCategory.SelectedIndexChanged
        objSubCategory = New subcategory
        If chkPaging.Checked = True Then
            grdCategory.AllowPaging = True
        Else
            grdCategory.AllowPaging = False
        End If

        If cmbCategory.SelectedValue > 0 Then
            dt = objSubCategory.GetSubCategory(cmbCategory.SelectedValue)
        Else
            dt = objSubCategory.GetSubCategory()
        End If

        grdCategory.DataSource = dt
        grdCategory.DataBind()

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()

        If chkPaging.Checked = True Then
            grdCategory.AllowPaging = True
        Else
            grdCategory.AllowPaging = False
        End If
        objSubCategory = New subcategory
        dt = objSubCategory.GetSubCategory()
        grdCategory.DataSource = dt
        grdCategory.DataBind()

    End Sub

    Private Sub FillDropDowns()
        Dim objCategorymst As New categorymst
        dr = objCategorymst.GetAllCategoryName()
        cmbCategory.DataSource = dr
        cmbCategory.DataValueField = "ID"
        cmbCategory.DataTextField = "Name"
        cmbCategory.DataBind()
        dr.Close()
    End Sub

#End Region

End Class