﻿Imports System.IO
Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class Addcategorymst
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private objCategory As categorymst
    Private UPLOADFOLDER As String = "../FileStore/Category"

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillForm()
        End If
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim CategoryId As Integer
        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then ' Checking adding privilage of current user
                objCategory = New categorymst
                With objCategory

                    .Name = txtName.Text.Trim
                    .Code = txtCode.Text.Trim
                    .NameAlt = txtName.Text.Trim
                    .Description = txtDescription.Text.Trim
                    .DescriptionAlt = txtDescription.Text.Trim
                    ' .ImageURL = txtImageURL.Text.Trim
                    '.ImageURLAlt = txtImageURL.Text.Trim
                    .ViewOrder = txtViewOrder.Text.Trim
                    .Status = Val(cmbStatus.SelectedValue())
                    .DisplayStatus = 1 'Val(cmbDisplayStatus.SelectedValue())
                    CategoryId = .CheckExist(.Name, .Code, 0)

                    If CategoryId <> 0 Then
                        .ErrorMsg = .Name & " or " & .Code & " Already Exist"
                        Exit Sub
                    Else
                        Dim BeforeText As String = ""
                        Dim AfterText As String = "Name =" & txtName.Text
                        AfterText += "|NameAlt =" & txtName.Text
                        AfterText += "|Code = " & txtCode.Text
                        AfterText += "|Description =" & txtDescription.Text
                        AfterText += "|DescriptionAlt =" & txtDescription.Text
                        'AfterText += "|ImageURL =" & txtImageURL.Text
                        'AfterText += "|ImageURLAlt = " & txtImageURL.Text
                        AfterText += "|ViewOrder =" & txtViewOrder.Text
                        AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                        AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            If FileUpload1.HasFile Then
                                Dim upldfile As New uploadedfiles
                                upldfile.FileName = FileUpload1.PostedFile.FileName
                                Dim ext As String() = Path.GetFileName(Me.FileUpload1.PostedFile.FileName).Split(".")
                                upldfile.FilePath = ext(ext.Length - 1)
                                'Path.GetFileName(Me.FileUpload1.PostedFile.FileName)
                                upldfile.FileType = UploadFileTypes.Category
                                upldfile.IpAddress = Request.UserHostAddress.ToString
                                upldfile.Status = 1
                                upldfile.UploadedDate = Now
                                upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                                If (FileUpload1.PostedFile.ContentLength / 1024) > 10240 Then
                                    lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    Exit Sub
                                End If
                                .ImageURL = upldfile.insertMember()
                                upldfile = New uploadedfiles(.ImageURL)
                                upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .ImageURL, ext)
                                upldfile.update()
                                If Val(IsNullValue(.ImageURL.Trim, "0")) > 0 Then
                                    If UploadFile(.ImageURL, ext) Then
                                        .ImageURL = GetImageFilePath(upldfile.FilePath)
                                    Else
                                        lblMsg.ForeColor = Drawing.Color.Red
                                        lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add category failed"
                                        Exit Sub
                                    End If
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add category failed"
                                    Exit Sub
                                End If
                            End If
                            .insertMember()
                            'Else
                            '    lblMsg.ForeColor = Drawing.Color.Red
                            '    lblMsg.Text = "Select image to upload"
                            '    Exit Sub
                            'End If
                        Else
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = "Add category failed"
                            Exit Sub
                        End If

                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Category added successfully"
                            Response.Redirect("Managecategorymst.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Category added successfully"
                        Response.Redirect("Managecategorymst.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else

            If EditPrivilage Then

                objCategory = New categorymst(Val(hdnCategoryId.Value))

                Dim BeforeText As String = ""

                With objCategory
                    BeforeText += "Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Code=" & .Code
                    BeforeText += "|Description=" & .Description
                    BeforeText += "|DescriptionAlt=" & .DescriptionAlt
                    BeforeText += "|ImageURL=" & .ImageURL
                    BeforeText += "|ImageURLAlt=" & .ImageURLAlt
                    BeforeText += "|ViewOrder=" & .ViewOrder
                    BeforeText += "|Status=" & .Status
                    BeforeText += "|DisplayStatus=" & .DisplayStatus

                    .Name = txtName.Text.Trim
                    .NameAlt = txtName.Text.Trim
                    .Code = txtCode.Text.Trim
                    .Description = txtDescription.Text.Trim
                    .DescriptionAlt = txtDescription.Text.Trim
                    '.ImageURL = txtImageURL.Text.Trim
                    '.ImageURLAlt = txtImageURL.Text.Trim
                    .ViewOrder = txtViewOrder.Text.Trim
                    .Status = Val(cmbStatus.SelectedValue)
                    .DisplayStatus = 1 'Val(cmbDisplayStatus.SelectedValue())
                    CategoryId = .CheckExist(.Name, .Code, Val(hdnCategoryId.Value))
                    If CategoryId <> 0 Then
                        .ErrorMsg = .Name & " or " & .Code & " Already Exist"
                    Else

                        Dim AfterText As String = "Name" & txtName.Text
                        AfterText += "|NameAlt =" & txtName.Text
                        AfterText += "|Code = " & txtCode.Text
                        AfterText += "|Description =" & txtDescription.Text
                        AfterText += "|DescriptionAlt =" & txtDescription.Text
                        'AfterText += "|ImageURL =" & txtImageURL.Text
                        ' AfterText += "|ImageURLAlt = " & txtImageURL.Text
                        AfterText += "|ViewOrder =" & txtViewOrder.Text
                        AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                        AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())



                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            If FileUpload1.HasFile Then
                                Dim upldfile As New uploadedfiles
                                upldfile.FileName = FileUpload1.PostedFile.FileName
                                Dim ext As String() = Path.GetFileName(Me.FileUpload1.PostedFile.FileName).Split(".")
                                upldfile.FilePath = ext(ext.Length - 1)
                                upldfile.FileType = UploadFileTypes.Category
                                upldfile.IpAddress = Request.UserHostAddress.ToString
                                upldfile.Status = 1
                                upldfile.UploadedDate = Now
                                upldfile.UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
                                If (FileUpload1.PostedFile.ContentLength / 1024) > 10240 Then
                                    lblMsg.Text = "File size exceeds the maximum limit 10 MB!"
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    Exit Sub
                                End If
                                .ImageURL = upldfile.insertMember()
                                upldfile = New uploadedfiles(.ImageURL)
                                upldfile.FilePath = getServerFilePath(UPLOADFOLDER, .ImageURL, ext)
                                upldfile.update()
                                If Val(IsNullValue(.ImageURL.Trim, "0")) > 0 Then
                                    If UploadFile(.ImageURL, ext) Then
                                        .ImageURL = GetImageFilePath(upldfile.FilePath)
                                    Else
                                        lblMsg.ForeColor = Drawing.Color.Red
                                        lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add category failed"
                                        Exit Sub
                                    End If
                                Else
                                    lblMsg.ForeColor = Drawing.Color.Red
                                    lblMsg.Text = lblMsg.Text & vbCrLf & upldfile.ErrorMsg & vbCrLf & "Add category failed"
                                    Exit Sub
                                End If
                            End If
                            .update()
                        Else
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = "Update category failed"
                            Exit Sub
                        End If

                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Category Updated successfully"
                            Response.Redirect("Managecategorymst.aspx")
                            ' ClearForm()
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Category Updated successfully"
                        Response.Redirect("Managecategorymst.aspx")
                        'ClearForm()
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated
            End If
        End If
    End Sub

    'Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

    '    If Val(hdnCategoryId.Value) = 0 Then
    '        ClearForm()
    '        lblMsg.Text = ""
    '    Else
    '        Response.Redirect("Managecategorymst.aspx")
    '    End If
    'End Sub

#End Region

#Region "Functions"

    Private Sub FillForm()
        Dim queryStr As String = ""
        Dim CategoryId As Integer
        objCategory = New categorymst
        Dim qsEnc As New AbhiQsEnc.AbhiQs

        queryStr = Request.QueryString("Qs")
        CategoryId = Val(qsEnc.ReadQS(queryStr, "id"))   'decrypt CategoryId from the query 
        hdnCategoryId.Value = CategoryId                  'Keep CategoryId in a hidden field for later use

        dr = objCategory.GetCategoryforEdit(CategoryId)
        If Not dr Is Nothing Then
            If dr.Read() Then
                txtName.Text = IsNullValue(dr("Name").ToString, "")
                txtName.Text = IsNullValue(dr("NameAlt").ToString, "")
                txtCode.Text = IsNullValue(dr("Code").ToString, "")
                txtDescription.Text = IsNullValue(dr("Description").ToString, "")
                txtDescription.Text = IsNullValue(dr("DescriptionAlt").ToString, "")
                ' txtImageURL.Text = IsNullValue(dr("ImageURL").ToString, "")
                'txtImageURL.Text = IsNullValue(dr("ImageURLAlt").ToString, "")
                txtViewOrder.Text = IsNullValue(dr("ViewOrder").ToString, 0)
                cmbStatus.SelectedValue() = dr("Status")
                'cmbDisplayStatus.SelectedValue() = dr("DisplayStatus").ToString
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Category"
            End If
            dr.Close()
        End If

    End Sub

    Private Sub ClearForm()
        txtName.Text = ""
        txtCode.Text = ""
        txtName.Text = ""
        txtDescription.Text = ""
        txtDescription.Text = ""
        ' txtImageURL.Text = ""
        'txtImageURL.Text = ""
        txtViewOrder.Text = ""
        cmbStatus.SelectedIndex() = 0
        'cmbDisplayStatus.SelectedIndex() = 0
    End Sub
    Public Function UploadFile(ByVal SaveFileName As String, ext As String()) As Boolean
        If FileUpload1.HasFile Then
            Try
                FileUpload1.SaveAs(getServerFilePath(UPLOADFOLDER, SaveFileName, ext))
                Return True
            Catch ex As Exception
                lblMsg.Text = "Error in File Upload ..."
            End Try
        Else

        End If
        Return False
    End Function

#End Region

End Class