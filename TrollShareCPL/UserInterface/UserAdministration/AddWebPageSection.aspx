﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="AddWebPageSection.aspx.vb" Inherits="TrollShareCPL.AddWebPageSection" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdnWebPageId" runat="server" Value="0" />
     <div class="row">
        <div class="col-lg-4" style="width: 500px; margin-left: 150px; margin-right: auto; margin-top: 80px; margin-bottom: auto;">
            <div class="panel panel-primary">
                 <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
                    <asp:Label ID="lblHeader" runat="server" Text="Add New Web Page Section"></asp:Label>
                      <a runat="server" href="~/UserInterface/UserAdministration/ManageWebPageSection.aspx">&nbsp;>>Manage WebPage Sections</a >
                </div>
                <div class="panel-heading">
                    <h4 style="text-align: center"><strong><asp:Label ID="lblHeadText" runat="server" Text="Add New Web Page Section"></asp:Label>
                    </strong></h4>
                </div>
                <table style="margin-left:38px; width: 450px;" ><br />
                    <tr>
                        
                        <td>Application: </td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="cmbApplication" runat="server" class="form-control" Style="width: 250px; height:auto"  required="required"></asp:DropDownList>
                           <%--<asp:RequiredFieldValidator ID="RqdValidatorcmbApplication" runat="server" ErrorMessage="Required" ControlToValidate="cmbApplication" InitialValue="0" style="margin-left:5px; color:#FF0000"></asp:RequiredFieldValidator>--%>
                              
                                 </div>
                           </td>
                    </tr>

                    <tr>

                        <td>Name: </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" class="form-control" MaxLength="20" Style="width: 250px;" required="required"></asp:TextBox>
                        </td>
                    </tr>

                    <tr style="height:50px">

                        <td>View Order: </td>
                        <td>
                            <asp:TextBox TextMode="Number" ToolTip="Accepts only Numbers" ID="txtViewOrder" MaxLength="5" runat="server" class="form-control" Style="width: 60px; "  required="required"></asp:TextBox>
                        </td>
                    </tr>

                    
                    <tr style="height: 40px">

                        <td>Status: </td>
                        <td style="vertical-align: bottom">
                            <div class="form-group">
                                <asp:DropDownList ID="cmbStatus" runat="server" class="form-control" Style="width: auto; height: 30px" required="required">
                                    <asp:ListItem Text="-Select-"  Value=""></asp:ListItem>
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Suspended" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Stopped" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RqdValidatorcmbStatus" runat="server" ErrorMessage="Required" ControlToValidate="cmbStatus" InitialValue="-1" Style="margin-left: 5px; color: #FF0000"  Display="Dynamic"></asp:RequiredFieldValidator>

                            </div>

                        </td>
                    </tr>
<tr style="height: 20px"><td colspan="3"></td></tr>
                    <tr>

                        <td></td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Text="ADD" class="btn btn-outline btn-success" Style="width: 100px" />
                            <asp:Button ID="btnCancel" OnClientClick="return resetForm();" runat="server" Text="CANCEL" class="btn btn-outline btn-danger" Style="width: 100px" UseSubmitBehavior="false" /> 

                             </td>
                    </tr>
                     <tr style="height: 30px"><td colspan="2"></td></tr>
                    <tr style="height: 20px"><td colspan="2">

                        <asp:Label ID="lblMsg" Font-Size="Larger" runat="server" Text="" CssClass="warning"></asp:Label>
                                             </td></tr>
                     
                    
                </table> 
         
            </div>
        </div>
         
    </div>

       <script type="text/javascript" language="javascript">

           validateTextboxForInputEntry('<%=txtViewOrder.ClientID%>', false, true, false);// for digits only
        
    </script>

</asp:Content>
