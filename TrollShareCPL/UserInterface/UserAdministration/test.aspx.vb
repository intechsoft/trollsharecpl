﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class test
    Inherits BaseUIPage


#Region "Variables"
    Private ObjUserRole As New UserRoles
    Private ObjWebPageSection As New WebPageSection
    Private ObjWebPages As New WebPages
    Private ObjApplicationType As New ApplicationType

    Dim Dr As mySqlDataReader

    Dim strPrivilage As String
    Dim p As String()
    Dim s As Char()

    Dim UID As Integer = 0
    Dim i As Integer

#End Region

#Region "Events"
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            BindDropDown()
            'listData()
        End If
    End Sub

    Protected Sub reportGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles reportGridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Dim Dr As mySqlDataReader
            Dim Dt As DataTable
            Dim gv As GridView
            gv = CType(e.Row.FindControl("reportGridViewInner"), GridView)
            Dim hd As HiddenField = CType(e.Row.FindControl("hdID"), HiddenField)
            'Dr = ObjWebPages.GetWebPages(CmbApplicationType.SelectedValue, hd.Value)
            Dt = ObjWebPages.GetWebPages(CmbApplicationType.SelectedValue, hd.Value)
            gv.DataSource = Dt
            gv.DataBind()
        End If
    End Sub

    Private Sub CmbRole_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbRole.DataBound
        CmbRole.Items.Insert(0, New ListItem("-------Select a Role ---------------", 0))
    End Sub

    Protected Sub CmbRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbRole.SelectedIndexChanged
        LoadApplications()
    End Sub

    Protected Sub linkADDRole_Click(ByVal sender As Object, ByVal e As EventArgs) Handles linkADDRole.Click
        Response.Redirect("AddRole.aspx")
    End Sub

    Protected Sub lnkManageRole_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkManageRole.Click
        Response.Redirect("ManageRole.aspx")
    End Sub

    Private Sub CmbApplicationType_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbApplicationType.DataBound
        CmbApplicationType.Items.Insert(0, New ListItem("-------Select a Application Type ---------------", 0))
    End Sub

    Protected Sub CmbApplicationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbApplicationType.SelectedIndexChanged
        listData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim gv As GridView
        Dim c As CheckBox
        Dim x As Integer
        Dim y As Integer
        Dim st As String = ""
        Dim PID As Integer
        For y = 0 To reportGridView.Rows.Count - 1
            gv = CType(reportGridView.Rows(y).FindControl("reportGridViewInner"), GridView)
            For x = 0 To gv.Rows.Count - 1
                c = CType(gv.Rows(x).FindControl("chk"), CheckBox)
                PID = CType(gv.Rows(x).FindControl("hdPID"), HiddenField).Value
                If c.Checked Then
                    If st <> "" Then st = st & ";"
                    st = st & PID
                End If
            Next
        Next

        Dim ObjApplicationTypeRoles As New ApplicationTypeRoles
        ObjApplicationTypeRoles.RoleID = CmbRole.SelectedValue
        ObjApplicationTypeRoles.ApplicationType = CmbApplicationType.SelectedValue
        ObjApplicationTypeRoles.Privilege = st
        ObjApplicationTypeRoles.Update()



        If ObjApplicationTypeRoles.ErrorMsg <> "" Then
            lblError.Text = ObjApplicationTypeRoles.ErrorMsg
        End If
    End Sub

#End Region

#Region "Procedures"

    Private Sub BindDropDown()
        Dr = ObjUserRole.GetUserRoles()
        CmbRole.DataSource = Dr
        CmbRole.DataValueField = "ID"
        CmbRole.DataTextField = "Name"
        CmbRole.DataBind()
        Dr.Close()



    End Sub


    Private Sub LoadApplications()
        Dr = ObjApplicationType.GetApplicationTypeList()
        CmbApplicationType.DataSource = Dr
        CmbApplicationType.DataValueField = "ID"
        CmbApplicationType.DataTextField = "Name"
        CmbApplicationType.DataBind()
        Dr.Close()
    End Sub
    Private Sub BindGrid()

        Dr = ObjWebPageSection.GetActiveWebPageSections(CmbApplicationType.SelectedValue)
        reportGridView.DataSource = Dr
        reportGridView.DataBind()
        Dr.Close()
    End Sub

    Private Sub listData()

        If CmbRole.SelectedValue <> 0 And CmbApplicationType.SelectedValue <> 0 Then
            strPrivilage = "0"
            Dr = ObjUserRole.GetUserRoles(CmbRole.SelectedValue, CmbApplicationType.SelectedValue)
            If Dr.Read() Then

                strPrivilage = Dr("Privilege")

            End If
            p = strPrivilage.Split(";")
            Dr.Close()
            BindGrid()
        End If

    End Sub

#End Region

#Region "Function"

    Public Function Bindchk(ByVal PageID As Integer) As Integer
        Bindchk = 0
        If strPrivilage <> "0" Then
            If Array.IndexOf(p, PageID.ToString) >= 0 Then Return 1
        End If
    End Function
#End Region


End Class