﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="AddContactUs.aspx.vb" Inherits="TrollShareCPL.AddContactUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
    <asp:HiddenField ID="hdnId" runat="server" Value="0" />
    <div class="row">

        <div class="col-lg-4" style="width: 600px; margin-left: 150px; margin-right: auto; margin-top: 10px; margin-bottom: auto;">
            <div class="panel panel-primary">
                 <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
                    <asp:Label ID="lblHeader" runat="server" Text="Add Contact Us"></asp:Label>
                    <a id="A1" runat="server" href="~/UserInterface/UserAdministration/ManageContactUs.aspx">&nbsp;>>Manage Contact Us</a>
                </div>
                <div class="panel-heading" style="height: 50px">

                    <h4 style="text-align: center"><strong>
                        <asp:Label ID="lblHeadText" runat="server" Text="Add Contact Us"></asp:Label>
                    </strong></h4>
                </div>
                <table style="margin-left: 38px; width: 450px; line-height: 40px;">
                    <br />

                      <tr>
                        
                        <td>Application: </td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="cmbApplication" runat="server" class="form-control" Style="width: 250px; height:auto" AutoPostBack="true" required="required">
                                </asp:DropDownList>
                             </div>
                    </td>
                    </tr>
                       <tr style="height: 20px">

                        <td>Company Name: </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr>

                    <tr style="height: 50px">

                        <td>Company Alt: </td>
                        <td>
                            <asp:TextBox ID="txtNameAlt" runat="server" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr>

                    <tr style="height: 50px"> 
                        <td>Phone Number: </td>
                        <td>
                            <asp:TextBox  ID="txtPhone" MaxLength="15" runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                        </td>
                    </tr>

                        <tr style="height: 50px"> 
                        <td>Fax: </td>
                        <td>
                            <asp:TextBox ID="txtFax" runat="server"  class="form-control" Style="width: 250px;" MaxLength="150" required="required"></asp:TextBox>
                          
                             </td>
                    </tr> 

                    <tr style="height: 20px"> 
                        <td>Email ID: </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" class="form-control" Style="width: 250px;" MaxLength="150" required="required"></asp:TextBox>
                        </td>
                    </tr>
                     
                    <tr style="height: 20px">

                        <td>WebSite URL: </td>
                        <td>
                            <asp:TextBox  ID="txtWebsite" MaxLength="5" runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                        </td>
                    </tr>

                    <tr style="height: 50px">

                        <td>Facebook: </td>
                        <td> 
                           <asp:TextBox  ID="txtFacebook"  runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                      
                        </td>
                    </tr>

                    
                    <tr style="height: 50px">

                        <td>Twitter: </td>
                        <td> 
                           <asp:TextBox  ID="txtTwitter" runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                      
                        </td>
                    </tr>

                    
                    <tr style="height: 50px">

                        <td>YouTube: </td>
                        <td> 
                           <asp:TextBox  ID="txtYouTube"  runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                      
                        </td>
                    </tr>

                    
                    <tr style="height: 50px">

                        <td>Google+: </td>
                        <td> 
                           <asp:TextBox  ID="txtGooglePlus" runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                      
                        </td>
                    </tr>

                    
                    <tr style="height: 50px">

                        <td>LinkedIn: </td>
                        <td> 
                           <asp:TextBox  ID="txtLinkedIn"  runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                      
                        </td>
                    </tr>

                    
                    <tr style="height: 50px">

                        <td>Instagram: </td>
                        <td> 
                           <asp:TextBox  ID="txtInstagram"  runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                      
                        </td>
                    </tr> 
                    <tr style="height: 60px">

                        <td>Address: </td>
                        <td>
                            <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" class="form-control" Style="width: 250px;" required="required"></asp:TextBox>
                        </td>
                    </tr>

                       <tr style="height: 50px">

                        <td>LastUpdatedDataSync: </td>
                        <td> 
                           <asp:TextBox  ID="txtLastUpdatedDataSync"  runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                      
                        </td>
                    </tr>

                    <tr>

                        <td></td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Text="ADD" class="btn btn-primary" Style="width: 100px" /> 
                            <%--<button type="submit" id="btnAdd" class="btn btn-primary" style="width:100px"><strong>ADD</strong></button>--%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnCancel" runat="server" Text="CANCEL" class="btn btn-danger" Style="width: 100px" UseSubmitBehavior="false" />
                        </td>
                    </tr>
                    <tr style="height: 20px">
                        <td colspan="2"></td>
                    </tr>
                    <tr style="height: 20px">
                        <td colspan="2">

                            <asp:Label ID="lblMsg" runat="server" Font-Size="Larger" Text="" CssClass="warning"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                    </tr>

                </table>

            </div>
        </div>   

    </div>


</asp:Content>
