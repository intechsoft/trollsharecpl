﻿Imports TrollShareBLL
Public Class ChangePassword
    Inherits BaseUIPage

#Region "==========variables============="
    Private objUserLogins As userlogins
#End Region

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        objUserLogins = New userlogins
        If objUserLogins.ChangeMyPassword(txtPassword.Text, txtConfirm.Text) Then
            lblMsg.Text = ""
        Else
            lblMsg.Text = "Current Password is invalid"
        End If
    End Sub
End Class