﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageWebPageSection1
    Inherits BaseUIPage

#Region "Variables"

    Private ObjappType As New ApplicationType
    Private dr As mySqlDataReader
    Private dt As DataTable
    Private objwebpage As New WebPages
    Private objWebSection As New WebPageSection
    'Private Enc As New AbhiQsEnc.AbhiQs

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            'FillApplication(Val(cmbApplicationList.SelectedIndex))
            FillApplication()
            FillSection(0)
            FillGridView()
        End If
    End Sub

    Private Sub cmbApplicationList_DataBound(sender As Object, e As EventArgs) Handles cmbApplicationList.DataBound
        cmbApplicationList.Items.Insert(0, New ListItem("--- Select Application ---", 0))
    End Sub

    Private Sub cmbWebpageSection_DataBound(sender As Object, e As EventArgs) Handles cmbWebpageSection.DataBound
        cmbWebpageSection.Items.Insert(0, New ListItem("--- Select Section ---", 0))
    End Sub

    Private Sub cmbApplicationList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbApplicationList.SelectedIndexChanged
        FillSection(Val(cmbApplicationList.SelectedValue))
        FillGridView(Val(cmbApplicationList.SelectedValue), Val(cmbWebpageSection.SelectedValue))
    End Sub

    Private Sub cmbWebpageSection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbWebpageSection.SelectedIndexChanged
        FillGridView(Val(cmbApplicationList.SelectedValue), Val(cmbWebpageSection.SelectedValue))
    End Sub

    Private Sub grdWebPages_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdWebPages.PageIndexChanging
        grdWebPages.PageIndex = e.NewPageIndex
        FillGridView(Val(cmbApplicationList.SelectedValue), Val(cmbWebpageSection.SelectedValue))
    End Sub
    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView(Val(cmbApplicationList.SelectedValue), Val(cmbWebpageSection.SelectedValue))
    End Sub
    Private Sub grdWebPages_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdWebPages.RowDataBound
        If DeletePrivilage Then
            grdWebPages.Columns(9).Visible = True
        Else
            grdWebPages.Columns(9).Visible = False
        End If
    End Sub

    Private Sub grdWebPages_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdWebPages.RowCommand
        Dim BeforeText As String = " "
        Dim AfterText As String = " "
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = "" 'Enc.WriteQS("id=" & id)
                Response.Redirect("AddWebPage.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objwebpage = New WebPages(id)
                With objwebpage
                    'BeforeText += " Application = " & .Application
                    BeforeText += "|Section=" & .SectionID
                    BeforeText += "|Title=" & .Title
                    BeforeText += "|FileName = " & .FileName
                    BeforeText += "|FilePath=" & .FilePath
                    BeforeText += "|SubSection = " & .SubSection
                    BeforeText += "|PageLevel=" & .PageLevel
                    BeforeText += "|ViewOrder = " & .ViewOrder
                    BeforeText += "|Parent=" & .Parent
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .Delete()
                    End If
                End With

                FillGridView(Val(cmbApplicationList.SelectedValue), Val(cmbWebpageSection.SelectedValue))
            End If
        End If
    End Sub

#End Region

#Region "Procedures"

    Private Sub FillApplication()
        dr = ObjappType.GetApplicationName
        cmbApplicationList.DataSource = dr
        cmbApplicationList.DataTextField = "Name"
        cmbApplicationList.DataValueField = "ID"
        cmbApplicationList.DataBind()
        dr.Close()
    End Sub

    Private Sub FillSection(appType As Integer)
        objWebSection = New WebPageSection
        dr = objWebSection.GetWebPageSection(appType)
        cmbWebpageSection.DataSource = dr
        cmbWebpageSection.DataTextField = "Name"
        cmbWebpageSection.DataValueField = "ID"
        cmbWebpageSection.DataBind()
        dr.Close()
    End Sub
    Private Sub FillGridView(Optional appType As Integer = 0, Optional secId As Integer = 0)
        If chkPaging.Checked = True Then
            grdWebPages.AllowPaging = True
        Else
            grdWebPages.AllowPaging = False
        End If
        objwebpage = New WebPages
        dt = objwebpage.GetWebPages(appType, secId)
        grdWebPages.DataSource = dt
        grdWebPages.DataBind()
    End Sub

#End Region

 
End Class