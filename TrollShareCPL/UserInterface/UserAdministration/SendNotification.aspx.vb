﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class SendNotification
    Inherits BaseUIPage


#Region "Variables"

    Private dr As mySqlDataReader
    Private objDeviceType As DeviceType
    Private objCustomer As CustomerMST 
    Private where As String = ""
    Private mode As Integer
    Private list As String
    Private chk As Boolean = False

#End Region
 
#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropdowns()
            'getWhereClause()
            'FillGrid()
            'cmbDeviceType.Attributes("disabled") = "disabled"
            'txtNumber.Attributes("disabled") = "disabled"
            'btnSearch.Attributes("disabled") = "disabled"
        End If 
    End Sub
 
    Private Sub cmbDeviceType_DataBound(sender As Object, e As EventArgs) Handles cmbDeviceType.DataBound
        cmbDeviceType.Items.Insert(0, New ListItem("--- Select Device Type---", ""))
        cmbDeviceType.Items.Insert(1, New ListItem(" All", -1))
    End Sub

    'Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
    '    FillGrid()
    'End Sub
    Private Sub cmbDeviceType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDeviceType.SelectedIndexChanged
        txtNumber.Text = "" 
        getWhereClause()
        FillGrid()
    End Sub

#End Region

#Region "Procedures"

    Private Sub FillDropdowns()

        objDeviceType = New DeviceType
        dr = objDeviceType.GetDevice()
        cmbDeviceType.DataSource = dr
        cmbDeviceType.DataTextField = "Name"
        cmbDeviceType.DataValueField = "ID"
        cmbDeviceType.DataBind()
        dr.Close() 
    End Sub

 
    Private Sub FillGrid()


        objCustomer = New CustomerMST
        'dr = objCustomer.GetCustomerForSendNotification(where)
        If dr.HasRows Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "changemode", "disabled(false);", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "changemode", "disabled(true);", True)
        End If
        chkCustomerList.DataSource = dr
        chkCustomerList.DataTextField = "Name"
        chkCustomerList.DataValueField = "ID"
        chkCustomerList.DataBind() 
        
        dr.Close()

        If chkCustomerList.Items.Count > 0 Then 
            lblMsg.Text = ""
        Else
            lblMsg.Text = "No Customers found"
        End If

    End Sub

    Private Sub getWhereClause()
        chkCustomerList.RepeatColumns = 6
        chk = False
        If txtNumber.Text.Trim <> "" Then
            If Val(txtNumber.Text.Trim) > 0 Then
                chkCustomerList.RepeatColumns = 1
                chk = True
                where = " and CM.ID = " & txtNumber.Text.Trim
            End If
        Else
            If cmbDeviceType.SelectedIndex = 0 Then
                where += " and 1=2"
            ElseIf cmbDeviceType.SelectedValue > 0 Then
                where += " and DT.ID=" & Val(cmbDeviceType.SelectedValue)
            End If
        End If
    End Sub

    Private Sub clearForm()
        cmbDeviceType.SelectedIndex = 0
        where = " and 1=2"
        FillGrid()
        lblMsg.Text = ""
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "changemode", "disabled(false);", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "clear", "resetForm(1);", True)
        'cmbDeviceType.Attributes("disabled") = "disabled"
        txtNumber.Text = "" 
        'txtNumber.Attributes("disabled") = "disabled"
        'cmbMode.SelectedIndex = 0
        txtMessage.Text = ""
        cmbDeviceType.SelectedIndex = 0


    End Sub

#End Region

    Private Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Dim custIDs(chkCustomerList.Items.Count) As String
        Dim count As Integer = 0 
        'mode = Val(cmbMode.SelectedValue())
        mode = NotificationMode.SelectedList
        If mode = NotificationMode.SelectedList Then
            For i = 0 To chkCustomerList.Items.Count - 1 
                If chkCustomerList.Items(i).Selected = True Then
                    custIDs(count) = chkCustomerList.Items(i).Value.ToString
                    count += 1
                    'If custIDs <> "" Then custIDs = custIDs & ","
                    'custIDs = custIDs & chkCustomerList.Items(i).Value.ToString
                End If
            Next
            ReDim Preserve custIDs(count)
        End If
        If mode = NotificationMode.SelectedList And count = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "NoCustomer", "alert('No customer selected');", True)
            Exit Sub
        End If

        Dim UserId As String = Session("UserID").ToString
        'Dim api As New CPLAPI.FetchServerForCplAPI
        'Dim auth As New CPLAPI.MobileAuthHeader
        'Dim response As New CPLAPI.NotificationResponse

        'With auth
        '    .CustomerID = AppSettings("CustomerID")
        '    .MacAddress = AppSettings("MacAddress") '"1q55Y/Azx4ltN8AAQYGzyDrMXXjZJ/2uDRhuXqdAMVqiB3JIUdS/YXFvSOnrHTgU" 'Local Encrypted Key1 which also known as Device data
        '    .Language = AppSettings("Language") '0 English 1 Arabic
        '    .AppTypeID = AppSettings("AppTypeID")
        '    .Version = AppSettings("Version")
        '    .WSDLKey = AppSettings("WSDLKey") 'Local Key Plain Text For Identifieng Version of WSDL
        '    .DeviceType = AppSettings("DeviceType")
        '    .DeviceID = DeviceTypeOrChannel.iPhone
        '    .MobileNumber = AppSettings("MobileNumber")
        '    .ChannelID = 2 'Channel.MobileApp
        'End With
        'api.MobileAuthHeaderValue = auth 
        'For i = 0 To count - 1 
        '    response = api.SendNotification(UserId, mode, custIDs(i), txtMessage.Text)
        '    If response.Status.Flag = 1 Then
        '        lblNotifStatus.Text = "Notification " & i + 1 & " of " & count & " sent."
        '    End If
        'Next
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "TakeAction", "TakeAnAction();", True)
        'clearForm()
        'GetAppSetings("")
    End Sub

    'Private Sub cmbMode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbMode.SelectedIndexChanged
    '    cmbDeviceType.SelectedIndex = 0
    '    getWhereClause()
    '    FillGrid()
    '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "changemode", "disabled(false);", True)
    '    If cmbMode.SelectedIndex > 0 And cmbMode.SelectedIndex < 4 Then
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "alert", "document.getElementById('ContentPlaceHolder1_btnSend').disabled = false;", True)
    '    End If

    '    'If Val(cmbMode.SelectedValue) = NotificationMode.SelectedList Then
    '    '    cmbDeviceType.Attributes.Remove("disabled")
    '    '    txtNumber.Attributes.Remove("disabled")
    '    '    btnSearch.Attributes.Remove("disabled")
    '    'Else
    '    '    cmbDeviceType.Attributes("disabled") = "disabled"
    '    '    txtNumber.Attributes("disabled") = "disabled"
    '    '    btnSearch.Attributes("disabled") = "disabled"
    '    'End If
    'End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If txtNumber.Text.Trim.Length > 0 Then 
            getWhereClause()
            FillGrid()
            cmbDeviceType.SelectedIndex = 0
            If chkCustomerList.Items.Count > 0 Then
                chkCustomerList.Items(0).Selected = chk
                lblMsg.Text = ""
            Else
                lblMsg.Text = "Invalid Customer ID"
            End If
        Else
            lblMsg.Text = "Please Fill Customer ID"
        End If
    End Sub
End Class