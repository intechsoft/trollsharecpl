﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageWebPageSection
    Inherits BaseUIPage

#Region "Variables"

    Private ObjappType As New ApplicationType
    Private dr As mySqlDataReader
    Private dt As DataTable
    Private objwebpage As New WebPages
    Private objWebSection As New WebPageSection
    'Private Enc As New AbhiQsEnc.AbhiQs
  

#End Region

#Region "Events"
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillGridView()
            FillApplication()
        End If
    End Sub

    Private Sub cmbApplicationList_DataBound(sender As Object, e As EventArgs) Handles cmbApplicationList.DataBound
        cmbApplicationList.Items.Insert(0, New ListItem("--- Select Application ---", 0))
    End Sub

    Private Sub cmbApplicationList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbApplicationList.SelectedIndexChanged
        FillGridView(Val(cmbApplicationList.SelectedValue))
    End Sub

    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView(Val(cmbApplicationList.SelectedValue))
    End Sub
    Private Sub grdWebPageSection_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdWebPageSection.PageIndexChanging
        grdWebPageSection.PageIndex = e.NewPageIndex
        FillGridView(Val(cmbApplicationList.SelectedValue))
    End Sub

    Private Sub grdWebPageSection_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdWebPageSection.RowDataBound
        If DeletePrivilage Then
            grdWebPageSection.Columns(7).Visible = True
        Else
            grdWebPageSection.Columns(7).Visible = False
        End If
    End Sub

    ' code for encrytpting a string

    Private Sub grdWebPageSection_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdWebPageSection.RowCommand
        Dim BeforeText As String = " "
        Dim AfterText As String = " "
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = "" 'Enc.WriteQS("id=" & id)
                Response.Redirect("AddWebPageSection.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objWebSection = New WebPageSection(id)
                With objWebSection
                    BeforeText += " applicationtype = " & .ApplicationType
                    BeforeText += "|Name=" & .Name
                    BeforeText += "|ViewOrder=" & .ViewOrder
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .Delete()
                    End If
                End With
                FillGridView(Val(cmbApplicationList.SelectedValue))
            End If
        End If
    End Sub

#End Region

#Region "Procedures"

    Private Sub FillApplication()
        dr = ObjappType.GetApplicationName
        cmbApplicationList.DataSource = dr
        cmbApplicationList.DataTextField = "Name"
        cmbApplicationList.DataValueField = "ID"
        cmbApplicationList.DataBind()
    End Sub

    Private Sub FillGridView(Optional appType As Integer = 0)
        If chkPaging.Checked = True Then
            grdWebPageSection.AllowPaging = True
        Else
            grdWebPageSection.AllowPaging = False
        End If
        objWebSection = New WebPageSection
        dt = objWebSection.GetWebPageSections(appType)
        grdWebPageSection.DataSource = dt
        grdWebPageSection.DataBind()
    End Sub

#End Region

   
End Class

