﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="ManagePrivilege.aspx.vb" Inherits="TrollShareCPL.ManagePrivilege" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <%--  <style type="text/css">
        .tblHeader
        {
            font-size: 11px;
            color: #fff;
            font-weight: Bold;
            font-family: verdana, arial, helvetica, sans-serif;
            text-decoration: none;
            background-color: #778888;
            text-align: center;
        }
        
        .tableFilter
        {
            border: 1px;
            background: #666699;
            width: 100%;
        }
        
        .textbox
        {
            border: 1px solid #b6b6b6;
            background-color: #ffffff;
            font-size: 11px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            color: #000;
            height: 18px;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:HiddenField ID="hdnAppRoleId" runat="server" Value="0" />

     <div class="col-lg-4 panel panel-primary" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; width: 99.9%; font-family: Arial; font-size: medium;">
        <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
            <asp:Label ID="lblHeader" runat="server" Text="Manage Privilage &nbsp;>> "></asp:Label>
                                              
                <asp:Label ID="Label7" runat="server"  Text="User Administration&gt;"></asp:Label>
                <a href="AddRole.aspx" >ADD Role</a>
                &nbsp;|
                <a href="ManageRole.aspx" >Manage Role</a>
         </div>
                         <div id="divFilters" align="center" style="border-bottom:1px solid navy; width:100%;height:70px;margin-top:15px; ">
            <table cellpadding="5" >
                <tr>
                    <td>
                       <asp:Label ID="Label3" runat="server"   Text="Role :" ForeColor="black" ></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label1" runat="server"   Text="Application :" ForeColor="black"  ></asp:Label>

                    </td>
                </tr>
                <tr>
                    <td>
                      <asp:DropDownList ID="CmbRole" runat="server" AutoPostBack="True" 
                                Width="200px">
                                <asp:ListItem Selected="True" Value="0">--- Select a Role --</asp:ListItem>
                            </asp:DropDownList>
                    </td>
                   <td>
                        <asp:DropDownList ID="CmbApplicationType" runat="server" AutoPostBack="True" Width="250px">
                                <asp:ListItem Selected="True" Value="0">--- Select an Application --</asp:ListItem>
                            </asp:DropDownList>
                   </td>
                </tr>

            </table>
                        
   </div>
                                             
                                
        <div style="margin: 10px 0px 0px 20px; width: 95%">
            
              <asp:GridView ID="reportGridView" class="table table-bordered" runat="server" AutoGenerateColumns="False"
                EmptyDataText="No Data Found" style="font-size:15px;">
                <HeaderStyle CssClass="tblHeader" BackColor="#428BCA" />
                <AlternatingRowStyle BackColor="WhiteSmoke" />
                  <%--   BorderColorlight="#000000" CellPadding="4" Font-Size="Smaller" ForeColor="#333333"
                    GridLines="None" Width="500px">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D"   Font-Bold="true" Font-Size="Small"
                        ForeColor="White" Wrap="false" />
                    <RowStyle BackColor="#F7F6F3" Font-Size="Medium" ForeColor="#333333" HorizontalAlign="left"
                        Wrap="false" />--%>
                   
                     <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>                                
                                <asp:LinkButton ID="lnkSection" runat="server">
                                    <asp:HiddenField ID="hdID" runat="server" Value='<%# Eval("ID") %>' />
                                    <%# Eval("Name") %>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:GridView ID="reportGridViewInner" runat="server" AutoGenerateColumns="false"
                                    BorderColor="#000000" BorderColordark="#FFFFFF" BorderColorlight="#000000" BorderWidth="1"
                                    CellPadding="0" CellSpacing="0" EmptyDataText="No Data Found" Font-Size="Smaller"
                                    ShowFooter="true" Width="550px" OnRowDataBound="reportGridViewInner_RowDataBound">
                                    <HeaderStyle BackColor="#5D7B9D" ForeColor="White" />
                                    <RowStyle Font-Size="X-Small" HorizontalAlign="center" Wrap="false" />
                                    <FooterStyle Font-Bold="true" ForeColor="red" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="Title" ItemStyle-Width="300px" />
                                        
                                        <asp:TemplateField HeaderText="Page View">
                                            <ItemTemplate>
                                                     <asp:HiddenField ID="hdPID" runat="server" Value='<%# Eval("ID") %>' />                                          
                                                <asp:CheckBox ID="chkview" runat="server"  /> 
                                            </ItemTemplate>
                                        </asp:TemplateField> 

                                         <asp:TemplateField HeaderText="Add">
                                            <ItemTemplate> 
                                                <asp:CheckBox ID="chkAdd" runat="server"   /> 
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate> 
                                                <asp:CheckBox ID="chkEdit" runat="server"   /> 
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate> 
                                                <asp:CheckBox ID="chkDelete" runat="server" /> 
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Details View">
                                            <ItemTemplate> 
                                                <asp:CheckBox ID="chkdtview" runat="server"   /> 
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
           
            <table>
               <tr>
            <td>
                &nbsp;
            </td>
        
        <tr>
            <td colspan="3">
                <img src="http://localhost:3937/images/pixel_black.jpg" style="width: 100%; height: 2px" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td colspan="2">
                 <asp:Button ID="btnSave" runat="server" Text="UPDATE" class="btn btn-primary" Style="width: 100px" />
               <%-- <asp:Button ID="btnSave" runat="server"  Text="Update" />--%>
                <asp:Label ID="lblError" runat="server"  ForeColor="#CC3300"></asp:Label>
            </td>
        </tr>
    </table>
                                </div>

</div>


</asp:Content>


