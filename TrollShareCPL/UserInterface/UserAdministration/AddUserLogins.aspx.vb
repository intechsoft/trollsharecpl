﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddUserLogins
    Inherits BaseUIPage

#Region "Variables"

    Private dr As mySqlDataReader
    Private dt As DataTable  
    Private objUserLogin As UserLogins
    Private objRole As UserRoles
    Private objUserTypes As UserType 


#End Region

#Region "Events"
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillForm() 
        End If
        If btnAdd.Text = "UPDATE" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DropRow", " document.getElementById('tblUser').deleteRow(8);", True)
        End If
        
    End Sub

    Private Sub cmbCompany_DataBound(sender As Object, e As EventArgs) Handles cmbCompany.DataBound
        cmbCompany.Items.Insert(0, New ListItem("-------Select Company----------", ""))
    End Sub
    Private Sub cmbRole_DataBound(sender As Object, e As EventArgs) Handles cmbRole.DataBound
        cmbRole.Items.Insert(0, New ListItem("-------Select Role--------", ""))
    End Sub

    Private Sub cmbUserType_DataBound(sender As Object, e As EventArgs) Handles cmbUserType.DataBound
        cmbUserType.Items.Insert(0, New ListItem("----Select User Type----", ""))
    End Sub

    Private Sub cmbEmployee_DataBound(sender As Object, e As EventArgs) Handles cmbEmployee.DataBound
        cmbEmployee.Items.Insert(0, New ListItem("-------Select Employee--------", ""))
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim UserId As Integer

        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then
                objUserLogin = New UserLogins
                With objUserLogin
                    .EmployeeID = Val(cmbEmployee.SelectedValue())
                    .CompanyID = Val(cmbCompany.SelectedValue())
                    .UserTypeId = Val(cmbUserType.SelectedValue())
                    .RoleID = Val(cmbRole.SelectedValue())
                    .LanguageOption = IIf(rbEnglish.Checked = True, "en", "ar")
                    .UserName = txtUserName.Text
                    .UserPwd = txtPassword.Text
                    .UserActualName = txtActualName.Text
                    .Status = Val(cmbStatus.SelectedValue())

                    UserId = .CheckExist(.UserName, 0)
                    If UserId <> 0 Then
                        .ErrorMsg = "Username '" & .UserName & "' Already Exist"
                    Else
                        Dim BeforeText As String = ""
                        Dim AfterText As String = " "
                        AfterText += " EmployeeID = " & Val(cmbEmployee.SelectedValue())
                        AfterText += "|CompanyID = " & Val(cmbCompany.SelectedValue())
                        AfterText += "|UserTypeId = " & Val(cmbUserType.SelectedValue())
                        AfterText += "|RoleID = " & Val(cmbRole.SelectedValue())
                        AfterText += "|LanguageOption = " & IIf(rbEnglish.Checked = True, "en", "ar")
                        AfterText += "|UserName = " & txtUserName.Text
                        AfterText += "|UserPwd = " & txtPassword.Text
                        AfterText += "|UserActualName = " & txtActualName.Text
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            .Insert()
                        End If
                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Added successfully"
                            Response.Redirect("ManageUserLogins.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Added successfully"
                        Response.Redirect("ManageUserLogins.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then
                objUserLogin = New UserLogins(Val(hdnUserId.Value()))
                With objUserLogin
                    Dim BeforeText As String = ""
                    BeforeText += " EmployeeID = " & .EmployeeID
                    BeforeText += "|CompanyID=" & .CompanyID
                    BeforeText += "|UserTypeId = " & .UserTypeId
                    BeforeText += "|RoleID=" & .RoleID
                    BeforeText += "|LanguageOption = " & .LanguageOption
                    BeforeText += "|UserName=" & .UserName
                    BeforeText += "|UserActualName = " & .UserActualName 
                    BeforeText += "|Status=" & .Status

                    '.EmployeeID = Val(cmbEmployee.SelectedValue())
                    '.CompanyID = Val(cmbCompany.SelectedValue())
                    '.UserTypeId = Val(cmbUserType.SelectedValue())
                    '.RoleID = Val(cmbRole.SelectedValue())
                    .LanguageOption = IIf(rbEnglish.Checked = True, "en", "ar")
                    .UserName = txtUserName.Text
                    .UserActualName = txtActualName.Text
                    .Status = Val(cmbStatus.SelectedValue())

                    UserId = .CheckExist(.UserName, Val(hdnUserId.Value()))
                    If UserId <> 0 Then
                        .ErrorMsg = "Username '" & .UserName & "' Already Exist"
                    Else
                        Dim AfterText As String = " "
                        AfterText += " EmployeeID = " & .EmployeeID
                        AfterText += "|CompanyID = " & .CompanyID
                        AfterText += "|UserTypeId = " & .UserTypeId
                        AfterText += "|RoleID = " & .RoleID
                        AfterText += "|LanguageOption = " & IIf(rbEnglish.Checked = True, "en", "ar")
                        AfterText += "|UserName = " & txtUserName.Text
                        AfterText += "|UserPwd = " & txtPassword.Text
                        AfterText += "|UserActualName = " & txtActualName.Text
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            .Update()
                        End If
                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg 
                            FillForm()
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Added successfully"
                            Response.Redirect("ManageUserLogins.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Added successfully"
                        Response.Redirect("ManageUserLogins.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated
            End If
        End If
    End Sub

#End Region

 
#Region "Procedures"

    Private Sub FillForm()
        Dim queryStr As String = ""
        objUserLogin = New UserLogins
        'Dim qsEnc As New AbhiQsEnc.AbhiQs
        'queryStr = Request.QueryString("Qs")   ' read query string 
        'hdnUserId.Value = Val(qsEnc.ReadQS(queryStr, "id")) 
        dr = objUserLogin.GetUserforEdit(Val(hdnUserId.Value))
        If Not dr Is Nothing Then
            If dr.Read Then 
                cmbCompany.SelectedValue() = IIf(dr("CompanyID") = 0, "", dr("CompanyID"))
                cmbUserType.SelectedValue() = IIf(dr("UserTypeId") = 0, "", dr("UserTypeId"))
                cmbRole.SelectedValue() = IIf(dr("RoleID") = 0, "", dr("RoleID"))
                cmbEmployee.SelectedValue() = IIf(dr("EmployeeID") = 0, "", dr("EmployeeID"))
                If dr("LanguageOption") = "en" Then
                    rbEnglish.Checked = True
                Else
                    rbArabic.Checked = True
                End If
                txtUserName.Text = dr("UserName") 
                txtActualName.Text = dr("UserActualName")
                cmbStatus.SelectedValue() = dr("Status")

                cmbCompany.Attributes("disabled") = "disabled"
                cmbUserType.Attributes("disabled") = "disabled"
                cmbRole.Attributes("disabled") = "disabled"
                cmbEmployee.Attributes("disabled") = "disabled"

                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update User Logins"
            End If
            dr.Close()
        End If 

    End Sub

    Private Sub FillDropDowns()
       
        '------------fill Role---------------
        objRole = New UserRoles
        dr = objRole.GetUserRoles()
        cmbRole.DataSource = dr
        cmbRole.DataTextField = "Name"
        cmbRole.DataValueField = "ID"
        cmbRole.DataBind()

        '------------fill User Type---------------
        objUserTypes = New UserType
        dt = objUserTypes.GetUserType()
        cmbUserType.DataSource = dt
        cmbUserType.DataTextField = "Name"
        cmbUserType.DataValueField = "ID"
        cmbUserType.DataBind()

    End Sub
 
#End Region


End Class