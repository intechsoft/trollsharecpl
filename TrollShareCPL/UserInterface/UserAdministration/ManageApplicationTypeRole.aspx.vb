﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageApplicationTypeRole
    Inherits BaseUIPage

#Region "Variables"
    Private ObjUserRole As New UserRoles 
    Private ObjApplicationType As New ApplicationType
    Private ObjApplicationTypeRoles As ApplicationTypeRoles
    Dim Dt As DataTable
    'Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
#End Region

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            BindGrid()
        End If
    End Sub

    Private Sub grdAppRoleType_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdAppRoleType.RowCommand
        Dim BeforeText As String = ""
        Dim AfterText As String = ""
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = "" 'Enc.WriteQS("id=" & id)
                Response.Redirect("ApplicationTypeRole.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                ObjApplicationTypeRoles = New ApplicationTypeRoles(id)
                With ObjApplicationTypeRoles
                    BeforeText += " RoleID = " & .RoleID
                    BeforeText += "|ApplicationType=" & .ApplicationType
                    BeforeText += "|Privilege=" & .Privilege
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .Delete()
                    End If
                End With
                BindGrid()
            End If
        End If
    End Sub
     

#Region "Procedures"
     
    Private Sub BindGrid()
        ObjApplicationTypeRoles = New ApplicationTypeRoles
        Dt = ObjApplicationTypeRoles.GetApplicationTypeRoles()
        grdAppRoleType.DataSource = Dt
        grdAppRoleType.DataBind() 
    End Sub

#End Region

    
End Class