﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master"  CodeBehind="AddFeedBackMsg.aspx.vb" Inherits="TrollShareCPL.AddFeedBackMsg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
    <asp:HiddenField ID="hdnId" runat="server" Value="0" />
    <div class="row">

        <div class="col-lg-4" style="width: 600px; margin-left: 150px; margin-right: auto; margin-top: 10px; margin-bottom: auto;">
            <div class="panel panel-primary">
                 <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
                    <asp:Label ID="lblHeader" runat="server" Text="Add Contact Us"></asp:Label>
                    <a id="A1" runat="server" href="~/UserInterface/UserAdministration/ManageFeedBackMsg.aspx">&nbsp;>>Manage FeedBack Message</a>
                </div>
                <div class="panel-heading" style="height: 50px">

                    <h4 style="text-align: center"><strong>
                        <asp:Label ID="lblHeadText" runat="server" Text="Add FeedBack Msg"></asp:Label>
                    </strong></h4>
                </div>
                <table style="margin-left: 38px; width: 450px; line-height: 40px;">
                    <br />

                      <tr>
                        
                        <td>Customer: </td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="cmbCustomer" runat="server" class="form-control" Style="width: 250px; height:auto" AutoPostBack="true" required="required">
                                </asp:DropDownList>
                             </div>
                    </td>
                    </tr>
                       <tr style="height: 20px">

                        <td>Name: </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr>

                    <tr style="height: 50px">

                        <td>Mobile Number: </td>
                        <td>
                            <asp:TextBox ID="txtMobileNumber" runat="server" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr>

                    <tr style="height: 50px"> 
                        <td>EmailID: </td>
                        <td>
                            <asp:TextBox  ID="txtEmailID" MaxLength="15" runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                        </td>
                    </tr>

                        <tr style="height: 50px"> 
                        <td>Message: </td>
                        <td>
                            <asp:TextBox ID="txtMessage"  TextMode="MultiLine" runat="server"  class="form-control" Style="width: 250px;" MaxLength="150" required="required"></asp:TextBox>
                          
                             </td>
                    </tr> 

                    <tr style="height: 20px"> 
                        <td>Message Time: </td>
                        <td>
                            <asp:TextBox ID="txtMessageTime" runat="server" class="form-control" Style="width: 250px;" MaxLength="150" required="required"></asp:TextBox>
                        </td>
                    </tr>
                     
                    <tr style="height: 20px">

                        <td>Remarks: </td>
                        <td>
                            <asp:TextBox  ID="txtRemarks" TextMode="MultiLine" runat="server" class="form-control" Style="width: 250px;" MaxLength="150" required="required"></asp:TextBox>
                        </td>
                    </tr>

                    <tr style="height: 50px">

                        <td>Action Time: </td>
                        <td> 
                           <asp:TextBox  ID="txtActionTime"  runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                      
                        </td>
                    </tr>

                    
                    <tr style="height: 50px">

                        <td>Action Taken By: </td>
                        <td> 
                           <asp:TextBox  ID="txtActionTakenBy" runat="server" class="form-control" Style="width: 250px; height: 32px" required="required"></asp:TextBox>
                      
                        </td>
                    </tr>
                     <tr style="height: 40px">

                        <td>Status: </td>
                        <td style="vertical-align: bottom">
                            <div class="form-group">
                                <asp:DropDownList ID="cmbStatus" runat="server" class="form-control" Style="width: auto; height: 30px" required="required">
                                    <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Suspended" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Stopped" Value="3"></asp:ListItem>
                                </asp:DropDownList>

                            </div>

                        </td>
                    </tr>

                    <tr>

                        <td></td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Text="ADD" class="btn btn-primary" Style="width: 100px" /> 
                            <%--<button type="submit" id="btnAdd" class="btn btn-primary" style="width:100px"><strong>ADD</strong></button>--%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <%--  <asp:Button ID="btnCancel" runat="server" Text="CANCEL" class="btn btn-danger" Style="width: 100px" UseSubmitBehavior="false" />--%>
                        </td>
                    </tr>
                    <tr style="height: 20px">
                        <td colspan="2"></td>
                    </tr>
                    <tr style="height: 20px">
                        <td colspan="2">

                            <asp:Label ID="lblMsg" runat="server" Font-Size="Larger" Text="" CssClass="warning"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                    </tr>

                </table>

            </div>
        </div>   

    </div>


</asp:Content>
