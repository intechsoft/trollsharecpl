﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageApplicationType
    Inherits BaseUIPage

#Region "Variables"

    Private ObjappType As New ApplicationType
    Private dr As mySqlDataReader
    Private dt As DataTable
    Private objApp As ApplicationType
    'Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load 
        If IsPostBack = False Then 
            FillGridView() 
        End If 
    End Sub

    Private Sub grdAppType_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdAppType.PageIndexChanging
        grdAppType.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub

    Private Sub grdAppType_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdAppType.RowCommand
        Dim BeforeText As String = ""
        Dim AfterText As String = ""
        Dim id As Integer = Convert.ToInt32(e.CommandArgument) 
        If e.CommandName = "Edit" Then
            If EditPrivilage Then 
                Dim Encrypted As String = "" ' Enc.WriteQS("id=" & id) 
                Response.Redirect("AddApplicationType.aspx?Qs=" + Encrypted)
            End If
        End If 
        If e.CommandName = "RowDelete" Then 
            If DeletePrivilage Then 
                objApp = New ApplicationType(id)
                With objApp 
                    BeforeText += " Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .Delete()
                    End If
                End With 
                FillGridView() 
            End If 
        End If

    End Sub

    Private Sub grdAppType_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdAppType.RowDataBound
        If DeletePrivilage Then
            grdAppType.Columns(6).Visible = True
        Else
            grdAppType.Columns(6).Visible = False 
        End If
    End Sub 

#End Region

#Region "Procedures"

    Private Sub FillGridView() 
        objApp = New ApplicationType
        dt = objApp.GetApplicationType()
        grdAppType.DataSource = dt
        grdAppType.DataBind() 
    End Sub

#End Region

End Class