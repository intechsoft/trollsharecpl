﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManagePrivilege
    Inherits BaseUIPage


#Region "Variables"
    Private ObjUserRole As New UserRoles
    Private ObjWebPageSection As New WebPageSection
    Private ObjWebPages As New WebPages
    Private ObjApplicationType As New ApplicationType
    Private ObjApplicationTypeRoles As ApplicationTypeRoles
    Dim Dr As mySqlDataReader

    Dim strPrivilage As String
    Dim p As String()
    Dim s As Char()

    Dim UID As Integer = 0
    Dim i As Integer

#End Region

#Region "Events"
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            BindDropDown()
            'listData()
        End If
    End Sub

    Protected Sub reportGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles reportGridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            ' Dim Dr As mySqlDataReader
            Dim dt As DataTable
            Dim gv As GridView
            gv = CType(e.Row.FindControl("reportGridViewInner"), GridView)
            Dim hd As HiddenField = CType(e.Row.FindControl("hdID"), HiddenField)
            dt = ObjWebPages.GetWebPages(CmbApplicationType.SelectedValue, hd.Value)
            gv.DataSource = dt
            gv.DataBind() 
        End If
    End Sub

    Private Sub CmbRole_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbRole.DataBound
        CmbRole.Items.Insert(0, New ListItem("-------Select a Role ---------------", 0))
    End Sub

    Protected Sub CmbRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbRole.SelectedIndexChanged
        LoadApplications()
        BindGrid()
    End Sub

    'Protected Sub linkADDRole_Click(ByVal sender As Object, ByVal e As EventArgs) Handles linkADDRole.Click
    '    Response.Redirect("AddRole.aspx")
    'End Sub

    'Protected Sub lnkManageRole_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkManageRole.Click
    '    Response.Redirect("ManageRole.aspx")
    'End Sub

    Private Sub CmbApplicationType_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmbApplicationType.DataBound
        CmbApplicationType.Items.Insert(0, New ListItem("-------Select a Application Type ---------------", 0))
    End Sub

    Protected Sub CmbApplicationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CmbApplicationType.SelectedIndexChanged
        listData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim gv As GridView
        ' Dim c As CheckBox
        Dim x As Integer
        Dim y As Integer
        Dim st As String = ""
        Dim PID As Integer
        For y = 0 To reportGridView.Rows.Count - 1
            gv = CType(reportGridView.Rows(y).FindControl("reportGridViewInner"), GridView)
            For x = 0 To gv.Rows.Count - 1
                ' c = CType(gv.Rows(x).FindControl("chk"), CheckBox)
                PID = CType(gv.Rows(x).FindControl("hdPID"), HiddenField).Value

                Dim chkview As CheckBox = CType(gv.Rows(x).FindControl("chkview"), CheckBox)
                Dim chkAdd As CheckBox = CType(gv.Rows(x).FindControl("chkAdd"), CheckBox)
                Dim chkEdit As CheckBox = CType(gv.Rows(x).FindControl("chkEdit"), CheckBox)
                Dim chkDelete As CheckBox = CType(gv.Rows(x).FindControl("chkDelete"), CheckBox)
                Dim chkdtview As CheckBox = CType(gv.Rows(x).FindControl("chkdtview"), CheckBox)

                If chkview.Checked Then
                    If st <> "" Then st = st & ";"
                    st = st & PID & "|" & IIf(chkview.Checked, 1, 0) & "," & IIf(chkAdd.Checked, 1, 0) & "," & IIf(chkEdit.Checked, 1, 0) & "," & IIf(chkDelete.Checked, 1, 0) & "," & IIf(chkdtview.Checked, 1, 0)
                End If
            Next
        Next

        ObjApplicationTypeRoles = New ApplicationTypeRoles(Val(hdnAppRoleId.Value()))
        With ObjApplicationTypeRoles
            .RoleID = CmbRole.SelectedValue()
            .ApplicationType = CmbApplicationType.SelectedValue()
            .Privilege = st
            .Update()
        End With
         

        If ObjApplicationTypeRoles.ErrorMsg <> "" Then
            lblError.Text = ObjApplicationTypeRoles.ErrorMsg
        End If
    End Sub
#End Region

#Region "Procedures"

    Private Sub BindDropDown()
        Dr = ObjUserRole.GetUserRoles()
        CmbRole.DataSource = Dr
        CmbRole.DataValueField = "ID"
        CmbRole.DataTextField = "Name"
        CmbRole.DataBind()
        Dr.Close() 
    End Sub 
    Private Sub LoadApplications()
        Dr = ObjApplicationType.GetApplicationTypeList()
        CmbApplicationType.DataSource = Dr
        CmbApplicationType.DataValueField = "ID"
        CmbApplicationType.DataTextField = "Name"
        CmbApplicationType.DataBind()
        Dr.Close()
    End Sub
    Private Sub BindGrid()

        Dr = ObjWebPageSection.GetActiveWebPageSections(CmbApplicationType.SelectedValue)
        reportGridView.DataSource = Dr
        reportGridView.DataBind()
        Dr.Close()
    End Sub

    Private Sub listData()

        If CmbRole.SelectedValue <> 0 And CmbApplicationType.SelectedValue <> 0 Then
            strPrivilage = "0"
            Dr = ObjUserRole.GetUserRoles(CmbRole.SelectedValue, CmbApplicationType.SelectedValue)
            If Dr.Read() Then

                strPrivilage = Dr("Privilege")
                hdnAppRoleId.Value = Dr("AppTypeRoleID")
            End If
            p = strPrivilage.Split(";")
            Dr.Close()
            BindGrid()
        End If

    End Sub

#End Region

#Region "Function"
     
    Public Function Bindchk(ByVal PageID As Integer) As Integer
        Bindchk = 0
        Dim webId As String = ""
        Dim pageIndex As Integer = -1
        If strPrivilage <> "0" Then
            For i As Integer = 0 To p.Length - 1
                Dim pagestr = p(i)
                If pagestr.Contains(PageID.ToString & "|") Then

                End If

            Next
            If Array.IndexOf(p, PageID.ToString & "|1,1,1,1") >= 0 Then

            End If
            Return 1
        End If
    End Function
#End Region

   
    Protected Sub reportGridViewInner_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chkview As CheckBox = CType(e.Row.FindControl("chkview"), CheckBox)
            Dim chkAdd As CheckBox = CType(e.Row.FindControl("chkAdd"), CheckBox)
            Dim chkEdit As CheckBox = CType(e.Row.FindControl("chkEdit"), CheckBox)
            Dim chkDelete As CheckBox = CType(e.Row.FindControl("chkDelete"), CheckBox)
            Dim chkdtview As CheckBox = CType(e.Row.FindControl("chkdtview"), CheckBox)
            Dim hdPID As HiddenField = CType(e.Row.FindControl("hdPID"), HiddenField)
            Dim pageIndex As Integer = -1
            If strPrivilage <> "0" Then
                For i As Integer = 0 To p.Length - 1
                    Dim pagestr = p(i)
                    If pagestr.StartsWith(hdPID.Value & "|") Then
                        Dim prevValues As String() = pagestr.Split("|,")
                        prevValues = prevValues(1).Split(",")
                        chkview.Checked = IsNullValue(prevValues(0), "0")
                        chkAdd.Checked = IsNullValue(prevValues(1), "0")
                        chkEdit.Checked = IsNullValue(prevValues(2), "0")
                        chkDelete.Checked = IsNullValue(prevValues(3), "0")
                        If prevValues.Length >= 5 Then
                            chkdtview.Checked = IsNullValue(prevValues(4), "0")
                        End If
                    End If
                Next 
            End If 
        End If
    End Sub 
End Class