﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageContactUs
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objContactus As contactus
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillGridView()
        End If
    End Sub

    Private Sub cmbAppicationType_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAppicationType.DataBound
        cmbAppicationType.Items.Insert(0, New ListItem("-------Select a Appication Type -------", 0))
    End Sub

    Private Sub grdContact_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdContact.PageIndexChanging
        grdContact.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub
    Private Sub grdContact_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdContact.RowDataBound

        If DeletePrivilage Then
            grdContact.Columns(7).Visible = True
        Else
            grdContact.Columns(7).Visible = False
        End If
    End Sub

    Private Sub grdServiceType_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdContact.RowCommand

        Dim BeforeText As String = ""
        Dim AfterText As String = ""

        id = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = Enc.WriteQS("id=" & id)                ' code for encrytpting a strings
                Response.Redirect("AddContactUS.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objContactus = New contactus(id)
                With objContactus

                    BeforeText += "CompanyName = " & .CompanyName
                    BeforeText += "|CompanyNameAlt=" & .CompanyNameAlt
                    BeforeText += "|PhoneNumber=" & .PhoneNumber
                    BeforeText += "|Fax = " & .Fax
                    BeforeText += "|EmailID = " & .EmailID
                    BeforeText += "|Website = " & .Website
                    BeforeText += "|Facebook = " & .Facebook
                    BeforeText += "|Twitter = " & .Twitter
                    BeforeText += "|Youtube = " & .YouTube
                    BeforeText += "|GooglePlus = " & .GooglePlus
                    BeforeText += "|LinkedIn = " & .LinkedIn
                    BeforeText += "|Instagram = " & .Instagram
                    BeforeText += "|LastUpdatedDateSync = " & .LastUpdatedDataSync
                    BeforeText += "|Address = " & .Address
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
            End If
            FillGridView()
        End If

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()

        If chkPaging.Checked = True Then
            grdContact.AllowPaging = True
        Else
            grdContact.AllowPaging = False
        End If
        objContactus = New contactus
        dt = objContactus.GetContact()
        grdContact.DataSource = dt
        grdContact.DataBind()

    End Sub

    Private Sub FillDropDowns()
        Dim objApplicationtype As New applicationtype
        dr = objApplicationtype.GetApplicationName()
        cmbAppicationType.DataSource = dr
        cmbAppicationType.DataValueField = "ID"
        cmbAppicationType.DataTextField = "Name"
        cmbAppicationType.DataBind()
        dr.Close()
    End Sub

#End Region

    Private Sub cmbAppicationType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAppicationType.SelectedIndexChanged
        objContactus = New contactus
        If chkPaging.Checked = True Then
            grdContact.AllowPaging = True
        Else
            grdContact.AllowPaging = False
        End If

        If cmbAppicationType.SelectedValue > 0 Then
            dt = objContactus.GetContactUs(cmbAppicationType.SelectedValue)
        Else
            dt = objContactus.GetContact()
        End If

        grdContact.DataSource = dt
        grdContact.DataBind()
    End Sub
End Class