﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageRole
    Inherits BaseUIPage


#Region "Variables"

    Private ObjappType As New ApplicationType
    Private dr As mySqlDataReader
    Private dt As DataTable
    Private objRole As UserRoles
    'Private Enc As New AbhiQsEnc.AbhiQs

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillGridView()
        End If
    End Sub

    Private Sub grdRole_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdRole.PageIndexChanging
        grdRole.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub
    Private Sub grdRole_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdRole.RowDataBound
        If DeletePrivilage Then
            grdRole.Columns(5).Visible = True
        Else
            grdRole.Columns(5).Visible = False
        End If
    End Sub

    ' code for encrytpting a string

    Private Sub grdRole_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdRole.RowCommand
        Dim BeforeText As String = ""
        Dim AfterText As String = ""
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = "" 'Enc.WriteQS("id=" & id)
                Response.Redirect("AddRole.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objRole = New UserRoles(id)
                With objRole
                    BeforeText += " Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .Delete()
                    End If
                End With
                FillGridView()
            End If
        End If
    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()
        If chkPaging.Checked = True Then
            grdRole.AllowPaging = True
        Else
            grdRole.AllowPaging = False
        End If
        objRole = New UserRoles
        dt = objRole.GetUserRole()
        grdRole.DataSource = dt
        grdRole.DataBind()
    End Sub

#End Region

 
End Class