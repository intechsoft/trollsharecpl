﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ApplicationTypeRole
    Inherits BaseUIPage

#Region "-------------variables-----------------"
    Private objRole As UserRoles
    Dim dr As mySqlDataReader
    Private ObjApplicationType As New ApplicationType
    Private ObjApplicationTypeRoles As ApplicationTypeRoles
#End Region
#Region "----------Events ----------"
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            LoadRole()
            LoadApplications()
            FillForm()
        End If
    End Sub

    Private Sub cmbApplication_DataBound(sender As Object, e As EventArgs) Handles cmbApplication.DataBound
        cmbApplication.Items.Insert(0, New ListItem("----- Select Application -----", ""))
    End Sub

    Private Sub cmbRole_DataBound(sender As Object, e As EventArgs) Handles cmbRole.DataBound
        cmbRole.Items.Insert(0, New ListItem("----- Select Role -----", ""))
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim AppRoleId As Integer = 0
        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then
                ObjApplicationTypeRoles = New ApplicationTypeRoles()
                With ObjApplicationTypeRoles
                    .RoleID = Val(cmbRole.SelectedValue())
                    .ApplicationType = Val(cmbApplication.SelectedValue())
                    .Status = Val(cmbStatus.SelectedValue)
                    .Privilege = ""
                    AppRoleId = .CheckExist(.RoleID, .ApplicationType, 0)
                    If AppRoleId <> 0 Then
                        .ErrorMsg = " Already Exist"
                    Else
                        Dim BeforeText As String = ""
                        Dim AfterText As String = " "
                        AfterText += " RoleID = " & .RoleID
                        AfterText += "|ApplicationType = " & .ApplicationType
                        AfterText += "|Status = " & .Status
                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            .Insert()
                        End If
                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = " added successfully"
                            Response.Redirect("ManageApplicationTypeRole.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "added successfully"
                        Response.Redirect("ManageApplicationTypeRole.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then
                 
                ObjApplicationTypeRoles = New ApplicationTypeRoles(Val(hdnAppTypeRoleId.Value))
                With ObjApplicationTypeRoles
                    Dim BeforeText As String = ""
                    BeforeText += " RoleID = " & .RoleID
                    BeforeText += "|ApplicationType=" & .ApplicationType
                    BeforeText += "|Status=" & .Status 
                    .Status = Val(cmbStatus.SelectedValue) 
                    AppRoleId = .CheckExist(.RoleID, .ApplicationType, Val(hdnAppTypeRoleId.Value))
                    If AppRoleId <> 0 Then
                        .ErrorMsg = " Already Exist"
                    Else
                        Dim AfterText As String = " "
                        AfterText += " RoleID = " & .RoleID
                        AfterText += "|ApplicationType = " & .ApplicationType
                        AfterText += "|Status = " & .Status
                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            .Update()
                        End If
                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Updated successfully"
                            Response.Redirect("ManageApplicationTypeRole.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Updated successfully"
                        Response.Redirect("ManageApplicationTypeRole.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated

            End If
        End If

    End Sub

#End Region


#Region "Procedures"

    Private Sub LoadRole()
        objRole = New UserRoles()
        dr = objRole.GetUserRoles()
        cmbRole.DataSource = dr
        cmbRole.DataValueField = "ID"
        cmbRole.DataTextField = "Name"
        cmbRole.DataBind()
        dr.Close()
    End Sub
    Private Sub LoadApplications()
        ObjApplicationType = New ApplicationType
        dr = ObjApplicationType.GetApplicationTypeList()
        cmbApplication.DataSource = dr
        cmbApplication.DataValueField = "ID"
        cmbApplication.DataTextField = "Name"
        cmbApplication.DataBind()
        dr.Close()
    End Sub

    Private Sub FillForm()
        Dim queryStr As String = ""
        Dim AppRoleId As Integer 
        'Dim qsEnc As New AbhiQsEnc.AbhiQs
        'queryStr = Request.QueryString("Qs")
        'AppRoleId = Val(qsEnc.ReadQS(queryStr, "id"))
        hdnAppTypeRoleId.Value = AppRoleId
        If AppRoleId > 0 Then
            ObjApplicationTypeRoles = New ApplicationTypeRoles(AppRoleId)
            With ObjApplicationTypeRoles
                cmbApplication.SelectedValue() = IsNullValue(.ApplicationType, "")
                cmbRole.SelectedValue() = IsNullValue(.RoleID, "")
                cmbStatus.SelectedValue() = IsNullValue(.Status, "")
                cmbApplication.Attributes("disabled") = "disabled"
                cmbRole.Attributes("disabled") = "disabled"
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Application Type Role"
            End With
             
        End If 
    End Sub
#End Region


End Class