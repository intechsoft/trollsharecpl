﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageUserType
    Inherits BaseUIPage


#Region "Variables"

    Private ObjappType As New ApplicationType
    Private dr As mySqlDataReader
    Private dt As DataTable
    Dim objType As UserType
    'Private Enc As New AbhiQsEnc.AbhiQs

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillGridView()
        End If
    End Sub

    Private Sub grdUserType_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdUserType.PageIndexChanging
        grdUserType.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub
    Private Sub grdUserType_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdUserType.RowDataBound
        If DeletePrivilage Then
            grdUserType.Columns(6).Visible = True
        Else
            grdUserType.Columns(6).Visible = False
        End If
    End Sub

    ' code for encrytpting a string

    Private Sub grdUserType_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdUserType.RowCommand
        Dim BeforeText As String = ""
        Dim AfterText As String = ""
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)

        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = "" 'Enc.WriteQS("id=" & id)
                Response.Redirect("AddUserType.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objType = New UserType(id)
                With objType
                    BeforeText += " Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .Delete()
                    End If
                End With
                FillGridView()
            End If
        End If

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()
        If chkPaging.Checked = True Then
            grdUserType.AllowPaging = True
        Else
            grdUserType.AllowPaging = False
        End If
        objType = New UserType
        dt = objType.GetUserType()
        grdUserType.DataSource = dt
        grdUserType.DataBind()
    End Sub

#End Region

  
End Class