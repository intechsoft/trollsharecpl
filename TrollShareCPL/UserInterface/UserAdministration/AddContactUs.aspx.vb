﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddContactUs
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objAppType As applicationtype
    Private objContact As contactus

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        txtLastUpdatedDataSync.Attributes.Add("onclick", "NewCssCal('" & txtLastUpdatedDataSync.ClientID & "','ddmmmyyyy','arrow',false,24,false)")
        If IsPostBack = False Then
            FillDropDowns()
            FillForm()
        End If
    End Sub

    Private Sub cmbApplication_DataBound(sender As Object, e As EventArgs) Handles cmbApplication.DataBound
        cmbApplication.Items.Insert(0, New ListItem("-------Select Application Type ---------------", ""))
    End Sub
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        If btnAdd.Text = "ADD" Then

            If AddPrivilage Then ' Checking adding privilage of current user

                objContact = New contactus

                With objContact

                    .AppID = Val(cmbApplication.SelectedValue())
                    .CompanyName = txtName.Text
                    .CompanyNameAlt = txtNameAlt.Text
                    .PhoneNumber = txtPhone.Text
                    .Fax = txtFax.Text
                    .EmailID = txtEmail.Text
                    .Website = txtWebsite.Text
                    .Facebook = txtFacebook.Text
                    .Twitter = txtTwitter.Text
                    .YouTube = txtYouTube.Text
                    .GooglePlus = txtGooglePlus.Text
                    .LinkedIn = txtLinkedIn.Text
                    .Instagram = txtInstagram.Text
                    .LastUpdatedDataSync = txtLastUpdatedDataSync.Text
                    .Address = txtAddress.Text

                    '    messageid = .CheckExist(.ServiceID, 0)

                    'If messageid <> 0 Then
                    '    .ErrorMsg = "A Message Already Active for This Service"

                    'Else
                    Dim BeforeText As String = ""
                    Dim AfterText As String = "AppID = " & Val(cmbApplication.SelectedValue())
                    AfterText += "|CompanyName = " & txtName.Text
                    AfterText += "|CompanyNameAr = " & txtNameAlt.Text
                    AfterText += "|PhoneNumber = " & txtPhone.Text
                    AfterText += "|Fax = " & txtFax.Text
                    AfterText += "|EmailID = " & txtEmail.Text
                    AfterText += "|Website = " & txtWebsite.Text
                    AfterText += "|Facebook = " & txtFacebook.Text
                    AfterText += "|Twitter = " & txtTwitter.Text
                    AfterText += "|YouTube = " & txtYouTube.Text
                    AfterText += "|GooglePlus = " & txtGooglePlus.Text
                    AfterText += "|LinkedIn = " & txtLinkedIn.Text
                    AfterText += "|Instagram = " & txtInstagram.Text
                    AfterText += "|LastUpdatedDataSync = " & txtLastUpdatedDataSync.Text
                    AfterText += "|Address = " & txtAddress.Text

                    If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                        .insertMember()
                    End If
                    'End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Added successfully"
                            Response.Redirect("ManageContactUs.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Added successfully"
                        Response.Redirect("ManageContactUs.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else

            If EditPrivilage Then

                objContact = New contactus(Val(hdnId.Value()))
                Dim BeforeText As String = ""
                With objContact

                    BeforeText += "AppID = " & .AppID
                    BeforeText += "|CompanyName=" & .CompanyName
                    BeforeText += "|CompanyNameAr=" & .CompanyNameAlt
                    BeforeText += "|PhoneNumber=" & .PhoneNumber
                    BeforeText += "|Fax=" & .Fax
                    BeforeText += "|EmailID=" & .EmailID
                    BeforeText += "|Website=" & .Website
                    BeforeText += "|Facebook=" & .Facebook
                    BeforeText += "|Twitter=" & .Twitter
                    BeforeText += "|YouTube=" & .YouTube
                    BeforeText += "|GooglePlus=" & .GooglePlus
                    BeforeText += "|LinkedIn=" & .LinkedIn
                    BeforeText += "|Instagram=" & .Instagram
                    BeforeText += "|LastUpdatedDataSync=" & .LastUpdatedDataSync
                    BeforeText += "|Address=" & .Address

                    .AppID = Val(cmbApplication.SelectedValue())
                    .CompanyName = txtName.Text
                    .CompanyNameAlt = txtNameAlt.Text
                    .PhoneNumber = txtPhone.Text
                    .Fax = txtFax.Text
                    .EmailID = txtEmail.Text
                    .Website = txtWebsite.Text
                    .Facebook = txtFacebook.Text
                    .Twitter = txtTwitter.Text
                    .YouTube = txtYouTube.Text
                    .GooglePlus = txtGooglePlus.Text
                    .LinkedIn = txtLinkedIn.Text
                    .Instagram = txtInstagram.Text
                    .LastUpdatedDataSync = txtLastUpdatedDataSync.Text
                    .Address = txtAddress.Text

                    'If Val(cmbService.SelectedValue()) > 0 Then
                    '    messageid = .CheckExist(.ServiceID, Val(hdnId.Value()))
                    '    If messageid <> 0 Then
                    '        .ErrorMsg = " A Message Already Active for This Service"
                    '    End If
                    'Else
                    Dim AfterText As String = "AppID = " & Val(cmbApplication.SelectedValue())
                    AfterText += "|CompanyName = " & txtName.Text
                    AfterText += "|CompanyNameAr = " & txtNameAlt.Text
                    AfterText += "|PhoneNumber = " & txtPhone.Text
                    AfterText += "|Fax = " & txtFax.Text
                    AfterText += "|EmailID = " & txtEmail.Text
                    AfterText += "|Website = " & txtWebsite.Text
                    AfterText += "|Facebook = " & txtFacebook.Text
                    AfterText += "|Twitter = " & txtTwitter.Text
                    AfterText += "|YouTube = " & txtYouTube.Text
                    AfterText += "|GooglePlus = " & txtGooglePlus.Text
                    AfterText += "|LinkedIn = " & txtLinkedIn.Text
                    AfterText += "|Instagram = " & txtInstagram.Text
                    AfterText += "|LastUpdatedDataSync = " & Now
                    AfterText += "|Address = " & txtAddress.Text
                    If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                        .update()
                    End If
                    'End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Update successfully"
                            Response.Redirect("ManageContactUs.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Update successfully"
                        Response.Redirect("ManageContactUs.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        End If
    End Sub

#End Region

#Region "Functions"

    Private Sub FillDropDowns()
        objAppType = New applicationtype
        dr = objAppType.GetApplicationName
        cmbApplication.DataSource = dr
        cmbApplication.DataTextField = "Name"
        cmbApplication.DataValueField = "ID"
        cmbApplication.DataBind()
        dr.Close()

    End Sub

    Private Sub FillForm()

        Dim queryStr As String = ""
        Dim ConatctId As Integer

        objContact = New contactus
        Dim qsEnc As New AbhiQsEnc.AbhiQs

        queryStr = Request.QueryString("Qs")   ' read query string 
        ConatctId = Val(qsEnc.ReadQS(queryStr, "id"))
        hdnId.Value = ConatctId

        dr = objContact.GetContactforEdit(ConatctId)

        If Not dr Is Nothing Then
            If dr.Read Then
                cmbApplication.SelectedValue() = dr("AppID")
                txtName.Text = IsNullValue(dr("CompanyName"), "")
                txtNameAlt.Text = IsNullValue(dr("CompanyNameAr"), "")
                txtPhone.Text = IsNullValue(dr("PhoneNumber"), "")
                txtEmail.Text = IsNullValue(dr("EmailID"), "")
                txtFax.Text = IsNullValue(dr("Fax"), "")
                txtWebsite.Text = IsNullValue(dr("Website"), "")
                txtFacebook.Text = IsNullValue(dr("Facebook"), "")
                txtTwitter.Text = IsNullValue(dr("Twitter"), "")
                txtYouTube.Text = IsNullValue(dr("YouTube"), "")
                txtGooglePlus.Text = IsNullValue(dr("GooglePlus"), "")
                txtLinkedIn.Text = IsNullValue(dr("LinkedIn"), "")
                txtInstagram.Text = IsNullValue(dr("Instagram"), "")
                txtAddress.Text = IsNullValue(dr("Address"), "")
                txtLastUpdatedDataSync.Text = IsNullValue(dr("LastUpdatedDataSync"), "")
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Contact"
            End If
        End If

    End Sub

#End Region


End Class