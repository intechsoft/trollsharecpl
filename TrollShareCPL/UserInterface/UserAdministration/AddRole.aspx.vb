﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddRole
    Inherits BaseUIPage
#Region "-------------variables-----------------"
    Dim objRole As userroles
    Dim dr As MySqlDataReader
#End Region

#Region "-------------Events---------------------"
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillForm()
        End If

    End Sub
    'Protected Sub btnCancel_Click1(sender As Object, e As EventArgs) Handles btnCancel.Click
    '    If Val(hdnRoleId.Value) = 0 Then
    '        ClearForm()
    '        lblMsg.Text = ""
    '    Else
    '        Response.Redirect("ManageRole.aspx")
    '    End If
    'End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim roleId As Integer = 0
        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then
                objRole = New userroles
                With objRole
                    .Name = txtRoleName.Text
                    .NameAlt = txtNameAlt.Text
                    .Status = Val(cmbStatus.SelectedValue)
                    roleId = .CheckExist(.Name, 0)
                    If roleId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else
                        Dim BeforeText As String = ""
                        Dim AfterText As String = " "
                        AfterText += " Name = " & txtRoleName.Text
                        AfterText += "|NameAlt = " & txtNameAlt.Text
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            .insertMember()
                            'ObjApplicationTypeRoles = New ApplicationTypeRoles
                            'With ObjApplicationTypeRoles
                            '    .RoleID = CmbRole.SelectedValue()
                            '    .ApplicationType = CmbApplicationType.SelectedValue()
                            '    .Privilege = st
                            '    .Status = 1
                            'End With
                        End If
                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Role added successfully"
                            Response.Redirect("ManageRole.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Role added successfully"
                        Response.Redirect("ManageRole.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then

                objRole = New userroles(Val(hdnRoleId.Value))
                With objRole
                    Dim BeforeText As String = ""
                    BeforeText += " Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Status=" & .Status

                    .Name = txtRoleName.Text
                    .NameAlt = txtNameAlt.Text
                    .Status = Val(cmbStatus.SelectedValue())
                    roleId = .CheckExist(.Name, Val(hdnRoleId.Value))
                    If roleId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else
                        Dim AfterText As String = " "
                        AfterText += " Name = " & txtRoleName.Text
                        AfterText += "|NameAlt = " & txtNameAlt.Text
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            .update()
                        End If
                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Role Updated successfully"
                            Response.Redirect("ManageRole.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Role Updated successfully"
                        Response.Redirect("ManageRole.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated

            End If
        End If

    End Sub


#End Region

#Region "Functions"
    Private Sub FillForm()
        Dim queryStr As String = ""
        Dim roleId As Integer
        objRole = New userroles
        'Dim qsEnc As New AbhiQsEnc.AbhiQs
        'queryStr = Request.QueryString("Qs")
        'roleId = Val(qsEnc.ReadQS(queryStr, "id"))
        hdnRoleId.Value = roleId
        dr = objRole.GetUserRoleforEdit(roleId)
        If Not dr Is Nothing Then
            If dr.Read() Then
                txtRoleName.Text = IsNullValue(dr("Name"), "")
                txtNameAlt.Text = IsNullValue(dr("NameAlt"), "")
                cmbStatus.SelectedValue() = dr("Status")
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update User Role"
            End If
            dr.Close()
        End If

    End Sub
    Private Sub ClearForm()
        txtRoleName.Text = ""
        btnAdd.Text = ""
        cmbStatus.SelectedIndex() = 0
    End Sub
#End Region

End Class