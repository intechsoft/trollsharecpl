﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddFeedBackMsg
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objFeedbackmsg As feedbackmessage
    Private objContact As contactus
    Private objCustmorMst As customermst

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'txtLastUpdatedDataSync.Attributes.Add("onclick", "NewCssCal('" & txtLastUpdatedDataSync.ClientID & "','ddmmmyyyy','arrow',false,24,false)")
        If IsPostBack = False Then
            FillDropDowns()
            FillForm()
        End If
    End Sub

    Private Sub cmbCustomer_DataBound(sender As Object, e As EventArgs) Handles cmbCustomer.DataBound
        cmbCustomer.Items.Insert(0, New ListItem("-------Select Customer ---------------", ""))
    End Sub
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        If btnAdd.Text = "ADD" Then

            If AddPrivilage Then ' Checking adding privilage of current user

                objFeedbackmsg = New feedbackmessage

                With objFeedbackmsg

                    .CustomerID = Val(cmbCustomer.SelectedValue())
                    .Name = txtName.Text
                    .MobileNumber = txtMobileNumber.Text
                    .EmailID = txtEmailID.Text
                    .Message = txtMessage.Text
                    .EmailID = txtEmailID.Text
                    '.MessageTime = txtMessageTime.Text
                    .Remarks = txtRemarks.Text
                    '.ActionTime = txtActionTime.Text
                    .ActionTakenBy = txtActionTakenBy.Text
                    .Status = Val(cmbStatus.SelectedValue())
                    '.Status = txtStatus.Text
                    '.LinkedIn = txtLinkedIn.Text
                    '.Instagram = txtInstagram.Text
                    '.LastUpdatedDataSync = txtLastUpdatedDataSync.Text
                    '.Address = txtAddress.Text

                    '    messageid = .CheckExist(.ServiceID, 0)

                    'If messageid <> 0 Then
                    '    .ErrorMsg = "A Message Already Active for This Service"

                    'Else
                    Dim BeforeText As String = ""
                    Dim AfterText As String = "CustomerID = " & Val(cmbCustomer.SelectedValue())
                    AfterText += "|Name = " & txtName.Text
                    AfterText += "|MobileNumber = " & txtMobileNumber.Text
                    AfterText += "|EmailID = " & txtEmailID.Text
                    AfterText += "|Message = " & txtMessage.Text
                    AfterText += "|MessageTime = " & txtMessagetime.Text
                    AfterText += "|Remarks = " & txtRemarks.Text
                    AfterText += "|ActionTime = " & txtActionTime.Text
                    AfterText += "|ActionTakenBy = " & txtActionTakenBy.Text
                    AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                   

                    If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                        .insertMember()
                    End If
                    'End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Added successfully"
                            Response.Redirect("ManageFeedBackMsg.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Added successfully"
                        Response.Redirect("ManageFeedBackMsg.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else

            If EditPrivilage Then

                objFeedbackmsg = New feedbackmessage(Val(hdnId.Value()))
                Dim BeforeText As String = ""
                With objFeedbackmsg

                    BeforeText += "CustomerID = " & .CustomerID
                    BeforeText += "|Name=" & .Name
                    BeforeText += "|MobileNumber=" & .MobileNumber
                    BeforeText += "|EmailID=" & .EmailID
                    BeforeText += "|Message=" & .Message
                    BeforeText += "|MessageTime=" & .MessageTime
                    BeforeText += "|Remarks=" & .Remarks
                    BeforeText += "|ActionTime=" & .ActionTime
                    BeforeText += "|ActionTakenBy=" & .ActionTakenBy
                    BeforeText += "|Status=" & .Status

                    .CustomerID = Val(cmbCustomer.SelectedValue())
                    .Name = txtName.Text
                    .MobileNumber = txtMobileNumber.Text
                    .EmailID = txtEmailID.Text
                    .Message = txtMessage.Text
                    .MessageTime = txtMessagetime.Text
                    .Remarks = txtRemarks.Text
                    .ActionTime = txtActionTime.Text
                    .ActionTakenBy = txtActionTakenBy.Text
                    .Status = Val(cmbStatus.SelectedValue)

                    'If Val(cmbService.SelectedValue()) > 0 Then
                    '    messageid = .CheckExist(.ServiceID, Val(hdnId.Value()))
                    '    If messageid <> 0 Then
                    '        .ErrorMsg = " A Message Already Active for This Service"
                    '    End If
                    'Else
                    Dim AfterText As String = "CustomerID = " & Val(cmbCustomer.SelectedValue())
                    AfterText += "|Name = " & txtName.Text
                    AfterText += "|MobileNumber = " & txtMobileNumber.Text
                    AfterText += "|EmailID = " & txtEmailID.Text
                    AfterText += "|Message = " & txtMessage.Text
                    AfterText += "|MessageTime = " & txtMessagetime.Text
                    AfterText += "|Remarks = " & txtRemarks.Text
                    AfterText += "|ActionTime = " & txtActionTime.Text
                    AfterText += "|ActionTakenBy = " & txtActionTakenBy.Text
                    AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                 
                    If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                        .update()
                    End If
                    'End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Update successfully"
                            Response.Redirect("ManageFeedBackMsg.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Update successfully"
                        Response.Redirect("ManageFeedBackMsg.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        End If
    End Sub

#End Region

#Region "Functions"

    Private Sub FillDropDowns()
        objCustmorMst = New customermst
        dr = objCustmorMst.GetCustomerName()
        cmbCustomer.DataSource = dr
        cmbCustomer.DataTextField = "Name"
        cmbCustomer.DataValueField = "ID"
        cmbCustomer.DataBind()
        dr.Close()

    End Sub

    Private Sub FillForm()

        Dim queryStr As String = ""
        Dim ConatctId As Integer

        objContact = New contactus
        Dim qsEnc As New AbhiQsEnc.AbhiQs

        queryStr = Request.QueryString("Qs")   ' read query string 
        ConatctId = Val(qsEnc.ReadQS(queryStr, "id"))
        hdnId.Value = ConatctId

        dr = objContact.GetContactforEdit(ConatctId)

        If Not dr Is Nothing Then
            If dr.Read Then
                cmbCustomer.SelectedValue() = dr("CustomerID")
                txtName.Text = IsNullValue(dr("Name"), "")
                txtMobileNumber.Text = IsNullValue(dr("MobileNumber"), "")
                txtEmailID.Text = IsNullValue(dr("EmailID"), "")
                txtEmailID.Text = IsNullValue(dr("EmailID"), "")
                txtMessage.Text = IsNullValue(dr("Message"), "")
                txtMessagetime.Text = IsNullValue(dr("MessageTime"), "")
                txtRemarks.Text = IsNullValue(dr("Remarks"), "")
                txtActionTime.Text = IsNullValue(dr("ActionTime"), "")
                txtActionTime.Text = IsNullValue(dr("ActionTime"), "")
                cmbStatus.SelectedValue() = dr("Status")
                'txtLinkedIn.Text = IsNullValue(dr("LinkedIn"), "")
                'txtInstagram.Text = IsNullValue(dr("Instagram"), "")
                'txtAddress.Text = IsNullValue(dr("Address"), "")
                'txtLastUpdatedDataSync.Text = IsNullValue(dr("LastUpdatedDataSync"), "")
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Contact"
            End If
        End If

    End Sub

#End Region


End Class