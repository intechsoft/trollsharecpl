﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageUserLogins
    Inherits BaseUIPage
#Region "Variables"

    Private ObjappType As New ApplicationType
    Private dr As mySqlDataReader
    Private dt As DataTable 
    Private objUserLogin As UserLogins
    Private objRole As UserRoles
    Private objUserTypes As UserType 
    'Private Enc As New AbhiQsEnc.AbhiQs
    Private where As String

#End Region
#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillGridView()
        End If
    End Sub

    Private Sub cmbCompany_DataBound(sender As Object, e As EventArgs) Handles cmbCompany.DataBound
        cmbCompany.Items.Insert(0, New ListItem("-------Select Company----------", ""))
    End Sub
    Private Sub cmbRole_DataBound(sender As Object, e As EventArgs) Handles cmbRole.DataBound
        cmbRole.Items.Insert(0, New ListItem("-------Select Role--------", ""))
    End Sub

    Private Sub cmbUserType_DataBound(sender As Object, e As EventArgs) Handles cmbUserType.DataBound
        cmbUserType.Items.Insert(0, New ListItem("----Select User Type----", ""))
    End Sub

    Private Sub grdUserLogins_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdUserLogins.PageIndexChanging
        grdUserLogins.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        FillGridView()
    End Sub

    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub

    Private Sub grdUserLogins_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdUserLogins.RowCommand
        Dim BeforeText As String = ""
        Dim AfterText As String = ""
        Dim id As Integer = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = "" 'Enc.WriteQS("id=" & id)
                Response.Redirect("AddUserLogins.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objUserLogin = New UserLogins(id)
                With objUserLogin
                    BeforeText += " EmployeeID = " & .EmployeeID
                    BeforeText += "|CompanyID=" & .CompanyID
                    BeforeText += "|UserTypeId = " & .UserTypeId
                    BeforeText += "|RoleID=" & .RoleID
                    BeforeText += "|LanguageOption = " & .LanguageOption
                    BeforeText += "|UserName=" & .UserName
                    BeforeText += "|UserActualName = " & .UserActualName
                    BeforeText += "|Status=" & .Status
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .Delete()
                    End If
                End With
                FillGridView()
            End If
        End If
    End Sub

    Private Sub grdUserLogins_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdUserLogins.RowDataBound
        If DeletePrivilage Then
            grdUserLogins.Columns(11).Visible = True
        Else
            grdUserLogins.Columns(11).Visible = False
        End If
    End Sub

#End Region

#Region "Procedures"

    Private Sub FillDropDowns()
       
        '------------fill Role---------------
        objRole = New UserRoles
        dr = objRole.GetUserRoles()
        cmbRole.DataSource = dr
        cmbRole.DataTextField = "Name"
        cmbRole.DataValueField = "ID"
        cmbRole.DataBind()

        '------------fill User Type---------------
        objUserTypes = New UserType
        dt = objUserTypes.GetUserType()
        cmbUserType.DataSource = dt
        cmbUserType.DataTextField = "Name"
        cmbUserType.DataValueField = "ID"
        cmbUserType.DataBind()
           
    End Sub

    Private Sub getWhere()
        where = ""
        where += " and (CC.ID = " & Val(cmbCompany.SelectedValue) & " or " & Val(cmbCompany.SelectedValue) & " = 0) "
        where += " and (UR.ID = " & Val(cmbRole.SelectedValue) & " or " & Val(cmbRole.SelectedValue) & " = 0 )"
        where += " and (UT.ID = " & Val(cmbUserType.SelectedValue) & " or " & Val(cmbUserType.SelectedValue) & " = 0 )"

    End Sub

    Private Sub FillGridView()
        If chkPaging.Checked = True Then
            grdUserLogins.AllowPaging = True
        Else
            grdUserLogins.AllowPaging = False
        End If
        objUserLogin = New UserLogins
        getWhere()
        dt = objUserLogin.GetAllUserDetails(where)
        grdUserLogins.DataSource = dt
        grdUserLogins.DataBind()
    End Sub

#End Region 
    
  
End Class