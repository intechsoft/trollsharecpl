﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="test.aspx.vb" Inherits="TrollShareCPL.test" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tblHeader
        {
            font-size: 11px;
            color: #fff;
            font-weight: Bold;
            font-family: verdana, arial, helvetica, sans-serif;
            text-decoration: none;
            background-color: #778888;
            text-align: center;
        }
        
        .tableFilter
        {
            border: 1px;
            background: #666699;
            width: 100%;
        }
        
        .textbox
        {
            border: 1px solid #b6b6b6;
            background-color: #ffffff;
            font-size: 11px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            color: #000;
            height: 18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../../Style/Controlstyle.css" rel="stylesheet" type="text/css" />
    <table class="panel panel-green"  style="padding:0px 0px 0px 0px; margin:0px 0px 0px 0px;  ">
        <tr>
            <td colspan="3">
                <asp:Label ID="Label7" runat="server"  Text="User Administration&gt;"></asp:Label>
                <asp:LinkButton ID="linkADDRole" runat="server" CausesValidation="False" >ADD Role</asp:LinkButton>
                &nbsp;|
                <asp:LinkButton ID="lnkManageRole" runat="server" CausesValidation="False" >Manage Role</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td class="tblHeader" colspan="3">
                Manage Privileges
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table border="1" class="tableFilter" style="width: 100%">
                    <tr>
                        <td style="width: 132px">
                            <asp:Label ID="Label1" runat="server"  Text="Role :"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="CmbRole" runat="server" AutoPostBack="True" 
                                Width="193px">
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 132px">
                            <asp:Label ID="Label2" runat="server"  Text="Application Type :"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="CmbApplicationType" runat="server" AutoPostBack="True" 
                                Width="250px">
                                <asp:ListItem Selected="True" Value="0">--- Select an Application --</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="reportGridView" runat="server" AutoGenerateColumns="False" BorderColordark="#FFFFFF"
                    BorderColorlight="#000000" CellPadding="4" Font-Size="Smaller" ForeColor="#333333"
                    GridLines="None" Width="500px">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D"   Font-Bold="true" Font-Size="Small"
                        ForeColor="White" Wrap="false" />
                    <RowStyle BackColor="#F7F6F3" Font-Size="Medium" ForeColor="#333333" HorizontalAlign="left"
                        Wrap="false" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdID" runat="server" Value='<%# Eval("ID") %>' />
                                <asp:Label ID="lblName" runat="server"  Text='<%# Eval("Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:GridView ID="reportGridViewInner" runat="server" AutoGenerateColumns="false"
                                    BorderColor="#000000" BorderColordark="#FFFFFF" BorderColorlight="#000000" BorderWidth="1"
                                    CellPadding="0" CellSpacing="0" EmptyDataText="No Data Found" Font-Size="Smaller"
                                    ShowFooter="true" Width="350px">
                                    <HeaderStyle BackColor="#5D7B9D" />
                                    <RowStyle Font-Size="X-Small" HorizontalAlign="center" Wrap="false" />
                                    <FooterStyle Font-Bold="true" ForeColor="red" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="Title" ItemStyle-Width="300px" />

                                        <asp:TemplateField HeaderText="Page View">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdPID" runat="server" Value='<%# Eval("ID") %>' />
                                                
                                                <asp:CheckBox ID="chk" runat="server" Checked='<%# Bindchk(Eval("ID"))%>' /> 
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                         <asp:TemplateField HeaderText="Add">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdPID" runat="server" Value='<%# Eval("ID") %>' />
                                                
                                                <asp:CheckBox ID="chk" runat="server" Checked='<%# Bindchk(Eval("ID"))%>' /> 
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdPID" runat="server" Value='<%# Eval("ID") %>' />
                                                
                                                <asp:CheckBox ID="chk" runat="server" Checked='<%# Bindchk(Eval("ID"))%>' /> 
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdPID" runat="server" Value='<%# Eval("ID") %>' />
                                                
                                                <asp:CheckBox ID="chk" runat="server" Checked='<%# Bindchk(Eval("ID"))%>' /> 
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Details View">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdPID" runat="server" Value='<%# Eval("ID") %>' />
                                                
                                                <asp:CheckBox ID="chk" runat="server" Checked='<%# Bindchk(Eval("ID"))%>' /> 
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <img src="http://localhost:3937/images/pixel_black.jpg" style="width: 100%; height: 2px" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td colspan="2">
                <asp:Button ID="btnSave" runat="server"  Text="Update" />
                <asp:Label ID="lblError" runat="server"  ForeColor="#CC3300"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>


