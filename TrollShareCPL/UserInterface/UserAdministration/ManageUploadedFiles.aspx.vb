﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageUploadedFiles
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objUploadedfiles As uploadedfiles
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillGridView()
        End If
    End Sub

    Private Sub cmbUser_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbUser.DataBound
        cmbUser.Items.Insert(0, New ListItem("-------Select a User -------", 0))
    End Sub

    Private Sub grdUploadedfiles_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdUploadedfiles.PageIndexChanging
        grdUploadedfiles.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub
    Private Sub grdUploadedfiles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdUploadedfiles.RowDataBound

        'If DeletePrivilage Then
        '    grdUploadedfiles.Columns(7).Visible = True
        'Else
        '    grdUploadedfiles.Columns(7).Visible = False
        'End If
    End Sub

    Private Sub grdUploadedfiles_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdUploadedfiles.RowCommand

        Dim BeforeText As String = ""
        Dim AfterText As String = ""

        id = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = Enc.WriteQS("id=" & id)                ' code for encrytpting a strings
                Response.Redirect("AddUploadedFiles.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objUploadedfiles = New uploadedfiles(id)
                With objUploadedfiles

                    BeforeText += "FileName = " & .FileName
                    BeforeText += "|FilePath=" & .FilePath
                    BeforeText += "|FileType=" & .FileType
                    BeforeText += "|IpAddress = " & .IpAddress
                    BeforeText += "|Status = " & .Status
                    BeforeText += "|UploadedDate = " & .UploadedDate
                   
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
            End If
            FillGridView()
        End If

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()

        If chkPaging.Checked = True Then
            grdUploadedfiles.AllowPaging = True
        Else
            grdUploadedfiles.AllowPaging = False
        End If
        objUploadedfiles = New uploadedfiles
        dt = objUploadedfiles.GetUploadedfile()
        grdUploadedfiles.DataSource = dt
        grdUploadedfiles.DataBind()

    End Sub

    Private Sub FillDropDowns()
        Dim objUserlogins As New userlogins
        dr = objUserlogins.getAllUsers()
        cmbUser.DataSource = dr
        cmbUser.DataValueField = "ID"
        cmbUser.DataTextField = "UserName"
        cmbUser.DataBind()
        dr.Close()
    End Sub

#End Region

    Private Sub cmbUser_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbUser.SelectedIndexChanged
        objUploadedfiles = New uploadedfiles
        If chkPaging.Checked = True Then
            grdUploadedfiles.AllowPaging = True
        Else
            grdUploadedfiles.AllowPaging = False
        End If

        If cmbUser.SelectedValue > 0 Then
            dt = objUploadedfiles.GetUploadedfile(cmbUser.SelectedValue)
        Else
            dt = objUploadedfiles.GetUploadedfile()
        End If

        grdUploadedfiles.DataSource = dt
        grdUploadedfiles.DataBind()
    End Sub
End Class