﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL
Public Class AddApplicationType
    Inherits BaseUIPage

#Region "-------------variables-----------------"
    Dim objAppType As applicationtype
    Dim dr As MySqlDataReader
#End Region

#Region "-------------Events---------------------"
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillForm()
        End If
    End Sub
    'Protected Sub btnCancel_Click1(sender As Object, e As EventArgs) Handles btnCancel.Click
    '    If Val(hdnAppId.Value) = 0 Then
    '        ClearForm()
    '        lblMsg.Text = ""
    '    Else
    '        Response.Redirect("ManageApplicationType.aspx")
    '    End If
    'End Sub
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim appid As Integer = 0

        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then
                objAppType = New applicationtype
                With objAppType
                    .Name = Trim(txtName.Text)
                    .NameAlt = Trim(txtNameAlt.Text)
                    .Status = Val(cmbStatus.SelectedValue)
                    appid = .CheckExist(.Name, 0)
                    If appid <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else
                        Dim BeforeText As String = ""
                        Dim AfterText As String = " "
                        AfterText += " Name = " & txtName.Text
                        AfterText += "|NameAlt = " & txtNameAlt.Text
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            .insertMember()
                        End If
                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Application Type added successfully"
                            Response.Redirect("ManageApplicationType.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Application Type added successfully"
                        Response.Redirect("ManageApplicationType.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then

                objAppType = New applicationtype(Val(hdnAppId.Value))
                With objAppType
                    Dim BeforeText As String = ""
                    BeforeText += " Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Status=" & .Status

                    .Name = Trim(txtName.Text)
                    .NameAlt = Trim(txtNameAlt.Text)
                    .Status = Val(cmbStatus.SelectedValue())
                    appid = .CheckExist(.Name, Val(hdnAppId.Value))
                    If appid <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else

                        Dim AfterText As String = " "
                        AfterText += " Name = " & txtName.Text
                        AfterText += "|NameAlt = " & txtNameAlt.Text
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            .update()
                        End If

                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Application Type Updated successfully"
                            Response.Redirect("ManageApplicationType.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Application Type Updated successfully"
                        Response.Redirect("ManageApplicationType.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated
            End If
        End If

    End Sub


#End Region

#Region "Functions"
    Private Sub FillForm()
        Dim queryStr As String = ""
        Dim appid As Integer
        objAppType = New applicationtype
        'Dim qsEnc As New AbhiQsEnc.AbhiQs
        queryStr = Request.QueryString("Qs")
        appid = Request.QueryString("id") 'Val(qsEnc.ReadQS(queryStr, "id"))
        hdnAppId.Value = appid
        dr = objAppType.GetAppTypeforEdit(appid)
        If Not dr Is Nothing Then
            If dr.Read() Then
                txtName.Text = IsNullValue(dr("Name"), "")
                txtNameAlt.Text = IsNullValue(dr("NameAlt"), "")
                cmbStatus.SelectedValue() = dr("Status")
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Application Type"
            End If
            dr.Close()
        End If
    End Sub
    Private Sub ClearForm()
        txtName.Text = ""
        txtNameAlt.Text = ""
        btnAdd.Text = ""
        cmbStatus.SelectedIndex() = 0
    End Sub
#End Region


End Class