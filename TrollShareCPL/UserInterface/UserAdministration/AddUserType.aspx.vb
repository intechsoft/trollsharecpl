﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddUserType
    Inherits BaseUIPage
#Region "-------------variables-----------------"
    Dim objType As UserType
    Dim dr As mySqlDataReader
#End Region

#Region "-------------Events---------------------"
    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillForm()
        End If 
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim typeId As Integer = 0
        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then
                objType = New UserType
                With objType
                    .Name = Trim(txtName.Text)
                    .NameAlt = Trim(txtNameAlt.Text)
                    .Status = Val(cmbStatus.SelectedValue)
                    typeId = .CheckExist(.Name, 0)
                    If typeId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else
                        Dim BeforeText As String = ""
                        Dim AfterText As String = " "
                        AfterText += " Name = " & txtName.Text
                        AfterText += "|NameAlt = " & txtNameAlt.Text
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            .InsertMember()
                        End If
                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "User Type added successfully"
                            Response.Redirect("ManageUserType.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "User Type added successfully"
                        Response.Redirect("ManageUserType.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then
                objType = New UserType(Val(hdnUserTypeId.Value))
                With objType
                    Dim BeforeText As String = ""
                    BeforeText += " Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Status=" & .Status

                    .Name = Trim(txtName.Text)
                    .NameAlt = Trim(txtNameAlt.Text)
                    .Status = Val(cmbStatus.SelectedValue())
                    typeId = .CheckExist(.Name, Val(hdnUserTypeId.Value))
                    If typeId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else
                        Dim AfterText As String = " "
                        AfterText += " Name = " & txtName.Text
                        AfterText += "|NameAlt = " & txtNameAlt.Text
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            .Update()
                        End If
                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "User Type Updated successfully"
                            Response.Redirect("ManageUserType.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "User Type Updated successfully"
                        Response.Redirect("ManageUserType.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated
            End If
        End If
    End Sub

#End Region

#Region "Functions"
    Private Sub FillForm()
        Dim queryStr As String = ""
        Dim typeId As Integer
        objType = New UserType
        'Dim qsEnc As New AbhiQsEnc.AbhiQs
        'queryStr = Request.QueryString("Qs")
        'typeId = Val(qsEnc.ReadQS(queryStr, "id"))
        hdnUserTypeId.Value = typeId
        dr = objType.GetUserTypeforEdit(typeId)
        If Not dr Is Nothing Then
            If dr.Read() Then
                txtName.Text = IsNullValue(dr("Name"), "")
                txtNameAlt.Text = IsNullValue(dr("NameAlt"), "")
                cmbStatus.SelectedValue() = dr("Status")
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update User Type"
            End If
            dr.Close()
        End If
    End Sub
    Private Sub ClearForm()
        txtName.Text = ""
        txtNameAlt.Text = ""
        btnAdd.Text = ""
        cmbStatus.SelectedIndex() = 0
    End Sub
#End Region

End Class