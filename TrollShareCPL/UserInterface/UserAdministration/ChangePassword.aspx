﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="ChangePassword.aspx.vb" Inherits="TrollShareCPL.ChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        function clearForm() { 
            $("input[type='password']").val(''); 
            var x = document.getElementById('<%=lblMsg.ClientID%>');
            x.innerHTML = ""
        }
        function checkPassword() {
            var pwd = document.getElementById('<%=txtNewPassword.ClientID%>').value;
            var cpwd = document.getElementById('<%=txtConfirm.ClientID%>').value; 
            if (pwd.length > 0 && cpwd.length > 0) {
                pwd = $.trim(pwd);
                cpwd = $.trim(cpwd); 
                if (pwd.length > 0 && cpwd.length > 0) {
                    if (pwd != cpwd) {
                        document.getElementById('<%=lblMsg.ClientID%>').innerHTML = "Password Mismatch.!";
                        return false;
                    }
                    else {
                        document.getElementById('<%=lblMsg.ClientID%>').innerHTML = "";
                        return true;
                    }
                }
                else {
                    document.getElementById('<%=lblMsg.ClientID%>').innerHTML = "Invalid Password";
                    $("input[type='password']").val('');
                    return false;
                }
            }
        }
    </script>

    <asp:HiddenField ID="hdnRoleId" runat="server" Value="0" />
    <div class="row">
        <div class="col-lg-4" style="width: 500px; margin-left: 150px; margin-right: auto; margin-top: 70px; margin-bottom: auto;">
            <div class="panel panel-primary"> 
                <div class="panel-heading" style="height:50px">
                    <h4 style="text-align: center"><strong><asp:Label ID="lblHeadText" runat="server" Text="Change Password"></asp:Label>
                    </strong></h4>
                </div> <br />
                <div style="text-align:center;">
                <asp:Label ID="lblMsg" runat="server" Font-Size="Larger" Text="" Style="color:red;" ></asp:Label>
                    </div>
                <table style="margin-left:70px; width: 400px; line-height:40px;">
                     
                                           
                    <tr>

                        <td>Current Password: </td>
                        <td>
                            <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>

                        <td>New Password: </td>
                        <td>
                            <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server" class="form-control" onblur="checkPassword();" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr> 
                   <tr>

                        <td>Confirm Password: </td>
                        <td>
                            <asp:TextBox ID="txtConfirm" TextMode="Password" runat="server" class="form-control" onblur="checkPassword();" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr> 
                      

                    <tr style="height: 20px"><td colspan="2"></td></tr>
                    <tr>

                        <td></td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Text="Change" OnClientClick="return checkPassword();" class="btn btn-outline btn-success" Style="width:100px" />
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <asp:Button ID="btnCancel" OnClientClick="return clearForm();" runat="server" Text="CANCEL" class="btn btn-outline btn-danger" Style="width: 100px" UseSubmitBehavior="false" /> 
                        </td>
                    </tr>
                     <tr style="height: 20px"><td colspan="2"></td></tr>
                     
                    
                </table> 

                       </div>
        </div>
         
    </div>

</asp:Content>
