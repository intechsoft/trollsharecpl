﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageFeedBackMsg
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objFeedbackmsg As feedbackmessage
    Private objCustomermst As customermst
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillGridView()
        End If
    End Sub

    Private Sub cmbCustomer_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCustomer.DataBound
        cmbCustomer.Items.Insert(0, New ListItem("-------Select a Customer -------", 0))
    End Sub

    Private Sub grdContact_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdFeedBack.PageIndexChanging
        grdFeedBack.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub
    Private Sub grdFeedBack_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdFeedBack.RowDataBound

        If DeletePrivilage Then
            grdFeedBack.Columns(7).Visible = True
        Else
            grdFeedBack.Columns(7).Visible = False
        End If
    End Sub

    Private Sub grdFeedBack_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdFeedBack.RowCommand

        Dim BeforeText As String = ""
        Dim AfterText As String = ""

        id = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = Enc.WriteQS("id=" & id)                ' code for encrytpting a strings
                Response.Redirect("AddFeedBackMsg.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objFeedbackmsg = New feedbackmessage(id)
                With objFeedbackmsg

                    BeforeText += "Name = " & .Name
                    BeforeText += "|MobileNumber=" & .MobileNumber
                    BeforeText += "|EmailID = " & .EmailID
                    BeforeText += "|Message = " & .Message
                    BeforeText += "|MessageTime = " & .MessageTime
                    BeforeText += "|Remarks = " & .Remarks
                    BeforeText += "|ActionTime = " & .ActionTime
                    BeforeText += "|ActionTakenBy = " & .ActionTakenBy
                    BeforeText += "|Status=" & .Status
                    'BeforeText += "|GooglePlus = " & .GooglePlus
                    'BeforeText += "|LinkedIn = " & .LinkedIn
                    'BeforeText += "|Instagram = " & .Instagram
                    'BeforeText += "|LastUpdatedDateSync = " & .LastUpdatedDataSync
                    'BeforeText += "|Address = " & .Address
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
            End If
            FillGridView()
        End If

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()

        If chkPaging.Checked = True Then
            grdFeedBack.AllowPaging = True
        Else
            grdFeedBack.AllowPaging = False
        End If
        objFeedbackmsg = New feedbackmessage
        dt = objFeedbackmsg.GetFeedback()
        grdFeedBack.DataSource = dt
        grdFeedBack.DataBind()

    End Sub

    Private Sub FillDropDowns()
        Dim objCustomermst As New customermst
        dr = objCustomermst.GetCustomerName()
        cmbCustomer.DataSource = dr
        cmbCustomer.DataValueField = "ID"
        cmbCustomer.DataTextField = "Name"
        cmbCustomer.DataBind()
        dr.Close()
    End Sub

#End Region

    Private Sub cmbCustomer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbCustomer.SelectedIndexChanged
        objFeedbackmsg = New feedbackmessage
        If chkPaging.Checked = True Then
            grdFeedBack.AllowPaging = True
        Else
            grdFeedBack.AllowPaging = False
        End If

        If cmbCustomer.SelectedValue > 0 Then
            dt = objFeedbackmsg.GetFeedback(cmbCustomer.SelectedValue)
        Else
            dt = objFeedbackmsg.GetFeedback()
        End If

        grdFeedBack.DataSource = dt
        grdFeedBack.DataBind()
    End Sub
End Class