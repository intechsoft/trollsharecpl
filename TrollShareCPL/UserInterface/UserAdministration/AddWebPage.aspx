﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="AddWebPage.aspx.vb" Inherits="TrollShareCPL.AddWebPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdnWebPageId" runat="server" Value="0" />
    <div class="row">
        <div class="col-lg-4" style="width: 500px; margin-left: 150px; margin-right: auto; margin-top: 80px; margin-bottom: auto;">
            <div class="panel panel-primary">
                 <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
                    <asp:Label ID="lblHeader" runat="server" Text="Add New Web Page"></asp:Label>
                     <a runat="server" href="~/UserInterface/UserAdministration/ManageWebPages.aspx">&nbsp;>>Manage WebPages</a >
                </div>
                <div class="panel-heading" style="height:50px">
                    <h4 style="text-align: center"><strong><asp:Label ID="lblHeadText" runat="server" Text="Add New Web Page"></asp:Label>
                    </strong></h4>
                </div>
                <table style="margin-left:38px; width: 450px;">
                    <br />

                     <tr style="height: 20px">

                        <asp:Label ID="lblMsg" runat="server" Font-Size="Larger" Text="" CssClass="warning"></asp:Label>
                                             </tr>
                     <tr><td colspan="3"></td></tr>
                    
                    <tr>
                        
                        <td>Application: </td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="cmbApplication" runat="server" class="form-control" Style="width: 250px; height:auto" AutoPostBack="true" required="required">
                                </asp:DropDownList>
<%--<asp:RequiredFieldValidator ID="RqdValidatorcmbApplication" runat="server" ErrorMessage="Required" ControlToValidate="cmbApplication" InitialValue="0" style="margin-left:5px; color:#FF0000"></asp:RequiredFieldValidator>--%>
                              
                            </div>
                    </td>
                    </tr>

                    <tr>

                        <td>Section: </td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="cmbSection" runat="server" class="form-control" Style="width: 250px; height:auto" AutoPostBack="true" required="required">
                                </asp:DropDownList>
<%--<asp:RequiredFieldValidator ID="RqdValidatorcmbSection" runat="server" ErrorMessage="Required" ControlToValidate="cmbSection" InitialValue="0" style="margin-left:5px; color:#FF0000"></asp:RequiredFieldValidator>--%>
                              
                            </div>

                        </td>
                    </tr>
                    <tr>

                        <td>Name: </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="height:50px">

                        <td>File Name: </td>
                        <td>
                            <asp:TextBox ID="txtFileName" runat="server" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="height:20px">

                        <td>File Path: </td>
                        <td>
                            <asp:TextBox ID="txtFilePath" runat="server" class="form-control" Style="width: 250px;" MaxLength="150" required="required"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="height:50px">

                        <td>Sub Section: </td>
                        <td>
                            <asp:TextBox ID="txtSubSection" runat="server" class="form-control" Style="width: 60px; " MaxLength="4" required="required"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="height:20px">

                        <td>Page Level: </td>
                        <td>
                            <asp:TextBox ID="txtPageLevel" runat="server" class="form-control" Style="width: 60px; " MaxLength="4" required="required">

                            </asp:TextBox>
                        </td>

                    </tr>
                    <tr style="height:50px">

                        <td>View Order: </td>
                        <td>
                            <asp:TextBox TextMode="Number" ToolTip="Accepts only Numbers" ID="txtViewOrder" MaxLength="4" runat="server" class="form-control" Style="width: 60px; height: 32px"  required="required"></asp:TextBox>
                        </td>
                    </tr>

                    <tr style="height:20px">

                        <td>Parent Page: </td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="cmbParentPage" runat="server" class="form-control" Style="width: 250px; height: 30px"  >
                                </asp:DropDownList>
                            </div>

                        </td>
                    </tr>

                   
                    <tr style="height: 40px">

                        <td>Status: </td>
                        <td style="vertical-align: bottom">
                            <div class="form-group">
                                <asp:DropDownList ID="cmbStatus" runat="server" class="form-control" Style="width: auto; height: 30px" required="required" >
                                    <asp:ListItem Text="-Select-"  Value=""></asp:ListItem>
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Suspended" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Stopped" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RqdValidatorcmbStatus" runat="server" ErrorMessage="Required" ControlToValidate="cmbStatus" InitialValue="-1" Style="margin-left: 5px; color: #FF0000"  Display="Dynamic"></asp:RequiredFieldValidator>

                            </div>

                        </td>
                    </tr>

                  
                      <tr>

                        <td></td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Text="ADD" class="btn btn-outline btn-success" Style="width: 100px" />
                            <asp:Button ID="btnCancel" OnClientClick="return resetForm();" runat="server" Text="CANCEL" class="btn btn-outline btn-danger" Style="width: 100px" UseSubmitBehavior="false" /> 
                            
                        </td>
                    </tr>
                     <tr style="height: 20px"><td colspan="2"></td></tr>
                   
                </table> 

                <%--<div class="panel-footer"></div>--%>
            </div>
        </div>
         
    </div>


   <script type="text/javascript" language="javascript">

       //validateTextboxForInputEntry("TextBox1", true, false, false); // for alphabets only
       validateTextboxForInputEntry('<%=txtViewOrder.ClientID%>', false, true, false); // for digits only
       //validateTextboxForInputEntry("TextBox3", false, false, true); // for special characters only

    </script>
                         
</asp:Content>
