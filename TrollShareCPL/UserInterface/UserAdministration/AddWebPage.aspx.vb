﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddWebPage
    Inherits BaseUIPage


#Region "Variables"
    Private dr As mySqlDataReader
    Private dt As DataTable
    Private objAppType As ApplicationType
    Private objWebSection As WebPageSection
    Private objWebPage As WebPages
#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack = False Then
            FillDropDowns()
            FillForm()
        End If
    End Sub

    Private Sub cmbApplication_DataBound(sender As Object, e As EventArgs) Handles cmbApplication.DataBound
        cmbApplication.Items.Insert(0, New ListItem("-------Select Application Type ---------------", ""))
    End Sub

    Private Sub cmbSection_DataBound(sender As Object, e As EventArgs) Handles cmbSection.DataBound
        cmbSection.Items.Insert(0, New ListItem("-------Select Section---------------", ""))
    End Sub

    Private Sub cmbParentPage_DataBound(sender As Object, e As EventArgs) Handles cmbParentPage.DataBound
        cmbParentPage.Items.Insert(0, New ListItem("-------Select Parent Page ---------------", ""))
    End Sub

    Private Sub cmbApplication_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbApplication.SelectedIndexChanged
        FillSection()
    End Sub

    Private Sub cmbSection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbSection.SelectedIndexChanged
        FillParent()
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim webId As Integer

        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then ' Checking adding privilage of current user
                objWebPage = New WebPages
                With objWebPage
                    '.Application = Val(cmbApplication.SelectedValue)
                    .SectionID = Val(cmbSection.SelectedValue)
                    .Title = txtName.Text
                    .FileName = txtFileName.Text
                    .FilePath = txtFilePath.Text
                    .SubSection = Val(txtSubSection.Text)
                    .PageLevel = Val(txtPageLevel.Text)
                    .ViewOrder = Val(txtViewOrder.Text)
                    .Parent = Val(cmbParentPage.SelectedValue)
                    .Status = Val(cmbStatus.SelectedValue)
                    webId = .CheckExist(.Title, 0, Val(cmbApplication.SelectedValue))
                    If webId <> 0 Then
                        .ErrorMsg = .Title & " Already Exist"
                    Else
                        Dim BeforeText As String = " "
                        Dim AfterText As String = " "
                        AfterText += " Application = " & Val(cmbApplication.SelectedValue)
                        AfterText += "|Section = " & Val(cmbSection.SelectedValue)
                        AfterText += "|Title = " & txtName.Text
                        AfterText += "|FileName = " & txtFileName.Text
                        AfterText += "|FilePath = " & txtFilePath.Text
                        AfterText += "|SubSection = " & Val(txtSubSection.Text)
                        AfterText += "|PageLevel = " & Val(txtPageLevel.Text)
                        AfterText += "|ViewOrder = " & Val(txtViewOrder.Text)
                        AfterText += "|Parent = " & Val(cmbParentPage.SelectedValue)
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            .InsertMember()
                        End If
                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Webpage added successfully"
                            Response.Redirect("ManageWebPages.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Webpage added successfully"
                        Response.Redirect("ManageWebPages.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then ' Checking edit privilage of current user

                objWebPage = New WebPages(Val(hdnWebPageId.Value))
                objWebSection = New webpagesection(objWebPage.SectionID)
                With objWebPage
                    Dim BeforeText As String = "" 
                    BeforeText += "Section=" & .SectionID
                    BeforeText += "|Title=" & .Title
                    BeforeText += "|FileName = " & .FileName
                    BeforeText += "|FilePath=" & .FilePath
                    BeforeText += "|SubSection = " & .SubSection
                    BeforeText += "|PageLevel=" & .PageLevel
                    BeforeText += "|ViewOrder = " & .ViewOrder
                    BeforeText += "|Parent=" & .Parent
                    BeforeText += "|Status=" & .Status

                    cmbApplication.SelectedValue = objWebSection.ApplicationType
                    .SectionID = Val(cmbSection.SelectedValue)
                    .Title = txtName.Text
                    .FileName = txtFileName.Text
                    .FilePath = txtFilePath.Text
                    .SubSection = Val(txtSubSection.Text)
                    .PageLevel = Val(txtPageLevel.Text)
                    .ViewOrder = Val(txtViewOrder.Text)
                    .Parent = Val(cmbParentPage.SelectedValue)
                    .Status = Val(cmbStatus.SelectedValue)
                    webId = .CheckExist(.Title, Val(hdnWebPageId.Value), Val(cmbApplication.SelectedValue))
                    If webId <> 0 Then
                        .ErrorMsg = .Title & " Already Exist"
                    Else
                        Dim AfterText As String = " " 
                        AfterText += "Section = " & .SectionID
                        AfterText += "|Title = " & txtName.Text
                        AfterText += "|FileName = " & txtFileName.Text
                        AfterText += "|FilePath = " & txtFilePath.Text
                        AfterText += "|SubSection = " & Val(txtSubSection.Text)
                        AfterText += "|PageLevel = " & Val(txtPageLevel.Text)
                        AfterText += "|ViewOrder = " & Val(txtViewOrder.Text)
                        AfterText += "|Parent = " & Val(cmbParentPage.SelectedValue)
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            .Update()
                        End If
                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Webpage Updated successfully"
                            Response.Redirect("ManageWebPages.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Webpage Updated successfully"
                        Response.Redirect("ManageWebPages.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated
            End If
        End If
    End Sub
    'Protected Sub btnCancel_Click1(sender As Object, e As EventArgs) Handles btnCancel.Click
    '    If Val(hdnWebPageId.Value) = 0 Then
    '        ClearForm()
    '        lblMsg.Text = ""
    '    Else
    '        Response.Redirect("ManageWebPages.aspx")
    '    End If
    'End Sub

#End Region

#Region "Functions"
    Private Sub FillForm()
        Dim queryStr As String = ""
        Dim webId As Integer
        objWebPage = New WebPages
        'Dim qsEnc As New AbhiQsEnc.AbhiQs
        'queryStr = Request.QueryString("Qs")   ' read query string 
        'webId = Val(qsEnc.ReadQS(queryStr, "id"))
        hdnWebPageId.Value = webId
        Dim dts As DataTable = objWebPage.GetWebPageforEdit(webId)
        If Not dts Is Nothing Then
            If dts.Rows.Count > 0 Then
              
                cmbApplication.SelectedValue() = IsNullValue(dts(0)("ApplicationType"), 0)
                FillSection()
                cmbSection.SelectedValue() = dts(0)("Section")
                FillParent()
                txtName.Text = IsNullValue(dts(0)("Title"), 0)
                txtFileName.Text = IsNullValue(dts(0)("FileName"), 0)
                txtFilePath.Text = IsNullValue(dts(0)("FilePath"), 0)
                txtSubSection.Text = IsNullValue(dts(0)("SubSection"), 0)
                txtPageLevel.Text = IsNullValue(dts(0)("PageLevel"), 0)
                txtViewOrder.Text = IsNullValue(dts(0)("ViewOrder"), 0)
                cmbParentPage.Text = dts(0)("Parent")
                cmbStatus.SelectedValue() = dts(0)("Status")
                cmbApplication.Attributes("disabled") = "disabled"
                cmbSection.Attributes("disabled") = "disabled"
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Web Page"
            End If
            dr.Close()
        End If 
    End Sub
#End Region

#Region "Procedures"

    Private Sub FillDropDowns()
        objAppType = New ApplicationType
        dr = objAppType.GetApplicationName
        cmbApplication.DataSource = dr
        cmbApplication.DataTextField = "Name"
        cmbApplication.DataValueField = "ID"
        cmbApplication.DataBind()
        dr.Close()
        FillSection()

    End Sub

    Private Sub FillSection()
        objWebSection = New WebPageSection
        dt = objWebSection.GetWebPageSections(Val(cmbApplication.SelectedValue))
        cmbSection.DataSource = dt
        cmbSection.DataTextField = "Name"
        cmbSection.DataValueField = "ID"
        cmbSection.DataBind()
        'dr.Close()
        FillParent()
    End Sub

    Private Sub FillParent()
        objWebPage = New WebPages
        dr = objWebPage.GetParentWebPage(Val(cmbSection.SelectedValue), Val(cmbApplication.SelectedValue))
        cmbParentPage.DataSource = dr
        cmbParentPage.DataTextField = "Title"
        cmbParentPage.DataValueField = "ID"
        cmbParentPage.DataBind()
        dr.Close()
    End Sub
    Private Sub ClearForm()
        cmbApplication.SelectedIndex() = 0
        FillSection()
        FillParent()
        txtName.Text = ""
        txtFileName.Text = ""
        txtFilePath.Text = ""
        txtSubSection.Text = ""
        txtPageLevel.Text = ""
        txtViewOrder.Text = ""
        cmbParentPage.SelectedIndex() = 0
        cmbStatus.SelectedIndex() = 0
    End Sub

#End Region

End Class