﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddWebPageSection
    Inherits BaseUIPage

#Region "---------------Variables-------------"

    Private dr As mySqlDataReader
    Private objAppType As ApplicationType
    Private objWebSection As WebPageSection

#End Region

#Region "-------------Events-------------------"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillDropDowns()
            FillForm()
        End If
    End Sub

    Private Sub cmbApplication_DataBound(sender As Object, e As EventArgs) Handles cmbApplication.DataBound
        cmbApplication.Items.Insert(0, New ListItem("-------Select Application Type ---------------", ""))
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim webId As Integer
        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then ' Checking adding privilage of current user
                objWebSection = New WebPageSection
                With objWebSection
                    .ApplicationType = Val(cmbApplication.SelectedValue)
                    .Name = txtName.Text
                    .ViewOrder = Val(txtViewOrder.Text)
                    .Status = Val(cmbStatus.SelectedValue)
                    .NameAlt = txtName.Text & "_Ar"
                    webId = .CheckExist(.Name, 0, .ApplicationType)
                    If webId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else
                        Dim BeforeText As String = " "
                        Dim AfterText As String = " "
                        AfterText += " applicationtype = " & Val(cmbApplication.SelectedValue())
                        AfterText += "|Name = " & txtName.Text
                        AfterText += "|NameAlt = " & txtName.Text & " "
                        AfterText += "|ViewOrder = " & Val(txtViewOrder.Text)
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            .InsertMember()
                        End If
                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Webpage Section added successfully"
                            Response.Redirect("ManageWebPageSection.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Webpage Section added successfully"
                        Response.Redirect("ManageWebPageSection.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else
            If EditPrivilage Then ' Checking edit privilage of current user

                objWebSection = New WebPageSection(Val(hdnWebPageId.Value))
                With objWebSection

                    Dim BeforeText As String = ""
                    BeforeText += " applicationtype = " & .ApplicationType
                    BeforeText += "|Name=" & .Name
                    BeforeText += "|ViewOrder=" & .ViewOrder
                    BeforeText += "|Status=" & .Status

                    '.ApplicationType = Val(cmbApplication.SelectedValue)
                    .Name = txtName.Text
                    .ViewOrder = Val(txtViewOrder.Text)
                    .Status = Val(cmbStatus.SelectedValue)
                    webId = .CheckExist(.Name, Val(hdnWebPageId.Value), .ApplicationType)
                    If webId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else
                        Dim AfterText As String = " "
                        AfterText += " applicationtype = " & .ApplicationType
                        AfterText += "|Name = " & txtName.Text
                        AfterText += "|NameAlt = " & txtName.Text & " "
                        AfterText += "|ViewOrder = " & Val(txtViewOrder.Text)
                        AfterText += "|Status = " & Val(cmbStatus.SelectedValue)
                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            .Update()
                        End If
                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                            FillForm()
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Webpage Section Updated successfully"
                            Response.Redirect("ManageWebPageSection.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Webpage Section Updated successfully"
                        Response.Redirect("ManageWebPageSection.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated
            End If
        End If
    End Sub

#End Region

#Region "-------------Functions----------------"

    Private Sub FillForm()

        Dim queryStr As String = ""
        Dim webId As Integer
        objWebSection = New WebPageSection
        'Dim qsEnc As New AbhiQsEnc.AbhiQs
        'queryStr = Request.QueryString("Qs")
        'webId = Val(qsEnc.ReadQS(queryStr, "id"))   'decrypt websection_id from the query 
        hdnWebPageId.Value = webId                  'Keep webpagesection_id in a hidden field for later use
        dr = objWebSection.getWebPageSectionforEdit(webId)

        If Not dr Is Nothing Then
            If dr.Read() Then 
                cmbApplication.SelectedValue() = dr("ApplicationType")
                txtName.Text = IsNullValue(dr("Name"), 0)
                txtViewOrder.Text = IsNullValue(dr("ViewOrder"), 0)
                cmbStatus.SelectedValue() = dr("Status")
                cmbApplication.Attributes("disabled") = "disabled"
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Web Page Section"
            End If
            dr.Close()
        End If 
    End Sub

    'Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

    '    If Val(hdnWebPageId.Value) = 0 Then
    '        ClearForm()
    '        lblMsg.Text = ""
    '    Else
    '        Response.Redirect("ManageWebPageSection.aspx")
    '    End If
    'End Sub

#End Region

#Region "----------------Procedures----------------"

    Private Sub FillDropDowns()
        objAppType = New ApplicationType
        dr = objAppType.GetApplicationName
        cmbApplication.DataSource = dr
        cmbApplication.DataTextField = "Name"
        cmbApplication.DataValueField = "ID"
        cmbApplication.DataBind()
        dr.Close()
    End Sub

    Private Sub ClearForm()
        txtName.Text = ""
        txtViewOrder.Text = ""
        cmbApplication.SelectedIndex = 0
        cmbStatus.SelectedIndex = 0
    End Sub

#End Region

End Class