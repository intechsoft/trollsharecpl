﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageFileType
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objFiletype As filetypes
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillGridView()
        End If
    End Sub

    Private Sub grdFileType_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdFileType.PageIndexChanging
        grdFileType.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub
    Private Sub grdArea_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdFileType.RowDataBound

        If DeletePrivilage Then
            grdFileType.Columns(6).Visible = True
        Else
            grdFileType.Columns(6).Visible = False
        End If
    End Sub

    Private Sub grdFileType_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdFileType.RowCommand

        Dim BeforeText As String = ""
        Dim AfterText As String = ""

        id = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = Enc.WriteQS("id=" & id)                ' code for encrytpting a strings
                Response.Redirect("AddArea.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objFiletype = New filetypes(id)
                With objFiletype
                    'BeforeText += "Code = " & .Code
                    BeforeText += "Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    'BeforeText += "|Code=" & .AppTypeID
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
            End If
            FillGridView()
        End If

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()

        If chkPaging.Checked = True Then
            grdFileType.AllowPaging = True
        Else
            grdFileType.AllowPaging = False
        End If
        objFiletype = New filetypes
        dt = objFiletype.GetFiletype()
        grdFileType.DataSource = dt
        grdFileType.DataBind()

    End Sub

#End Region

End Class