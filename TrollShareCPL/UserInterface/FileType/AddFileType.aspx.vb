﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AddFileType

    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private objFiletype As filetypes


#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillForm()
            'FillDropDowns()
        End If
    End Sub
    'Private Sub cmbAppType_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAppType.DataBound
    '    cmbAppType.Items.Insert(0, New ListItem("-------Select a Application Type -------", 0))
    'End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim CategoryId As Integer
        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then ' Checking adding privilage of current user
                objFiletype = New filetypes
                With objFiletype
                    '.Code = txtCode.Text.Trim
                    .Name = txtName.Text.Trim
                    .NameAlt = txtNameAlt.Text.Trim
                    '.AppTypeID = Val(cmbAppType.SelectedValue)
                    .Status = Val(cmbStatus.SelectedValue())
                    CategoryId = .CheckExist(.Name, 0)

                    If CategoryId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                        Exit Sub
                    Else
                        Dim BeforeText As String = ""
                        Dim AfterText As String = "Code =" & txtName.Text
                        'AfterText += "|Name =" & txtName.Text
                        AfterText += "|NameAlt =" & txtNameAlt.Text
                        'AfterText += "|AppTypeID =" & Val(cmbAppType.SelectedValue())
                        AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                        AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            .insertMember()
                        Else
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = "Add category failed"
                            Exit Sub
                        End If

                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "File Type added successfully"
                            Response.Redirect("ManageFileType.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "File Type added successfully"
                        Response.Redirect("ManageFileType.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else

            If EditPrivilage Then

                objFiletype = New filetypes(Val(hdnFileTypeID.Value))

                Dim BeforeText As String = ""

                With objFiletype
                    BeforeText += "Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    'BeforeText += "|AppTypeID=" & .AppTypeID
                    BeforeText += "|Status=" & .Status
                    '.Code = txtCode.Text.Trim
                    .Name = txtName.Text.Trim
                    .NameAlt = txtNameAlt.Text.Trim
                    '.AppTypeID = Val(cmbAppType.SelectedValue)
                    .Status = Val(cmbStatus.SelectedValue)
                    CategoryId = .CheckExist(.Name, Val(hdnFileTypeID.Value))
                    If CategoryId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else

                        Dim AfterText As String = "Code" & txtName.Text
                        'AfterText += "|Name =" & txtName.Text
                        AfterText += "|NameAlt =" & txtName.Text
                        'AfterText += "|AppType =" & Val(cmbAppType.SelectedValue())
                        AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                        AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())
                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            .update()
                        Else
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = "Update area failed"
                            Exit Sub
                        End If

                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "File Type Updated successfully"
                            Response.Redirect("ManageFileType.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "File Type Updated successfully"
                        Response.Redirect("ManageFileType.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated
            End If
        End If
    End Sub

    'Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

    '    If Val(hdnCategoryId.Value) = 0 Then
    '        ClearForm()
    '        lblMsg.Text = ""
    '    Else
    '        Response.Redirect("Managecategorymst.aspx")
    '    End If
    'End Sub

#End Region

#Region "Functions"

    Private Sub FillForm()
        Dim queryStr As String = ""
        Dim FileTypeID As Integer
        objFiletype = New filetypes
        Dim qsEnc As New AbhiQsEnc.AbhiQs

        queryStr = Request.QueryString("Qs")
        FileTypeID = Val(qsEnc.ReadQS(queryStr, "id"))   'decrypt CategoryId from the query 
        hdnFileTypeID.Value = FileTypeID                  'Keep CategoryId in a hidden field for later use

        dr = objFiletype.GetfiletypeforEdit(FileTypeID)
        If Not dr Is Nothing Then
            If dr.Read() Then
                txtName.Text = IsNullValue(dr("Name").ToString, "")
                txtNameAlt.Text = IsNullValue(dr("NameAlt").ToString, "")
                'cmbAppType.SelectedValue = dr("AppTypeID")
                cmbStatus.SelectedValue() = dr("Status")
                'cmbDisplayStatus.SelectedValue() = dr("DisplayStatus").ToString
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Service Type"
            End If
            dr.Close()
        End If

    End Sub

    Private Sub ClearForm()
        txtName.Text = ""
        txtNameAlt.Text = ""
        'cmbAppType.SelectedValue = 0
        cmbStatus.SelectedIndex = 0
        'cmbDisplayStatus.SelectedIndex() = 0
    End Sub
    'Private Sub FillDropDowns()
    '    Dim objApp As New applicationtype
    '    dr = objApp.GetApplicationTypeList()
    '    cmbAppType.DataSource = dr
    '    cmbAppType.DataValueField = "ID"
    '    cmbAppType.DataTextField = "Name"
    '    cmbAppType.DataBind()
    '    dr.Close()
    'End Sub

#End Region

End Class