﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL


Public Class AddDeviceType
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private objDeviceType As devicetype

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillForm()
            'FillDropDowns()
        End If
    End Sub
    'Private Sub cmbAppType_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAppType.DataBound
    '    cmbAppType.Items.Insert(0, New ListItem("-------Select a Application Type -------", 0))
    'End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        Dim CategoryId As Integer
        If btnAdd.Text = "ADD" Then
            If AddPrivilage Then ' Checking adding privilage of current user
                objDeviceType = New devicetype
                With objDeviceType

                    .Name = txtName.Text.Trim
                    .NameAlt = txtNameAlt.Text.Trim
                    .Description = txtDescription.Text.Trim
                    .DescriptionAlt = txtDescription.Text.Trim
                    '.LastDataSyncDate = txtLastDataSyncDate.Text.Trim
                    '.AppTypeID = Val(cmbAppType.SelectedValue)
                    .Status = Val(cmbStatus.SelectedValue())
                    CategoryId = .CheckExist(.Name, .ID)

                    If CategoryId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                        Exit Sub
                    Else
                        Dim BeforeText As String = ""
                        Dim AfterText As String = "Name =" & txtName.Text
                        AfterText += "|NameAlt =" & txtNameAlt.Text
                        AfterText += "|Description =" & txtDescription.Text
                        AfterText += "|DescriptionAlt =" & txtDescription.Text
                        AfterText += "|LastDataSyncDate =" & txtLastDataSyncDate.Text
                        'AfterText += "|AppTypeID =" & Val(cmbAppType.SelectedValue())
                        AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                        'AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())

                        If CreateCPLActivitiesLog(ActivitiesLog.Insert, BeforeText, AfterText) Then
                            .insertMember()
                        Else
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = "Add device type failed"
                            Exit Sub
                        End If

                    End If

                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Device Type added successfully"
                            Response.Redirect("ManageDeviceType.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Service Type added successfully"
                        Response.Redirect("ManageDeviceType.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.AddPrivilage_Violated
            End If
        Else

            If EditPrivilage Then

                objDeviceType = New devicetype(Val(hdnDeviceTypeID.Value))

                Dim BeforeText As String = ""

                With objDeviceType
                    BeforeText += "Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Description=" & .Description
                    BeforeText += "|DescriptionAlt=" & .DescriptionAlt
                    BeforeText += "|LastDataSyncDate=" & .LastDataSyncDate
                    'BeforeText += "|AppTypeID=" & .AppTypeID
                    BeforeText += "|Status=" & .Status

                    .Name = txtName.Text.Trim
                    .NameAlt = txtNameAlt.Text.Trim
                    .Description = txtDescription.Text.Trim
                    .DescriptionAlt = txtDescription.Text.Trim
                    .LastDataSyncDate = txtLastDataSyncDate.Text.Trim
                    '.AppTypeID = Val(cmbAppType.SelectedValue)
                    .Status = Val(cmbStatus.SelectedValue)
                    CategoryId = .CheckExist(.Name, .ID)
                    If CategoryId <> 0 Then
                        .ErrorMsg = .Name & " Already Exist"
                    Else

                        Dim AfterText As String = "Name" & txtName.Text
                        AfterText += "|NameAlt =" & txtName.Text
                        AfterText += "|Description =" & txtDescription.Text
                        AfterText += "|DescriptionAlt =" & txtDescription.Text
                        AfterText += "|LastDataSyncDate =" & txtLastDataSyncDate.Text
                        'AfterText += "|AppType =" & Val(cmbAppType.SelectedValue())
                        AfterText += "|Status =" & Val(cmbStatus.SelectedValue())
                        'AfterText += "|DisplayStatus = " & 1 'Val(cmbDisplayStatus.SelectedValue())
                        If CreateCPLActivitiesLog(ActivitiesLog.Update, BeforeText, AfterText) Then
                            .update()
                        Else
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = "Update device type failed"
                            Exit Sub
                        End If

                    End If
                    If Not .ErrorMsg Is Nothing Then
                        If .ErrorMsg.Trim.Length > 0 Then
                            lblMsg.ForeColor = Drawing.Color.Red
                            lblMsg.Text = .ErrorMsg
                        Else
                            lblMsg.ForeColor = Drawing.Color.Green
                            lblMsg.Text = "Device Type Updated successfully"
                            Response.Redirect("ManageDeviceType.aspx")
                        End If
                    Else
                        lblMsg.ForeColor = Drawing.Color.Green
                        lblMsg.Text = "Device Type Updated successfully"
                        Response.Redirect("ManageDeviceType.aspx")
                    End If
                End With
            Else
                lblMsg.Text = PrivilageMsgs.EditPrivilage_Violated
            End If
        End If
    End Sub

    'Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

    '    If Val(hdnCategoryId.Value) = 0 Then
    '        ClearForm()
    '        lblMsg.Text = ""
    '    Else
    '        Response.Redirect("Managecategorymst.aspx")
    '    End If
    'End Sub

#End Region

#Region "Functions"

    Private Sub FillForm()
        Dim queryStr As String = ""
        Dim DeviceTypeID As Integer
        objDeviceType = New devicetype
        Dim qsEnc As New AbhiQsEnc.AbhiQs

        queryStr = Request.QueryString("Qs")
        DeviceTypeID = Val(qsEnc.ReadQS(queryStr, "id"))   'decrypt CategoryId from the query 
        hdnDeviceTypeID.Value = DeviceTypeID                  'Keep CategoryId in a hidden field for later use

        dr = objDeviceType.GetDeviceTypeforEdit(DeviceTypeID)
        If Not dr Is Nothing Then
            If dr.Read() Then
                txtName.Text = IsNullValue(dr("Name").ToString, "")
                txtNameAlt.Text = IsNullValue(dr("NameAlt").ToString, "")
                txtDescription.Text = IsNullValue(dr("Description").ToString, "")
                txtDescription.Text = IsNullValue(dr("DescriptionAlt").ToString, "")
                txtLastDataSyncDate.Text = IsNotNullValue(dr("LastDataSyncDate").ToString, "")
                'cmbAppType.SelectedValue = dr("AppTypeID")
                cmbStatus.SelectedValue() = dr("Status")
                'cmbDisplayStatus.SelectedValue() = dr("DisplayStatus").ToString
                btnAdd.Text = "UPDATE"
                lblHeadText.Text = "Update Service Type"
            End If
            dr.Close()
        End If

    End Sub

    Private Sub ClearForm()
        txtName.Text = ""
        txtNameAlt.Text = ""
        txtDescription.Text = ""
        txtDescription.Text = ""
        txtLastDataSyncDate.Text = ""
        'cmbAppType.SelectedValue = 0
        cmbStatus.SelectedIndex = 0
        'cmbDisplayStatus.SelectedIndex() = 0
    End Sub
    'Private Sub FillDropDowns()
    '    Dim objApp As New applicationtype
    '    dr = objApp.GetApplicationTypeList()
    '    cmbAppType.DataSource = dr
    '    cmbAppType.DataValueField = "ID"
    '    cmbAppType.DataTextField = "Name"
    '    cmbAppType.DataBind()
    '    dr.Close()
    'End Sub

#End Region

End Class