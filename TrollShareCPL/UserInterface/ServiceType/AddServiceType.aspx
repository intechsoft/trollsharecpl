﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="AddServiceType.aspx.vb" Inherits="TrollShareCPL.AddServiceType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:HiddenField ID="hdnServiceTypeID" runat="server" Value="0" />
    <div class="row">
        <div class="col-lg-4" style="width: 500px; margin-left: 150px; margin-right: auto; margin-top: 10px; margin-bottom: auto;">
            <div class="panel panel-primary">
                <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
                    <asp:Label ID="lblHeader" runat="server" Text="Add New Category"></asp:Label>
                    <a href="ManageServiceType.aspx">&nbsp;>>Manage Servicetype</a>

                </div>
                <div class="panel-heading" style="height: 50px">
                    <h4 style="text-align: center"><strong>
                        <asp:Label ID="lblHeadText" runat="server" Text="Add New Servicetype"></asp:Label>
                    </strong></h4>
                </div>
                <table style="margin-left: 38px; width: 450px;">
                    <br />
                    <tr style="height: auto; text-align: center">
                        <td colspan="2">

                            <asp:Label ID="lblMsg" runat="server" Font-Size="Larger" Text="" CssClass="warning"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3"></td>
                    </tr>


                    <tr style="height: auto">
                        <td>Name: </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" class="form-control" Style="width: 250px;" required="required"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="height: 40px">
                        <td>Name Alt: </td>
                        <td>
                            <asp:TextBox ID="txtNameAlt" runat="server" class="form-control" Style="width: 100px;"></asp:TextBox>
                        </td>
                            
                    <tr style="height: 40px">

                        <td>Application: </td>
                        <td style="vertical-align: bottom">
                            <div class="form-group">
                                <asp:DropDownList ID="cmbAppType" runat="server" class="form-control" Style="width: auto; height: 30px" required="required">
                                </asp:DropDownList>

                            </div>

                        </td>
                    </tr>

                    <tr style="height: 40px">

                        <td>Status: </td>
                        <td style="vertical-align: bottom">
                            <div class="form-group">
                                <asp:DropDownList ID="cmbStatus" runat="server" class="form-control" Style="width: auto; height: 30px" required="required">
                                    <asp:ListItem Text="-Select-" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Suspended" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Stopped" Value="3"></asp:ListItem>
                                </asp:DropDownList>

                            </div>

                        </td>
                    </tr>

                    <tr style="height: 20px;">
                        <td colspan="2"></td>
                    </tr>
                    <tr>

                        <td></td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Text="ADD" class="btn btn-outline btn-success" Style="width: 100px" />

                            <%--<button type="submit" id="btnAdd" class="btn btn-primary" style="width:100px"><strong>ADD</strong></button>--%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             
                           <%--  <asp:Button ID="btnCancel" runat="server" Text="CANCEL" OnClientClick="return resetForm();" class="btn btn-outline btn-danger" Style="width: 100px; margin-left: 20px;" UseSubmitBehavior="false" />--%>
                        </td>
                    </tr>
                    <tr style="height: 20px;">
                        <td colspan="2"></td>
                    </tr>

                    <tr>
                        <td colspan="3"></td>
                    </tr>

                </table>

                <%--<div class="panel-footer"></div>--%>
            </div>
        </div>

    </div>



</asp:Content>

