﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class ManageServiceType
    Inherits BaseUIPage

#Region "Variables"

    Private dr As MySqlDataReader
    Private dt As DataTable
    Private objServicetype As servicetype
    Private Enc As New AbhiQsEnc.AbhiQs
    Private msg As Boolean
    Private id As Integer

#End Region

#Region "Events"

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            FillGridView()
        End If
    End Sub

    Private Sub grdServiceType_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdServiceType.PageIndexChanging
        grdServiceType.PageIndex = e.NewPageIndex
        FillGridView()
    End Sub
    Private Sub chkPaging_CheckedChanged(sender As Object, e As EventArgs) Handles chkPaging.CheckedChanged
        FillGridView()
    End Sub
    Private Sub grdServiceType_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdServiceType.RowDataBound

        If DeletePrivilage Then
            grdServiceType.Columns(7).Visible = True
        Else
            grdServiceType.Columns(7).Visible = False
        End If
    End Sub

    Private Sub grdServiceType_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdServiceType.RowCommand

        Dim BeforeText As String = ""
        Dim AfterText As String = ""

        id = Convert.ToInt32(e.CommandArgument)
        If e.CommandName = "Edit" Then
            If EditPrivilage Then
                Dim Encrypted As String = Enc.WriteQS("id=" & id)                ' code for encrytpting a strings
                Response.Redirect("AddServiceType.aspx?Qs=" + Encrypted)
            End If
        End If
        If e.CommandName = "RowDelete" Then
            If DeletePrivilage Then
                objServicetype = New servicetype(id)
                With objServicetype

                    BeforeText += "Name = " & .Name
                    BeforeText += "|NameAlt=" & .NameAlt
                    BeforeText += "|Code=" & .AppTypeID
                    If CreateCPLActivitiesLog(ActivitiesLog.Delete, BeforeText, AfterText) Then
                        .delete()
                    End If
                End With
            End If
            FillGridView()
        End If

    End Sub

#End Region

#Region "Procedures"

    Private Sub FillGridView()

        If chkPaging.Checked = True Then
            grdServiceType.AllowPaging = True
        Else
            grdServiceType.AllowPaging = False
        End If
        objServicetype = New servicetype
        dt = objServicetype.GetServiceTypes()
        grdServiceType.DataSource = dt
        grdServiceType.DataBind()

    End Sub

#End Region

End Class