﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class TestRpt
    Inherits BaseUIPage

    Dim Dr As MySqlDataReader
    Dim TCID As Integer = 0
    Dim whereClause As String = ""
    Dim TransTotal As Decimal
    Dim VoucherTotal As Decimal
    Dim NumberOfTransactions As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        FilterGridView()

    End Sub

    Private Sub FilterGridView()

        Dim dt As New DataTable

        If dt.Rows.Count > 0 Then
            reportGridView.Visible = True
            reportGridView.DataSource = dt
            reportGridView.DataBind()
        Else
            reportGridView.Visible = False
        End If

    End Sub


#Region "Functions"

    Private Function GetQuery() As String
        Return whereClause
    End Function

#End Region

End Class