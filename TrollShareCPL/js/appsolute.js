﻿function isEnteredKeyIsInteger(keycode, allowalphabets, allowdigits, allowspecialcharacters) {
    try {
        switch (keycode) {
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 46:
                if (allowdigits)
                    return 1;
                else
                    return -1;
                break;
            case 33:
            case 64:
            case 35:
            case 36:
            case 37:
            case 94:
            case 38:
            case 42:
            case 41:
            case 95:
            case 43:
            case 45:
            case 61:
            case 96:
            case 126:
            case 44:
            case 60:
            case 46:
            case 62:
            case 59:
            case 58:
            case 39:
            case 43:
            case 34:
            case 91:
            case 123:
            case 93:
            case 125:
            case 92:
            case 124:
            case 47:
            case 42:
            case 45:
            case 43:
            case 46:
                if (allowspecialcharacters)
                    return 1;
                else
                    return -1;
                break;
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case 80:
            case 81:
            case 82:
            case 83:
            case 84:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:

            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113:
            case 114:
            case 115:
            case 116:
            case 117:
            case 118:
            case 119:
            case 120:
            case 121:
            case 122:
                if (allowalphabets)
                    return 1;
                else
                    return -1;
                break;
            default:
                return -1;
        }
    } catch (e) {

    }
    return -1;
}

function validateTextboxForInputEntry(ctrlID, allowalphabets, allowdigits, allowspecialcharacters) {
    $("#" + ctrlID).keypress(function (e) {
        if (isEnteredKeyIsInteger(e.which, allowalphabets, allowdigits, allowspecialcharacters) > 0) {
            return true;
        } else {
            return false;
        }
    });
}

function sethistory() {

    if (localStorage.getItem("postbackcount") == undefined) {
        localStorage.setItem("postbackcount", -1);
        localStorage.setItem("postbackurl", window.location.pathname);
    } else {
        if (localStorage.getItem("postbackurl") == window.location.pathname) {
            localStorage.setItem("postbackcount", localStorage.getItem("postbackcount") - 1);
        } else {
            localStorage.setItem("postbackcount", -1);
            localStorage.setItem("postbackurl", window.location.pathname);
        }
        
    }

    $(window).on("navigate", function (event, data) {
        var direction = data.state.direction;
        if (direction == 'back') {
            localStorage.removeItem("postbackcount");
        }
        if (direction == 'forward') {
            if (localStorage.getItem("postbackcount") == undefined) {
                localStorage.setItem("postbackcount", -1);
                localStorage.setItem("postbackurl", window.location.pathname);
            } else {
                if (localStorage.getItem("postbackurl") == window.location.pathname) {
                    localStorage.setItem("postbackcount", localStorage.getItem("postbackcount") - 1);
                } else {
                    localStorage.setItem("postbackcount", -1);
                    localStorage.setItem("postbackurl", window.location.pathname);
                }

            }
        }
    });
} 

function redirectmetoback() {
    window.history.go(parseInt(localStorage.getItem("postbackcount"))); 
}

function validateForm(controltovalidate, initalvalue, controltodisplayerr, errmsg) {
    if (validationSucces) {
        var ctype = $('#' + controltovalidate).get(0).tagName;
        switch (ctype) {
            case 'SELECT':
                if (parseInt($('#' + controltovalidate).val()) <= initalvalue) {
                    validationSucces = false;
                    document.getElementById(controltodisplayerr).innerHTML = errmsg;
                }
                break;
            case 'INPUT':
                if ($.trim($('#' + controltovalidate).val()) == initalvalue) {
                    validationSucces = false;
                    document.getElementById(controltodisplayerr).innerHTML = errmsg;
                }
                else if ($.trim($('#' + controltovalidate).val()).length <= 0) {
                    validationSucces = false;
                    document.getElementById(controltodisplayerr).innerHTML = errmsg;
                }
                //else if (isNaN(parseInt($('#' + controltovalidate).val()))) {
                //    validationSucces = false;
                //    document.getElementById(controltodisplayerr).innerHTML = errmsg;
                //}
                break;
        }
    }
}

function isvalidForm() {
    return validationSucces;
}

function resetForm(a) {
    a = a || 0;
    if (($('input[type=submit][value=ADD]').attr('value') == "ADD") || (a != 0)) {
        $('form select').removeAttr('selected');
        $('form select').find('option:first').attr('selected', 'selected');
        
        $('form [type=text]').attr('value', '');
        $('form [type=email]').attr('value', '');
        var x = document.getElementById('ContentPlaceHolder1_lblMsg');
        x.innerHTML=""
        //for (var i = 49; i < x.length; i++)
        //    x[i].innerHTML="";
        //document.getElementById('ContentPlaceHolder1_lblMsg').innerHTML="";
        var x = document.getElementsByTagName("textarea");
        for (var i = 0; i < x.length; i++)
            x[i].value = "";
        var x = document.getElementsByTagName("input");
        for (var i = 0; i < x.length; i++) {
            if (x[i].type == 'text' || x[i].type == 'checkbox' || x[i].type == 'password') {
                x[i].value = "";
                x[i].checked = false;
            }
            if (x[i].type == 'radio') {
                if (x[i].getAttribute('checked') == 'checked')
                    x[i].checked = true
                else
                    x[i].checked = false;
            }
        }

    }
    else {
        window.history.go(parseInt(localStorage.getItem("postbackcount")));
    }
    return false;
}

function test() {

}


