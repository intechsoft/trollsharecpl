﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="ApplicationTypeRole.aspx.vb" Inherits="TrollShareCPL.ApplicationTypeRole" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:HiddenField ID="hdnAppTypeRoleId" runat="server" Value="0" />
    <div class="row">
        <div class="col-lg-4" style="width: 500px; margin-left: 150px; margin-right: auto; margin-top: 30px; margin-bottom: auto;">
            <div class="panel panel-primary">
                 <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
                     <asp:Label ID="lblHeader" runat="server" Text="Add Application Type Role"></asp:Label>
                     <a id="A1" runat="server" href="~/UserInterface/UserAdministration/ManageApplicationTypeRole.aspx">&nbsp;>>Manage&nbsp;Application&nbsp;Type&nbsp;Role</a >
                </div>
                <div class="panel-heading" style="height:50px">
                    <h4 style="text-align: center"><strong><asp:Label ID="lblHeadText" runat="server" Text="Add Aplication Type Role"></asp:Label>
                    </strong></h4>
                </div><br />
                <table style="margin-left:70px; width: 400px; line-height:40px;">
  
                    <tr>

                        <td>Role: </td>
                        <td>
                            <asp:DropDownList ID="cmbRole" runat="server" class="form-control" Style="width: auto; height: 30px" required="required">
                            </asp:DropDownList>
                        </td>
                    </tr>
                        <tr>

                        <td>Application Type: </td>
                        <td>
                             <asp:DropDownList ID="cmbApplication" runat="server" class="form-control" Style="width: auto; height: 30px" required="required">
                            </asp:DropDownList>
                        </td>
                    </tr> 
                   
                     <tr style="height: 40px">

                        <td>Status: </td>
                        <td>
                                <asp:DropDownList ID="cmbStatus" runat="server" class="form-control" Style="width: auto; height: 30px" required="required">
                                    <asp:ListItem Text="-Select-"  Value=""></asp:ListItem>
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Suspended" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Stopped" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                                  
                        </td>
                    </tr>
                     <tr style="height: 10px"><td colspan="2">

                        <asp:Label ID="lblMsg" runat="server" Font-Size="Larger" Text="" CssClass="warning"></asp:Label>
                                             </td></tr>
                    <tr style="height: 10px"><td colspan="2"></td></tr>
                    <tr>

                        <td></td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Text="ADD" class="btn btn-outline btn-success" Style="width:100px" />
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <asp:Button ID="btnCancel" OnClientClick="return resetForm();" runat="server" Text="CANCEL" class="btn btn-outline btn-danger" Style="width: 100px" UseSubmitBehavior="false" /> 
                        </td>
                    </tr>
                     <tr style="height: 20px"><td colspan="2"></td></tr>
                    
                    
                </table> 

                       </div>
        </div>
         
    </div>

</asp:Content>
