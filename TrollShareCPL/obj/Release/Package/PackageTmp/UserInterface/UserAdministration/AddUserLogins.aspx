﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="AddUserLogins.aspx.vb" Inherits="TrollShareCPL.AddUserLogins" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:HiddenField ID="hdnUserId" runat="server" Value="0" />
    <div class="row">
        <div class="col-lg-4" style="width: 500px; margin-left: 150px; margin-right: auto; margin-top: 10px; margin-bottom: auto;">
            <div class="panel panel-primary">
                 <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
                    <asp:Label ID="lblHeader" runat="server" Text="Add New Web Page"></asp:Label>
                     <a id="A1"  href=" ManageUserLogins.aspx">&nbsp;>>Manage User Logins</a >
                </div>
                <div class="panel-heading" style="height:50px">
                    <h4 style="text-align: center"><strong><asp:Label ID="lblHeadText" runat="server" Text="Add New User"></asp:Label>
                    </strong></h4>
                </div>
                <br />
                <table id="tblUser" style="margin-left:38px; width: 450px;line-height:40px" >
                    
                     <tr style="height: 20px">
                        <asp:Label ID="lblMsg" runat="server" Font-Size="Larger" Text="" CssClass="warning"></asp:Label>
                     </tr>
                     <tr><td colspan="3"></td></tr>
                    <tr id="UserRow">
                        
                        <td>User Type: </td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="cmbUserType" runat="server" class="form-control" Style="width: 250px; height:auto"  required="required">
                                </asp:DropDownList>
                            </div>
                    </td>
                    </tr>
                    <tr id="ToggleRow1">
                        
                        <td>Employee: </td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="cmbEmployee" runat="server" class="form-control" Style="width: 250px; height:auto"  required="required">
                                </asp:DropDownList>
                            </div>
                    </td>
                    </tr>                    
                    <tr>
                        <td>Role: </td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="cmbRole" runat="server" class="form-control" Style="width: 250px; height:auto" required="required">
                                </asp:DropDownList>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td>Company: </td>
                        <td>
                            <div class="form-group">
                                <asp:DropDownList ID="cmbCompany" runat="server" class="form-control" Style="width: 250px; height:auto" required="required">
                                </asp:DropDownList>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td>Language : </td>
                        <td> 
                                <asp:RadioButton ID="rbEnglish" runat="server"  GroupName="language" Checked="true" Text="&nbsp;English"/>
                                <asp:RadioButton ID="rbArabic" runat="server"   GroupName="language" Text="&nbsp;Arabic"/>
                           

                        </td>
                    </tr>
                    <tr>

                        <td>User Name: </td>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="height:50px">

                        <td>Password: </td>
                        <td>
                            <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" class="form-control" Style="width: 250px;" MaxLength="50" required="required"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>

                        <td>User Actual Name: </td>
                        <td>
                            <asp:TextBox ID="txtActualName" runat="server" class="form-control" Style="width: 250px;" MaxLength="150" required="required"></asp:TextBox>
                        </td>
                    </tr>
                                       
                    <tr>
                       
                        <td>Status: </td>
                         <td>
                                <asp:DropDownList ID="cmbStatus" runat="server" class="form-control" Style="width: auto;"  required="required" >
                                    <asp:ListItem Text="-Select-"  Value=""></asp:ListItem>
                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Suspended" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Stopped" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                            

                        </td>
                    </tr>

                  <tr style="height: 20px"><td colspan="2"></td></tr>
                      <tr>

                        <td></td>
                        <td colspan="2">
                            <asp:Button ID="btnAdd" runat="server" Text="ADD" class="btn btn-outline btn-success" Style="width: 100px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <asp:Button ID="btnCancel" runat="server" Text="CANCEL" class="btn btn-outline btn-danger" OnClientClick="return resetForm();" Style="width: 100px" />
                            <%--<button id="btnreset" runat="server"  class="btn btn-outline btn-danger" Style="width: 100px"  type="reset">CANCEL</button>--%>
                        </td>
                    </tr>
                     <tr style="height: 20px"><td colspan="2"></td></tr>
                   
                </table> 

                <%--<div class="panel-footer"></div>--%>
            </div>
        </div>
         
    </div>

 <script type="text/javascript">
     $(document).ready(function () {
         if($('#<%=btnAdd.ClientID%>').val == "ADD")
         {
             if ($('#<%=cmbUserType.ClientID%>  option:selected').val() == 3) {
                 $('#<%=cmbCompany.ClientID%>').prop('disabled', false);
                 $('#<%=cmbEmployee.ClientID%>').val('');
                 $('#<%=cmbEmployee.ClientID%>').prop('disabled', true);
             }
             else if ($('#<%=cmbUserType.ClientID%>  option:selected').val() == 1 || $('#<%=cmbUserType.ClientID%>  option:selected').val() == 2) {
                 $('#<%=cmbEmployee.ClientID%>').prop('disabled', false);
                 $('#<%=cmbCompany.ClientID%>').val('');
                 $('#<%=cmbCompany.ClientID%>').prop('disabled', true);
             }
             else {
                 $('#<%=cmbEmployee.ClientID%>').val('');
                 $('#<%=cmbEmployee.ClientID%>').prop('disabled', true);
                 $('#<%=cmbCompany.ClientID%>').val('');
                 $('#<%=cmbCompany.ClientID%>').prop('disabled', true);
             }
         }
     });

     $('#<%=cmbEmployee.ClientID%>').change(function () {  
         if ($('#<%=btnAdd.ClientID%>').val() == "ADD") {
             if ($('#<%=cmbEmployee.ClientID%>  option:selected').val() != '') {
             var str = $('#<%=cmbEmployee.ClientID%>  option:selected').text(); 
                 $('#<%=txtActualName.ClientID%>').val(str);
             }
             else
                 $('#<%=txtActualName.ClientID%>').val("");
         }
     });
     $('#<%=cmbUserType.ClientID%>').change(function () {  
         if ($('#<%=btnAdd.ClientID%>').val() == "ADD") {
             if ($('#<%=cmbUserType.ClientID%>  option:selected').val() == 3) {
                 $('#<%=cmbCompany.ClientID%>').prop('disabled', false);
                 $('#<%=cmbEmployee.ClientID%>').val('');
                 $('#<%=cmbEmployee.ClientID%>').prop('disabled', true);
                 $('#<%=txtActualName.ClientID%>').val("");
             }
             else if ($('#<%=cmbUserType.ClientID%>  option:selected').val() == 1 || $('#<%=cmbUserType.ClientID%>  option:selected').val() == 2) {
                 $('#<%=cmbEmployee.ClientID%>').prop('disabled', false);
                 $('#<%=cmbCompany.ClientID%>').val('');
                 $('#<%=cmbCompany.ClientID%>').prop('disabled', true);
             }
             else {
                 $('#<%=cmbEmployee.ClientID%>').val('');
                 $('#<%=cmbEmployee.ClientID%>').prop('disabled', true);
                 $('#<%=cmbCompany.ClientID%>').val('');
                 $('#<%=cmbCompany.ClientID%>').prop('disabled', true);
                 $('#<%=txtActualName.ClientID%>').val("");
             }
         }
     });
 </script>

</asp:Content>
