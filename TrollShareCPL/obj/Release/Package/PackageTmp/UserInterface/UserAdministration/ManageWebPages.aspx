﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="ManageWebPages.aspx.vb" Inherits="TrollShareCPL.ManageWebPageSection1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
        <script src='<%= ResolveClientUrl("~/CommonJS/Common.js") %>' type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-lg-4 panel panel-primary" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; width: 99.9%; font-family: Arial; font-size: medium;">
        <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
            <asp:Label ID="lblHeader" runat="server" Text="Manage WebPages &nbsp;>> "></asp:Label>
            <asp:LinkButton ID="lnkRelatdLink" runat="server" PostBackUrl="~/UserInterface/UserAdministration/AddWebPage.aspx">&nbsp;Add WebPage</asp:LinkButton>

        </div>

        <div id="divFilters" align="center" style="border-bottom: 1px solid navy; width: 100%; height: 100px; margin-top: 15px;">
            <table cellpadding="5">
                <tr>
                    <td>
                        <asp:Label ID="lblApplication" runat="server" Text="  Application :"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblWebpageSection" runat="server" Text="WebPage Section :"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="cmbApplicationList" runat="server" AutoPostBack="true" Style="width: 200px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbWebpageSection" runat="server" AutoPostBack="true" Style="width: 200px">
                        </asp:DropDownList>
                    </td>
                </tr>

            </table>
               <div style="margin: 10px 0px 0px 850px;">
                <asp:CheckBox ID="chkPaging" runat="server" Text="&nbsp;Allow Paging" CssClass="btn btn-link" Style="margin-left: 25px;" Checked="true" AutoPostBack="true" />
            </div>
        </div>
         
        <div style="margin: 10px 0px 0px 20px; width: 95%">

            <asp:GridView ID="grdWebPages" class="table table-bordered" runat="server" AutoGenerateColumns="False"
                EmptyDataText="No Data Found" Style="font-size: smaller;" AllowPaging="true" PageSize="50">
                <HeaderStyle CssClass="tblHeader" BackColor="#428BCA" />
                <AlternatingRowStyle BackColor="WhiteSmoke" />

                <Columns>

                     <asp:TemplateField HeaderText="SrNo" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>

                            <asp:Label ID="lbtnCounter" runat="server" Text='<%#String.Format("{0}", Container.DataItemIndex + 1 & ".")%> '></asp:Label>
                        </ItemTemplate>
                           <ItemStyle HorizontalAlign="Center" Wrap="false"></ItemStyle>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="ID">
                        <ItemTemplate>

                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Title">
                        <ItemTemplate>
                            <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title")%>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="FileName">
                        <ItemTemplate>
                            <asp:Label ID="lblFileName" runat="server" Text='<%# Bind("FileName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Section">
                        <ItemTemplate>
                            <asp:Label ID="lblSection" runat="server" Text='<%# Bind("Name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SubSection">
                        <ItemTemplate>
                            <asp:Label ID="lblSubSection" runat="server" Text='<%# Bind("SubSection")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ViewOrder">
                        <ItemTemplate>
                            <asp:Label ID="lblViewOrder" runat="server" Text='<%# Bind("ViewOrder")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="Middle"
                                ImageUrl='<%# getActiveImage(Eval("Status"))%>' Visible="true" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdID" runat="server" Value='<%# Eval("ID") %>' />
                            <asp:LinkButton ID="lbEdit" runat="server" Title="Edit" CommandName="Edit" Text="" CssClass="fa fa-edit fa-fw" CommandArgument='<%# Bind("ID")%>'></asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdDelID" runat="server" Value='<%# Eval("ID") %>' />
                            <asp:LinkButton ID="lnkbtnDelete" runat="server" Title="Delete" CommandName="RowDelete" CssClass="fa fa-trash-o fa-fw" Text="" CommandArgument='<%# Bind("ID")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                        </ItemTemplate>

                        <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle HorizontalAlign = "Right"   ForeColor="green"  CssClass = "GridPager" />
            </asp:GridView>
        </div>
    </div>
    
</asp:Content>
