﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="SendNotification.aspx.vb" Inherits="TrollShareCPL.SendNotification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .mychkboxlist tr td {
            padding: 8px 20px 6px 4px;
            border: 1px solid #56a1b2;
        }

            .mychkboxlist tr td input {
                color: #000;
                font-size: medium;
                font-style: normal;
                font-weight: normal;
                margin-right: 6px;
            }

            .mychkboxlist tr td label {
                color: #000;
                font-size: medium;
                font-style: normal;
                font-weight: normal;
            }
    </style>

    <script language="javascript">

        $(document).ready(function () {
            //disabled(true);

        });

        function disabled(mode) {
            document.getElementById("<%=btnSelectAll.ClientID%>").disabled = mode;
            document.getElementById("<%=btnDeselectAll.ClientID%>").disabled = mode;
            document.getElementById("<%=btnSend.ClientID%>").disabled = mode;
        }

        function CheckAll(chk) {

            var intIndex = 0;
            var chklist = document.getElementById('<%=chkCustomerList.ClientID%>');
            //if (chklist == null)
            //    $('input[type=submit][value=Search]').click();
            var rowCount = document.getElementById('<%=chkCustomerList.ClientID%>').getElementsByTagName("input").length;
            for (i = 0; i < rowCount; i++) {
                if (chk == 1)
                    document.getElementById('<%=chkCustomerList.ClientID%>_' + i).checked = true
                else
                    document.getElementById('<%=chkCustomerList.ClientID%>_' + i).checked = false
            }
            return false;
        }
        function CheckRequired() {
            var $cmbProf = $('form')
            if (!$cmbProf[0].checkValidity()) {
                document.getElementById("<%=btnSend.ClientID%>").disabled = false;
                $('input[type=submit][value=Send]').click();
                disabled(true);
                return false;
            }
            else {
                disabled(false);
                return true;
            }
        }

        function TakeAnAction() {
            $('#myModal').modal('toggle');
        }
         


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    &nbsp;<asp:Literal ID="Literal1" runat="server"></asp:Literal>

    <div class="col-lg-4 panel panel-primary" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; width: 99%; font-family: Arial; font-size: medium;">
        <div align="center" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
            <asp:Label ID="lblHeader" runat="server" Text="Send Notification "></asp:Label>
        </div>

        <div id="divFilters" align="center" style="width: 95%; height: 180px; margin-top: 10px;">
            <%--<div style="width:50%; height: 130px; border: 1px solid green;">--%>

            <table style="width: 40%; height: auto; line-height: 30px; border: 1px solid #428bca; margin-top: 5px;">
                <%--<tr align="center" >
                        <td style="padding-top:10px;padding-bottom:10px;" >
                            <asp:Label ID="lblDeviceType" runat="server" Text="Send Notification to " ></asp:Label>
                        </td>
                         <td style="padding-top:10px;padding-bottom:10px; text-align:left;">
                            <asp:DropDownList ID="cmbMode" runat="server" class="form-control" Width="250px" required="required" AutoPostBack="true">
                                <asp:ListItem Value="" Text="-- Select --"></asp:ListItem>
                                <%--<asp:ListItem Value="4" Text="All Devices"></asp:ListItem> 
                                <asp:ListItem Value="2" Text="all Android Devices"></asp:ListItem>
                                <asp:ListItem Value="3" Text="all iPhone Devices"></asp:ListItem>
                                <asp:ListItem Value="1" Text="a Customer"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>--%>

                <tr>

                    <td style="padding-left: 30px;">
                        Customer ID: 
                            <div class="input-group" style="width: 220px;">
                                <asp:TextBox ID="txtNumber" runat="server" MaxLength="10" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-btn">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary btn-sm" Text="Search" UseSubmitBehavior="false" />
                                </span>
                            </div>
                    </td>
                    <td style="width: 30%;"></td>

                    <td style="padding-left: 30px;">
                        <asp:Label ID="lblMessage" runat="server" Text=" Message: "></asp:Label>
                        <asp:TextBox ID="txtMessage" TextMode="MultiLine" runat="server" Rows="4" class="form-control" Style="width: 250px;" required="required"></asp:TextBox>
                    </td>
                </tr>

            </table>

            <%--</div>--%>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <div style="background-color:rgba(0, 0, 0, 0.50);height:200px;width:100%;position:absolute;padding-top:80px;">
                    <asp:Label ID="lblLoading" runat="server" Text="Loading..." Font-Bold="true" Font-Size="XX-Large" ForeColor="White"></asp:Label>
                        </div>
                    
                </ProgressTemplate> 
            </asp:UpdateProgress>

            <div align="center" valign="middle" style="width: 100%; height: 45px; border: 1px solid navy; padding-top: 5px; margin: 10px 0px 0px 20px;">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
                <table>
                    <tr align="center">

                        <td>
                            <asp:DropDownList ID="cmbDeviceType" runat="server" class="form-control" Width="250px" AutoPostBack="true">
                            </asp:DropDownList>
                        <td>
                            <asp:Button CssClass="btn btn-primary" ID="btnSelectAll" runat="server" OnClientClick="return CheckAll(1)" Text="Select All" /></td>
                        <td>
                            <asp:Button CssClass="btn btn-primary" ID="btnDeselectAll" runat="server" OnClientClick="return CheckAll()" Text="Deselect All" /></td>
                        <td>
                            <asp:Button CssClass="btn btn-primary" ID="btnSend" runat="server" Text="Send" />
                        </td>

                    </tr>
                </table> 
                </ContentTemplate>
            </asp:UpdatePanel>
            </div>
        </div>

        <div style="margin: 30px 0px 0px 20px; width: 98%; min-height: 300px;" align="center">
            
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblMsg" runat="server" Text="" ForeColor="Red" Font-Size="Large" Font-Bold="true"></asp:Label>
            <asp:CheckBoxList ID="chkCustomerList" runat="server" CssClass="mychkboxlist" RepeatColumns="6" RepeatDirection="Horizontal"
                CellPadding="26" CellSpacing="26">
            </asp:CheckBoxList>  
            </ContentTemplate> 
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="cmbDeviceType" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" />
                </Triggers>
                </asp:UpdatePanel>


        </div>
    </div>

    <!-- Modal -->
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 150px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" align="center">
                    <div style="float: right">
                        <button type="button" class="btn btn-danger btn-circle " data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="modal-body" style="text-align: center; color: green;">
                    <h2>
                        <button type="button" class="btn btn-success btn-circle btn-lg">
                            <i class="fa fa-check"></i>
                        </button>
                        <asp:Label ID="lblNotifStatus" runat="server" Text="Notification Sent Successfully"></asp:Label>
                    </h2>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
            </ContentTemplate>
        <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSend" />
                </Triggers>
        </asp:UpdatePanel>
    <!-- /.modal -->
    <script type="text/javascript">
        validateTextboxForInputEntry('<%=txtNumber.ClientID%>', false, true, false);

        document.getElementById('<%=txtNumber.ClientID%>').addEventListener('keypress', function (event) { 
            if (event.which == 13) {
                event.preventDefault();
                document.getElementById('<%=btnSearch.ClientID%>').click();
            }
        });
    </script>
</asp:Content>
