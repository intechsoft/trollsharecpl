﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="ManageApplicationTypeRole.aspx.vb" Inherits="TrollShareCPL.ManageApplicationTypeRole" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-lg-4 panel panel-primary" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; width: 99.9%; font-family: Arial; font-size: medium;">
        <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
            <asp:Label ID="lblHeader" runat="server" Text="Manage Application Type Role &nbsp;>> "></asp:Label>
            <asp:LinkButton ID="lnkRelatdLink" runat="server" PostBackUrl="~/UserInterface/UserAdministration/ApplicationTypeRole.aspx">&nbsp;Add Application Type Role</asp:LinkButton>

        </div> 

        <div style="margin: 10px 0px 0px 20px; width: 95%">

            <asp:GridView ID="grdAppRoleType" class="table table-bordered" runat="server" AutoGenerateColumns="False"
                EmptyDataText="No Data Found" Style="font-size: smaller;" AllowPaging="true" PageSize="50">
                <HeaderStyle CssClass="tblHeader" BackColor="#428BCA" />
                <AlternatingRowStyle BackColor="WhiteSmoke" />

                <Columns>
                     <asp:TemplateField HeaderText="#" HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>

                            <asp:Label ID="lbtnCounter" runat="server" Text='<%#String.Format("{0}", Container.DataItemIndex + 1 & ".")%> '></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ID">
                        <ItemTemplate>

                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Role">
                        <ItemTemplate>
                            <asp:Label ID="lblRole" runat="server" Text='<%# Bind("Role")%>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Application">
                        <ItemTemplate>
                            <asp:Label ID="lblApplicationType" runat="server" Text='<%# Bind("[Application]")%>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="Middle"
                                ImageUrl='<%# getActiveImage(Eval("Status"))%>' Visible="true" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdID" runat="server" Value='<%# Eval("ID") %>' />
                            <asp:LinkButton ID="lbEdit" runat="server" Title="Edit" CommandName="Edit" Text="" CssClass="fa fa-edit fa-fw" CommandArgument='<%# Bind("ID")%>'></asp:LinkButton>                          
                        </ItemTemplate>
                    </asp:TemplateField>

                       <asp:TemplateField HeaderText="" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdDelID" runat="server" Value='<%# Eval("ID") %>' />                           
                            <asp:LinkButton ID="lnkbtnDelete" runat="server" Title="Delete" CommandName="RowDelete" cssClass="fa fa-trash-o fa-fw" Text="" CommandArgument='<%# Bind("ID")%>' OnClientClick="return confirmDelete();"   ></asp:LinkButton>
                        </ItemTemplate>

                        <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle HorizontalAlign = "Right"   ForeColor="green"  CssClass = "GridPager" />
            </asp:GridView>

        </div>
    </div>
</asp:Content>
