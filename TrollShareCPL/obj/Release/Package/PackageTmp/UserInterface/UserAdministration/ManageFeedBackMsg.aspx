﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="ManageFeedBackMsg.aspx.vb" Inherits="TrollShareCPL.ManageFeedBackMsg" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src='<%= ResolveClientUrl("~/CommonJS/Common.js") %>' type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="col-lg-4 panel panel-primary" style="padding: 0px 0px 0px 0px; margin: 10px 0px 0px 1.5px; width: 98.0%; font-family: Arial; font-size: medium;">
        <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
            <asp:Label ID="lblHeader" runat="server" Text="Manage Feed Back Message &nbsp;>> "></asp:Label>
            <a href="AddFeedBackMsg.aspx"> Add Feed Back Message</a>

        </div>
        <div class="col-lg-12" style="margin:5px 5px 0 5px;">
            <table class="table table-bordered" border="0" style="border:0px solid transparent;">
                <tr align="center">

                    <td>Customer:<%--</td>
                    </tr>
                <tr>
                    <td>--%>
                        <asp:DropDownList ID="cmbCustomer" runat="server" class="form-control" Style="width: auto; height: 30px" AutoPostBack="true">
                        </asp:DropDownList>

                    </td>
                    </tr> 
                </table> 
             </div>

        <div style="margin: 10px 0px 0px 20px; width: 95%">
            <div style="margin: 10px 0px 0px 650px;">
                <asp:CheckBox ID="chkPaging" runat="server" Text="&nbsp;Allow Paging" CssClass="btn btn-link" Style="margin-left: 25px;" Checked="true" AutoPostBack="true" />
            </div> 

            <asp:GridView ClientIDMode="Static" ID="grdFeedBack" class="table table-bordered" runat="server" AutoGenerateColumns="False"
                EmptyDataText="No Data Found" Style="font-size: smaller;"
                AllowSorting="true" AllowPaging="true" PageSize="5">
                <HeaderStyle CssClass="tblHeader" BackColor="#428BCA" />
                <AlternatingRowStyle BackColor="WhiteSmoke" />
                <Columns>

                    <%--<asp:BoundField DataField="SrNo" HeaderText="SrNo" />--%>
                     <asp:TemplateField HeaderText="SrNo" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbtnCounter" runat="server" Text='<%#String.Format("{0}", Container.DataItemIndex + 1 & ".")%> '></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center">

                        <ItemTemplate>
                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID")%>'></asp:Label>

                        </ItemTemplate>

                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <%-- <asp:BoundField DataField="AppTypeName" HeaderText="AppTypeName" />--%>

                    <asp:BoundField DataField="Name" HeaderText="Name" />

                    <asp:BoundField DataField="MobileNumber" HeaderText="MobileNumber" Visible="true"/>

                     <asp:BoundField DataField="Message" HeaderText="Message" Visible="true"/>

                     <asp:BoundField DataField="MessageTime" HeaderText="MessageTime" Visible="true"/>

                        <asp:BoundField DataField="ActionTime" HeaderText="ActionTime" Visible="true"/>

                       <asp:BoundField DataField="ActionTakenBy" HeaderText="ActionTakenBy" Visible="true"/>

                   <%-- <asp:BoundField DataField="AppName" HeaderText="AppName" />--%>

                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="Middle"
                                ImageUrl='<%# getActiveImage(Eval("Status"))%>' Visible="true" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Edit" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdID" runat="server" Value='<%# Eval("ID") %>' />
                            <asp:LinkButton ID="lbEdit" runat="server" Title="Edit" CommandName="Edit" Text="" CommandArgument='<%# Bind("ID")%>' CssClass="fa fa-edit fa-fw"></asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Delete" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdDelID" runat="server" Value='<%# Eval("ID") %>' />
                            <asp:LinkButton ID="lnkbtnDelete" runat="server" Title="Delete" CommandName="RowDelete" CssClass="fa fa-trash-o fa-fw" Text="" CommandArgument='<%# Bind("ID")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                    </asp:TemplateField>

                </Columns>
                <PagerStyle HorizontalAlign = "Right"   ForeColor="green"  CssClass = "GridPager" />
            </asp:GridView>
        </div>
    </div>


</asp:Content>

