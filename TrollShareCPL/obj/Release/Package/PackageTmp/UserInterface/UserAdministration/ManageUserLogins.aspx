﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="ManageUserLogins.aspx.vb" Inherits="TrollShareCPL.ManageUserLogins" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="col-lg-4 panel panel-primary" style="padding: 0px 0px 0px 10px; margin: 0px 0px 20px 0px; width: 99.9%; font-family: Arial;  font-size: medium;">
        <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
            <asp:Label ID="lblHeader" runat="server" Text="Manage User Logins  &nbsp;>> "></asp:Label>
            <a href="AddUserLogins.aspx">&nbsp;Add User Logins </a>
        </div>


        <div id="divFilters" align="center" style="border: 1px solid navy; width: 100%; height: 100px; margin-top: 15px;">
            <table cellpadding="5">
                <tr>
                    <td align="center">
                        User Type:
                    </td>
                    <td  align="center">
                         User Role:
                    </td>
                    <td  align="center">
                         Company:
                    </td>
                </tr>
                <tr>
                    <td  align="center">
                        <asp:DropDownList ID="cmbUserType" runat="server" AutoPostBack="false" Style="width: 200px">
                        </asp:DropDownList>
                    </td>
                    <td  align="center">
                        <asp:DropDownList ID="cmbRole" runat="server" AutoPostBack="false" Style="width: 200px">
                        </asp:DropDownList>
                    </td>
                    <td  align="center">
                        <asp:DropDownList ID="cmbCompany" runat="server" AutoPostBack="true" Style="width: 200px">
                        </asp:DropDownList>
                    </td>
                    
               </tr>


            </table>

              <div style="margin: 10px 0px 0px 850px;">
                <asp:CheckBox ID="chkPaging" runat="server" Text="&nbsp;Allow Paging" CssClass="btn btn-link" Style="margin-left: 25px;" Checked="true" AutoPostBack="true" />
            </div>

        </div>
        <div style="  width: 100%;  margin-top: 5px; padding-left:100px;">
            <table>
                <tr>
                    <td align="center" colspan="3">
                         <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                    </td>
                </tr>
            </table>
           
        </div>

        <div style="margin: 10px 0px 0px 20px; width: 95%;">
            <asp:GridView ID="grdUserLogins" class="table table-bordered" runat="server" AutoGenerateColumns="False"
                EmptyDataText="No Data Found" Style="font-size: smaller;" AllowPaging="true" PageSize="50">
                <HeaderStyle CssClass="tblHeader" BackColor="#428BCA" HorizontalAlign="Center" VerticalAlign="Middle" />
                <AlternatingRowStyle BackColor="WhiteSmoke" />

                <Columns>

                        <asp:BoundField DataField="SrNo" HeaderText="SrNo" />
                  
                    <asp:TemplateField HeaderText="Service ID">
                        <ItemTemplate>

                            <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID")%>'></asp:Label>
                        </ItemTemplate>
                               <ItemStyle HorizontalAlign="Center" Wrap="false"></ItemStyle>
                    </asp:TemplateField>

                        <asp:BoundField DataField="UserName" HeaderText="UserName" />
                   
                        <asp:BoundField DataField="Company" HeaderText="Company" />
                  
                        <asp:BoundField DataField="UserActualName" HeaderText="ActualName" />
                   
                        <asp:BoundField DataField="Role" HeaderText="Role" />
                
                        <asp:BoundField DataField="LanguageOption" HeaderText="LanguageOption" /> 

                        <asp:BoundField DataField="UserType" HeaderText="UserType" />

                        <asp:BoundField DataField="EmployeeName" HeaderText="EmployeeName" />
                  
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="Middle"
                                ImageUrl='<%# getActiveImage(Eval("Status"))%>' Visible="true" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdID" runat="server" Value='<%# Eval("ID") %>' />
                            <asp:LinkButton ID="lbEdit" runat="server" Title="Edit" CommandName="Edit" CssClass="fa fa-edit fa-fw" Text="" CommandArgument='<%# Bind("ID")%>'></asp:LinkButton>

                        </ItemTemplate>

                        <ItemStyle Wrap="False"></ItemStyle>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdDelID" runat="server" Value='<%# Eval("ID") %>' />
                            <asp:LinkButton ID="lnkbtnDelete" runat="server" Title="Delete" CommandName="RowDelete" CssClass="fa fa-trash-o fa-fw" Text="" CommandArgument='<%# Bind("ID")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                        </ItemTemplate>

                        <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                    </asp:TemplateField>

                </Columns>
                <PagerStyle HorizontalAlign = "Right"   ForeColor="green"  CssClass = "GridPager" />
            </asp:GridView>

        </div>
    </div>
</asp:Content>
