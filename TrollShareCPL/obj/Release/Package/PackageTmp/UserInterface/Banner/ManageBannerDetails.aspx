﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master" CodeBehind="ManageBannerDetails.aspx.vb" Inherits="TrollShareCPL.ManageBannerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src='<%= ResolveClientUrl("~/CommonJS/Common.js") %>' type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-lg-4 panel panel-primary" style="padding: 0px 0px 0px 0px; margin: 10px 0px 0px 1.5px; width: 103.0%; font-family: Arial; font-size: medium;">
        <div align="left" id="divHeader" style="padding: 6px 0px 0px 25px; font-family: Arial; font-size: medium; color: #FFFFFF; height: 35px; background-color: #428bca; width: 100%;">
            <asp:Label ID="lblHeader" runat="server" Text="Manage Banner Details &nbsp;>> "></asp:Label>
            <a href="AddBannerDetails.aspx">Add Banner Details</a>

        </div>
        <div class="col-lg-12" style="margin:5px 5px 0 5px;">
            <table class="table table-bordered" border="0" style="border:0px solid transparent;">
                <tr align="center">

                    <td>Category:</td>
                    <td>Sub Category  :   </td>
                    <td>Banner   :    </td>
                    <td>Banner Owner  :    </td>
                </tr>
                <tr align="center">
                    <td>
                        <asp:DropDownList ID="cmbCategory" runat="server" class="form-control" Style="width: auto; height: 30px" AutoPostBack="true">
                        </asp:DropDownList>
                        </td> 
                    <td>
                        <asp:DropDownList ID="cmbSubcategory" runat="server" class="form-control" Style="width: auto; height: 30px" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbBanner" runat="server" class="form-control" Style="width: auto; height: 30px" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbBannerOwner" runat="server" class="form-control" Style="width: auto; height: 30px" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align:center; margin:0 auto;"> 
                        <asp:Button ID="btnSearch" runat="server" Text="SEARCH" class="btn btn-outline btn-success" Style="width: 100px" />   
          <asp:CheckBox ID="chkPaging" runat="server" Text="&nbsp;Allow Paging" CssClass="btn btn-link" Style="margin-left: 25px;" Checked="true"  />
                                                     
               </td>
                </tr>
            </table>
        </div>

        <asp:GridView ID="grdbannerdetails" class="table table-bordered" runat="server" AutoGenerateColumns="False"
            EmptyDataText="No Data Found" PageSize="10" Style="font-size: smaller;">
            <HeaderStyle CssClass="tblHeader" BackColor="#428BCA" />
            <AlternatingRowStyle BackColor="WhiteSmoke" />
            <PagerStyle HorizontalAlign="Right" ForeColor="green" CssClass="GridPager" />
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        ID
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ID" runat="server" Text='<%# Bind("ID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField>
                    <HeaderTemplate>
                        Banner Owner
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="BannerOwner" runat="server" Text='<%# Bind("Name")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField>
                    <HeaderTemplate>
                       Category Name
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="CategoryName" runat="server" Text='<%# Bind("Name")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                       Banner Data
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="BannerData" runat="server" Text='<%# Bind("BannerData")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Status">
                    <%-- <ItemTemplate>
               <asp:Image ID="Image1" runat="server" ImageAlign="Middle"
                        ImageUrl='<%# getActiveImage(Eval("Status"))%>' Visible="true" />
                </ItemTemplate>--%>
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>Edit</HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hdID" runat="server" Value='<%# Eval("ID") %>' />
                        <asp:LinkButton ID="lbEdit" runat="server" Title="Edit" CommandName="Edit" Text="" CssClass="fa fa-edit fa-fw" CommandArgument='<%# Bind("ID")%>'></asp:LinkButton>

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
</asp:Content>
