﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebForm1.aspx.vb" Inherits="TrollShareCPL.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
   <div class="row">
        <div class="col-lg-4" style="width: 500px; margin-left: 150px; margin-right: auto; margin-top: 10px; margin-bottom: auto; top: 0px; left: 3px;">
            <div class="panel panel-primary">
                <asp:Button ID="btnMail" runat="server" Text="test mail" />
                <div class="panel-heading" style="height: 50px">
                    <h4 style="text-align: center"><strong>
                        <asp:Label ID="lblHeadText" runat="server" Text="File Upload"></asp:Label>
                    </strong></h4>
                </div>
                <div id="adduserlogins">
                    <table style="width: 100%; margin: 30px auto; width: 400px; line-height: 50px;">
                        <tr>
                            <td>File Type</td>
                            <td>
                                <asp:DropDownList ID="cmbFileType" runat="server" Style="width: 250px; min-width: 250px; height: 30px" required="required">
                                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Customer" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="ItemList" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                        </tr>
                        <tr>
                            <td>Select File</td>
                            <td align="center">
                                <asp:FileUpload ID="FileUpload1" runat="server" Style="width: 250px; min-width: 450px; height: 30px" />
                        </tr>
                        <tr style="height: 20px">
                            <td colspan="2">

                                <asp:Label ID="lblMsg" runat="server" Font-Size="Medium" Text="" CssClass="warning"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div style="text-align: center; margin: 10px auto; height: 50px; width: 30%;">

                        <div style="float: left; width: 100%; margin: 0px auto;">
                            <asp:Button ID="btnUpload" runat="server" Text="Upload" class="btn btn-outline btn-success" />
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
