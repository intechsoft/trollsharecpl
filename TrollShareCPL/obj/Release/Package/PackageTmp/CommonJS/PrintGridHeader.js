﻿function AddTHEAD(tableName) {
    var table = document.getElementById(tableName.id);    
    if (table != null) {
        var head = document.createElement("THEAD");
        head.style.display = "table-header-group";
        head.appendChild(table.rows[0]);
        table.insertBefore(head, table.childNodes[0]);
        return;
    }
}

function Highlight_On(obj) {
    if (obj != null) {
        obj.originalBgColor = obj.style.backgroundColor;
        obj.style.backgroundColor = "#DBE7F6";
    }
}
function Highlight_Off(obj) {
    if (obj != null) {
        obj.style.backgroundColor = obj.originalBgColor;
    }
}