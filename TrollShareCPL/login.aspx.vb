﻿Imports TrollShareBLL
Public Class login
    Inherits BaseUIPage

    Private LoginCount As Integer = 1
    Private cptchactrl As CaptchaCtrl

    Private Sub login_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If IsSessionValid() Then
            Response.Redirect("Default.aspx")
        End If

    End Sub

    Protected Overloads Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Master.FindControl("DivMenuItems").Visible = False 
        LoginBox.Focus()
        If Not Session("LoginCount") Is Nothing Then
            LoginCount = Val(Session("LoginCount"))
        End If
        cptchactrl = CType(LoginBox.FindControl("cptchactrl"), CaptchaCtrl)
        If LoginCount >= 3 Then
            cptchactrl.Visible = True
        Else
            cptchactrl.Visible = False
        End If
    End Sub


    Protected Sub Login1_Authenticate(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.AuthenticateEventArgs) Handles LoginBox.Authenticate
        'Try
        '    Dim lg As New userlogins(1)
        '    lg.UserPwd = "abcd"
        '    lg.update()
        'Catch ex As Exception

        'End Try
        Dim CaptchaRequired As Boolean = False
        Dim CaptchaSuccess As Boolean = False

        If Session("LoginCount") Is Nothing Then
            Session.Add("LoginCount", 1)
            LoginCount = 0
        Else
            Session("LoginCount") = Val(Session("LoginCount")) + 1
            LoginCount = Val(Session("LoginCount"))
        End If

        If LoginCount >= 3 Then

            cptchactrl = CType(LoginBox.FindControl("cptchactrl"), CaptchaCtrl)
            cptchactrl.Visible = True

            CaptchaRequired = True

            Dim CaptchaText As String = ""
            Dim CaptchaTextent As String = ""

            If Session("Prev_CaptchaText") Is Nothing Then
                CaptchaText = "<a>"
            Else
                CaptchaText = Session("Prev_CaptchaText")
            End If

            CaptchaTextent = cptchactrl.txtSecurity.Text
            If (CaptchaText.Trim = CaptchaTextent.Trim) Then
                CaptchaSuccess = True
            Else
                CaptchaSuccess = False
            End If
            cptchactrl.txtSecurity.Text = ""
        Else
            CaptchaRequired = False
        End If

        If (CaptchaRequired = False And CaptchaSuccess = False) Or (CaptchaRequired = True And CaptchaSuccess = True) Then

            Dim User As New userlogins
            Dim LoginState As Integer = User.LoginUser(LoginBox.UserName, LoginBox.Password)
            If LoginState = LoginStatus.ValidUser Then
                Session.Add("UserName", LoginBox.UserName)
                HttpContext.Current.Cache.Remove("Reports")
                e.Authenticated = True

                'If System.Configuration.ConfigurationManager.AppSettings("ApplySSL").ToString = "True" Then
                '    Response.Redirect(Replace(DefaultSitePath, "https", "http") & "Default.aspx")
                'Else
                '    Response.Redirect("Default.aspx")
                'End If


                'If Not Session("lastPage") Is Nothing Then
                '    Response.Redirect(Session("lastPage").ToString)
                'Else
                '    Response.Redirect("Default.aspx")
                'End If
                Response.Redirect("Default.aspx")
            ElseIf LoginState = LoginStatus.InValidUser Then '
                LoginBox.FailureText = "Invalid login attempt"
            ElseIf LoginState = LoginStatus.FailAttemptExec Then
                LoginBox.FailureText = "Unatuthorized use of the Alaaly Reporting  is prohibited."
            End If

        End If


        If Session("Prev_CaptchaText") Is Nothing Then
            Session.Add("Prev_CaptchaText", IsNullValue(Session("CaptchaText"), "<b>"))
        Else
            Session("Prev_CaptchaText") = IsNullValue(Session("CaptchaText"), "<c>")
        End If

    End Sub


End Class