﻿Imports MySql.Data.MySqlClient
Imports TrollShareBLL

Public Class AppMaster
    Inherits System.Web.UI.MasterPage
    Protected hPages As New Hashtable
    Private ItemFound As Boolean
    Private SelectedReport As PageInfo
    Private Pages As WebPages
    Private AppID As Integer = 0

    Protected Structure PageInfo
        Public Id As Integer
        Public Title As String
        Public FileName As String
        Public FilePath As String
        Public Section As Integer
        Public SubSection As Integer
        Public Status As Boolean
        Public ParentId As Integer
    End Structure

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

    'End Sub 

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
       
        AppID = Val(GetAppSetings("AppID"))

        If Application("SitePath") Is Nothing Then Application("SitePath") = CurrentPagePath()
        If CheckPrivRequired() And Not IsSessionValid() Then
            Response.Redirect(LoginSitePath())
        End If

        Pages = New WebPages
        If Session("Pages") Is Nothing Then
            SessionPages()
        Else
            hPages = CType(Session("Pages"), Hashtable)
        End If
        SelectedReport = hPages(Privilege.getPageName().ToLower())



        If Not HttpContext.Current.Session("selectedlang") Is Nothing Then
            If Session("selectedlang").ToString.ToLower = "ar" Then
                BtnLang.Text = "en"
            Else
                BtnLang.Text = "ar"
            End If
        Else
            If Not HttpContext.Current.Session("languageoption") Is Nothing Then
                If Session("languageoption").ToString.ToLower = "ar" Then
                    BtnLang.Text = "en"
                Else
                    BtnLang.Text = "ar"
                End If
            End If
        End If

        If Session("UserName") Is Nothing Then
            LblLoginInfo.Text = "No user is logged in, please log in."
            btnLogin.Text = "Login"
            DivMenuItems.Attributes.Add("style", "display:none")
            btnHide.Attributes.Add("style", "display:none")
        Else

            HidUserType.Value = Session("UserTypeId").ToString
            btnLogin.Text = "Logout"
            LblLoginInfo.Text = "You are logged in as <strong>" & Session("UserName").ToString() & " </strong>"
        End If

        If Page.IsPostBack = False Then
            BindSections()
            If Not Session("LoginTime") Is Nothing And Not IsDBNull(Session("LoginTime")) Then
                lblLoginDate.Text = "Login @ : " & DateTime.Parse(Session("LoginTime")).ToString("yyyy-MM-dd HH:mm:ss tt")
            End If

        End If

        If CheckPrivRequired() Then
            If AccessDenied() Then
                Response.Clear()
                Response.Write(SelectedReport.Section & "," & SelectedReport.SubSection & "," & Privilege.PagePrive(SelectedReport.Id, SelectedReport.ParentId) & "<br>")
                Response.Write("Report: " & Privilege.getPageName().ToLower() & "<br>")
                Response.Write("Access Denied")
                Response.End()
            Else
                If SelectedReport.Section > 0 Then

                    If Not Session("Privilege") Is Nothing Then
                        Dim ps As String = HttpContext.Current.Session("Privilege").ToString
                        Dim p1 As String() = ps.Split(";")
                        Dim p(p1.Count) As String
                        Dim i As Integer = 0
                        For i = 0 To p1.Count - 1
                            If p1(i).Contains("|") Then
                                p(i) = p1(i).Split("|")(0)
                            End If
                        Next
                        If Array.IndexOf(p, SelectedReport.Id.ToString) >= 0 Then
                            i = Array.IndexOf(p, SelectedReport.Id.ToString)
                            Dim Privilages As String() = p1(i).Split("|")(1).Split(",")
                            Privilege.ActionPrivilage(QueryStrings.PageViewPrivilage, Val(Privilages(0)))
                            Privilege.ActionPrivilage(QueryStrings.AddPrivilage, Val(Privilages(1)))
                            Privilege.ActionPrivilage(QueryStrings.EditPrivilage, Val(Privilages(2)))
                            Privilege.ActionPrivilage(QueryStrings.DeletePrivilage, Val(Privilages(3)))
                            Privilege.ActionPrivilage(QueryStrings.DetailsPrivilage, Val(Privilages(4)))
                        End If
                    End If

                    Dim script As String
                    script = _
                    "<script type=""text/javascript"" language=""javascript"">" & vbCrLf & _
                    "SwitchMenu('subDiv" & SelectedReport.Section.ToString & "')" & vbCrLf & _
                    "</script>" & vbCrLf
                End If
            End If
        End If

        LblUserActualName.Text = IsNullValue(HttpContext.Current.Session("UserActualName"), "")
        LblLastLoginDate.Text = IsNullValue(HttpContext.Current.Session("LastLoginDate"), "")
        LblLastLoginIP.Text = IsNullValue(HttpContext.Current.Session("LastLoginIP"), "")
        LblLastLoginAttempt.Text = IsNullValue(HttpContext.Current.Session("LastLoginAttempt"), "")
        LblLastLoginAttemptIP.Text = IsNullValue(HttpContext.Current.Session("LastLoginAttemptIP"), "")
        LblLastPasswordChangeDate.Text = IsNullValue(HttpContext.Current.Session("LastPasswordChangeDate"), "")
        'LblUserActualName.Text = HttpContext.Current.Session("UserActualName")

    End Sub

    Private Function AccessDenied() As Boolean
        If Session("UserName") = "admin" Then Return False
        If (Page.IsPostBack = False AndAlso Not Privilege.PagePrive(SelectedReport.Id, SelectedReport.ParentId)) Then Return True
    End Function

    Private Sub BindSections()
        Try
            Dim secCount As Integer = 0
            Dim secId As String = ""
            Dim sections As New webpagesection
            Dim dr As MySqlDataReader
            If Session("UICulture").ToString.ToLower = "ar" Then
                dr = sections.GetActiveWebPageSections(AppID, "ar")
            Else
                dr = sections.GetActiveWebPageSections(AppID, "en")
            End If

            rSections.DataSource = dr
            rSections.DataBind()
            dr.Close()

            For ind As Integer = 0 To rSections.Items.Count - 1
                If rSections.Items(ind).Visible Then
                    secCount += 1
                    'secId = rSections.Items(ind).
                    Dim HidSecID As HiddenField = CType(rSections.Items(ind).FindControl("HidSecID"), HiddenField)
                    secId = HidSecID.Value
                End If
            Next
            If secCount = 1 Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "openActiveSection", "openActiveSection(" & secId & ");", True)

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub SessionPages()
        Dim dr As MySqlDataReader = Pages.GetWebPages(AppID)
        Do While dr.Read()
            Dim r As PageInfo
            r.Id = dr("id")
            r.Title = dr("Title")
            r.FileName = dr("FileName")
            r.FilePath = dr("FilePath")
            r.Section = dr("SectionID")
            r.SubSection = dr("SubSection")
            r.Status = dr("Status") = 1
            r.ParentId = dr("Parent")
            hPages.Add(r.FileName.ToLower, r)
        Loop
        dr.Close()
        Session("Pages") = hPages
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim lang As String = Session("UICulture").ToString
        Session.Abandon()
        Session("UICulture") = lang
        Response.Redirect(LoginSitePath())
    End Sub


    Protected Sub rSections_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rSections.ItemDataBound
        ItemFound = False
        Dim img As HiddenField = e.Item.FindControl("HidSecID")
        Dim rep As Repeater = e.Item.FindControl("divRepeater")
        If Not img Is Nothing Then
            Dim I As Integer = CInt(img.Value)
            'Dim dr As mySqlDataReader
            'If Session("UICulture").ToString.ToLower = "ar" Then
            '    dr = Pages.GetWebPages(AppID, I, "ar")
            'Else
            '    dr = Pages.GetWebPages(AppID, I, "en")
            'End If
            'rep.DataSource = dr
            'rep.DataBind()
            'dr.Close()

            Dim dt As DataTable
            If Session("UICulture").ToString.ToLower = "ar" Then
                dt = Pages.GetWebPages(AppID, I, "ar")
            Else
                dt = Pages.GetWebPages(AppID, I, "en")
            End If

            rep.DataSource = dt
            rep.DataBind()

            e.Item.Visible = ItemFound
        End If

    End Sub

    Protected Sub divRepeater_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If Session("Pages") Is Nothing Or hPages.Count = 0 Then
            e.Item.Visible = False
            ItemFound = ItemFound Or e.Item.Visible
        Else
            Dim tr As HtmlTableRow = e.Item.FindControl("trLink")
            Dim al As HtmlAnchor = e.Item.FindControl("aLink")
            If Not al Is Nothing Then
                Dim a As String() = Split(al.HRef, "/")
                If a.Length > 0 Then
                    Try
                        Dim cr As PageInfo = CType(hPages(a(UBound(a)).ToLower), PageInfo)
                        e.Item.Visible = Session("UserName") = "admin" Or Privilege.PagePrive(cr.Id, cr.ParentId)

                        If cr.ParentId <> 0 Then
                            e.Item.Visible = False
                        Else
                            ItemFound = ItemFound Or e.Item.Visible
                        End If

                        If SelectedReport.Section = cr.Section And SelectedReport.SubSection = cr.SubSection Then
                            al.Attributes("class") = "SelectedSubmenu"
                        End If

                        al.Attributes.Add("onclick", "openmySection(" & cr.Section & ",this)")

                    Catch ex As Exception
                        e.Item.Visible = False
                        ItemFound = ItemFound Or e.Item.Visible
                    End Try
                End If
            End If
        End If
    End Sub

    Public Function getURL(ByVal Path As String, ByVal Name As String) As String
        If Path = "" Or Path = "root" Or Path = "/" Then
            Return Name
        Else
            Return Path & "/" & Name
        End If
    End Function

    Private Sub BtnLang_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLang.Click
        'System.Threading.Thread.Sleep(5000)
        BindSections()
    End Sub

    Private Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        Dim lang As String = Session("UICulture").ToString
        Session.Abandon()
        Session("UICulture") = lang
        Response.Redirect(LoginSitePath())
    End Sub
End Class