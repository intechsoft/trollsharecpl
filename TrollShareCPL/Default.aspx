﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master"
    CodeBehind="Default.aspx.vb" Inherits="TrollShareCPL._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="DivLogin" align="center" valign="middle">
        <div id="divLoginInfo" align="center"
            style="padding: 0px 0px 0px 0px; background-color: white; z-index: 99999; width: 70%; margin: 0px auto;">

            <fieldset>
                <legend style="border-bottom: 2px solid #bdbdbd;"><u style="font-size: large; padding-left: 25px;">Login Info </u>
                </legend>
                <div id="divLoginInfo2" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; margin-left: 10px; margin-right: 10px; overflow: auto;">

                    <table class="table-condensed" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; height: 100%; width: 100%; color: #000000; font-size: medium;">
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label1" runat="server" Text="Welcome"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LblUserActualName" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label7" runat="server" Text="Last Login On: "></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LblLastLoginDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label2" runat="server" Text="Last Login From :"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LblLastLoginIP" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label3" runat="server" Text="Last Login Attempt On :"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LblLastLoginAttempt" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label4" runat="server" Text="Last Login Attempt From :"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LblLastLoginAttemptIP" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label5" runat="server" Text="Last Login Password Changed On :"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LblLastPasswordChangeDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>

                </div>
            </fieldset>
        </div>
    </div>
    <script type="text/javascript" language="javascript">

        var docHeight = parseInt($(window).height());
        var DivLoginHeight = parseInt($("#DivLogin").css("height"));


        if (docHeight > DivLoginHeight) {
            var DivLoginPadding = 0;
            DivLoginPadding = (docHeight / 2) - ((DivLoginHeight / 2) + 40);

            $("#DivLogin").css("margin-top", DivLoginPadding);

        }
        else {
        }
    </script>
</asp:Content>
