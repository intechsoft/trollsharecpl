﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/AppMaster.Master"
    CodeBehind="login.aspx.vb" Inherits="TrollShareCPL.login" %>

<%@ Register Src="~/UserControls/CaptchaCtrl.ascx" TagPrefix="uc1" TagName="CaptchaCtrl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="DivLogin" align="center" valign="middle">
        <asp:Login ID="LoginBox" runat="server" DisplayRememberMe="False" PasswordLabelText=""
            UserNameLabelText="" TextLayout="TextOnTop" BackColor="Transparent" LoginButtonStyle-BackColor="Transparent"
            LoginButtonStyle-BorderColor="Transparent" TextBoxStyle-BackColor="Transparent"
            TextBoxStyle-BorderColor="Transparent" Style="width: 100%;">
            <LayoutTemplate>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4" style="border: 0px solid #ff8000; -webkit-border-radius: 5px;
                        -moz-boxborder-radius: 5px; border-radius: 5px; -moz-box-shadow: 0 0 100px #bdbdbd;
                        -webkit-box-shadow: 0 0 100px #bdbdbd; box-shadow: 0 0 100px #bdbdbd; padding: 0px 0px 0px 0px;">
                        <div class="login-panel panel panel-default" style="margin: 0px 0px 0px 0px; font-size: large;
                            color: #000000;">
                            <div align="center" class="panel-heading">
                                  Sign In
                            </div>
                            <div class="panel-body">
                                <fieldset>
                                    <div class="form-group">
                                        <asp:TextBox ID="UserName" runat="server" CssClass="form-control" placeholder="Username"
                                            autofocus></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="Password" TextMode="Password" CssClass="form-control" runat="server"
                                            placeholder="Password"></asp:TextBox>
                                    </div>
                                    <uc1:CaptchaCtrl runat="server" ID="cptchactrl" />
                                    <asp:Button ID="submitLoginBtn" ClientIDMode="Static" CommandName="Login" runat="server" Text="Login" CssClass="btn btn-lg btn-success btn-block" />
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </LayoutTemplate>
        </asp:Login>
    </div>
    <script type="text/javascript" language="javascript">
        var btnPressed = false;
        $(function () {
            $("form").keypress(function (e) { 
                if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                    $('#submitLoginBtn').click();
                    //                     
                    btnPressed = true;
                    return false;
                } else {
                    return true;
                }
            });
        });

        //          alert(btnPressed);


        var docHeight = parseInt($(window).height());
        var DivLoginHeight = parseInt($("#DivLogin").css("height"));
         

        if (docHeight > DivLoginHeight) {
            var DivLoginPadding = 0;
            DivLoginPadding = (docHeight / 2) - ((DivLoginHeight/2) + 40  );
             
                $("#DivLogin").css("margin-top", DivLoginPadding); 

        }
        else {
        }

        localStorage.clear(); 


      


    </script>
</asp:Content>
