﻿Imports System.IO
Imports System.Net.Mail
Imports TrollShareBLL

Public Class WebForm1
    Inherits System.Web.UI.Page

    Private UPLOADFOLDER As String = "http://polyapp.interstellarz.com/UserInterface/PolyGuard/FileUploads"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Try

            Dim objFile As New uploadedfiles
            With objFile

                If Me.FileUpload1.HasFile Then
                    .FileName = FileUpload1.PostedFile.FileName
                    .FileType = cmbFileType.SelectedValue
                    .FilePath = Path.GetFileName(Me.FileUpload1.PostedFile.FileName)
                    .IpAddress = Request.UserHostAddress.ToString
                    .Status = 1
                    .UploadedDate = Now
                    .UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))

                    If (FileUpload1.PostedFile.ContentLength / 1024) > 102400 Then
                        lblMsg.Text = "File size exceeds the maximum limit 100 MB!"
                        lblMsg.ForeColor = Drawing.Color.Red
                        Exit Sub
                    End If
                    Dim RecID As Integer = 1 ' .insertMember()
                    If RecID > 0 Then
                        UploadFile(RecID)
                    Else
                        lblMsg.Text = .ErrorMsg
                        lblMsg.ForeColor = Drawing.Color.Red
                    End If
                Else
                    lblMsg.Text = "Please select a file"
                    lblMsg.ForeColor = Drawing.Color.Red
                End If

            End With

        Catch ex As Exception
            lblMsg.Text = "error " & ex.Message
        End Try

    End Sub

    Private Sub uploadItemList()

    End Sub

    Private Sub uploadCustomerData()

    End Sub
    Private Sub UploadFile(ByVal SaveFileName As String)

        If Me.FileUpload1.HasFile Then
            Try
                Dim path__1 As String = "G:\PleskVhosts\interstellarz.com\PolyApp.interstellarz.com\UserInterface\PolyGuard\FileUploads\"
                Dim fileName As String = Path.GetFileName(Me.FileUpload1.PostedFile.FileName)
                Dim pth As String = path__1 & "\" & SaveFileName & Path.GetExtension(Me.FileUpload1.PostedFile.FileName)
                FileUpload1.SaveAs(pth)
                If cmbFileType.SelectedValue = "1" Then
                    uploadCustomerData()
                ElseIf cmbFileType.SelectedValue = "2" Then
                    uploadItemList()
                Else
                    lblMsg.Text = "Select an option"
                    lblMsg.ForeColor = Drawing.Color.Red
                    Exit Sub
                End If
                lblMsg.Text = "File Upload Success"
                lblMsg.ForeColor = Drawing.Color.Green
            Catch ex As Exception
                lblMsg.Text = "Error in File Upload ..." & ex.Message
                lblMsg.ForeColor = Drawing.Color.Red
            End Try
        Else

        End If


    End Sub
    'Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
    '    Try

    '        Dim objFile As New uploadedfiles
    '        With objFile

    '            If Me.FileUpload1.HasFile Then
    '                'Select Case FileUpload1.PostedFile.ContentType
    '                '    Case "image/pjpeg"
    '                '        .DocImageIcon = "jpg.gif"
    '                '    Case "image/jpeg"
    '                '        .DocImageIcon = "jpg.gif"
    '                '    Case "image/gif"
    '                '        .DocImageIcon = "gif.gif"
    '                '    Case "image/x-png"
    '                '        .DocImageIcon = "jpg.gif"
    '                '    Case "image/bmp"
    '                '        .DocImageIcon = "bmp.gif"
    '                '    Case "text/plain"
    '                '        .DocImageIcon = "txt.gif"
    '                '    Case "text/HTML"
    '                '        .DocImageIcon = "html.gif"
    '                '    Case "application/pdf"
    '                '        .DocImageIcon = "pdf.gif"
    '                '    Case "application/vnd.ms-excel"
    '                '        .DocImageIcon = "excel.gif"
    '                '    Case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    '                '        .DocImageIcon = "excel.gif"
    '                '    Case "application/vnd.ms-powerpoint"
    '                '        .DocImageIcon = "ppt.gif"
    '                '    Case "application/msword"
    '                '        .DocImageIcon = "word.gif"
    '                '    Case "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    '                '        .DocImageIcon = "word.gif"
    '                '    Case "application/msaccess"
    '                '        .DocImageIcon = "acess.gif"
    '                '    Case "application/x-shockwave-flash"
    '                '        .DocImageIcon = "swf.gif"
    '                '    Case Else
    '                '        .DocImageIcon = "unknon.gif"
    '                'End Select

    '                .FileName = FileUpload1.PostedFile.FileName
    '                '.FileType = cmbFileType.SelectedValue
    '                .FilePath = Path.GetFileName(Me.FileUpload1.PostedFile.FileName)
    '                .IpAddress = Request.UserHostAddress.ToString
    '                .Status = 1
    '                .UploadedDate = Now
    '                .UserID = Val(IsNullValue(HttpContext.Current.Session("UserID"), 0))
    '                '.DocImageFileSize = FileUpload1.PostedFile.ContentLength

    '                If (FileUpload1.PostedFile.ContentLength / 1024) > 102400 Then
    '                    lblError.Text = "File size exceeds the maximum limit 100 MB!"
    '                    lblError.ForeColor = Drawing.Color.Red
    '                    Exit Sub
    '                End If
    '                Dim RecID As Integer = .insertMember()
    '                If RecID > 0 Then
    '                    UploadFile(RecID)
    '                Else
    '                    lblError.Text = .ErrorMsg
    '                    lblError.ForeColor = Drawing.Color.Red
    '                End If
    '            Else
    '                lblError.Text = "Please select a file"
    '                lblError.ForeColor = Drawing.Color.Red
    '            End If

    '        End With

    '    Catch ex As Exception
    '        lblError.Text = "error " & ex.Message
    '    End Try
    'End Sub

    ''Error: Converting Declarations and Fields 

    'Private Function GenerateExcelData(fpath As String) As DataTable
    '    Try
    '        Dim dtExcelSchema As DataTable
    '        Dim dtExcelSource As DataTable
    '        Dim ExcelProvider1 As String
    '        Dim ExcelProvider2 As String
    '        Dim myCommand As OleDb.OleDbDataAdapter
    '        Dim myConnection As OleDb.OleDbConnection
    '        ExcelProvider1 = System.Configuration.ConfigurationManager.AppSettings("ExcelProvider1")
    '        ExcelProvider2 = System.Configuration.ConfigurationManager.AppSettings("ExcelProvider2")
    '        'myConnection = New OleDb.OleDbConnection(ExcelProvider1 & FileSource & ExcelProvider2)
    '        myConnection = New OleDb.OleDbConnection(ExcelProvider1 & fpath & ExcelProvider2)
    '        myConnection.Open()
    '        dtExcelSchema = myConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
    '        Dim sheetName As String = dtExcelSchema.Rows(1)("TABLE_NAME").ToString()
    '        myCommand = New OleDb.OleDbDataAdapter("Select  * from [" & sheetName & "] ", myConnection)
    '        Dim DataSet As New System.Data.DataSet()
    '        myCommand.Fill(DataSet, "SourceTbl")

    '        dtExcelSource = DataSet.Tables("SourceTbl")
    '        GridView1.DataSource = dtExcelSource
    '        GridView1.DataBind()
    '        Return dtExcelSource
    '        '' need to pass relative path after deploying on server
    '        ''Dim path As String = System.IO.Path.GetFullPath(Server.MapPath("~/InformationNew.xlsx")) '              connection String  to work with excel file. HDR=Yes - indicates 
    '        ''                that the first row contains columnnames, not data. HDR=No - indicates 
    '        ''                the opposite. "IMEX=1;" tells the driver to always read "intermixed" 
    '        ''                (numbers, dates, strings etc) data columns as text. 
    '        ''             Note that Me option might affect excel sheet write access negative. */
    '        'Dim oledbConn As OleDbConnection
    '        'If Path.GetExtension(fpath) = ".xls" Then
    '        '    oledbConn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source = " & fpath & "; Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2""")
    '        'ElseIf Path.GetExtension(fpath) = ".xlsx" Then
    '        '    oledbConn = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0; Data Source = " & fpath & "; Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';")
    '        'End If
    '        'oledbConn.Open()
    '        'Dim cmd As New OleDbCommand()
    '        'Dim oleda As New OleDbDataAdapter()
    '        'Dim ds As New DataSet()
    '        '' selecting distinct list of Slno 
    '        'cmd.Connection = oledbConn
    '        'cmd.CommandType = CommandType.Text
    '        'cmd.CommandText = "SELECT * FROM [Sheet1$]"
    '        'oleda = New OleDbDataAdapter(cmd)
    '        'oleda.Fill(ds, "dsSlno")
    '        'Return ds.Tables(0)
    '    Catch ex As Exception
    '        lblError.Text = "error 1 " & ex.Message
    '    End Try
    '    Return Nothing

    'End Function

    'Private Sub readexcelusingxml()

    'End Sub

    'Private Sub readexcelusingxml(fileName As String)
    '    ReadExcelFileDOM(fileName)    ' DOM
    '    ReadExcelFileSAX(fileName)
    'End Sub
    'Private Sub ReadExcelFileDOM(ByVal fileName As String)
    '    Try
    '        'Using spreadsheetDocument As SpreadsheetDocument = spreadsheetDocument.Open(fileName, False)
    '        '    Dim workbookPart As WorkbookPart = spreadsheetDocument.WorkbookPart
    '        '    Dim worksheetPart As WorksheetPart = workbookPart.WorksheetParts.First()
    '        '    Dim sheetData As DocumentFormat.OpenXml.Spreadsheet.SheetData = worksheetPart.Worksheet.Elements(Of DocumentFormat.OpenXml.Spreadsheet.SheetData)().First()
    '        '    Dim text As String
    '        '    For Each r As DocumentFormat.OpenXml.Spreadsheet.Row In sheetData.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Row)()
    '        '        For Each c As DocumentFormat.OpenXml.Spreadsheet.Cell In r.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Cell)()
    '        '            text = c.CellValue.Text
    '        '            lblError.Text = lblError.Text & (text & " <br/>")
    '        '        Next
    '        '    Next
    '        'End Using
    '    Catch ex As Exception

    '    End Try
    '    lblError.Text = lblError.Text & vbCrLf
    'End Sub

    '' The SAX approach.
    'Private Sub ReadExcelFileSAX(ByVal fileName As String)
    '    Try
    '        'Using spreadsheetDocument As SpreadsheetDocument = spreadsheetDocument.Open(fileName, False)
    '        '    Dim workbookPart As WorkbookPart = spreadsheetDocument.WorkbookPart
    '        '    Dim worksheetPart As WorksheetPart = workbookPart.WorksheetParts.First()

    '        '    Dim reader As DocumentFormat.OpenXml.OpenXmlReader = DocumentFormat.OpenXml.OpenXmlReader.Create(worksheetPart)
    '        '    Dim text As String
    '        '    While reader.Read()
    '        '        If reader.ElementType = GetType(DocumentFormat.OpenXml.Spreadsheet.CellValue) Then
    '        '            text = reader.GetText()
    '        '            lblError.Text = lblError.Text & (text & " <br/>") & vbCrLf
    '        '        End If
    '        '    End While
    '        'End Using

    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub btnMail_Click(sender As Object, e As EventArgs) Handles btnMail.Click

        Dim MailBody As New StringBuilder("")
        MailBody = prepareMailBody("") 'FormatSerialNumber(Right("000000000000000000000" + tr.PayID, 10), tr.ServicesCode & "-")

        Dim MailFrom As String = GetAppSetings("MailFrom")
        Dim CCMail As String = GetAppSetings("CCMail")
        Dim Password As String = GetAppSetings("Password")
        Dim ToMail As String = GetAppSetings("ToEmail")

        SendMail2Client(MailFrom, ToMail, CCMail, "Order Confirmation", MailBody.ToString, Password)

    End Sub
    Private Function prepareMailBody(strInQry As String) As StringBuilder

        Dim MailBody As New StringBuilder("")
        Dim contactNumber As String = GetAppSetings("contactNumber")

        MailBody.Append(" <table width='500px'>     ")
        MailBody.Append("  <tr> <td align='center' colspan='2'>")

        MailBody.Append("  </td>")
        MailBody.Append(" </tr>")
        MailBody.Append("  <tr>")
        MailBody.Append(" <td colspan='2'>")

        MailBody.Append("       Hi Team,  ")
        MailBody.Append("  </td>")
        MailBody.Append(" </tr>")
        MailBody.Append(" <tr>")
        MailBody.Append("   <td colspan='2'>")
        MailBody.Append(" we successfully process the follwing orders. ")
        MailBody.Append("</td>")
        MailBody.Append("</tr>")

        MailBody.Append("<tr>")
        MailBody.Append("<td>")
        MailBody.Append("&nbsp;</td>")
        MailBody.Append("<td>")
        MailBody.Append("&nbsp;</td>")
        MailBody.Append("</tr>")

        Return MailBody
    End Function

    Public Sub SendMail2Client(ByVal MailFrom As String, ByVal ToMail As String, ByVal CCMail As String, ByVal SubjectMail As String, ByVal BodyMail As String, Password As String)

        Dim Client As New SmtpClient("hostsaleapp.com", 25)
        Client.DeliveryMethod = SmtpDeliveryMethod.Network
        Client.UseDefaultCredentials = False
        Client.Credentials = New Net.NetworkCredential(MailFrom, Password)
        Client.EnableSsl = False

        Dim objMail As New System.Net.Mail.MailMessage()
        With objMail

            .From = New MailAddress(MailFrom)

            .Subject = SubjectMail
            .To.Add(New MailAddress(ToMail))
            .CC.Add(New MailAddress(CCMail))
            .Body = BodyMail
            .BodyEncoding = Encoding.UTF8

            .Priority = Net.Mail.MailPriority.Normal

            .IsBodyHtml = True
            ' Set the priority of the mail message to normal


            Try
                'SmtpMail.SmtpServer.Insert(0, "smtp.qualitynet.net")
                'SmtpMail.SmtpServer = Client

                Client.Send(objMail)
                lblMsg.Text = "no err MailFrom " & MailFrom & " ToMail " & ToMail & " Password " & Password
            Catch ex As Exception
                lblMsg.Text = ex.ToString
            End Try
        End With



    End Sub

End Class